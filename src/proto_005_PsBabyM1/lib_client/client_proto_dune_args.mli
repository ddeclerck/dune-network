(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol_client_context

val batch_size_arg: (int option, full) Clic.arg
val out_arg : (string option, full) Clic.arg
val collect_switch: (bool, full) Clic.arg
val forge_switch : (bool, full) Clic.arg
val collect_fee_gas_arg : (Z.t option, full) Clic.arg
val verbose_signing_switch : (bool, full) Clic.arg
val collect_call_switch : (bool, full) Clic.arg
val output_arg : (Format.formatter, full) Clic.arg
