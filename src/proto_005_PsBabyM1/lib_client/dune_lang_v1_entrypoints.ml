(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol

let script_entrypoint_type = Michelson_v1_entrypoints.script_entrypoint_type

let contract_entrypoint_type = Michelson_v1_entrypoints.contract_entrypoint_type

let print_entrypoint_type = Michelson_v1_entrypoints.print_entrypoint_type

let print_entrypoints_list = Michelson_v1_entrypoints.print_entrypoints_list

let list_contract_entrypoints = Michelson_v1_entrypoints.list_contract_entrypoints

let list_entrypoints = Michelson_v1_entrypoints.list_entrypoints

let print_unreachables cctxt ~chain ~block
    ?on_errors ~emacs ?contract ?script_name
    program =
  match program with
  | Dune_lang_repr.Michelson_expr _ ->
      Michelson_v1_entrypoints.list_unreachables
        cctxt ~chain ~block program >>= fun entrypoints ->
      Michelson_v1_entrypoints.print_unreachables
        ~emacs
        ?script_name
        ?on_errors
        ?contract
        cctxt
        entrypoints
  | _ -> assert false (* TODO Love *)
