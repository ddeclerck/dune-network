(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Client_proto_contracts
open Client_proto_args
open Client_proto_context_commands
open Client_proto_dune_args
open Client_keys
open Clic

let commands = ref []
let command ~group ~desc args path f =
  let cmd = Clic.command ~group ~desc args path f in
  commands := cmd :: !commands

let destinations_encoding =
  let open Data_encoding in
  list
    (obj5
       (req "address" Contract.encoding)
       (req "amount" Tez.encoding)
       (opt "collect_call_fee_gas" z)
       (dft "entrypoint" string "default")
       (opt "arg" string)
    )

let read_destinations_file filename =
  Lwt_utils_unix.Json.read_file filename >>=? fun json ->
  match Data_encoding.Json.destruct destinations_encoding json with
  | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
      Format.kasprintf (fun s -> failwith "%s" s)
        "Invalid destinations file: %a %a"
        (fun ppf -> Data_encoding.Json.print_error ppf) exn
        Data_encoding.Json.pp json
  | destinations ->
      return destinations

let batch_transfer_command =
  command ~group:Client_proto_context_commands.group
    ~desc: "Batch transfer tokens."
    (args14 fee_arg dry_run_switch verbose_signing_switch
       gas_limit_arg storage_limit_arg counter_arg no_print_source_flag
       minimal_fees_arg
       minimal_nanotez_per_byte_arg
       minimal_nanotez_per_gas_unit_arg
       force_low_fee_arg
       fee_cap_arg
       burn_cap_arg
       batch_size_arg)
    (prefixes [ "dune"; "batch"; "transfer" ]
     @@ tez_param
       ~name: "total" ~desc: "total amount taken from source"
     @@ prefix "from"
     @@ ContractAlias.destination_param
       ~name: "src" ~desc: "name of the source contract"
     @@ prefix "to"
     @@ string
       ~name: "file"
       ~desc:
         "JSON file with destinations and amounts\nof the form\n\
          [ {\"address\": \"dn1...\", \"amount\": \"10000\", \"arg\": \"Unit\"}, ... ]\n\
          The field \"amount\" is given in mutez, and \"arg\" is optional."
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, no_print_source, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap, batch_size)
      total (_, source) destination_filename cctxt ->
      let fee_parameter = {
        Injection.minimal_fees ;
        minimal_nanotez_per_byte ;
        minimal_nanotez_per_gas_unit ;
        force_low_fee ;
        fee_cap ;
        burn_cap ;
      } in
      match Contract.is_implicit source with
      | None -> failwith "only implicit accounts can be the source of a batch transfer"
      | Some source ->
          read_destinations_file destination_filename >>=? fun destinations ->
          Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
          Client_proto_dune_context.batch_transfer cctxt
            ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
            ~dry_run ~verbose_signing
            ~fee_parameter ?batch_size
            ~source ?fee ~src_pk ~src_sk ~destinations ~total ?gas_limit ?storage_limit ?counter () >>=
          report_michelson_errors ~no_print_source ~msg:"transfer simulation failed" cctxt >>= function
          | None -> return_unit
          | Some (_res, _contracts) ->
              return_unit
    end


let protocol_group =
  { Clic.name = "Dune Protocol" ;
    title = "Dune Protocol specific commands" }

let file_parameter =
  Clic.parameter (fun _ p ->
      if not (Sys.file_exists p) then
        failwith "File doesn't exist: '%s'" p
      else
        return p)

let activate_protocol_command =
  command ~group:protocol_group ~desc: "Activate a protocol"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "activate" ; "protocol" ]
     @@ Protocol_hash.param ~name:"version" ~desc:"Protocol version (b58check)"
     @@ prefixes [ "with" ; "key" ]
     @@ ContractAlias.destination_param
       ~name: "activator" ~desc: "Activator's key"
     @@ prefixes [ "and" ; "parameters" ]
     @@ param ~name:"parameters"
       ~desc:"Protocol parameters (as JSON file)"
       file_parameter
     @@ prefixes [ "at" ; "level" ]
     @@ (param
           ~name: "level"
           ~desc: "Level"
           (parameter (fun _ctx s ->
                try return (Int32.of_string s)
                with _ -> failwith "%s is not an int32 value" s)))
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      protocol (_, source) param_json_file level
      (cctxt : Protocol_client_context.full) ->

      match Contract.is_implicit source with
      | None -> failwith "only implicit accounts can be the source of \
                          an activate protocol operation"
      | Some source ->
          Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
          Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
          (* TODO Dune: check whether `Shell_services.Protocol.list
             cctxt` contains the protocol *)
          >>=? fun json ->
          (match Dune_parameters.destruct_parameters_exn json with
           | exception exn -> Lwt.return (error_exn exn)
           | s -> return s)
          >>=? fun protocol_parameters ->
          let fee_parameter = {
            Injection.minimal_fees ;
            minimal_nanotez_per_byte ;
            minimal_nanotez_per_gas_unit ;
            force_low_fee ;
            fee_cap ;
            burn_cap ;
          } in
          Client_proto_dune_context.activate_protocol_operation cctxt
            ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
            ~dry_run ~verbose_signing
            ~fee_parameter
            ~level ~protocol ~protocol_parameters
            ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
          >>=? fun _res ->
          return_unit
    end

let change_parameters_command =
  command ~group:protocol_group ~desc: "Change protocol parameters"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "change" ; "parameters" ]
     @@ param ~name:"parameters"
       ~desc:"Protocol parameters (as JSON file)"
       file_parameter
     @@ prefixes [ "with" ; "key" ]
     @@ ContractAlias.destination_param
       ~name: "activator" ~desc: "Activator's key"
     @@ prefixes [ "at" ; "level" ]
     @@ (param
           ~name: "level"
           ~desc: "Level"
           (parameter (fun _ctx s ->
                try return (Int32.of_string s)
                with _ -> failwith "%s is not an int32 value" s)))
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      param_json_file (_, source) level
      (cctxt : Protocol_client_context.full) ->
      match Contract.is_implicit source with
      | None ->
          failwith "only implicit accounts can be the source of a \
                    change of protocol parameters"
      | Some source ->
          Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
          Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
          >>=? fun json ->
          (match Dune_parameters.destruct_parameters_exn json with
           | exception exn -> Lwt.return (error_exn exn)
           | s -> return s)
          >>=? fun protocol_parameters ->
          let fee_parameter = {
            Injection.minimal_fees ;
            minimal_nanotez_per_byte ;
            minimal_nanotez_per_gas_unit ;
            force_low_fee ;
            fee_cap ;
            burn_cap ;
          } in
          Client_proto_dune_context.activate_protocol_operation cctxt
            ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
            ~dry_run ~verbose_signing
            ~fee_parameter
            ~level ~protocol_parameters
            ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
          >>=? fun _res ->
          return_unit
    end

let manage_accounts_command =
  command ~group:protocol_group ~desc: "Manage accounts"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "manage" ; "accounts" ]
     @@ prefixes [ "with" ; "key" ]
     @@ ContractAlias.destination_param
       ~name: "activator" ~desc: "Activator's key"
     @@ prefixes [ "and" ; "parameters" ]
     @@ param ~name:"parameters"
       ~desc:"Accounts parameters (as JSON file)"
       file_parameter
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      (_, source) param_json_file
      (cctxt : Protocol_client_context.full) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      match Contract.is_implicit source with
      | None -> failwith "only implicit accounts can be the source of a manage accounts command"
      | Some source ->
          Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
          begin
            match Dune_parameters.Accounts.destruct_exn json with
            | exception exn -> Lwt.return (error_exn exn)
            | s -> return s
          end >>=? fun protocol_parameters ->
          let protocol_parameters =
            Dune_parameters.Accounts.encode protocol_parameters in
          let fee_parameter = {
            Injection.minimal_fees ;
            minimal_nanotez_per_byte ;
            minimal_nanotez_per_gas_unit ;
            force_low_fee ;
            fee_cap ;
            burn_cap ;
          } in
          Client_proto_dune_context.manage_accounts_operation cctxt
            ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
            ~dry_run ~verbose_signing
            ~fee_parameter
            ~protocol_parameters
            ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
          >>=? fun _res ->
          return_unit
    end

let () =
  command ~group ~desc: "Clear Delegations"
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune"; "clear" ; "delegations" ; "of" ]
     @@ Public_key_hash.source_param
       ~name: "mgr" ~desc: "the delegate key"
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      source
      (cctxt : Protocol_client_context.full) ->
      Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
      let fee_parameter = {
        Injection.minimal_fees ;
        minimal_nanotez_per_byte ;
        minimal_nanotez_per_gas_unit ;
        force_low_fee ;
        fee_cap ;
        burn_cap ;
      } in
      Client_proto_dune_context.clear_delegations_operation cctxt
        ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
        ~dry_run ~verbose_signing
        ~fee_parameter
        ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
      >>=? fun _res ->
      return_unit
    end

let manage_account_command_template ~desc ~cmd f =
  command ~group ~desc
    Client_proto_context_commands.(
      args12 fee_arg dry_run_switch verbose_signing_switch
        gas_limit_arg storage_limit_arg counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    (prefixes [ "dune" ]
     @@ cmd
     @@ prefixes [ "of" ]
     @@ ContractAlias.destination_param
       ~name: "target" ~desc: "Target's key"
     @@ prefixes [ "with" ; "admin" ]
     @@ ContractAlias.destination_param
       ~name: "admin" ~desc: "Admin's key"
     @@ stop)
    begin fun (fee, dry_run, verbose_signing, gas_limit, storage_limit,
               counter, minimal_fees,
               minimal_nanotez_per_byte, minimal_nanotez_per_gas_unit,
               force_low_fee, fee_cap, burn_cap)
      arg (_, contract) (_,admin)
      (cctxt : Protocol_client_context.full) ->
      begin
        match Contract.is_implicit contract with
        | None -> failwith "only implicit accounts can be the managed"
        | Some target -> return target
      end >>=? fun target ->
      begin
        match Contract.is_implicit admin with
        | None -> failwith "only implicit accounts can be admin here"
        | Some admin -> return admin
      end >>=? fun source ->
      f contract arg >>=? fun options ->
      let options =
        Dune_operation.Options.encode options in
      Client_keys.get_key cctxt source >>=? fun (_, src_pk, src_sk) ->
      Client_keys.get_key cctxt target  >>=? fun (_, _tgt_pk, tgt_sk) ->
      begin
        if Signature.Public_key_hash.equal source target then
          return_none
        else
          Client_keys.sign cctxt
            ~interactive:(cctxt:>Client_context.io_wallet)
            tgt_sk options >>=? fun sign ->
          return_some ( target , sign )
      end >>=? fun target ->
      let fee_parameter = {
        Injection.minimal_fees ;
        minimal_nanotez_per_byte ;
        minimal_nanotez_per_gas_unit ;
        force_low_fee ;
        fee_cap ;
        burn_cap ;
      } in
      Client_proto_dune_context.manage_account_operation cctxt
        ~chain:cctxt#chain ~block:cctxt#block ?confirmations:cctxt#confirmations
        ~dry_run ~verbose_signing
        ~fee_parameter
        ?target ~options
        ~source ?fee ~src_pk ~src_sk  ?gas_limit ?storage_limit ?counter ()
      >>=? fun _res ->
      return_unit
    end

let manage_account_command =
  manage_account_command_template ~desc: "Manage account"
    ~cmd:(fun next ->
        prefixes [ "change" ; "account" ; "parameters" ]
        @@ param ~name:"parameters"
          ~desc:"Account parameters (as JSON file)"
          file_parameter
        @@ next)
    (fun _target param_json_file ->
       Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
       >>=? fun json ->
       begin
         match Dune_operation.Options.destruct_exn json with
         | exception exn -> Lwt.return (error_exn exn)
         | s -> return s
       end)

let empty_options = Dune_operation.{
    admin = None ;
    maxrolls = None ;
    white_list = None ;
    delegation = None ;
  }

let set_admin_command =
  manage_account_command_template ~desc: "set admin"
    ~cmd:(fun next ->
        prefixes [ "set" ; "admin" ]
        @@ ContractAlias.destination_param
          ~name: "new admin" ~desc: "New admin"
        @@ next)
    (fun target (_, admin) ->
       let admin =
         if Contract.( admin = target ) then
           None
         else
           Some admin
       in
       return Dune_operation.{ empty_options with admin = Some admin })

let bool_param ~name ~desc next =
  Clic.param ~name ~desc
    (Clic.parameter (fun (_ : < .. >) s -> return @@ bool_of_string s)) next

let int_option_param ~name ~desc next =
  Clic.param ~name ~desc
    (Clic.parameter (fun (_ : < .. >) s ->
         return @@
         if s = "none" then
           None
         else
           Some ( int_of_string s ))) next

let set_delegation_command =
  manage_account_command_template ~desc: "set delegation"
    ~cmd:(fun next ->
        prefixes [ "set" ; "delegation" ]
        @@ bool_param
          ~name: "new-delegation" ~desc: "Whether delegation is ok"
        @@ next)
    (fun _target delegation ->
       return Dune_operation.{ empty_options with
                               delegation = Some delegation })

let set_maxrolls_command =
  manage_account_command_template ~desc: "set maxrolls"
    ~cmd:(fun next ->
        prefixes [ "set" ; "maxrolls" ]
        @@ int_option_param
          ~name: "new-maxrolls" ~desc: "maxrolls: int or 'none'"
        @@ next)
    (fun _target maxrolls ->
       return Dune_operation.{ empty_options with
                               maxrolls = Some maxrolls })

let set_whitelist_command =
  manage_account_command_template ~desc: "set white_list"
    ~cmd:(fun next ->
        prefixes [ "set" ; "whitelist" ]
        @@ Clic.string
          ~name: "new-whitelist"
          ~desc: "whitelist: comma separated list of contracts'"
        @@ next)
    (fun _target whitelist ->
       begin
         if whitelist = "none" then
           return []
         else
           String.split ',' whitelist
           |> map_s (fun s ->
               Lwt.return (Environment.wrap_error (Contract.of_b58check s)))
       end >>=? fun white_list ->
       return Dune_operation.{ empty_options with
                               white_list = Some white_list })

let get_parameters_command =
  command ~group ~desc: "Get the parameters of a contract."
    no_options
    (prefixes [ "dune" ; "get" ; "parameters" ; "for" ]
     @@ ContractAlias.destination_param ~name:"src" ~desc:"source contract"
     @@ stop)
    begin fun () (_, contract) (cctxt : Protocol_client_context.full) ->
      Alpha_services.Contract.parameters cctxt (cctxt#chain, cctxt#block) contract
      >>=? fun options ->
      cctxt#answer "%a" Dune_operation.Options.pp options >>= fun () ->
      return_unit
    end

let get_account_info_command =
  command ~group ~desc: "Get the account info of a contract."
    no_options
    (prefixes [ "dune" ; "account" ; "info" ; "for" ]
     @@ ContractAlias.destination_param ~name:"src" ~desc:"source contract"
     @@ stop)
    begin fun () (_, contract) (cctxt : Protocol_client_context.full) ->
      Alpha_services.Contract.account_info cctxt (cctxt#chain, cctxt#block) contract
      >>=? fun info ->
      cctxt#answer "%a" Alpha_services.Contract.Account_info.pp info >>= fun () ->
      return_unit
    end

let check_fundraiser_account =
  command ~group:Client_proto_context_commands.group
    ~desc:"Activate a fundraiser account."
    no_options
    (prefixes [ "dune" ; "check" ; "fundraiser" ; "account" ]
     @@ Client_proto_dune_context.Ed25519_public_key_hash.param
     @@ prefixes [ "with" ]
     @@ param ~name:"code"
       (Clic.parameter (fun _ctx code ->
            protect (fun () ->
                return (Blinded_public_key_hash.activation_code_of_hex code))))
       ~desc:"Activation code obtained from the Tezos foundation."
     @@ stop)
    (fun () pkh code ( cctxt : Protocol_client_context.full ) ->
       let blinded_pkh =
         Blinded_public_key_hash.of_ed25519_pkh code pkh in
       cctxt#message
         "@[Blinded key hash = %a@]@."
         Blinded_public_key_hash.pp blinded_pkh
       >>= fun () ->
       return_unit
    )

let sign_command =
  command ~group ~desc: "Sign an operation."
    (args1 out_arg)
    (prefixes [ "sign" ; "operation" ]
     @@ param ~name:"op"
       (Clic.parameter (fun cctxt s ->
            let ignore_new_lines s = String.split '\n' s |> String.concat "" in
            match String.split ~limit:1 ':' s with
            | [ "file" ; s ] -> cctxt#read_file s >>|? ignore_new_lines
            | [ "hex" ; s ] -> return s
            | _ ->
                try MBytes.of_hex (`Hex s) |> ignore; return s
                with Invalid_argument _ -> cctxt#read_file s >>= function
                  | Ok res -> return (ignore_new_lines res)
                  | Error _ ->
                      failwith "%S is neither a file nor a valid hexadecimal value" s
          ))
       ~desc:"Serialized operation in hexadecimal (in a file or directly)."
     @@ prefix "for"
     @@ ContractAlias.destination_param
       ~name:"signer" ~desc: "name of the signer"
     @@ stop)
    (fun (out) op (_, src) (cctxt : #Client_context.full) ->
       match Contract.is_implicit src with
       | None -> failwith "only implicit accounts can sign an operation"
       | Some src_pkh ->
           Client_keys.get_key cctxt src_pkh >>=? fun (_src_name, _src_pk, src_sk) ->
           let bytes = MBytes.of_hex (`Hex op) in
           let (_shell, Contents_list contents) =
             Data_encoding.Binary.of_bytes_exn
               Operation.unsigned_encoding bytes in
           Shell_services.Chain.chain_id cctxt ~chain:cctxt#chain () >>=? fun chain_id ->
           let watermark =
             match contents with
             | Single (Endorsement _) -> Signature.(Endorsement chain_id)
             | _ -> Signature.Generic_operation in
           Injection.simulate cctxt ~chain:cctxt#chain ~block:cctxt#block contents >>=
           report_michelson_errors ~exit_on_error:false
             ~msg:"simulation failed" cctxt >>= begin function
             | None -> Lwt.return_unit
             | Some (_oph, _op, result) ->
                 cctxt#message "@[<v 2>Parsed operation:@,%a@]"
                   Operation_result.pp_operation_result
                   (contents, result.contents)
           end >>= fun () ->
           begin match contents with
             | Single (Manager_operation { source ; _ }) ->
                 if Signature.Public_key_hash.(source <> src_pkh) then
                   cctxt#warning
                     "Signing an operation whose source is %a \
                      for %a will fail when injecting."
                     Signature.Public_key_hash.pp src_pkh
                     Signature.Public_key_hash.pp source >>= return
                 else return_unit
             | _ -> return_unit
           end >>=? fun () ->
           Dune_injection.confirm cctxt
             ~prompt:"Would you like to sign this operation?"
             ~msg:"Signing was rejected by the user." >>=? fun () ->
           Client_keys.sign cctxt ~watermark
             ~interactive:(cctxt:>Client_context.io_wallet) src_sk bytes
           >>=? fun signature ->
           cctxt#message "Signature: %a" Signature.pp signature >>= fun () ->
           let signed_bytes = MBytes.concat "" [ bytes ; Signature.to_bytes signature ] in
           begin match out with
             | None ->
                 cctxt#message "@[<v>Signed operation:@,%a@]" MBytes.pp_hex signed_bytes
             | Some f ->
                 let `Hex hex = MBytes.to_hex signed_bytes in
                 let oc = open_out f in
                 output_string oc hex;
                 close_out oc;
                 cctxt#message "@[<v>Serialized signed operation written to %s@]" f
           end >>= return
    )

let inject_command =
  command ~group ~desc: "Inject an operation."
    (args1 no_print_source_flag)
    (prefixes [ "inject" ; "operation" ]
     @@ param ~name:"op"
       (Clic.parameter (fun cctxt s ->
            let ignore_new_lines s = String.split '\n' s |> String.concat "" in
            match String.split ~limit:1 ':' s with
            | [ "file" ; s ] -> cctxt#read_file s >>|? ignore_new_lines
            | [ "hex" ; s ] -> return s
            | _ ->
                try MBytes.of_hex (`Hex s) |> ignore; return s
                with Invalid_argument _ -> cctxt#read_file s >>= function
                  | Ok res -> return (ignore_new_lines res)
                  | Error _ ->
                      failwith "%S is neither a file nor a valid hexadecimal value" s
          ))
       ~desc:"Serialized operation in hexadecimal (in a file or directly)."
     @@ stop)
    (fun (no_print_source) op (cctxt : #Client_context.full) ->
       let bytes = MBytes.of_hex (`Hex op) in
       let op =
         Data_encoding.Binary.of_bytes_exn
           Operation.encoding bytes in
       let Operation_data protocol_data = op.protocol_data in
       Injection.simulate cctxt ~chain:cctxt#chain ~block:cctxt#block
         protocol_data.contents >>=? fun (_oph, _op, result) ->
       cctxt#message "@[<v 2>Parsed operation:@,%a@]"
         Operation_result.pp_operation_result
         (protocol_data.contents, result.contents)
       >>= fun () ->
       Dune_injection.confirm cctxt
         ~prompt:"Would you like to inject this operation?"
         ~msg:"Injection was rejected by the user." >>=? fun () ->
       Injection.force_inject_operation cctxt ~chain:cctxt#chain
         ?confirmations:cctxt#confirmations
         ~preapply_result:result
         { shell = op.shell ; protocol_data } >>=
       report_michelson_errors ~no_print_source ~msg:"injection failed" cctxt >>= fun _ ->
       return_unit
    )

let output_protocol_parameters_command =
  command ~group ~desc: "Output protocol parameters."
    (args1 Client_proto_dune_args.output_arg)
    (fixed [ "dune" ; "output" ; "protocol" ; "parameters" ])
    begin fun ppf (cctxt : Protocol_client_context.full) ->
      Alpha_services.Constants.all cctxt (cctxt#chain, cctxt#block)
      >>= function
        Ok constants ->
          Format.fprintf ppf "%a@."
            Alpha_context.Constants.JSON.pp
            constants.parametric;
          return_unit
      | Error _ ->
          Format.eprintf "Warning: could not retrieve constants, printing defaults@.";
          Format.fprintf ppf "%a@."
            Alpha_context.Constants.JSON.pp
            Alpha_context.Constants.default;
          return_unit

    end

let commands () = !commands
let () =
  ignore
    [
      () ;
      batch_transfer_command ;
      activate_protocol_command ;
      change_parameters_command ;
      manage_accounts_command ;
      manage_account_command ;
      get_parameters_command ;
      get_account_info_command ;
      check_fundraiser_account ;
      sign_command ;
      inject_command ;
      output_protocol_parameters_command ;
    ]
