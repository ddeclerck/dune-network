(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions

let translate_exn (_env : 'a Love_tenv.t) (e : Love_ast.exn_name) :
  Love_runtime_ast.exn_name =
  match e with
  | Fail t -> RFail t
  | Exception id -> RException id

let translate_const (_env : 'a Love_tenv.t) (c : Love_ast.const) :
  Love_runtime_ast.const =
  match c.content with
  | CUnit -> RCUnit
  | CBool b -> RCBool b
  | CString s -> RCString s
  | CBytes b -> RCBytes b
  | CInt i -> RCInt i
  | CNat n -> RCNat n
  | CDun d -> RCDun d
  | CKey k -> RCKey k
  | CKeyHash kh -> RCKeyHash kh
  | CSignature s -> RCSignature s
  | CTimestamp t -> RCTimestamp t
  | CAddress a -> RCAddress a
  | CPrimitive p -> RCPrimitive p

let rec translate_pattern (env : 'a Love_tenv.t) (p : Love_ast.pattern) :
  Love_runtime_ast.pattern * 'a Love_tenv.t =
  match p.content with
  | PAny -> RPAny, env
  | PVar v -> RPVar v, env
  | PAlias (p, v) ->
    let p, env = translate_pattern env p in
    RPAlias (p, v), env
  | PConst c -> RPConst (translate_const env c), env
  | PNone -> RPNone, env
  | PSome p ->
    let p, env = translate_pattern env p in
    RPSome p, env
  | PList pl ->
    let pl, env = List.fold_left (fun (pl, env) p ->
        let p, env = translate_pattern env p in
        p :: pl, env
      ) ([], env) pl
    in
    RPList (List.rev pl), env
  | PTuple pl ->
    let pl, env = List.fold_left (fun (pl, env) p ->
        let p, env = translate_pattern env p in
        p :: pl, env
      ) ([], env) pl
    in
    RPTuple (List.rev pl), env
  | PConstr (c, pl) ->
    let pl, env = List.fold_left (fun (pl, env) p ->
        let p, env = translate_pattern env p in
        p :: pl, env
      ) ([], env) pl
    in
    RPConstr (c, (List.rev pl)), env
  | PContract (n, Anonymous s) ->
    let new_env = Love_tenv.contract_sig_to_env (Some n) () s env in
    RPContract (n, Anonymous s), Love_tenv.add_subcontract_env n new_env env
  | PContract (n, Named id) ->
    RPContract (n, Named id), Love_tenv.add_signed_subcontract n id env

let rec translate_raw_exp (env : 'a Love_tenv.t) (e : Love_ast.raw_exp) :
  Love_runtime_ast.exp =
  match e with
  | Const c -> RConst (translate_const env c)
  | Var v -> RVar v
  | Let { bnd_pattern = p; bnd_val = e1; body = e2 } ->
    let e1 = translate_exp env e1 in
    let p, env = translate_pattern env p in
    let e2 = translate_exp env e2 in
    RLet { bnd_pattern = p; bnd_val = e1; body = e2 }
  | LetRec { bnd_var; bnd_val = e1; body = e2; fun_typ } ->
    let e1 = translate_exp env e1 in
    let e2 = translate_exp env e2 in
    RLetRec { bnd_var; bnd_val = e1; val_typ = fun_typ; body = e2 }
  | Lambda _ as l -> translate_lambda env l (* could add free vars *)
  | TLambda _ as l -> translate_lambda env l
  | Apply _ as a -> translate_apply env a
  | TApply _ as a -> translate_apply env a
  | Seq el -> RSeq (List.map (translate_exp env) el)
  | If { cond = e1; ifthen = e2; ifelse = e3 } ->
    let e1 = translate_exp env e1 in
    let e2 = translate_exp env e2 in
    let e3 = translate_exp env e3 in
    RIf { cond = e1; ifthen = e2; ifelse = e3 }
  | Match { arg = e; cases = pel } ->
    let e = translate_exp env e in
    let pel = List.map (fun (p, e) ->
        let p, env = translate_pattern env p in
        p, translate_exp env e) pel in
    RMatch { arg = e; cases = pel }
  | Constructor { constr; ctyps; args = el } ->
    begin match Love_tenv.find_constr constr env with
      | Some { result = { cparent = TUser (tname, _); cname; _ }; _ } ->
        let el = List.map (translate_exp env) el in
        let type_name = Ident.change_last constr tname in
        let constr = cname in
        RConstructor { type_name; constr; ctyps; args = el }
      | _ -> raise (InvariantBroken "Constructor not found")
    end
  | ENone -> RNone
  | ESome e -> RSome (translate_exp env e)
  | Nil -> RNil
  | List (el, e) -> RList (List.map (translate_exp env) el, translate_exp env e)
  | Tuple el -> RTuple (List.map (translate_exp env) el)
  | Project { tuple = e; indexes } ->
    RProject { tuple = translate_exp env e; indexes }
  | Update { tuple = e; updates = iel } ->
    let e = translate_exp env e in
    let iel = List.map (fun (i, e) -> i, translate_exp env e) iel in
    RUpdate { tuple = e; updates = iel }
  | Record { path; contents = fel } ->
    let res = match path with
      | None -> Love_tenv.find_field (fst (List.hd fel)) env
      | Some path -> Love_tenv.find_field_in path (fst (List.hd fel)) env
    in
    begin match res with
      | Some { result = { fparent = TUser (type_name, _); _ }; _ } ->
        let fel = List.map (fun (f, e) -> f, translate_exp env e) fel in
        RRecord { type_name; contents = fel }
      | _ -> raise (InvariantBroken "Field not found")
    end
  | GetField { record = e; field } ->
    RGetField { record = translate_exp env e; field }
  | SetFields { record = e; updates = fel } ->
    let e = translate_exp env e in
    let fel = List.map (fun (f, e) -> f, translate_exp env e) fel in
    RSetFields { record = e; updates = fel }
  | PackStruct r -> RPackStruct (translate_reference env r)
  | Raise { exn = en; args = el; loc } ->
    let en = translate_exn env en in
    let el = List.map (translate_exp env) el in
    RRaise { exn = en; args = el; loc }
  | TryWith { arg = e; cases = pel } ->
    let e = translate_exp env e in
    let pel = List.map (fun ((en, pl), e) ->
        let pl, env = List.fold_left (fun (pl, env) p ->
            let p, env = translate_pattern env p in
            p :: pl, env
          ) ([], env) pl
        in
        (translate_exn env en, List.rev pl), translate_exp env e) pel in
    RTryWith { arg = e; cases = pel }

and translate_exp (env : 'a Love_tenv.t) (e : Love_ast.exp) :
  Love_runtime_ast.exp =
  translate_raw_exp env e.content

and translate_lambda (env : 'a Love_tenv.t) (l : Love_ast.raw_exp) :
  Love_runtime_ast.exp =
  let open Love_ast in
  let open Love_runtime_ast in
  let rec aux env args = function
    | Lambda { arg_pattern = p; arg_typ = t; body } ->
      let p, env = translate_pattern env p in
      let args = RPattern (p, t) :: args in
      aux env args body.content
    | TLambda { targ = tv; exp } ->
      let args = RTypeVar tv :: args in
      aux env args exp.content
    | e -> RLambda { args = List.rev args; body = translate_raw_exp env e }
  in
  aux env [] l

and translate_apply (env : 'a Love_tenv.t) (l : Love_ast.raw_exp) :
  Love_runtime_ast.exp =
  let open Love_ast in
  let open Love_runtime_ast in
  let rec aux args = function
    | Apply { fct = e; args = el } ->
      let args = List.fold_left (fun args e ->
          (RExp (translate_exp env e)) :: args) args (List.rev el) in
      aux args e.content
    | TApply { exp = e; typ } ->
      aux ((RType typ) :: args) e.content
    | e -> RApply { exp = translate_raw_exp env e; args }
  in
  aux [] l

and translate_entry (env : 'a Love_tenv.t) (e : Love_ast.entry) :
  Love_runtime_ast.entry =
  Love_runtime_ast.{
    entry_code = translate_exp env e.entry_code;
    entry_fee_code = Option.map ~f:(translate_exp env) e.entry_fee_code;
    entry_typ = e.entry_typ;
  }

and translate_view (env : 'a Love_tenv.t) (v : Love_ast.view) :
  Love_runtime_ast.view =
  Love_runtime_ast.{
    view_code = translate_exp env v.view_code;
    view_typ = v.view_typ;
  }

and translate_value (env : 'a Love_tenv.t) (v : Love_ast.value) :
  Love_runtime_ast.value =
  Love_runtime_ast.{
    value_code = translate_exp env v.value_code;
    value_typ = v.value_typ;
    value_visibility = v.value_visibility;
  }

and translate_content (env : 'a Love_tenv.t) (n : string)
    (c : Love_ast.content) :
  Love_runtime_ast.content =
  match c with
  | DefType (k, td) -> RDefType (k, td)
  | DefException tl -> RDefException tl
  | Entry e -> REntry (translate_entry env e)
  | View v -> RView (translate_view env v)
  | Value v -> RValue (translate_value env v)
  | Structure s ->
    begin match Love_tenv.get_subenv_struct n env with
      | Some env -> RStructure (translate_structure env s)
      | None -> raise (InvariantBroken ("Structure " ^ n ^ " not found"))
    end
  | Signature s -> RSignature s

and translate_structure (env : 'a Love_tenv.t) (s : Love_ast.structure) :
  Love_runtime_ast.structure =
  Love_runtime_ast.{
    structure_content =
      List.map (fun (n, c) ->
          n, translate_content env n c) s.structure_content;
    kind = s.kind
  }

and translate_reference (_env : 'a Love_tenv.t) (r : Love_ast.reference) :
  Love_runtime_ast.reference =
  match r with
  | Anonymous s ->
(* TODO : handle deps *)
      (* let deps = match s.kind with
        | Love_type.Module -> []
        | Love_type.Contract l -> l in *)
      (* Love_typechecker.extract_deps None ctxt
        (Love_tenv.empty code.root_struct.kind () ()) deps *)
      let deps_env = Love_tenv.empty s.kind () () in
      let root_sig = Love_ast_utils.sig_of_structure s in
      let env = Love_tenv.contract_sig_to_env None () root_sig deps_env in
      RAnonymous (translate_structure env s)
  | Named id -> RNamed id

let translate_to_runtime (env : 'a Love_tenv.t) (c : Love_ast.top_contract) :
  Love_runtime_ast.top_contract =
  Love_runtime_ast.{
    version = c.version;
    code = translate_structure env c.code
  }

let translate_structure env s =
  Love_tenv.set_environment_mode true;
  let s = translate_structure env s in
  Love_tenv.set_environment_mode false;
  s

let translate_to_runtime env c =
  Love_tenv.set_environment_mode true;
  let c = translate_to_runtime env c in
  Love_tenv.set_environment_mode false;
  c



open Love_value

let inline_value ctxt (v : Value.t) : Value.t =

  let rec map_value v =
    let open Value in
    Value.rec_closure @@
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VNone | VContractInstance (_, _)
    | VPackedStructure _ as v -> v (* is already self-contained *)
    | VEntryPoint (_, _) | VView (_, _) | VOperation _ ->
        raise (InvariantBroken "Forbidden value in contract")
    | VSome v -> VSome (map_value v)
    | VTuple vl -> VTuple (List.map map_value vl)
    | VConstr (cstr, vl) -> VConstr (cstr, List.map map_value vl)
    | VRecord fvl -> VRecord (List.map (fun (f, v) -> f, map_value v) fvl)
    | VList vl -> VList (List.map map_value vl)
    | VSet vs -> VSet (ValueSet.map map_value vs)
    | VMap vm ->
      VMap (ValueMap.fold (fun k v vm ->
          ValueMap.add (map_value k) (map_value v) vm) ValueMap.empty vm)
    | VBigMap ({ diff = vm; _ } as bm) ->
      let vm = ValueMap.fold (fun k v vm ->
          let v = match v with
            | None -> None
            | Some v -> Some (map_value v)
          in
          ValueMap.add (map_value k) v vm
        ) ValueMap.empty vm
      in
      VBigMap { bm with diff = vm }
    | VPrimitive (p, vl) -> VPrimitive (p, List.map map_value vl)
    | VClosure { call_env; lambda } ->
      let values = List.map (fun (id, p) ->
          id, match p with
          | Local v -> Local (map_value v)
          | Global (Inlined (p, v)) -> Global (Inlined (p, (map_value v)))
          | Global (Pointer p) ->
            (* should deep inline if the function belongs
               to the current contract *)
            Global (Inlined (p, map_value (Love_context.get_value ctxt p)))
        ) call_env.values in
      let structs = List.map (fun (id, p) ->
          id, match p with
          | Inlined (p, s) -> Inlined (p, map_struct s)
          | Pointer p ->
            (* should deep inline if the struct belongs
               to the current contract *)
            Inlined (p, map_struct (Love_context.get_struct ctxt p))
        ) call_env.structs in
      let sigs = List.map (fun (id, p) ->
          id, match p with
          | Inlined _ -> p
          | Pointer p -> Inlined (p, Love_context.get_sig ctxt p)
        ) call_env.sigs in
      let call_env = { call_env with values; structs; sigs } in
      VClosure { call_env; lambda }

  and map_struct s =
    let content = List.map (fun (n, c) ->
        let open LiveStructure in
        let c' = match c with
          | VType _ | VException _ | VSignature _ -> c
          | VEntry e ->
            VEntry { e with ventry_code = map_value e.ventry_code }
          | VView v ->
            VView { v with vview_code = map_value v.vview_code }
          | VValue v ->
            VValue { v with vvalue_code = map_value v.vvalue_code }
          | VStructure s -> VStructure (map_struct s)
        in
        n, c'
      ) s.content
    in
    { s with content }
  in

  map_value v



let make_type_absolute ctxt env (t : Love_type.t) :
  (Love_context.t * Love_type.t) Error_monad.tzresult Lwt.t =

  let open Error_monad in
  let open Collections in
  let open Love_type in

  let rec aux (ctxt, ids) t =
    match t with
    | TUnit | TBool | TString | TBytes | TInt | TNat
    | TDun | TKey | TKeyHash | TSignature | TTimestamp
    | TAddress | TOperation | TVar _
    | TPackedStructure (Anonymous _)
    | TContractInstance (Anonymous _) ->
        return (ctxt, ids)
    | TOption t | TList t | TSet t | TEntryPoint t
    | TForall (_, t) ->
        aux (ctxt, ids) t
    | TMap (t1, t2) | TBigMap (t1, t2) | TArrow (t1, t2)
    | TView (t1, t2) ->
        aux (ctxt, ids) t1 >>=? fun acc -> aux acc t2
    | TTuple tl ->
        fold_left_s aux (ctxt, ids) tl
    | TUser (id, tl) ->
        Love_env.find_type_name_opt ctxt env id >>=?
(* TODO : if already absolute, do nothing *)
        begin function
          | _ctxt, None -> raise (InvariantBroken "Type not found")
          | ctxt, Some path ->
              fold_left_s aux (ctxt, (StringIdentMap.add id path ids)) tl
        end
    | TPackedStructure (Named id)
    | TContractInstance (Named id) ->
        Love_env.find_raw_sig_opt ctxt env id >>|?
        begin function
          | ctxt, Some (Pointer path | Inlined (path, _)) ->
              ctxt, StringIdentMap.add id path ids
          | _ -> raise (InvariantBroken "Signature not found")
        end
  in

  aux (ctxt, StringIdentMap.empty) t >>|? fun (ctxt, ids) ->

  let rec aux t =
    match t with
    | TUnit | TBool | TString | TBytes | TInt | TNat
    | TDun | TKey | TKeyHash | TSignature | TTimestamp
    | TAddress | TOperation | TVar _
    | TPackedStructure (Anonymous _)
    | TContractInstance (Anonymous _) -> t
    | TOption t -> TOption t
    | TList t -> TList (aux t)
    | TSet t -> TSet (aux t)
    | TEntryPoint t -> TEntryPoint (aux t)
    | TForall (tv, t) -> TForall (tv, aux t)
    | TMap (t1, t2) -> TMap (aux t1, aux t2)
    | TBigMap (t1, t2) -> TBigMap (aux t1, aux t2)
    | TArrow (t1, t2) -> TArrow (aux t1, aux t2)
    | TView (t1, t2) -> TView (aux t1, aux t2)
    | TTuple tl -> TTuple (List.map aux tl)
    | TUser (id, tl) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Type not found")
          | Some id -> TUser (id, List.map aux tl)
        end
    | TPackedStructure (Named id) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Signature not found")
          | Some id -> TPackedStructure (Named id)
        end
    | TContractInstance (Named id) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Signature not found")
          | Some id -> TPackedStructure (Named id)
        end
  in

  ctxt, aux t



let make_types_absolute_in_value ctxt env (v : Value.t) :
  (Love_context.t * Value.t) Error_monad.tzresult Lwt.t =

  let open Error_monad in
  let open Value in

  let add_absolute_type ctxt t types =
    if List.exists (fun (rt, _) -> rt == t) types then
      return (ctxt, types)
    else
      make_type_absolute ctxt env t >>|? fun (ctxt, at) ->
      ctxt, (t, at) :: types
  in

  let rec aux (ctxt, types) v =
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VOperation _ | VNone
    | VContractInstance _ | VPackedStructure _
    | VEntryPoint (_, _) | VView (_, _) ->
        return (ctxt, types)
    | VSome v ->
        aux (ctxt, types) v
    | VList vl | VTuple vl | VConstr (_, vl) | VPrimitive (_, vl) ->
        fold_left_s aux (ctxt, types) vl
    | VRecord fvl ->
        fold_left_s (fun acc (_f, v) -> aux acc v) (ctxt, types) fvl
    | VSet vs ->
        fold_left_s aux (ctxt, types) (ValueSet.elements vs)
    | VMap vm ->
        fold_left_s (fun acc (k, v) ->
          aux acc k >>=? fun acc -> aux acc v
        ) (ctxt, types) (ValueMap.bindings vm)
    | VBigMap { diff = vm; key_type; value_type; _ } ->
        fold_left_s (fun acc (k, v) ->
          aux acc k >>=? fun acc ->
          match v with
          | Some v -> aux acc v
          | None -> return acc
        ) (ctxt, types) (ValueMap.bindings vm) >>=? fun (ctxt, types) ->
        add_absolute_type ctxt key_type types >>=? fun (ctxt, types) ->
        add_absolute_type ctxt value_type types
    | VClosure { call_env = { values; _ }; _ } ->
        fold_left_s (fun acc (_id, vc) ->
          match vc with
          | Local v' -> aux acc v'
          | Global (Inlined (_p, v)) -> aux acc v
          | Global (Pointer _p) -> return acc
        ) (ctxt, types) values
  in

  aux (ctxt, []) v >>|? fun (ctxt, types) ->

  let subst_absolute_type t types =
    match List.find_opt (fun (rt, _) -> rt == t) types with
    | Some (_, at) -> at
    | None -> t
  in

  let rec aux v =
    Value.rec_closure @@
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VOperation _ | VNone
    | VContractInstance _ | VPackedStructure _
    | VEntryPoint (_, _) | VView (_, _) -> v
    | VSome v -> VSome (aux v)
    | VList vl -> VList (List.map aux vl)
    | VTuple vl -> VTuple (List.map aux vl)
    | VConstr (cstr, vl) -> VConstr (cstr, List.map aux vl)
    | VPrimitive (p, vl) -> VPrimitive (p, List.map aux vl)
    | VRecord fvl -> VRecord (List.map (fun (f, v) -> f, aux v) fvl)
    | VSet vs -> VSet (ValueSet.map aux vs)
    | VMap vm ->
        VMap (ValueMap.fold (fun k v vm ->
          ValueMap.add (aux k) (aux v) vm) ValueMap.empty vm)
    | VBigMap ({ diff = vm; key_type; value_type; _ } as bm) ->
        let vm = ValueMap.fold (fun k v vm ->
            let v = match v with
              | None -> None
              | Some v -> Some (aux v)
            in
            ValueMap.add (aux k) v vm
          ) ValueMap.empty vm
        in
        let key_type = subst_absolute_type key_type types in
        let value_type = subst_absolute_type value_type types in
        VBigMap { bm with diff = vm; key_type; value_type }
    | VClosure { call_env = { values; _ } as call_env; lambda } ->
        let values = List.map (fun (id, p) ->
          id, match p with
          | Local v -> Local (aux v)
          | Global (Inlined (p, v)) -> Global (Inlined (p, (aux v)))
          | Global (Pointer _p) -> p
        ) values in
        let call_env = { call_env with values } in
        VClosure { call_env; lambda }
  in

  ctxt, aux v

let rebase_contract _ctxt ~from_path ~to_path
    ({ root_struct; _ } as c : LiveContract.t) : LiveContract.t =

  let rebase_path path =
    let rec aux p f =
      match p, f with
      | Some p, Some f ->
        begin match Ident.split p, Ident.split f with
        | (n1, p), (n2, f) when String.equal n1 n2 -> aux p f
        | _ -> None
        end
      | Some p, None -> Some (Ident.concat to_path p)
      | None, None -> Some to_path
      | None, Some _ -> None
    in
    aux (Some path) (Some from_path)
  in

  let rec map_value v =
    let open Value in
    Value.rec_closure @@
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VNone | VContractInstance (_, _)
    | VPackedStructure _ as v -> v (* is already self-contained *)
    | VEntryPoint (_, _) | VView (_, _) | VOperation _ | VBigMap _ ->
        raise (InvariantBroken "Forbidden value in contract")
    | VSome v -> VSome (map_value v)
    | VTuple vl -> VTuple (List.map map_value vl)
    | VConstr (cstr, vl) -> VConstr (cstr, List.map map_value vl)
    | VRecord fvl -> VRecord (List.map (fun (f, v) -> f, map_value v) fvl)
    | VList vl -> VList (List.map map_value vl)
    | VSet vs -> VSet (ValueSet.map map_value vs)
    | VMap vm ->
      VMap (ValueMap.fold (fun k v vm ->
          ValueMap.add (map_value k) (map_value v) vm) ValueMap.empty vm)
    | VPrimitive (p, vl) -> VPrimitive (p, List.map map_value vl)
    | VClosure { call_env; lambda } ->
      let values = List.map (fun (id, p) ->
          id, match p with
          | Local v -> Local (map_value v)
          | Global (Inlined (p, v)) ->
              begin match rebase_path p with
                | Some p -> Global (Inlined (p, v))
                | None -> Global (Inlined (p, v))
              end
          | Global (Pointer p) ->
              begin match rebase_path p with
                | Some p -> Global (Pointer p)
                | None -> Global (Pointer p)
              end
        ) call_env.values in
      let structs = List.map (fun (id, p) ->
          id, match p with
          | Inlined (p, s) ->
              begin match rebase_path p with
                | Some p -> Inlined (p, s)
                | None -> Inlined (p, s)
              end
          | Pointer p ->
              begin match rebase_path p with
                | Some p -> Pointer p
                | None -> Pointer p
              end
        ) call_env.structs in
      let sigs = List.map (fun (id, p) ->
          id, match p with
          | Inlined (p, s) ->
              begin match rebase_path p with
                | Some p -> Inlined (p, s)
                | None -> Inlined (p, s)
              end
          | Pointer p ->
              begin match rebase_path p with
                | Some p -> Pointer p
                | None -> Pointer p
              end
        ) call_env.sigs in
      let types = List.map (fun (ids, idd) ->
          match rebase_path idd with
          | Some idd -> ids, idd
          | None -> ids, idd
        ) call_env.types in
      (* tvars don't need to be rebased, they are always relative *)
      let call_env = { call_env with values; structs; sigs; types } in
      VClosure { call_env; lambda }

  and map_struct s =
    let content = List.map (fun (n, c) ->
        let open LiveStructure in
        let c' = match c with
          | VType _ | VException _ | VSignature _ -> c
          | VEntry e ->
            VEntry { e with ventry_code = map_value e.ventry_code }
          | VView v ->
            VView { v with vview_code = map_value v.vview_code }
          | VValue v ->
            VValue { v with vvalue_code = map_value v.vvalue_code }
          | VStructure s -> VStructure (map_struct s)
        in
        n, c'
      ) s.content
    in
    { s with content }
  in

  { c with root_struct = map_struct root_struct }
