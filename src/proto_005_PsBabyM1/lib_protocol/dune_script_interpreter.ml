(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Script_interpreter

open Script

type error += Unsupported_language of string
type error += Invalid_language_argument

let () =
  let open Data_encoding in
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_script_interpreter.unsupported_language"
    ~title: "The specified language is not supported"
    ~description: "The language is not supported on this network"
    (obj1 (req "lang" string))
    (function Unsupported_language s -> Some s | _ -> None)
    (fun s -> Unsupported_language s);
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_script_interpreter.invalid_language_argument"
    ~title: "Invalid language argument for the requested operation"
    ~description: "The requested operation can't be performed on the\
                   specified language or language combination"
    (unit)
    (function Invalid_language_argument -> Some () | _ -> None)
    (fun () -> Invalid_language_argument)

let module_of_code code =
  let module Lang = (val (Script_dune.module_of_code code) : Script_dune.S) in
  if (String.equal Config.config.network "Mainnet") then
    fail (Unsupported_language Lang.lang_str)
  else
    return (Dune_script_interpreter_registration.get_module Lang.lang)

let module_of_const const =
  let module Lang = (val (Script_dune.module_of_const const) : Script_dune.S) in
  if (String.equal Config.config.network "Mainnet") then
    fail (Unsupported_language Lang.lang_str)
  else
    return (Dune_script_interpreter_registration.get_module Lang.lang)

(* Called once by helpers_services.ml *)
let normalize_script ctxt script =
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      normalize_script ctxt { code ; storage } >>=? fun (script, ctxt) ->
      return (Script_michelson.to_canonical_script script, ctxt)
  | Dune_code code, Dune_expr storage ->
     module_of_code code >>=? fun m ->
     let module Lang = (val m : Dune_script_interpreter_registration.S) in
     let open Lang in
     begin match code, storage with
     | Repr.Code code, Repr.Const storage ->
        Interpreter.normalize_script ctxt code storage
        >>=? fun ((code, storage, fee_code), ctxt) ->
        let script = Script.{
            code = Script.lazy_expr (Dune_code (Repr.Code code));
            storage = Script.lazy_expr (Dune_expr (Repr.Const storage));
            fee_code = Option.map ~f:(fun c -> Script.lazy_expr
                         (Dune_code (Repr.Code c))) fee_code } in
        return (script, ctxt)
     | _ -> fail Invalid_language_argument
     end
  | _ -> fail Invalid_language_argument

(* Called by contract_services.ml and here (readable_script) *)
let denormalize_script ctxt { code ; storage ; fee_code } =
  match fee_code with
  | None -> return ({ code ; storage }, ctxt)
  | Some fee_code ->
      force_decode ctxt code >>=? fun (code, ctxt) ->
      force_decode ctxt storage >>=? fun (storage, ctxt) ->
      force_decode ctxt fee_code >>=? fun (fee_code, ctxt) ->
      match code, storage, fee_code with
      | Michelson_expr code, Michelson_expr storage, Michelson_expr fee_code ->
          let open Script_ir_translator in
          denormalize_script ctxt { code ; storage ; fee_code = Some fee_code }
          >>=? fun (script, ctxt) ->
          return (Script_michelson.to_script script, ctxt)
      | Dune_code code, Dune_expr storage, Dune_code fee_code ->
          module_of_code code >>=? fun m ->
          let module Lang = (val m : Dune_script_interpreter_registration.S) in
          let open Lang in
          begin match code, storage, fee_code with
            | Repr.Code code, Repr.Const storage, Repr.Code fee_code ->
                Interpreter.denormalize_script ctxt code storage (Some fee_code)
                >>=? fun ((code, storage), ctxt) ->
                let script = Script.{
                    code = Script.lazy_expr (Dune_code (Repr.Code code));
                    storage = Script.lazy_expr
                        (Dune_expr (Repr.Const storage)) } in
                return (script, ctxt)
            | _ -> fail Invalid_language_argument
          end
      | _ -> fail Invalid_language_argument

(* Called once by contract_services.ml *)
let readable_storage ctxt ~code ~storage =
  force_decode ctxt code >>=? fun (code, ctxt) ->
  force_decode ctxt storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt ~legacy:false { code ; storage }
      >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Michelson_expr script.storage)
  | Dune_code _code, Dune_expr _storage ->
      return storage
  | _ -> fail Invalid_language_argument

(* Called once by contract_services.ml *)
let readable_script ctxt script =
  denormalize_script ctxt script >>=? fun (script, ctxt) ->
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt ~legacy:false { code ; storage }
      >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Script_michelson.to_script script)
  | Dune_code _code, Dune_expr _storage ->
      return script
  | _ -> fail Invalid_language_argument

let typecheck_code ctxt = function
  | Michelson_expr expr ->
      Script_ir_translator.typecheck_code ctxt expr
  | Dune_code code ->
     module_of_code code >>=? fun m ->
     let module Lang = (val m : Dune_script_interpreter_registration.S) in
     begin match code with
     | Lang.Repr.Code code ->
         Lang.Interpreter.typecheck_code ctxt code >>=? fun ctxt ->
         return ([], ctxt)
     | _ -> fail Invalid_language_argument
     end
  | Dune_expr _ -> fail Invalid_language_argument

let typecheck_data ctxt = function
  | Michelson_expr data, Michelson_expr ty ->
      Script_ir_translator.typecheck_data ctxt (data, ty)
  | Dune_expr data, Dune_expr type_expr ->
     module_of_const data >>=? fun m ->
     let module Lang = (val m : Dune_script_interpreter_registration.S) in
     begin match data, type_expr with
     | Lang.Repr.Const data, Lang.Repr.Const type_expr ->
         Lang.Interpreter.typecheck_data ctxt data type_expr
     | _ -> fail Invalid_language_argument
     end
  | _ -> fail Invalid_language_argument

let typecheck ?(type_only=false) ctxt script self ~internal =
  Script.force_decode ctxt script.storage
  >>=? fun (storage, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost storage))
  >>=? fun ctxt ->
  Script.force_decode ctxt script.code >>=? fun (code, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost code)) >>=? fun ctxt ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      Script_ir_translator.parse_script ctxt ~legacy:false { code ; storage }
      >>=? fun (Ex_full_script parsed_script, ctxt) ->
      let to_update = Script_ir_translator.no_big_map_id in
      begin
        if type_only then
          Script_ir_translator.extract_big_map_diff ctxt Optimized
            parsed_script.storage_type parsed_script.storage
            ~to_duplicate: Script_ir_translator.no_big_map_id
            ~to_update
            ~temporary:false
        else
          Script_ir_translator.collect_big_maps ctxt
            parsed_script.storage_type parsed_script.storage
          >>=? fun (to_duplicate, ctxt) ->
          Script_ir_translator.extract_big_map_diff ctxt Optimized
            parsed_script.storage_type parsed_script.storage
            ~to_duplicate
            ~to_update
            ~temporary:false
      end >>=?
      fun (storage, big_map_diff, ctxt) ->
      Script_ir_translator.unparse_data ctxt Optimized
        parsed_script.storage_type storage >>=? fun (storage, ctxt) ->
      let storage = Micheline.strip_locations storage in
      Script_ir_translator.normalize_script ctxt { code ; storage }
      >>=? fun (script, ctxt) ->
      return ((Script_michelson.to_canonical_script script, big_map_diff), ctxt)
  | Dune_code code, Dune_expr storage ->
     module_of_code code >>=? fun m ->
     let module Lang = (val m : Dune_script_interpreter_registration.S) in
     let open Lang in
     begin match code, storage with
     | Repr.Code code, Repr.Const storage ->
        Interpreter.typecheck ctxt ~code ~storage ~self ~internal
        >>=? fun ((code, storage, fee_code), big_map_diff, ctxt) ->
        return (({ code = Script.lazy_expr (Dune_code (Repr.Code code));
                   storage = Script.lazy_expr (Dune_expr (Repr.Const storage));
                   fee_code = Option.map ~f:(fun c -> Script.lazy_expr
                                (Dune_code (Repr.Code c))) fee_code },
                 big_map_diff), ctxt)
     | _ -> fail Invalid_language_argument
     end
  | _ -> fail Invalid_language_argument

let execute ctxt mode step_constants ~code ~storage
    ~entrypoint ~parameter ~apply_manager_operation_content =
  match code, storage, parameter with
  | Michelson_expr code, Michelson_expr storage, Michelson_expr parameter ->
      Script_interpreter.execute
        ctxt mode step_constants ~code ~storage ~entrypoint ~parameter
  | Dune_code code, Dune_expr storage, Dune_expr parameter ->
      module_of_code code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin
        match code, storage, parameter with
        | Repr.Code code, Repr.Const storage, Repr.Const parameter ->
            Interpreter.execute
              ctxt mode step_constants ~code ~storage ~entrypoint ~parameter
              ~apply:(fun ctxt ->
                  apply_manager_operation_content ctxt mode
                    ~payer:step_constants.payer
                    ~source:step_constants.source
                    ~chain_id:step_constants.chain_id
                    ~internal:true
                )
            >>=? fun { ctxt; storage; big_map_diff; result = _; operations  } ->
            let storage = Dune_expr (Repr.Const storage) in
            return { ctxt; storage; big_map_diff; operations }
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument (* TODO Love : translation to/from Michelson exprs  *)

let execute_fee_script ctxt step_constants ~fee_code ~storage
    ~entrypoint ~parameter =
  match fee_code, storage, parameter with
  | Michelson_expr fee_code, Michelson_expr storage, Michelson_expr parameter ->
      Script_interpreter.execute_fee_script
        ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter
  | Dune_code fee_code, Dune_expr storage, Dune_expr parameter ->
      module_of_code fee_code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin
        match fee_code, storage, parameter with
        | Repr.Code fee_code, Repr.Const storage, Repr.Const parameter ->
            Interpreter.execute_fee_script
              ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter
            >>=? fun { ctxt; max_fee; max_storage } ->
            return { ctxt; max_fee; max_storage }
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument (* TODO Love : translation to/from Michelson exprs  *)

open Script_all

let trace ctxt mode step_constants ~code ~storage ~entrypoint ~parameter =
  match code, storage, parameter with
  | Michelson_expr code, Michelson_expr storage, Michelson_expr parameter ->
      Script_interpreter.trace ctxt mode step_constants ~code ~storage
        ~entrypoint ~parameter
  | Dune_code code, Dune_expr storage, Dune_expr parameter ->
      module_of_code code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin
        match code, storage, parameter with
        | Repr.Code code, Repr.Const storage, Repr.Const parameter ->
            Interpreter.execute
              ctxt mode step_constants ~code ~storage ~entrypoint ~parameter
              ~apply:(fun _ctxt -> assert false)
            >>=? fun Interpreter.{ ctxt; storage; big_map_diff;
                                   result = _; operations } ->
            let storage = Dune_expr (Repr.Const storage) in
            return ({ ctxt; storage; big_map_diff; operations }, [])
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument
