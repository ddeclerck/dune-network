(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections
open Love_ast

module Serializer = struct

  module LBB = Love_binary_buffer

  (** Combining serializers **)

  let ser_x_option ser_x state = function
    | None -> LBB.write_bool state false
    | Some x -> LBB.write_bool state true; ser_x state x

  let ser_list_size state l =
    let l = List.length l in
    if Compare.Int.(l = 0) then (* size = 0 *)
      LBB.write state 0L 2 (* 00 *)
    else if Compare.Int.(l < 9) then begin (* size = 1 - 8 *)
      LBB.write state 1L 1; (* 1 *)
      LBB.write state (Int64.of_int (l - 1)) 4;
    end
    else begin (* size >= 9 *)
      LBB.write state 1L 2; (* 01 *)
      LBB.write_uint state (l - 9)
    end

  let ser_x_list ser_x state xl =
    (* LBB.write_uint state (List.length xl); *)
    ser_list_size state xl;
    List.iter (fun x -> ser_x state x) xl

  let ser_x_y_list ser_x ser_y state xyl =
    (* LBB.write_uint state (List.length xyl); *)
    ser_list_size state xyl;
    List.iter (fun (x, y) -> ser_x state x; ser_y state y) xyl

  let ser_x_x_list ser_x state xxl =
    (* LBB.write_uint state (List.length xxl); *)
    ser_list_size state xxl;
    List.iter (fun (x1, x2) -> ser_x state x1; ser_x state x2) xxl

  let ser_x_x_pair ser_x state (x1,x2) =
    ser_x state x1; ser_x state x2

  let ser_x_y_pair ser_x ser_y state (x, y) =
    ser_x state x; ser_y state y


  (** Utility serializers **)

  let ser_ident state ident =
    let l = Ident.get_list ident in
    ser_x_list LBB.write_str_repr state l

  let ser_ident_x_list ser_x state ixl =
    ser_x_y_list ser_ident ser_x state ixl

  let ser_str_repr_x_list ser_x state sxl =
    ser_x_y_list LBB.write_str_repr ser_x state sxl

  let ser_str_repr_id_x_list ser_x state sxl =
    ser_x_y_list ser_ident ser_x state sxl

  let ser_int_x_list ser_x state ixl =
    ser_x_y_list (fun s i -> LBB.write_uint s i) ser_x state ixl


  (** Main serializer *)

  let ser serial e =
    let state = LBB.initialize_writer () in
    serial state e;
    (* Format.printf "%a" LBB.print_stats state; *)
    LBB.finalize state


  (** Qualifier serializers **)

  let ser_visibility state =
    function
    | Private -> LBB.write_bool state false
    | Public -> LBB.write_bool state true


  (** Type serializers **)

  module Type = struct

    open Love_type

    let ser_type_name = ser_ident

    let ser_trait state { tcomparable } = LBB.write_bool state tcomparable

    let ser_rec state = function
      | Rec -> LBB.write_bool state false
      | NonRec -> LBB.write_bool state true

    let ser_type_var state Love_type.{ tv_name; tv_traits } =
      LBB.write_str_repr state tv_name;
      ser_trait state tv_traits

    let rec ser_type state =
      let ser_type_list = ser_x_list ser_type in
      let write state i =
        LBB.stats_add_i_custom state "Type" 5;
        LBB.write state (Int64.of_int i) 5 in
      function
      | TUnit -> write state 0
      | TBool -> write state 1
      | TString -> write state 2
      | TBytes -> write state 3
      | TInt -> write state 4
      | TNat -> write state 5
      | TOption t -> write state 6; ser_type state t
      | TTuple tl -> write state 7; ser_type_list state tl
      | TUser (tn, tl) ->
        write state 8; ser_type_name state tn; ser_type_list state tl
      | TList t -> write state 9; ser_type state t
      | TSet t -> write state 10; ser_type state t
      | TMap (t1, t2) ->
        write state 11; ser_type state t1; ser_type state t2
      | TBigMap (t1, t2) ->
        write state 12; ser_type state t1; ser_type state t2
      | TDun -> write state 13
      | TKey -> write state 14
      | TKeyHash -> write state 15
      | TSignature -> write state 16
      | TTimestamp -> write state 17
      | TAddress -> write state 18
      | TOperation -> write state 19
      | TContractInstance ct ->
        write state 20; ser_structure_type state ct
      | TPackedStructure st ->
        write state 21; ser_structure_type state st
      | TEntryPoint t ->
        write state 22; ser_type state t
      | TView (t1, t2) ->
        write state 23; ser_x_x_pair ser_type state (t1, t2)
      | TArrow (t1, t2) ->
        write state 24; ser_type state t1; ser_type state t2
      | TVar tv -> write state 25; ser_type_var state tv
      | TForall (tv, t) ->
        write state 26; ser_type_var state tv; ser_type state t

    and ser_structure_type state = function
      | Anonymous cs -> LBB.write_bool state false; ser_signature state cs
      | Named cn -> LBB.write_bool state true; ser_ident state cn

    and ser_contract_type state = function
      | StructType st -> LBB.write_bool state false; ser_structure_type state st
      | ContractInstance cn -> LBB.write_bool state true; ser_ident state cn

    and ser_typedef state = function
      | Alias { aparams; atype } ->
        LBB.write state Int64.zero 2;
        ser_x_list ser_type_var state aparams;
        ser_type state atype
      | SumType { sparams; scons; srec } ->
        LBB.write state Int64.one 2;
        ser_x_list ser_type_var state sparams;
        ser_str_repr_x_list (ser_x_list ser_type) state scons;
        ser_rec state srec
      | RecordType { rparams; rfields; rrec } ->
        LBB.write state (Int64.of_int 2) 2;
        ser_x_list ser_type_var state rparams;
        ser_str_repr_x_list ser_type state rfields;
        ser_rec state rrec

    and ser_sigtype state = function
      | SPublic td ->
        LBB.write state Int64.zero 2;
        ser_typedef state td
      | SPrivate td ->
        LBB.write state Int64.one 2;
        ser_typedef state td
      | SAbstract tl ->
        LBB.write state (Int64.of_int 2) 2;
        ser_x_list ser_type_var state tl

    and ser_sig_content state =
      let write i = LBB.write state (Int64.of_int i) 3 in
      function
      | SType s -> write 0; ser_sigtype state s
      | SException tl -> write 1; ser_x_list ser_type state  tl
      | SEntry t -> write 2; ser_type state t
      | SView (t1, t2) -> write 3; ser_type state t1; ser_type state t2
      | SValue t -> write 4; ser_type state t
      | SStructure st -> write 5; ser_structure_type state st
      | SSignature s -> write 6; ser_signature state s

    and ser_kind state = function
      | Module -> LBB.write_bool state false
      | Contract l -> (
          LBB.write_bool state true;
          ser_x_y_list LBB.write_str_repr LBB.write_string state l
        )

    and ser_signature state { sig_kind; sig_content; } =
      ser_kind state sig_kind;
      ser_str_repr_x_list ser_sig_content state sig_content

    let serialize = ser ser_type
  end

  (** Primitive serializer **)

  let ser_primitive state p =
    let open Love_primitive in
    LBB.stats_add_i_custom state "Primitives" 8;
    let () = LBB.write_uint8 state (Love_primitive.id_of_prim p)
    in match p with
    | PBMEmpty (tk, tv) -> Type.ser_type state tk; Type.ser_type state tv
    | PBUnpack t -> Type.ser_type state t
    | PCSelf ct -> Type.ser_contract_type state ct
    | PCAt ct -> Type.ser_contract_type state ct
    | _ -> ()

  let ser_key state key =
    LBB.write_string state (Signature.Public_key.to_b58check key)

  let ser_key_hash state key_hash =
    LBB.write_string state (Signature.Public_key_hash.to_b58check key_hash)

  let ser_signature state sign =
    LBB.write_string state (Signature.to_b58check sign)

  let ser_timestamp state ts =
    LBB.write_string state (Script_timestamp_repr.to_string ts)

  let ser_address state a =
    LBB.write_string state (Contract_repr.to_b58check a)

  let ser_dun state d =
    let di64 = Tez_repr.to_mutez d in
    Log.debug "[ser_dun] Serializing dun value %Ld@." di64;
    LBB.write_z state (Z.of_int64 di64)

  let ser_type_kind state = function
      Love_ast.TPublic -> LBB.write state Int64.zero 2;
    | TPrivate -> LBB.write state Int64.one 2;
    | TInternal -> LBB.write state (Int64.of_int 2) 2;
    | TAbstract -> LBB.write state (Int64.of_int 3) 2

  let ser_loc state {pos_lnum;pos_bol;pos_cnum} =
    LBB.write_uint32 state pos_lnum;
    LBB.write_uint32 state pos_bol;
    LBB.write_uint32 state pos_cnum

  module SerAst
      (AnnotSerializer : sig val ser_annot : LBB.writer ->
         location option -> unit end) =
  struct

    (** Const serializers **)

    let ser_annoted ser state annoted =
      ser state annoted.Utils.content;
      AnnotSerializer.ser_annot state annoted.annot

    let ser_exn_name state = function
      | Love_ast.Fail t -> LBB.write_bool state true; Type.ser_type state t
      | Exception e     -> LBB.write_bool state false; ser_ident state e

    let rec ser_raw_const state =
      let write state i =
        LBB.stats_add_i_custom state "Const" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | CUnit -> write state 0
      | CBool b -> write state 1; LBB.write_bool state b
      | CString s -> write state 2; LBB.write_string state s
      | CBytes b -> write state 3; LBB.write_bytes state b
      | CInt i -> write state 4; LBB.write_z state i
      | CNat i -> write state 5; LBB.write_z state i
      | CDun d -> write state 6; ser_dun state d
      | CKey k -> write state 7; ser_key state k
      | CKeyHash kh -> write state 8; ser_key_hash state kh
      | CSignature s -> write state 9; ser_signature state s
      | CTimestamp t -> write state 10; ser_timestamp state t
      | CAddress a -> write state 11; ser_address state a
      | CPrimitive prim -> write state 12; ser_primitive state prim

    and ser_const s = ser_annoted ser_raw_const s

    (** Pattern serializer **)

    and ser_raw_pattern state =
      let ser_pattern_list = ser_x_list ser_pattern in
      let write state i =
        LBB.stats_add_i_custom state "Pattern" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | PAny -> write state 0
      | PVar v -> write state 1; LBB.write_str_repr state v
      | PAlias (p, v) ->
        write state 2; ser_pattern state p; LBB.write_str_repr state v
      | PConst c -> write state 3; ser_const state c
      | PNone -> write state 4
      | PSome p -> write state 5; ser_pattern state p
      | PList pl -> write state 6; ser_pattern_list state pl
      | PTuple pl -> write state 7; ser_pattern_list state pl
      | PConstr (cstr, pl) ->
        write state 8; LBB.write_str_repr state cstr; ser_pattern_list state pl
      | PContract (n, st) ->
        write state 9; LBB.write_str_repr state n;
        Type.ser_structure_type state st


    and ser_pattern s = ser_annoted ser_raw_pattern s

    and ser_exn_pattern state (exn, pl) =
      ser_exn_name state exn; ser_x_list ser_pattern state pl


    (** Exp serializer **)

    and ser_raw_exp state =
      let ser_exp_list = ser_x_list ser_exp in
      let write state i =
        LBB.stats_add_i_custom state "Exp" 5;
        LBB.write state (Int64.of_int i) 5 in
      function
      | Const c ->
        write state 0; ser_const state c
      | Var v ->
        write state 1; ser_ident state v
      | Let { bnd_pattern; bnd_val; body } ->
        write state 2; ser_pattern state bnd_pattern;
        ser_exp state bnd_val; ser_exp state body;
      | LetRec { bnd_var; bnd_val; body; fun_typ } ->
        write state 3; LBB.write_str_repr state bnd_var;
        ser_exp state bnd_val; ser_exp state body; Type.ser_type state fun_typ
      | Lambda l ->
        write state 4; ser_lambda state l
      | Apply { fct; args } ->
        write state 5; ser_exp state fct; ser_exp_list state args
      | TLambda { targ; exp } ->
        write state 6; Type.ser_type_var state targ; ser_exp state exp
      | TApply { exp; typ } ->
        write state 7; ser_exp state exp; Type.ser_type state typ
      | Seq el ->
        write state 8; ser_exp_list state el
      | If { cond; ifthen; ifelse } ->
        write state 9; ser_exp state cond;
        ser_exp state ifthen; ser_exp state ifelse
      | Match { arg; cases } ->
        write state 10; ser_exp state arg;
        ser_x_y_list ser_pattern ser_exp state cases
      | Constructor { constr; ctyps; args } ->
        write state 11;
        ser_ident state constr;
        ser_x_list Type.ser_type state ctyps;
        ser_exp_list state args
      | ESome e ->
        write state 12; ser_exp state e
      | List (el, e) ->
        write state 13; ser_exp_list state el; ser_exp state e
      | Tuple el ->
        write state 14; ser_exp_list state el
      | Project { tuple; indexes } ->
        write state 15; ser_exp state tuple;
        ser_x_list LBB.write_uint state indexes
      | Update { tuple; updates } ->
        write state 16; ser_exp state tuple;
        ser_int_x_list ser_exp state updates
      | Record { path; contents = fel } ->
        write state 17;
        ser_x_option ser_ident state path;
        ser_str_repr_x_list ser_exp state fel
      | GetField { record; field } ->
        write state 18; ser_exp state record;
        LBB.write_str_repr state field
      | SetFields { record; updates } ->
        write state 19; ser_exp state record;
        ser_str_repr_x_list ser_exp state updates
      | PackStruct sr ->
        write state 20;
        ser_struct_ref state sr
      | Raise { exn; args; loc } ->
        write state 21; ser_exn_name state exn;
        ser_exp_list state args;
        ser_x_option ser_loc state loc
      | TryWith { arg; cases } ->
        write state 22; ser_exp state arg;
        ser_x_y_list ser_exn_pattern ser_exp state cases
      | ENone -> write state 23;
      | Nil -> write state 24;

    and ser_exp s = ser_annoted ser_raw_exp s

    and ser_lambda state { arg_pattern; body; arg_typ } =
      ser_pattern state arg_pattern;
      ser_exp state body;
      Type.ser_type state arg_typ


    (** Structure serializers **)

    and ser_entry state { entry_code; entry_fee_code; entry_typ } =
      ser_exp state entry_code; ser_x_option ser_exp state entry_fee_code;
      Type.ser_type state entry_typ

    and ser_view state { view_code; view_typ } =
      ser_exp state view_code; ser_x_x_pair Type.ser_type state view_typ

    and ser_value state { value_code; value_typ; value_visibility } =
      ser_exp state value_code; Type.ser_type state value_typ;
      ser_visibility state value_visibility;

    and ser_content state =
      let write state i =
        LBB.stats_add_i_custom state "Content" 3;
        LBB.write state (Int64.of_int i) 3 in
      function
      | DefType (k, td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | DefException tl -> write state 1; ser_x_list Type.ser_type state tl
      | Entry e -> write state 2; ser_entry state e
      | View v -> write state 3; ser_view state v
      | Value v -> write state 4; ser_value state v
      | Structure s -> write state 5; ser_structure state s
      | Signature s -> write state 6; Type.ser_signature state s

    and ser_structure state { kind; structure_content } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state structure_content;

    and ser_struct_ref state = function
      | Anonymous s -> LBB.write_bool state false; ser_structure state s
      | Named sn -> LBB.write_bool state true; ser_ident state sn

    and ser_top_contract state { version; code } =
      ser_x_x_pair (LBB.write_uint) state version;
      ser_structure state code

    let serialize_const = ser ser_const
    let serialize_exp = ser ser_exp
    let serialize_structure = ser ser_structure
    let serialize_top_contract = ser ser_top_contract
  end

  module Located = SerAst(struct let ser_annot = ser_x_option ser_loc end)
  (*  module Raw = SerAst(struct let ser_annot _ _ = () end) *)




  module Runtime = struct

    module SerType = Type

    open Love_runtime_ast

    module Type = SerType

    let ser_exn_name state = function
      | RFail t -> LBB.write_bool state true; Type.ser_type state t
      | RException e -> LBB.write_bool state false; ser_ident state e

    (** Const serializers **)

    let rec ser_const state =
      let write state i =
        LBB.stats_add_i_custom state "Const" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | RCUnit -> write state 0
      | RCBool b -> write state 1; LBB.write_bool state b
      | RCString s -> write state 2; LBB.write_string state s
      | RCBytes b -> write state 3; LBB.write_bytes state b
      | RCInt i -> write state 4; LBB.write_z state i
      | RCNat i -> write state 5; LBB.write_z state i
      | RCDun d -> write state 6; ser_dun state d
      | RCKey k -> write state 7; ser_key state k
      | RCKeyHash kh -> write state 8; ser_key_hash state kh
      | RCSignature s -> write state 9; ser_signature state s
      | RCTimestamp t -> write state 10; ser_timestamp state t
      | RCAddress a -> write state 11; ser_address state a
      | RCPrimitive prim -> write state 12; ser_primitive state prim

    (** Pattern serializer **)

    and ser_pattern state =
      let ser_pattern_list = ser_x_list ser_pattern in
      let write state i =
        LBB.stats_add_i_custom state "Pattern" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | RPAny -> write state 0
      | RPVar v -> write state 1; LBB.write_str_repr state v
      | RPAlias (p, v) ->
        write state 2; ser_pattern state p; LBB.write_str_repr state v
      | RPConst c -> write state 3; ser_const state c
      | RPNone -> write state 4
      | RPSome p -> write state 5; ser_pattern state p
      | RPList pl -> write state 6; ser_pattern_list state pl
      | RPTuple pl -> write state 7; ser_pattern_list state pl
      | RPConstr (cstr, pl) ->
        write state 8; LBB.write_str_repr state cstr; ser_pattern_list state pl
      | RPContract (n, st) ->
        write state 9; LBB.write_str_repr state n;
        Type.ser_structure_type state st

    and ser_exn_pattern state (exn, pl) =
      ser_exn_name state exn; ser_x_list ser_pattern state pl


    (** Exp serializer **)

    and ser_exp state =
      let ser_exp_list = ser_x_list ser_exp in
      let write state i =
        LBB.stats_add_i_custom state "Exp" 5;
        LBB.write state (Int64.of_int i) 5 in
      function
      | RConst c ->
        write state 0; ser_const state c
      | RVar v ->
        write state 1; ser_ident state v
      | RLet { bnd_pattern; bnd_val; body } ->
        write state 2; ser_pattern state bnd_pattern;
        ser_exp state bnd_val; ser_exp state body;
      | RLetRec { bnd_var; bnd_val; val_typ; body } ->
        write state 3; LBB.write_str_repr state bnd_var;
        ser_exp state bnd_val; Type.ser_type state val_typ; ser_exp state body
      | RLambda l ->
        write state 4; ser_lambda state l
      | RApply { exp; args } ->
        write state 5; ser_exp state exp; ser_x_list ser_param state args
      | RSeq el ->
        write state 6; ser_exp_list state el
      | RIf { cond; ifthen; ifelse } ->
        write state 7; ser_exp state cond;
        ser_exp state ifthen; ser_exp state ifelse
      | RMatch { arg; cases } ->
        write state 8; ser_exp state arg;
        ser_x_y_list ser_pattern ser_exp state cases
      | RConstructor { type_name; constr; ctyps; args } ->
        write state 9;
        ser_ident state type_name;
        LBB.write_str_repr state constr;
        ser_x_list Type.ser_type state ctyps;
        ser_exp_list state args
      | RSome e ->
        write state 10; ser_exp state e
      | RList (el, e) ->
        write state 11; ser_exp_list state el; ser_exp state e
      | RTuple el ->
        write state 12; ser_exp_list state el
      | RProject { tuple; indexes } ->
        write state 13; ser_exp state tuple;
        ser_x_list LBB.write_uint state indexes
      | RUpdate { tuple; updates } ->
        write state 14; ser_exp state tuple;
        ser_int_x_list ser_exp state updates
      | RRecord { type_name; contents = fel } ->
        write state 15;
        ser_ident state type_name;
        ser_str_repr_x_list ser_exp state fel
      | RGetField { record; field } ->
        write state 16; ser_exp state record;
        LBB.write_str_repr state field
      | RSetFields { record; updates } ->
        write state 17; ser_exp state record;
        ser_str_repr_x_list ser_exp state updates
      | RPackStruct sr ->
        write state 18;
        ser_struct_ref state sr
      | RRaise { exn; args; loc } ->
        write state 19; ser_exn_name state exn;
        ser_exp_list state args;
        ser_x_option ser_loc state loc
      | RTryWith { arg; cases } ->
        write state 20; ser_exp state arg;
        ser_x_y_list ser_exn_pattern ser_exp state cases
      | RNone -> write state 21;
      | RNil -> write state 22;

    and ser_binding state b =
      let write state i =
        LBB.stats_add_i_custom state "Binding" 1;
        LBB.write state (Int64.of_int i) 1 in
      match b with
      | RPattern (p, t) ->
        write state 0; ser_x_y_pair ser_pattern Type.ser_type state (p, t)
      | RTypeVar tv ->
        write state 1; Type.ser_type_var state tv

    and ser_param state p =
      let write state i =
        LBB.stats_add_i_custom state "Param" 1;
        LBB.write state (Int64.of_int i) 1 in
      match p with
      | RExp e -> write state 0; ser_exp state e
      | RType t -> write state 1; Type.ser_type state t

    and ser_lambda state { args; body } =
      ser_x_list ser_binding state args;
      ser_exp state body


    (** Structure serializers **)

    and ser_entry state { entry_code; entry_fee_code; entry_typ } =
      ser_exp state entry_code; ser_x_option ser_exp state entry_fee_code;
      Type.ser_type state entry_typ

    and ser_view state { view_code; view_typ } =
      ser_exp state view_code; ser_x_x_pair Type.ser_type state view_typ

    and ser_value state { value_code; value_typ; value_visibility } =
      ser_exp state value_code; Type.ser_type state value_typ;
      ser_visibility state value_visibility;

    and ser_content state =
      let write state i =
        LBB.stats_add_i_custom state "Content" 3;
        LBB.write state (Int64.of_int i) 3 in
      function
      | RDefType (k, td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | RDefException tl -> write state 1; ser_x_list Type.ser_type state tl
      | REntry e -> write state 2; ser_entry state e
      | RView v -> write state 3; ser_view state v
      | RValue v -> write state 4; ser_value state v
      | RStructure s -> write state 5; ser_structure state s
      | RSignature s -> write state 6; Type.ser_signature state s

    and ser_structure state { kind; structure_content; } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state structure_content;

    and ser_struct_ref state = function
      | RAnonymous s -> LBB.write_bool state false; ser_structure state s
      | RNamed sn -> LBB.write_bool state true; ser_ident state sn

    and ser_top_contract state { version; code } =
      ser_x_x_pair (LBB.write_uint) state version;
      ser_structure state code

    let serialize_const = ser ser_const
    let serialize_exp = ser ser_exp
    let serialize_structure = ser ser_structure
    let serialize_top_contract = ser ser_top_contract
  end



  module Value = struct
    open Love_value
    open Value

    let rec ser_value state value =
      let open Signature in
      let write state i = LBB.write state (Int64.of_int i) 5 in
      match Value.unrec_closure value with

      (* Base *)
      | VUnit -> write state 0
      | VBool b -> write state 1; LBB.write_bool state b
      | VString s -> write state 2; LBB.write_string state s
      | VBytes b ->  write state 3; LBB.write_bytes state b
      | VInt z ->  write state 4; LBB.write_z state z
      | VNat z ->  write state 5; LBB.write_z state z

      (* Composite *)
      | VNone -> write state 6
      | VSome v -> write state 7; ser_value state v
      | VTuple vl -> write state 8; ser_x_list ser_value state vl
      | VConstr (c, tl) ->
        write state 9; LBB.write_string state c; ser_x_list ser_value state tl
      | VRecord fl ->
        write state 10; ser_x_y_list LBB.write_str_repr ser_value state fl

      (* Collections *)
      | VList l ->  write state 11; ser_x_list ser_value state l
      | VSet s ->
        write state 12;
        ser_x_list ser_value state (ValueSet.elements s)
      | VMap m ->
        write state 13;
        ser_x_list (ser_x_x_pair ser_value) state (ValueMap.bindings m)
      | VBigMap { id; diff; key_type; value_type } ->
        write state 14;
        ser_x_option LBB.write_z state id;
        Type.ser_type state key_type;
        Type.ser_type state value_type;
        ser_x_list (ser_x_y_pair ser_value
                      (ser_x_option ser_value)) state (ValueMap.bindings diff)

      (* Domain-specific *)
      | VDun d ->
        write state 15; LBB.write_z state (Z.of_int64 (Tez_repr.to_mutez d))
      | VKey k ->
        write state 16;
        LBB.write_string state (Public_key.to_b58check k)
      | VKeyHash kh ->
        write state 17;
        LBB.write_string state (Public_key_hash.to_b58check kh)
      | VSignature s ->
        write state 18;
        LBB.write_string state (Signature.to_b58check s)
      | VTimestamp ts ->
        write state 19; LBB.write_z state (Script_timestamp_repr.to_zint ts)
      | VAddress a ->
        write state 20;
        LBB.write_string state (Contract_repr.to_b58check a)
      | VOperation op ->
        write state 21; ser_op state op

      (* Structures and entry points *)
      | VPackedStructure (p, c) ->
        write state 22; ser_ident state p; ser_live_contract state c
      | VContractInstance (s, c) ->
        write state 23;
        Type.ser_signature state s;
        LBB.write_string state (Contract_repr.to_b58check c)
      | VEntryPoint (c, str) ->
        write state 24;
        LBB.write_string state (Contract_repr.to_b58check c);
        LBB.write_string state str
      | VView (c, str) ->
        write state 25;
        LBB.write_string state (Contract_repr.to_b58check c);
        LBB.write_string state str

      (* Closures *)
      | VClosure { call_env; lambda } ->
        write state 26;
        ser_str_repr_id_x_list
          (ser_local_or_global ser_value
             (ser_pointer_or_inlined ser_value)) state call_env.values;
        ser_str_repr_id_x_list
          (ser_pointer_or_inlined ser_live_structure) state call_env.structs;
        ser_str_repr_id_x_list
          (ser_pointer_or_inlined Type.ser_signature) state call_env.sigs;
        ser_x_list (ser_x_x_pair ser_ident) state call_env.exns;
        ser_x_list (ser_x_x_pair ser_ident) state call_env.types;
        ser_str_repr_x_list Type.ser_type state call_env.tvars;
        Runtime.ser_lambda state lambda

      | VPrimitive (p, l) ->
        write state 27;
        ser_primitive state p;
        ser_x_list ser_value state l

    and ser_local_or_global :
      type t1 t2. (LBB.writer -> t1 -> unit) -> (LBB.writer -> t2 -> unit) ->
        LBB.writer -> (t1, t2) Value.local_or_global -> unit =
      let write state i = LBB.write state (Int64.of_int i) 1 in
      fun ser_local ser_global state -> function
        | Local l -> write state 0; ser_local state l
        | Global g -> write state 1; ser_global state g

    and ser_pointer_or_inlined :
      type t. (LBB.writer -> t -> unit) -> LBB.writer ->
                t Value.ptr_or_inlined -> unit =
      let write state i = LBB.write state (Int64.of_int i) 1 in
      fun ser_val state -> function
        | Inlined (p, c) -> write state 0; ser_ident state p; ser_val state c
        | Pointer p -> write state 1; ser_ident state p


    (** Operation serializers **)

    and ser_manager_operation state op =
      let write state i = LBB.write state (Int64.of_int i) 2 in
      match op with
      | Op.Origination { delegate; script; credit; preorigination } ->
        write state 0;
        ser_x_option ser_key_hash state delegate;
        ser_x_y_pair ser_value ser_live_contract state script;
        ser_dun state credit;
        ser_x_option ser_address state preorigination
      | Transaction { amount; parameters; entrypoint; destination } ->
        write state 1;
        ser_dun state amount;
        ser_x_option ser_value state parameters;
        LBB.write_str_repr state entrypoint;
        ser_address state destination;
      | Delegation delegate ->
        write state 2;
        ser_x_option ser_key_hash state delegate;
      | Dune_manage_account
          { target; maxrolls; admin; white_list; delegation } ->
        write state 3;
        ser_x_option (fun state a -> LBB.write_string state
                         (Signature.Public_key_hash.to_b58check a)
                     ) state target;
        ser_x_option (ser_x_option LBB.write_uint) state maxrolls;
        ser_x_option (ser_x_option (fun state a ->
            LBB.write_string state (Contract_repr.to_b58check a)
          )) state admin;
        ser_x_option (ser_x_list (fun state a ->
            LBB.write_string state (Contract_repr.to_b58check a)
          )) state white_list;
        ser_x_option LBB.write_bool state delegation

    and ser_internal_op state Op.{ source; operation; nonce } =
        ser_address state source;
        ser_manager_operation state operation;
        LBB.write_uint state nonce

    and ser_big_map_diff_item state item =
      let write state i = LBB.write state (Int64.of_int i) 2 in
      match item with
      | Op.Update { big_map; diff_key; diff_key_hash; diff_value } ->
        write state 0;
        LBB.write_z state big_map;
        ser_value state diff_key;
        LBB.write_string state (Script_expr_hash.to_b58check diff_key_hash);
        ser_x_option ser_value state diff_value
      | Clear id ->
        write state 1;
        LBB.write_z state id
      | Copy (ids, idd) ->
        write state 2;
        LBB.write_z state ids;
        LBB.write_z state idd
      | Alloc { big_map; key_type; value_type }  ->
        write state 3;
        LBB.write_z state big_map;
        Type.ser_type state key_type;
        Type.ser_type state value_type

    and ser_op state (operation, big_map_diff_opt) =
      ser_internal_op state operation;
      ser_x_option (ser_x_list ser_big_map_diff_item) state big_map_diff_opt


    (** Structure serializers **)

    and ser_entry state
        ({ ventry_code; ventry_fee_code; ventry_typ } : LiveStructure.entry) =
      ser_value state ventry_code;
      ser_x_option ser_value state ventry_fee_code;
      Type.ser_type state ventry_typ

    and ser_view state ({ vview_code; vview_typ } : LiveStructure.view) =
      ser_value state vview_code;
      ser_x_x_pair Type.ser_type state vview_typ

    and ser_cvalue state
        LiveStructure.{ vvalue_code; vvalue_typ; vvalue_visibility } =
      ser_value state vvalue_code;
      Type.ser_type state vvalue_typ;
      ser_visibility state vvalue_visibility;

    and ser_content state =
      let open LiveStructure in
      let write state i = LBB.write state (Int64.of_int i) 3 in
      function
      | VType (k,td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | VException tl -> write state 1; ser_x_list Type.ser_type state tl
      | VEntry e -> write state 2; ser_entry state e
      | VView v -> write state 3; ser_view state v
      | VValue v -> write state 4; ser_cvalue state v
      | VStructure s -> write state 5; ser_live_structure state s
      | VSignature s -> write state 6; Type.ser_signature state s

    and ser_live_structure state LiveStructure.{ kind; content } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state content;

    and ser_live_contract state LiveContract.{ version; root_struct } =
      ser_x_x_pair LBB.write_uint state version;
      ser_live_structure state root_struct

    and ser_fee_code state FeeCode.{ version; root_struct; fee_codes } =
      ser_x_x_pair LBB.write_uint state version;
      ser_live_structure state root_struct;
      ser_str_repr_x_list (ser_x_y_pair ser_value Type.ser_type) state fee_codes

    let serialize = ser ser_value
    let serialize_live_structure = ser ser_live_structure
    let serialize_live_contract = ser ser_live_contract
    let serialize_fee_code = ser ser_fee_code
  end

end

module Deserializer = struct

  module LBB = Love_binary_buffer

  (** Combining deserializers **)

  let des_x_option des_x state =
    if LBB.read_bool state
    then Some (des_x state)
    else None

  let des_list_size state =
    let i = Int64.to_int (LBB.read state 1) in
    if Compare.Int.(i = 0) then begin
      let i = Int64.to_int (LBB.read state 1) in
      if Compare.Int.(i = 0) then 0 (* 00 / size = 0 *)
      else (LBB.read_uint state) + 9 (* 01 / size >= 9 *)
    end else (Int64.to_int (LBB.read state 4)) + 1 (* 1 / size = 1 - 8 *)

  let des_x_list des_x state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xl = ref [] in
    for _i = 1 to length do
      xl := (des_x state) :: !xl
    done;
    List.rev !xl

  let des_x_y_list des_x des_y state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xyl = ref [] in
    for _i = 1 to length do
      let x = des_x state in
      let y = des_y state in
      xyl := (x, y) :: !xyl
    done;
    List.rev !xyl

  let des_x_x_list des_x state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xxl = ref [] in
    for _i = 1 to length do
      let x1 = des_x state in
      let x2 = des_x state in
      xxl := (x1, x2) :: !xxl
    done;
    List.rev !xxl

  let des_x_x_pair des_x state =
    let x1 = des_x state in
    let x2 = des_x state in
    (x1, x2)

  let des_x_y_pair des_x des_y state =
    let x = des_x state in
    let y = des_y state in
    (x, y)


  (** Utility deserializers **)

  let des_ident state =
    Ident.put_list (des_x_list LBB.read_str_repr state)

  let des_ident_x_list des_x state =
    des_x_y_list des_ident des_x state

  let des_str_repr_x_list des_x state =
    des_x_y_list LBB.read_str_repr des_x state

  let des_str_repr_id_x_list des_x state =
    des_x_y_list des_ident des_x state

  let des_int_x_list des_x state =
    des_x_y_list (fun i -> LBB.read_uint i) des_x state


  (** Main deserializer *)

  let des deser b =
    let state = LBB.initialize_reader b in
    deser state


  (** Qualifier deserializers **)

  let des_visibility state =
    if LBB.read_bool state then Public else Private


  (** Type deserializers **)

  module Type = struct

    open Love_type

    let des_type_name = des_ident

    let des_trait state = {tcomparable = LBB.read_bool state}

    let des_rec state = if LBB.read_bool state then NonRec else Rec

    let des_type_var state : type_var =
      let tv_name = LBB.read_str_repr state in
      let tv_traits = des_trait state in
      { tv_name; tv_traits }

    let rec des_type state =
      let des_type_list = des_x_list des_type in
      match LBB.read state 5 |> Int64.to_int with
      | 0 -> TUnit
      | 1 -> TBool
      | 2 -> TString
      | 3 -> TBytes
      | 4 -> TInt
      | 5 -> TNat
      | 6 -> TOption (des_type state)
      | 7 -> TTuple (des_type_list state)
      | 8 ->
        let tname = des_type_name state in
        let tl = des_type_list state in
        TUser (tname, tl)
      | 9 -> TList (des_type state)
      | 10 -> TSet (des_type state)
      | 11 ->
        let t1 = des_type state in
        let t2 = des_type state in
        TMap (t1, t2)
      | 12 ->
        let t1 = des_type state in
        let t2 = des_type state in
        TBigMap (t1, t2)
      | 13 -> TDun
      | 14 -> TKey
      | 15 -> TKeyHash
      | 16 -> TSignature
      | 17 -> TTimestamp
      | 18 -> TAddress
      | 19 -> TOperation
      | 20 -> TContractInstance (des_structure_type state)
      | 21 -> TPackedStructure (des_structure_type state)
      | 22 -> TEntryPoint (des_type state)
      | 23 ->
        let t1 = des_type state in
        let t2 = des_type state in
        TView (t1, t2)
      | 24 ->
        let t1 = des_type state in
        let t2 = des_type state in
        TArrow (t1, t2)
      | 25 -> TVar (des_type_var state)
      | 26 ->
        let v = des_type_var state in
        let t = des_type state in
        TForall (v, t)
      | i -> raise (DeserializeError (i, "Unknown type"))

    and des_structure_type state =
      if LBB.read_bool state
      then Named (des_ident state)
      else Anonymous (des_signature state)

    and des_contract_type state =
      if LBB.read_bool state
      then ContractInstance (des_ident state)
      else StructType (des_structure_type state)

    and des_sigtype state =
      match LBB.read state 2 |> Int64.to_int with
      | 0 -> SPublic (des_typedef state)
      | 1 -> SPrivate (des_typedef state)
      | 2 -> SAbstract (des_x_list des_type_var state)
      | i -> raise (DeserializeError (i, "Unknown sigtype"))

    and des_typedef state =
      match LBB.read state 2 |> Int64.to_int with
      | 0 ->
        let aparams = des_x_list des_type_var state in
        let atype = des_type state in
        Alias { aparams; atype }
      | 1 ->
        let sparams = des_x_list des_type_var state in
        let scons = des_str_repr_x_list (des_x_list des_type) state in
        let srec = des_rec state in
        SumType { sparams; scons; srec}
      | 2 ->
        let rparams = des_x_list des_type_var state in
        let rfields = des_str_repr_x_list des_type state in
        let rrec = des_rec state in
        RecordType { rparams; rfields; rrec }
      | i -> raise (DeserializeError (i, "Unknown type definition"))

    and des_sig_content state =
      match LBB.read state 3 |> Int64.to_int with
      | 0 -> SType (des_sigtype state)
      | 1 -> SException (des_x_list des_type state)
      | 2 -> SEntry (des_type state)
      | 3 ->
         let t1 = des_type state in
         let t2 = des_type state in
         SView (t1, t2)
      | 4 -> SValue (des_type state)
      | 5 -> SStructure (des_structure_type state)
      | 6 -> SSignature (des_signature state)
      | i -> raise (DeserializeError (i, "Unknown signature content"))

    and des_kind state =
      if LBB.read_bool state
      then Contract (des_x_y_list LBB.read_str_repr LBB.read_string state)
      else Module

    and des_signature state =
      let sig_kind = des_kind state in
      let sig_content = des_str_repr_x_list des_sig_content state in
      { sig_kind; sig_content}

    let deserialize = des des_type
  end

  (** Primitive deserializer **)

  let des_primitive state =
    let open Love_primitive in
    let i = LBB.read_uint8 state in
    try
      match Love_primitive.prim_of_id i with
      | PBMEmpty _ ->
          let tk = Type.des_type state in
          let tv = Type.des_type state in
          PBMEmpty (tk, tv)
      | PBUnpack _ -> PBUnpack (Type.des_type state)
      | PCSelf _ -> PCSelf (Type.des_contract_type state)
      | PCAt _ -> PCAt (Type.des_contract_type state)
      | p -> p
    with _ -> raise (DeserializeError (i, "Unknown primitive"))

  (** Const deserializers **)

  let des_key state =
    match Signature.Public_key.of_b58check_opt
            (LBB.read_string state) with
    | None -> raise (DeserializeError ((-1), "Bad public key"))
    | Some k -> k

  let des_key_hash state =
    match Signature.Public_key_hash.of_b58check_opt
            (LBB.read_string state) with
    | None -> raise (DeserializeError ((-1), "Bad public key hash"))
    | Some kh -> kh

  let des_signature state =
    match Signature.of_b58check_opt (LBB.read_string state) with
    | None -> raise (DeserializeError ((-1), "Bad signature"))
    | Some sign -> sign

  let des_timestamp state =
    match Script_timestamp_repr.of_string (LBB.read_string state) with
    | None -> raise (DeserializeError ((-1), "Bad timestamp"))
    | Some t -> t

  let des_address state =
    let open Error_monad in
    let res = ref None in
    let _ = Contract_repr.of_b58check (LBB.read_string state)
      >|? fun a -> res := Some a in
    match !res with
    | None -> raise (DeserializeError ((-1), "Bad address"))
    | Some a -> a

  let des_dun state =
    Log.debug "[des_dun] Deserializing dun value@.";
    let di64 = LBB.read_z state |> Z.to_int64 in
    Log.debug "[des_dun] Deserializing dun value %Ld@." di64;
    match Tez_repr.of_mutez di64 with
    | None -> raise (DeserializeError ((-1), "Bad dun"))
    | Some d -> d

  let des_type_kind state =
    match LBB.read state 2 |> Int64.to_int with
    | 0 -> Love_ast.TPublic
    | 1 -> TPrivate
    | 2 -> TInternal
    | 3 -> TAbstract
    | i ->  raise (DeserializeError (i, "Bad type kind"))

  let des_loc state =
    let pos_lnum = LBB.read_uint32 state in
    let pos_bol =  LBB.read_uint32 state in
    let pos_cnum = LBB.read_uint32 state in
    {pos_lnum; pos_bol; pos_cnum}

  module DesAst
      (AnnotDeserializer: sig val des_annot : LBB.reader ->
         location option end) =
  struct

    let des_annoted des state =
      let content = des state in
      let annot = AnnotDeserializer.des_annot state in
      Utils.{content; annot}

    let des_exn_name state =
      if LBB.read_bool state
      then let t = Type.des_type state in Love_ast.Fail t
      else let n = des_ident state in Exception n

    let rec des_raw_const state =
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> CUnit
      | 1 -> CBool (LBB.read_bool state)
      | 2 -> CString (LBB.read_string state)
      | 3 -> CBytes (LBB.read_bytes state)
      | 4 -> CInt (LBB.read_z state)
      | 5 as i ->
        let n = LBB.read_z state in
        if Compare.Z.(n < Z.zero)
        then raise (DeserializeError (i, "Negative CNat"))
        else CNat n
      | 6 -> CDun (des_dun state)
      | 7 -> CKey (des_key state)
      | 8 -> CKeyHash (des_key_hash state)
      | 9 -> CSignature (des_signature state)
      | 10 -> CTimestamp (des_timestamp state)
      | 11 -> CAddress (des_address state)
      | 12 -> CPrimitive (des_primitive state)
      | i -> raise (DeserializeError (i, "Unknown constant"))

    and des_const s = des_annoted des_raw_const s

    (** Pattern deserializer **)

    and des_raw_pattern state =
      let des_pattern_list = des_x_list des_pattern in
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> PAny
      | 1 -> PVar (LBB.read_str_repr state)
      | 2 ->
        let p = des_pattern state in
        let v = LBB.read_str_repr state in
        PAlias (p, v)
      | 3 -> PConst (des_const state)
      | 4 -> PNone
      | 5 -> PSome (des_pattern state)
      | 6 -> PList (des_pattern_list state)
      | 7 -> PTuple (des_pattern_list state)
      | 8 ->
        let cstr = LBB.read_str_repr state in
        let pl = des_pattern_list state in
        PConstr (cstr, pl)
      | 9 ->
        let n = LBB.read_str_repr state in
        let st = Type.des_structure_type state in
        PContract (n, st)
      | i -> raise (DeserializeError (i, "Unknown pattern"))

    and des_pattern s = des_annoted des_raw_pattern s

    and des_exn_pattern state =
      let exn = des_exn_name state in
      let pl = des_x_list des_pattern state in
      exn, pl


    (** Exp deserializer **)

    and des_raw_exp state =
      let des_exp_list = des_x_list des_exp in
      match LBB.read state 5 |> Int64.to_int with
      | 0 -> Const (des_const state)
      | 1 -> Var (des_ident state)
      | 2 ->
        let bnd_pattern = des_pattern state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        Let { bnd_pattern; bnd_val; body }
      | 3 ->
        let bnd_var = LBB.read_str_repr state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        let fun_typ = Type.des_type state in
        LetRec { bnd_var; bnd_val; body; fun_typ }
      | 4 -> Lambda (des_lambda state)
      | 5 ->
        let fct = des_exp state in
        let args = des_exp_list state in
        Apply { fct; args }
      | 6 ->
        let targ = Type.des_type_var state in
        let exp = des_exp state in
        TLambda { targ; exp }
      | 7 ->
        let exp = des_exp state in
        let typ = Type.des_type state in
        TApply { exp; typ }
      | 8 -> Seq (des_exp_list state)
      | 9 ->
        let cond = des_exp state in
        let ifthen = des_exp state in
        let ifelse = des_exp state in
        If { cond; ifthen; ifelse }
      | 10 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_pattern des_exp state in
        Match { arg; cases }
      | 11 ->
        let constr = des_ident state in
        let ctyps = des_x_list Type.des_type state in
        let args = des_exp_list state in
        Constructor { constr; ctyps; args }
      | 12 -> ESome (des_exp state)
      | 13 ->
        let el = des_exp_list state in
        let e = des_exp state in
        List (el, e)
      | 14 -> Tuple (des_exp_list state)
      | 15 ->
        let tuple = des_exp state in
        let indexes = des_x_list LBB.read_uint state in
        Project { tuple; indexes }
      | 16 ->
        let tuple = des_exp state in
        let updates = des_int_x_list des_exp state in
        Update { tuple; updates }
      | 17 ->
        let path = des_x_option des_ident state in
        let fel = des_str_repr_x_list des_exp state in
        Record { path; contents = fel }
      | 18 ->
        let record = des_exp state in
        let field = LBB.read_str_repr state in
        GetField { record; field }
      | 19 ->
        let record = des_exp state in
        let updates = des_str_repr_x_list des_exp state in
        SetFields { record; updates }
      | 20 -> PackStruct (des_struct_ref state)
      | 21 ->
        let exn = des_exn_name state in
        let args = des_exp_list state in
        let loc = des_x_option des_loc state in
        Raise { exn; args; loc }
      | 22 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_exn_pattern des_exp state in
        TryWith { arg; cases }
      | 23 -> ENone
      | 24 -> Nil
      | i -> raise (DeserializeError (i, "Unknown expression"))

    and des_exp state = des_annoted des_raw_exp state

    and des_lambda state =
      let arg_pattern = des_pattern state in
      let body = des_exp state in
      let arg_typ = Type.des_type state in
      { arg_pattern; body; arg_typ }

    (** Structure deserializers **)

    and des_entry state =
      let entry_code = des_exp state in
      let entry_fee_code = des_x_option des_exp state in
      let entry_typ = Type.des_type state in
      { entry_code; entry_fee_code; entry_typ }

    and des_view state =
      let view_code = des_exp state in
      let view_typ = des_x_x_pair Type.des_type state in
      { view_code; view_typ }

    and des_value state =
      let value_code = des_exp state in
      let value_typ = Type.des_type state in
      let value_visibility = des_visibility state in
      { value_code; value_typ; value_visibility }

    and des_content state =
      match LBB.read state 3 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in DefType (k, Type.des_typedef state)
      | 1 -> DefException (des_x_list Type.des_type state)
      | 2 -> Entry (des_entry state)
      | 3 -> View (des_view state)
      | 4 -> Value (des_value state)
      | 5 -> Structure (des_structure state)
      | 6 -> Signature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_structure state =
      let kind = Type.des_kind state in
      let structure_content = des_str_repr_x_list des_content state in
      { kind; structure_content }

    and des_struct_ref state =
      if LBB.read_bool state
      then Named (des_ident state)
      else Anonymous (des_structure state)

    and des_top_contract state =
      let version = des_x_x_pair (LBB.read_uint) state in
      let code = des_structure state in
      { version; code }

    (** Main deserializers **)

    let deserialize_const     = des des_const
    let deserialize_exp       = des des_exp
    let deserialize_structure = des des_structure
    let deserialize_top_contract = des des_top_contract
  end

  module Located = DesAst (struct let des_annot = des_x_option des_loc end)
  (*  module Raw = DesAst (struct let des_annot _ = None end) *)



  module Runtime = struct

    module DesType = Type

    open Love_runtime_ast

    module Type = DesType

    let des_exn_name state =
      if LBB.read_bool state
      then let t = Type.des_type state in RFail t
      else let n = des_ident state in RException n

    let rec des_const state =
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> RCUnit
      | 1 -> RCBool (LBB.read_bool state)
      | 2 -> RCString (LBB.read_string state)
      | 3 -> RCBytes (LBB.read_bytes state)
      | 4 -> RCInt (LBB.read_z state)
      | 5 as i ->
        let n = LBB.read_z state in
        if Compare.Z.(n < Z.zero)
        then raise (DeserializeError (i, "Negative CNat"))
        else RCNat n
      | 6 -> RCDun (des_dun state)
      | 7 -> RCKey (des_key state)
      | 8 -> RCKeyHash (des_key_hash state)
      | 9 -> RCSignature (des_signature state)
      | 10 -> RCTimestamp (des_timestamp state)
      | 11 -> RCAddress (des_address state)
      | 12 -> RCPrimitive (des_primitive state)
      | i -> raise (DeserializeError (i, "Unknown constant"))

    (** Pattern deserializer **)

    and des_pattern state =
      let des_pattern_list = des_x_list des_pattern in
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> RPAny
      | 1 -> RPVar (LBB.read_str_repr state)
      | 2 ->
        let p = des_pattern state in
        let v = LBB.read_str_repr state in
        RPAlias (p, v)
      | 3 -> RPConst (des_const state)
      | 4 -> RPNone
      | 5 -> RPSome (des_pattern state)
      | 6 -> RPList (des_pattern_list state)
      | 7 -> RPTuple (des_pattern_list state)
      | 8 ->
        let cstr = LBB.read_str_repr state in
        let pl = des_pattern_list state in
        RPConstr (cstr, pl)
      | 9 ->
        let n = LBB.read_str_repr state in
        let st = Type.des_structure_type state in
        RPContract (n, st)
      | i -> raise (DeserializeError (i, "Unknown pattern"))

    and des_exn_pattern state =
      let exn = des_exn_name state in
      let pl = des_x_list des_pattern state in
      exn, pl


    (** Exp deserializer **)

    and des_exp state =
      let des_exp_list = des_x_list des_exp in
      match LBB.read state 5 |> Int64.to_int with
      | 0 -> RConst (des_const state)
      | 1 -> RVar (des_ident state)
      | 2 ->
        let bnd_pattern = des_pattern state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        RLet { bnd_pattern; bnd_val; body }
      | 3 ->
        let bnd_var = LBB.read_str_repr state in
        let bnd_val = des_exp state in
        let val_typ = Type.des_type state in
        let body = des_exp state in
        RLetRec { bnd_var; bnd_val; val_typ; body }
      | 4 -> RLambda (des_lambda state)
      | 5 ->
        let exp = des_exp state in
        let args = des_x_list des_param state in
        RApply { exp; args }
      | 6 -> RSeq (des_exp_list state)
      | 7 ->
        let cond = des_exp state in
        let ifthen = des_exp state in
        let ifelse = des_exp state in
        RIf { cond; ifthen; ifelse }
      | 8 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_pattern des_exp state in
        RMatch { arg; cases }
      | 9 ->
        let type_name = des_ident state in
        let constr = LBB.read_str_repr state in
        let ctyps = des_x_list Type.des_type state in
        let args = des_exp_list state in
        RConstructor { type_name; constr; ctyps; args }
      | 10 -> RSome (des_exp state)
      | 11 ->
        let el = des_exp_list state in
        let e = des_exp state in
        RList (el, e)
      | 12 -> RTuple (des_exp_list state)
      | 13 ->
        let tuple = des_exp state in
        let indexes = des_x_list LBB.read_uint state in
        RProject { tuple; indexes }
      | 14 ->
        let tuple = des_exp state in
        let updates = des_int_x_list des_exp state in
        RUpdate { tuple; updates }
      | 15 ->
        let type_name = des_ident state in
        let fel = des_str_repr_x_list des_exp state in
        RRecord { type_name; contents = fel }
      | 16 ->
        let record = des_exp state in
        let field = LBB.read_str_repr state in
        RGetField { record; field }
      | 17 ->
        let record = des_exp state in
        let updates = des_str_repr_x_list des_exp state in
        RSetFields { record; updates }
      | 18 -> RPackStruct (des_struct_ref state)
      | 19 ->
        let exn = des_exn_name state in
        let args = des_exp_list state in
        let loc = des_x_option des_loc state in
        RRaise { exn; args; loc }
      | 20 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_exn_pattern des_exp state in
        RTryWith { arg; cases }
      | 21 -> RNone
      | 22 -> RNil
      | i -> raise (DeserializeError (i, "Unknown expression"))

    and des_binding state =
      match LBB.read state 1 |> Int64.to_int with
      | 0 ->
        let p, t = des_x_y_pair des_pattern Type.des_type state in
        RPattern (p, t)
      | 1 -> RTypeVar (Type.des_type_var state)
      | i -> raise (DeserializeError (i, "Unknown binding"))

    and des_param state =
      match LBB.read state 1 |> Int64.to_int with
      | 0 -> RExp (des_exp state)
      | 1 -> RType (Type.des_type state)
      | i -> raise (DeserializeError (i, "Unknown param"))

    and des_lambda state =
      let args = des_x_list des_binding state in
      let body = des_exp state in
      { args; body }


    (** Structure deserializers **)

    and des_entry state =
      let entry_code = des_exp state in
      let entry_fee_code = des_x_option des_exp state in
      let entry_typ = Type.des_type state in
      { entry_code; entry_fee_code; entry_typ }

    and des_view state =
      let view_code = des_exp state in
      let view_typ = des_x_x_pair Type.des_type state in
      { view_code; view_typ }

    and des_value state =
      let value_code = des_exp state in
      let value_typ = Type.des_type state in
      let value_visibility = des_visibility state in
      { value_code; value_typ; value_visibility }

    and des_content state =
      match LBB.read state 3 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in RDefType (k, Type.des_typedef state)
      | 1 -> RDefException (des_x_list Type.des_type state)
      | 2 -> REntry (des_entry state)
      | 3 -> RView (des_view state)
      | 4 -> RValue (des_value state)
      | 5 -> RStructure (des_structure state)
      | 6 -> RSignature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_structure state =
      let kind = Type.des_kind state in
      let structure_content = des_str_repr_x_list des_content state in
      { kind; structure_content }

    and des_struct_ref state =
      if LBB.read_bool state
      then RNamed (des_ident state)
      else RAnonymous (des_structure state)

    and des_top_contract state =
      let version = des_x_x_pair (LBB.read_uint) state in
      let code = des_structure state in
      { version; code }

    (** Main deserializers **)

    let deserialize_const     = des des_const
    let deserialize_exp       = des des_exp
    let deserialize_structure = des des_structure
    let deserialize_top_contract = des des_top_contract
  end




  module Value = struct
    open Love_value
    open Value

    let rec des_value state =
      let open Signature in
      let i = LBB.read state 5 |> Int64.to_int in
      Value.rec_closure @@
      match i with
      | 0 -> VUnit
      | 1 -> VBool (LBB.read_bool state)
      | 2 -> VString (LBB.read_string state)
      | 3 -> VBytes (LBB.read_bytes state)
      | 4 -> VInt (LBB.read_z state)
      | 5 -> VNat (LBB.read_z state)
      | 6 -> VNone
      | 7 -> VSome (des_value state)
      | 8 -> VTuple (des_x_list des_value state)
      | 9 ->
        let c = LBB.read_string state in
        let args = des_x_list des_value state in
        VConstr (c, args)
      | 10 -> VRecord (des_str_repr_x_list des_value state)
      | 11 -> VList (des_x_list des_value state)
      | 12 -> VSet (ValueSet.of_list (des_x_list des_value state))
      | 13 ->
        VMap (
          List.fold_left (fun acc (k,b) -> ValueMap.add k b acc)
            ValueMap.empty (des_x_list (des_x_x_pair des_value) state))
      | 14 ->
        let id = des_x_option LBB.read_z state in
        let key_type = Type.des_type state in
        let value_type = Type.des_type state in
        let diff = List.fold_left (fun acc (k, b) ->
            ValueMap.add k b acc
          ) ValueMap.empty (des_x_list (des_x_y_pair des_value
                                          (des_x_option des_value)) state) in
        VBigMap { id; diff; key_type; value_type }
      | 15 -> (
          match Tez_repr.of_mutez (LBB.read_z state |> Z.to_int64) with
            None -> raise (DeserializeError (i, "Badly formed VDun"))
          | Some d -> VDun d
        )
      | 16 -> (
          match Public_key.of_b58check_opt (LBB.read_string state) with
            None -> raise (DeserializeError (i, "Badly formed VKey"))
          | Some k -> VKey k
        )
      | 17 -> (
          match Public_key_hash.of_b58check_opt (LBB.read_string state) with
            None -> raise (DeserializeError (i, "Badly formed VKeyHash"))
          | Some k -> VKeyHash k
        )
      | 18 -> (
          match Signature.of_b58check_opt (LBB.read_string state) with
            None -> raise (DeserializeError (i, "Badly formed VSignature"))
          | Some s -> VSignature s
        )
      | 19 -> VTimestamp (Script_timestamp_repr.of_zint (LBB.read_z state))
      | 20 -> (
          match Contract_repr.of_b58check (LBB.read_string state) with
            Error _ -> raise (DeserializeError (i, "Badly formed VAddress"))
          | Ok a -> VAddress a
        )
      | 21 -> VOperation (des_op state)
      | 22 ->
        let p = des_ident state in
        let c = des_live_contract state in
        VPackedStructure (p, c)
      | 23 ->
        let s = Type.des_signature state in
        let c = match Contract_repr.of_b58check (LBB.read_string state) with
          | Error _ ->
              raise (DeserializeError (i, "Badly formed VContractInstance"))
          | Ok c -> c in
        VContractInstance (s, c)
      | 24 ->
        let c = match Contract_repr.of_b58check (LBB.read_string state) with
          | Error _ -> raise (DeserializeError (i, "Badly formed VEntryPoint"))
          | Ok c -> c in
        let str = LBB.read_string state in
        VEntryPoint (c, str)
      | 25 ->
        let c = match Contract_repr.of_b58check (LBB.read_string state) with
          | Error _ -> raise (DeserializeError (i, "Badly formed VView"))
          | Ok c -> c in
        let str = LBB.read_string state in
        VView (c, str)
      | 26 ->
        let values =
          des_str_repr_id_x_list
            (des_local_or_global des_value
               (des_pointer_or_inlined des_value)) state in
        let structs =
          des_str_repr_id_x_list
            (des_pointer_or_inlined des_live_structure) state in
        let sigs =
          des_str_repr_id_x_list
            (des_pointer_or_inlined Type.des_signature) state in
        let exns = des_x_list (des_x_x_pair des_ident) state in
        let types = des_x_list (des_x_x_pair des_ident) state in
        let tvars = des_str_repr_x_list Type.des_type state in
        let lambda = Runtime.des_lambda state in
        VClosure { call_env = { values; structs; sigs; exns; types; tvars };
                   lambda }
      | 27 ->
        let p = des_primitive state in
        let l = des_x_list des_value state in
        VPrimitive (p, l)
      | _ -> raise (DeserializeError (i, "Unknown value"))

    and des_local_or_global :
      type t1 t2. (LBB.reader -> t1) -> (LBB.reader -> t2) ->
        LBB.reader -> (t1, t2) Value.local_or_global =
      fun des_local des_global state ->
        match LBB.read state 1 |> Int64.to_int with
        | 0 -> Local (des_local state)
        | 1 -> Global (des_global state)
        | i -> raise (DeserializeError (i, "Unknown local or global"))

    and des_pointer_or_inlined :
      type t. (LBB.reader -> t) -> LBB.reader -> t Value.ptr_or_inlined =
      fun des_content state ->
        match LBB.read state 1 |> Int64.to_int with
        | 0 ->
            let p = des_ident state in
            let c = des_content state in
            Inlined (p, c)
        | 1 -> Pointer (des_ident state)
        | i -> raise (DeserializeError (i, "Unknown pointer or inlined"))


    (** Operation deserializers **)

    and des_manager_operation state =
      let open Op in
      match LBB.read state 2 |> Int64.to_int with
      | 0 ->
        let delegate = des_x_option des_key_hash state in
        let script = des_x_y_pair des_value des_live_contract state in
        let credit = des_dun state in
        let preorigination = des_x_option des_address state in
        Origination { delegate; script; credit; preorigination }
      | 1 ->
        let amount = des_dun state in
        let parameters = des_x_option des_value state in
        let entrypoint = LBB.read_str_repr state in
        let destination = des_address state in
        Transaction { amount; parameters; entrypoint; destination }
      | 2 -> Delegation (des_x_option des_key_hash state)
      | 3 ->
        let target = des_x_option (fun state ->
          match Signature.Public_key_hash.of_b58check_opt
                  (LBB.read_string state) with
          | None -> raise (DeserializeError (3, "Badly formed manager op"))
          | Some a -> a
          ) state in
        let maxrolls = des_x_option (des_x_option LBB.read_uint) state in
        let admin = des_x_option (des_x_option (fun state ->
            match Contract_repr.of_b58check (LBB.read_string state) with
            | Error _ -> raise (DeserializeError (3, "Badly formed manager op"))
            | Ok a -> a
          )) state in
        let white_list = des_x_option (des_x_list (fun state ->
            match Contract_repr.of_b58check (LBB.read_string state) with
            | Error _ -> raise (DeserializeError (3, "Badly formed manager op"))
            | Ok a -> a
          )) state in
        let delegation = des_x_option LBB.read_bool state in
        Dune_manage_account { target; maxrolls; admin; white_list; delegation }
      | i -> raise (DeserializeError (i, "Unknown internal operation"))

    and des_internal_op state =
      let source = des_address state in
      let operation = des_manager_operation state in
      let nonce = LBB.read_uint state in
      Op.{ source; operation; nonce }

    and des_big_map_diff_item state =
      let i = LBB.read state 2 |> Int64.to_int in
      match i with
      | 0 ->
        let big_map = LBB.read_z state in
        let diff_key = des_value state in
        let diff_key_hash =
          LBB.read_string state |>
          Script_expr_hash.of_b58check_opt |> function
          | None -> raise (DeserializeError (i, "Bad script expr hash"))
          | Some h -> h
        in
        let diff_value = des_x_option des_value state in
        Op.Update { big_map; diff_key; diff_key_hash; diff_value }
      | 1 ->
        Clear (LBB.read_z state)
      | 2 ->
        let ids = LBB.read_z state in
        let idd = LBB.read_z state in
        Copy (ids, idd)
      | 3 ->
        let big_map = LBB.read_z state in
        let key_type = Type.des_type state in
        let value_type = Type.des_type state in
        Alloc { big_map; key_type; value_type }
      | i -> raise (DeserializeError (i, "Unknown big map diff item"))

    and des_op state =
      des_internal_op state,
      des_x_option (des_x_list des_big_map_diff_item) state


    (** Structure deserializers **)

    and des_entry state =
      let ventry_code = des_value state in
      let ventry_fee_code = des_x_option des_value state in
      let ventry_typ = Type.des_type state in
      ({ ventry_code; ventry_fee_code; ventry_typ } : LiveStructure.entry)

    and des_view state =
      let vview_code = des_value state in
      let vview_typ = des_x_x_pair Type.des_type state in
      ({ vview_code; vview_typ } : LiveStructure.view)

    and des_cvalue state =
      let vvalue_code = des_value state in
      let vvalue_typ = Type.des_type state in
      let vvalue_visibility = des_visibility state in
      LiveStructure.{ vvalue_code; vvalue_typ; vvalue_visibility }

    and des_content state =
      let open LiveStructure in
      match LBB.read state 3 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in VType (k, Type.des_typedef state)
      | 1 -> VException (des_x_list Type.des_type state)
      | 2 -> VEntry (des_entry state)
      | 3 -> VView (des_view state)
      | 4 -> VValue (des_cvalue state)
      | 5 -> VStructure (des_live_structure state)
      | 6 -> VSignature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_live_structure state =
      let kind = Type.des_kind state in
      let content = des_str_repr_x_list des_content state in
      LiveStructure.{ kind; content }

    and bindings : type a. (int * a) list -> a IntMap.t =
      fun (l : (int * a) list) ->
      List.fold_left
        (fun acc (i, b) -> IntMap.add i b acc) IntMap.empty l

    and des_live_contract state =
      let version = des_x_x_pair LBB.read_uint state in
      let root_struct = des_live_structure state in
      LiveContract.{ version; root_struct }

    and des_fee_code state =
      let version = des_x_x_pair LBB.read_uint state in
      let root_struct = des_live_structure state in
      let fee_codes =
        des_str_repr_x_list (des_x_y_pair des_value Type.des_type) state in
      FeeCode.{ version; root_struct; fee_codes }

    let deserialize = des des_value
    let deserialize_live_structure = des des_live_structure
    let deserialize_live_contract = des des_live_contract
    let deserialize_fee_code = des des_fee_code
  end
end


open Data_encoding


module Type = struct
  let encoding =
    conv Serializer.Type.serialize
      Deserializer.Type.deserialize
      Data_encoding.bytes
end

module Value = struct
  let encoding =
    conv Serializer.Value.serialize
      Deserializer.Value.deserialize
      Data_encoding.bytes

  let live_structure_encoding =
    conv Serializer.Value.serialize_live_structure
      Deserializer.Value.deserialize_live_structure
      Data_encoding.bytes

  let live_contract_encoding =
    conv Serializer.Value.serialize_live_contract
      Deserializer.Value.deserialize_live_contract
      Data_encoding.bytes

let fee_code_encoding =
  conv Serializer.Value.serialize_fee_code
    Deserializer.Value.deserialize_fee_code
    Data_encoding.bytes
end

module Ast = struct
  let exp_encoding =
    conv Serializer.Located.serialize_exp
      Deserializer.Located.deserialize_exp
      Data_encoding.bytes
  let const_encoding =
    conv Serializer.Located.serialize_const
      Deserializer.Located.deserialize_const
      Data_encoding.bytes
  let structure_encoding =
    conv Serializer.Located.serialize_structure
      Deserializer.Located.deserialize_structure
      Data_encoding.bytes
  let top_contract_encoding =
    conv Serializer.Located.serialize_top_contract
      Deserializer.Located.deserialize_top_contract
      Data_encoding.bytes
end

(*
module Raw = struct
  let exp_encoding =
    conv Serializer.Raw.serialize_exp
      Deserializer.Raw.deserialize_exp
      Data_encoding.bytes
  let const_encoding =
    conv Serializer.Raw.serialize_const
      Deserializer.Raw.deserialize_const
      Data_encoding.bytes
  let structure_encoding =
    conv Serializer.Raw.serialize_structure
      Deserializer.Raw.deserialize_structure
      Data_encoding.bytes
  let top_contract_encoding =
    conv Serializer.Raw.serialize_top_contract
      Deserializer.Raw.deserialize_top_contract
      Data_encoding.bytes
end
*)

module RuntimeAst = struct
  let exp_encoding =
    conv Serializer.Runtime.serialize_exp
      Deserializer.Runtime.deserialize_exp
      Data_encoding.bytes
  let const_encoding =
    conv Serializer.Runtime.serialize_const
      Deserializer.Runtime.deserialize_const
      Data_encoding.bytes
  let structure_encoding =
    conv Serializer.Runtime.serialize_structure
      Deserializer.Runtime.deserialize_structure
      Data_encoding.bytes
  let top_contract_encoding =
    conv Serializer.Runtime.serialize_top_contract
      Deserializer.Runtime.deserialize_top_contract
      Data_encoding.bytes
end
