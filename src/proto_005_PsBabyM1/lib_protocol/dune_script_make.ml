(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type kind = ..
type open_const = ..
type open_code = ..

type const = open_const
type code = open_code

module type S = sig
  include Dune_script_sig.S
  val tag : int
  val const : const -> open_const
  val code : code -> open_code
  val is_const : open_const -> const option
  val is_code : open_code -> code option
  val kind_str : string
  val kind_tuple : string * int * int
  val kind_encoding : kind Data_encoding.t
end

module MapKind = Map.Make (struct
    type t = string * int * int
    let compare (n1, v1, x1) (n2, v2, x2)  =
      match String.compare n1 n2 with
      | 0 ->
          begin match Compare.Int.compare v1 v2 with
            | 0 -> Compare.Int.compare x1 x2
            | c -> c
          end
      | c -> c
  end)

let languages = ref MapKind.empty

let lang_tag = ref 0

let kind_to_tuple = ref (fun _ -> raise Not_found)
let lang_of_const = ref (fun _ -> raise Not_found)
let lang_of_code = ref (fun _ -> raise Not_found)

module Make0 (D : Dune_script_sig.S) = struct

  type kind += Kind of D.kind
  type open_const += Const of D.const
  type open_code += Code of D.code

  include D

  let tag = !lang_tag

  let const c = Const c
  let code c = Code c
  let is_const = function
    | Const c -> Some c
    | _ -> None
  let is_code = function
    | Code c -> Some c
    | _ -> None

  let kind_str =
    let { name; version = v, x } = kind in
    Format.asprintf "%s.%d.%d" name v x

  let kind_tuple =
    let { name; version = v, x } = = kind in
    (name, v, x)

  let kind_encoding =
    let open Data_encoding in
    conv
      (fun { name ; version = v, x } ->
         let { name = name' ; version = w, y} = kind in
         assert (String.equal name name' && Compare.Int.(equal v w && equal x y));
         ())
      (fun () -> kind)
      (constant kind_str)

  let () =
    incr lang_tag

  let () =
    kind_to_tuple := fun k ->
      try !kind_to_tuple k
      with Not_found -> match k with
        | Kind { name ; version = v, x } -> name, version, v x
        | _ -> raise Not_found

end

module Make (D : Dune_script_sig.S) = struct

  module M = Make0 (D)

  let () =
    languages := MapKind.add M.kind_tuple (module M : S) !languages

  let () =
    lang_of_const := fun c ->
      try !lang_of_const c
      with Not_found -> match k with
        | Const _ -> (module M : S)
        | _ -> raise Not_found

  let () =
    lang_of_code := fun c ->
      try !lang_of_code c
      with Not_found -> match k with
        | Code _ -> (module M : S)
        | _ -> raise Not_found

end
