(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Gas

module Cost_of = struct

  (* Instructions *)

  let cycle = step_cost 1

  let const = step_cost 1
  let var = step_cost 1

  let letin (* pattern size *) = alloc_cost 1
  let letrec = alloc_cost 1
  let lambda (* pattern size *) = alloc_cost 2
  let apply = step_cost 2
  let tlambda (* pattern size *) = alloc_cost 1
  let tapply = step_cost 1

  let seq = free
  let ite = step_cost 2
  let matchwith = step_cost 2

  let construct n = alloc_cost (1+n)
  let opt = alloc_cost 1
  let list n = alloc_cost (1+n)

  let tuple n = alloc_cost n
  let project = step_cost 1
  let update n = alloc_cost n

  let record n = alloc_cost n
  let getfield = step_cost 1
  let setfields n = alloc_cost n

  let pack_contract = alloc_cost 5

  let raiseexn n = alloc_cost (1+n)
  let trywith = step_cost 2

  (* Comparisons *)

  let compare_bool _ _ = step_cost 1
  let compare_string s1 s2 =
    step_cost ((7 + Compare.Int.max (String.length s1)
                  (String.length s2)) / 8) +@ step_cost 1
  let compare_bytes s1 s2 =
    step_cost ((7 + Compare.Int.max (MBytes.length s1)
                  (MBytes.length s2)) / 8) +@ step_cost 1
  let compare_tez _ _ = step_cost 1
  let compare_zint n1 n2 =
    step_cost ((7 + Compare.Int.max (Z.numbits n1)
                  (Z.numbits n2)) / 8) +@ step_cost 1
(*
  let compare_int n1 n2 = compare_zint (Script_int.to_zint n1) (Script_int.to_zint n2)

  let compare_nat = compare_int
*)
  let compare_key_hash _ _ = alloc_bytes_cost 36
  let compare_timestamp t1 t2 =
    compare_zint (Script_timestamp.to_zint t1) (Script_timestamp.to_zint t2)
  let compare_address _ _ = step_cost 20

  let compare_res = step_cost 1

  (* Arithmetic operations *)

  let mul n1 n2 =
    let steps =
      (Z.numbits n1) * (Z.numbits n2) in
    let bits =
      (Z.numbits n1) + (Z.numbits n2) in
    step_cost steps +@ alloc_bits_cost bits

  let div n1 n2 =
    mul n1 n2 +@ alloc_cost 2

  let add_sub_z n1 n2 =
    let bits =
      Compare.Int.max (Z.numbits n1) (Z.numbits n2) in
    step_cost bits +@ alloc_cost bits

  let add n1 n2 =
    add_sub_z n1 n2

  let sub = add

  let abs n =
    alloc_bits_cost (Z.numbits n)

  let neg = abs
  let int _ = step_cost 1

  let add_timestamp t n =
    add_sub_z (Script_timestamp.to_zint t) n

  let sub_timestamp t n =
    add_sub_z (Script_timestamp.to_zint t) n

  let diff_timestamps t1 t2 =
    add_sub_z (Script_timestamp.to_zint t1) (Script_timestamp.to_zint t2)

  let int64_op = step_cost 1 +@ alloc_cost 1

  let z_to_int64 = step_cost 2 +@ alloc_cost 1

  let int64_to_z = step_cost 2 +@ alloc_cost 1

  (* Boolean operations *)

  let bool_binop _ _ = step_cost 1
  let bool_unop _ = step_cost 1

  (* Bitwise operations *)

  let bitwise_binop n1 n2 =
    let bits = Compare.Int.max (Z.numbits n1) (Z.numbits n2) in
    step_cost bits +@ alloc_bits_cost bits

  let logor = bitwise_binop
  let logand = bitwise_binop
  let logxor = bitwise_binop
  let lognot n =
    let bits = Z.numbits n in
    step_cost bits +@ alloc_cost bits

  let unopt ~default = function
    | None -> default
    | Some x -> x

  let max_int = 1073741823

  let shift_left x y =
    alloc_bits_cost (Z.numbits x + (Z.to_int y))

  let shift_right x y =
    alloc_bits_cost (Compare.Int.max 1 (Z.numbits x - (Z.to_int y)))

  (* Collection primitives *)

  let loop_cycle = step_cost 2

  let log2 =
    let rec help acc = function
      | 0 -> acc
      | n -> help (acc + 1) (n / 2)
    in help 1

  let module_cost = alloc_cost 10

  (* Lists *)

  let list_size = step_cost 1

  (* Sets *)

  let empty_set = module_cost

  let set_size = step_cost 2
(*
  let set_to_list : type item. item Script_typed_ir.set -> cost
    = fun (module Box) ->
      alloc_cost @@ Pervasives.(Box.size * 2)

  let set_access : type elt. elt -> elt Script_typed_ir.set -> int
    = fun _key (module Box) ->
      log2 @@ Box.size

  let set_mem key set = step_cost (set_access key set)

  let set_update key _presence set =
    set_access key set *@ alloc_cost 3
*)
  (* Maps *)

  let empty_map = module_cost

  let map_size = step_cost 2
(*
  let map_to_list : type key value. (key, value) Script_typed_ir.map -> cost
    = fun (module Box) ->
      let size = snd Box.boxed in
      3 *@ alloc_cost size

  let map_access : type key value. (key, value) Script_typed_ir.map -> int
    = fun (module Box) ->
      log2 (snd Box.boxed)

  let map_mem _key map = step_cost (map_access map)

  let map_get = map_mem

  let map_update _ _ map =
    map_access map *@ alloc_cost 3
*)
  (* Big maps *)

  let big_map_empty _key _map = step_cost 10
  let big_map_mem _key _map = step_cost 50
  let big_map_get _key _map = step_cost 50
  let big_map_update _key _value _map = step_cost 10

  (* String/Bytes primitives *)

  let string length =
    alloc_bytes_cost length

  let bytes length =
    alloc_mbytes_cost length

  let zint z =
    alloc_bits_cost (Z.numbits z)

  let concat cost length ss =
    let rec cum acc = function
      | [] -> acc
      | s :: ss -> cum (cost (length s) +@ acc) ss in
    cum free ss

  let concat_string ss = concat string String.length ss
  let concat_bytes ss = concat bytes MBytes.length ss

  let slice_string length = string length
  let slice_bytes = alloc_cost 0

  (* Pack/unpack *)

  let unpack_failed bytes =
    let len = MBytes.length bytes in
    (len *@ alloc_mbytes_cost 1) +@
      (len *@ (log2 len *@ (alloc_cost 3 +@ step_cost 1)))

  (* Cryptographic operations *)

  let hash data len = 10 *@ step_cost (MBytes.length data) +@ bytes len
  let hash_key = step_cost 3 +@ bytes 20
  let check_signature = step_cost 1000

  (* Contract interactions *)

  let self = step_cost 1
  let address = step_cost 1
  let contract = Gas.read_bytes_cost Z.zero +@ step_cost 10000
  let create_contract = step_cost 10
  let create_account = step_cost 10
  let implicit_account = step_cost 10
  let transfer = step_cost 10
  let view = step_cost 1
  let set_delegate = step_cost 10 +@ write_bytes_cost (Z.of_int 32)

  (* General info *)

  let balance = step_cost 1 +@ read_bytes_cost (Z.of_int 8)
  let now = step_cost 5
  let amount = step_cost 1
  let steps_to_quota = step_cost 1
  let cself = step_cost 1
  let source = step_cost 1
  let sender = step_cost 1
  let level = step_cost 1
  let ccycle = step_cost 1
  let getinfo = step_cost 5
  let manage = step_cost 5

  (* Conversions *)

  let address_of_keyhash = step_cost 1
  let keyhash_of_address = step_cost 1
  let int_of_nat = step_cost 1
  let nat_of_int = step_cost 1

end
