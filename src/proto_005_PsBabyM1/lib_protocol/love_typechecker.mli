(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

exception TypingError of string * Love_ast.location option * unit Love_tenv.t
exception UnknownType of Love_type.type_name
exception UnknownConstr of Love_ast.cstr_name
exception UnknownField of Love_ast.field_name
exception TypingErrorLwt of (Love_context.t * string *  unit Love_tenv.t)

type where =
    TopLevel
  | Storage
  | Parameter
  | ViewReturn
  | MapKey
  | BigMapKey
  | BigMapValue

val extract_deps :
  Love_ast.location option ->
  Love_context.t ->
  unit Love_tenv.t ->
  (string * string) list ->
  (Love_context.t * unit Love_tenv.t) tzresult Lwt.t

val typecheck_value :
  unit Love_tenv.t ->
  Love_context.t ->
  Love_type.type_exp ->
  Love_value.ValueSet.elt -> Love_context.t tzresult Lwt.t

val typecheck_struct :
  Love_ast.location option ->
  unit Love_tenv.t ->
  Love_ast.structure -> Love_ast.structure * unit Love_tenv.t

val typecheck_top_contract_deps :
  Love_ast.location option ->
  Love_context.t ->
  unit Love_tenv.t ->
  Love_ast.top_contract ->
  (Love_context.t * (Love_ast.top_contract * unit Love_tenv.t))
    tzresult Lwt.t
