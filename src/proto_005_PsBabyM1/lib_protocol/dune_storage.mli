(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* Signature re-exported by Alpha_context, to avoid multiple
   redefinitions of types and values *)

type error += Unsupported_revision of int * int
type error += Bad_revision_change of int * int

module type S = sig
  type context
  type dune_parameters
  type parametric

  val get_foundation_pubkey : context -> Signature.Public_key.t Lwt.t
  val remove_foundation_pubkey : context -> context Lwt.t
  val set_foundation_pubkey : context -> Signature.Public_key.t -> context Lwt.t

  val set_activate_protocol : context ->
    ?protocol:Protocol_hash.t ->
    ?protocol_parameters:dune_parameters ->
    int32 ->
    context Lwt.t
  val get_activate_protocol_level :
    context -> int32 option tzresult Lwt.t
  val get_activate_protocol_and_cleanup :
    context ->
    (context * Protocol_hash.t option * dune_parameters option) tzresult Lwt.t
  val patch_constants : context ->
    (parametric -> parametric) -> context Lwt.t

  val protocol_revision : context -> int
  val set_protocol_revision : context -> int -> context Lwt.t
  val check_protocol_revision : context -> unit tzresult Lwt.t
  val has_protocol_revision : context -> int -> bool
end

include S with type context := Raw_context.t
           and type dune_parameters := Dune_parameters_repr.parameters
           and type parametric := Constants_repr.parametric
