(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives

exception EnvironmentError of string

type typ_constructor = private
  {
    cparent : Love_type.t;
    cname   : string;
    cargs   : Love_type.t list;
    crec    : Love_type.recursive
  }

type typ_field = private
  {
    fparent : Love_type.t;
    fname   : string;
    ftyp    : Love_type.t;
    frec    : Love_type.recursive
  }

type val_kind =
  | Entry
  | View
  | Value of Love_ast.visibility
  | Internal

type 'a t

and 'a contract_type = 'a t

type ('res, 'env) request_result = {
  result : 'res; (* The request result *)
  path : Path.t; (* The relative path of the environment in which the result has been found *)
  last_env : 'env t; (* The environment in which the result has been found *)
  index : int (* The eventual index of the result *)
}

type ('res, 'env) env_request = ('res, 'env) request_result option

(** Environment mode is by default to false. If set to true, the environment
    will display Internal and Abstract definition types. *)
val set_environment_mode : bool -> unit

(** The empty environment *)
val empty : Love_type.struct_kind -> 'a -> unit -> 'a t

val env_path_str : 'a t -> string list

(** Applies the types to the constructor
    constr_with_args (('a, 'b) t_) [int; bool] = (int, bool) t.
    Fails if the list size if different than the number of arguments *)
val constr_with_targs : typ_constructor -> Love_type.t list -> 'a t -> typ_constructor

(** Same than before, but for fields *)
val field_with_targs : typ_field -> Love_type.t list -> 'a t-> typ_field

(** Registers a variable with the given type. *)
val add_var : ?kind:val_kind -> string -> Love_type.t -> 'a t -> 'a t

(** 'add_sum params typ components env' registers the sum type 'typ' with polymorphic
    parameters 'params' as a list of (constructor_name * typ list) = 'components'.
*)
val add_sum :
  Love_type.type_var list -> Love_type.t -> (string * Love_type.t list) list -> Love_type.recursive -> 'a t -> 'a t

(** Same than add_sum, but for record types *)
val add_record :
  Love_type.type_var list -> Love_type.t -> (string * Love_type.t) list -> Love_type.recursive -> 'a t -> 'a t

(** Updates the set of free variables. *)
val add_forall : Love_type.type_var -> 'a t -> 'a t

val add_typedef : string -> Love_ast.type_kind -> Love_type.typedef -> 'a t -> 'a t

(** Adds a type definition as it would been defined by the id in argument.
    For example, add_typedef () M1.M2.t 'a typedef' env adds the type t
    to the substructure M1.M2 in env. The name of the typedef must be the same
    than the name given in the type identifyer.
 *)
val add_typedef_in_subcontract :
  'a -> string Ident.t -> Love_ast.type_kind -> Love_type.typedef -> 'a t -> 'a t

(** Adds exceptions to the environment *)
val add_exception : string -> Love_type.t list -> 'a t -> 'a t

(** Adds signatures (as environments) to the environment *)
val add_signature : string -> 'a t -> 'a t -> 'a t

(** Adds an abstract type to the environment *)
val add_abstract_type : string -> Love_type.type_var list -> 'a t -> 'a t

val add_subcontract_env : string -> 'a t -> 'a t -> 'a t

val new_subcontract_env : string -> Love_type.struct_kind -> 'a -> 'a t -> 'a t

val add_signed_subcontract : string -> string Ident.t -> 'a t -> 'a t

(** Add a Path.t-prefixed variablt to the environent **)
val add_var_path : string Ident.t -> Love_type.t -> unit t -> unit t

(** Returns the type associated to the variable in argument *)
val find_var    : Love_ast.var_ident -> 'a t -> (val_kind * Love_type.t, 'a) env_request

(** Returns the list of constructors associated to the type name in argument *)
val find_sum    :
  string Ident.t -> 'a t -> (typ_constructor list, 'a) env_request

(** Returns the list of fields associated to the type name in argument *)
val find_record : string Ident.t -> 'a t -> (typ_field list, 'a) env_request

(** Returns the details the constructor associated to the constructor name in argument *)
val find_constr : Love_ast.cstr_name -> 'a t -> (typ_constructor, 'a) env_request

(** Returns the details the field associated to the field name in argument *)
val find_field  : Love_ast.field_name -> 'a t -> (typ_field, 'a) env_request

(** Same as above, but search with the specified path *)
val find_field_in  : string Ident.t -> Love_ast.field_name -> 'a t -> (typ_field, 'a) env_request

(** Returns the details the field associated to the field name in argument *)
val find_contract : string Ident.t -> 'a t -> ('a contract_type, 'a) env_request

(** Returns the signature associated to the id in argument *)
val find_signature  : string Ident.t -> 'a t -> ('a t, 'a) env_request

(** Returns the arguments on the exception name in argument *)
val find_exception : string Ident.t -> 'a t -> (Love_type.t list, 'a) env_request

(** Returns the arguments on the exception name in argument *)
val find_type_kind :
  string Ident.t -> 'a t ->
  ((Love_ast.type_kind * Love_type.type_var list * Love_type.traits), 'a) env_request

(** Applies the function in argument to the environment that first matches
    the ident. Searches in parent environments if it doesn't found it in
    a sub environment or if the correspondign sub environment has a higher
    id than before (i.e. has been registered after).. *)
val find_env :
  ?before : int ->
  string Ident.t -> 'a t -> (string -> 'a t -> ('b * int) option) -> ('b, 'a) env_request

val isComp : string Ident.t -> 'a t -> bool

(** Returns the storage type of the contract if it has been registered *)
val get_storage_type : 'a t -> Love_type.t option

(** Tests if the typevar in argument is universally quantified. *)
val is_free : Love_type.type_var -> 'a t -> bool

(** Returns the set of free variables *)
val get_free : 'a t -> Love_type.TypeVarSet.t

val pp_env : Format.formatter -> 'a t -> unit

val env_to_contract_sig : 'a t -> Love_type.structure_sig

val contract_sig_to_env : string option -> 'a -> Love_type.structure_sig -> 'a t -> 'a t

val get_data : 'a t -> 'a

val set_data : 'a -> 'a t -> 'a t

val find_tdef : Love_type.type_name -> 'a t -> (Love_type.typedef, 'a) env_request

(** Given a list of field * type and a user type name, returns the user type on which the correct
    type arguments are given
    Ex : if type name = {a : 'a; b : 'b} in the environment, then
    record_type [a, int; b, float] name = TUser (name, [int;float]) = {a : int; b : float}
    Fails if the list of fields is different than the one registered in the environment
 *)
val record_type : (string Ident.t option) -> (Love_ast.field_name * Love_type.t) list -> 'a t -> Love_type.t

(** Same, but for a constructors *)
val constr_type : (Love_ast.cstr_name * Love_type.t list) -> 'a t -> Love_type.t

(** Same, but for aliases *)
val alias :
  ?before:int ->
  Love_type.type_name -> Love_type.t list -> 'a t ->
  ((Love_type.t * Love_type.multiple_path), 'a) env_request

(** Normalizes the type in argument. If relative is set to true, returns the complete type
    composed of types relatively normalized. If set to false, returns the type with absolute names.
*)
val normalize_type : relative:bool -> Love_type.t -> 'a t -> Love_type.t

(** Tests if a given type is composed of an internal type. *)
val is_internal : Love_type.t -> 'a t -> bool

(** Returns the ident prefixed by the deepest kt1 dependency *)
val correct_absolute_name_with_kt1 : string Ident.t -> 'a t -> string Ident.t

(** Same, but with types. Fails on anonymous contract instances & packed strucutres (todo) *)
val correct_absolute_type_with_kt1 : Love_type.t -> 'a t -> Love_type.t


val get_subenv_struct : string -> 'a t -> 'a t option
val get_subenv_sig : string -> 'a t -> 'a t option
