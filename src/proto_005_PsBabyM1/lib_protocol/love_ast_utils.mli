(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_ast

(** Type definition builders *)
val mk_sumtype :
  Love_type.type_var list ->
  (string * Love_type.t list) list ->
  Love_type.recursive ->
  Love_type.typedef

val mk_rectype :
  Love_type.type_var list ->
  (string * Love_type.t) list ->
  Love_type.recursive ->
  Love_type.typedef

(** Constant builder *)
val mk_cunit       : ?loc:location -> unit -> const
val mk_cbool       : ?loc:location -> bool -> const
val mk_cstring     : ?loc:location -> string -> const
val mk_cbytes      : ?loc:location -> MBytes.t -> const
val mk_cint        : ?loc:location -> Z.t -> const
val mk_cnat        : ?loc:location -> Z.t -> const
val mk_cdun        : ?loc:location -> Tez_repr.t -> const
val mk_ckey        : ?loc:location -> Signature.Public_key.t -> const
val mk_ckeyhash    : ?loc:location -> Signature.Public_key_hash.t -> const
val mk_csig        : ?loc:location -> Signature.t -> const
val mk_caddress    : ?loc:location -> Contract_repr.t -> const
val mk_ctimestamp  : ?loc:location -> Script_timestamp_repr.t -> const
val mk_cprimitive  : ?loc:location -> Love_primitive.t -> const
(*
val is_runtime_only_const : const -> bool
*)

(** Pattern builder *)
val mk_pany        : ?loc:location -> unit -> pattern
val mk_pvar        : ?loc:location -> string -> pattern
val mk_palias      : ?loc:location -> pattern -> string -> pattern
val mk_pconst      : ?loc:location -> const -> pattern
val mk_plist       : ?loc:location -> pattern list -> pattern
val mk_ptuple      : ?loc:location -> pattern list -> pattern
val mk_pconstr     : ?loc:location -> string -> pattern list -> pattern
val mk_pcontract   : ?loc:location -> string -> Love_type.structure_type -> pattern

val mk_psome       : ?loc:location -> pattern -> pattern
val mk_pnone       : ?loc:location -> unit -> pattern

(** Expression builder *)
val mk_const       : ?loc:location -> const -> exp
val mk_var         : ?loc:location -> string Ident.t -> exp
val mk_let         : ?loc:location -> pattern -> exp -> exp -> exp
val mk_let_rec     : ?loc:location -> string -> exp -> exp -> Love_type.t -> exp
val mk_lambda      : ?loc:location -> pattern -> exp -> Love_type.t -> exp
val mk_apply       : ?loc:location -> exp -> exp list -> exp
val mk_apply_c     : ?loc:location -> const -> const list -> exp
val mk_tlambda     : ?loc:location -> Love_type.type_var -> exp -> exp
val mk_tapply      : ?loc:location -> exp -> Love_type.t -> exp
val mk_seq         : ?loc:location -> exp list -> exp
val mk_if          : ?loc:location -> exp -> exp -> exp -> exp
val mk_match       : ?loc:location -> exp -> (pattern * exp) list -> exp
val mk_constr      : ?loc:location -> string Ident.t -> Love_type.t list -> exp list -> exp
val mk_list        : ?loc:location -> ?typ:Love_type.t -> exp list -> exp
val mk_concat_list : ?loc:location -> exp list -> exp -> exp
val mk_tuple       : ?loc:location -> exp list -> exp
val mk_projection  : ?loc:location -> exp -> int list -> exp
val mk_update      : ?loc:location -> exp -> (int * exp) list -> exp
val mk_record      : ?loc:location -> string Ident.t option -> (string * exp) list -> exp
val mk_get_field   : ?loc:location -> exp -> string -> exp
val mk_set_field   : ?loc:location -> exp -> (string * exp) list -> exp
val mk_packstruct  : ?loc:location -> reference -> exp
val mk_raise       : ?loc:location -> exn_name -> exp list -> exp
val mk_try_with    : ?loc:location -> exp -> ((exn_name * pattern list) * exp) list -> exp

val mk_none        : ?loc:location -> ?typ:Love_type.t -> unit -> exp
val mk_some        : ?loc:location -> exp -> exp
val mk_enil        : ?loc:location -> ?typ:Love_type.t -> unit -> exp
val mk_emptyset    : ?loc:location -> ?typ:Love_type.t -> unit -> exp
val mk_emptymap    : ?loc:location -> ?typ_key:Love_type.t -> ?typ_bnd:Love_type.t -> unit -> exp
val mk_emptybigmap : ?loc:location -> ?typ_key:Love_type.t -> ?typ_bnd:Love_type.t -> unit -> exp
(*val mk_primitive_lambda :
  ?loc:location ->
  (Love_type.type_name -> Love_type.typedef) ->
  Love_primitive.t -> Love_type.t -> exp*)


val mk_entry_point_lambda :
  ?loc:location ->
  string -> Love_type.t ->
  string -> Love_type.t ->
  exp -> exp

(** Contracts *)
val mk_type : Love_ast.type_kind -> Love_type.typedef -> content

val mk_entry :
  exp ->
  exp option ->
  Love_type.t ->
  content

val mk_view :
  exp ->
  Love_type.t ->
  Love_type.t ->
  content

val mk_value :
  exp ->
  Love_type.t ->
  Love_ast.visibility ->
  content

val mk_structure : structure -> content

val mk_contract_struct :
  (string * content) list ->
  structure

val mk_contract :
  (string * content) list ->
  content

val mk_module_struct :
  (string * content) list ->
  structure

val mk_module :
  (string * content) list ->
  content

val get_content :
  string Ident.t -> structure -> content option
val get_entries :
  structure -> (string * entry) list
val get_values :
  structure -> (string * value) list
val get_structures :
  structure -> (string * structure) list
val get_contracts :
  structure -> (string * structure) list
val get_modules :
  structure -> (string * structure) list


val sig_of_structure : structure -> Love_type.structure_sig


module Raw : sig
  val lambdas_to_params :
    exp -> (pattern * Love_type.t) list

  val params_to_lambdas :
    (pattern * Love_type.t) list ->
    exp -> exp
end

val lambdas_to_params :
  Love_runtime_ast.exp -> Love_runtime_ast.binding list

val params_to_lambdas :
  Love_runtime_ast.binding list -> Love_runtime_ast.exp -> Love_runtime_ast.exp
