(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Error_monad
open Love_pervasives
open Exceptions
open Love_value

module ContractMap = Map.Make (Contract)

type contract_data = {
  version: (int * int); (* major, minor *)
  code: LiveStructure.t;
  storage: Value.t
}

type t = {
  actxt: Alpha_context.t;
  self: Contract.t;
  source: Contract.t;
  payer: Contract.t;
  amount: Tez.t;
  contracts: contract_data ContractMap.t;
}

type v =
  | Exp of Love_runtime_ast.param list
  | Val of Love_value.Value.t list

exception UserException of t * (Love_runtime_ast.exn_name * Value.t list) *
                           Love_ast.location option

let dummy_address =
  Contract_repr.implicit_contract (Signature.Public_key_hash.zero)

let init ?(self=dummy_address) ?(source=dummy_address)
    ?(payer=dummy_address) ?(amount=Tez.zero)
    ?(code=LiveContract.unit) ?(storage=Value.VUnit) actxt =
  let contracts = ContractMap.singleton self
      { version = code.version; code = code.root_struct; storage = storage } in
  { actxt; self; source; payer; amount; contracts }

let set_script_code ctxt addr (code : LiveContract.t) =
  { ctxt with
    contracts = ContractMap.add addr
        { version = code.version; code = code.root_struct; storage = VUnit }
        ctxt.contracts }

let unset_script ctxt addr =
  { ctxt with contracts = ContractMap.remove addr ctxt.contracts }

let get_script ctxt addr =
  match ContractMap.find_opt addr ctxt.contracts with
  | Some { version = _; code; storage } ->
      return (ctxt, Some (code, storage))
  | None ->
    Contract.exists ctxt.actxt addr >>=?
      begin function
      | false -> return (ctxt, None)
      | true ->
         Contract.get_script ctxt.actxt addr >>=? fun (actxt, script) ->
         let ctxt = { ctxt with actxt } in
         match script with (* Can only fail because of gas *)
         | None -> return (ctxt, Some (LiveStructure.unit, Value.VUnit))
         | Some { code; storage } ->
            begin
            Script_all.force_decode ctxt.actxt code >>=? fun (code, actxt) ->
            Script_all.force_decode actxt storage >>=? fun (storage, actxt) ->
            let ctxt = { ctxt with actxt } in
            let code = match code with
              | Michelson_expr _code -> None (* TODO : Michelson wrapper *)
              | Dune_code code -> Love_repr.is_code code
              | Dune_expr _ -> None
            in
            let storage = match storage with
              | Michelson_expr _code -> None
              | Dune_code _ -> None
              | Dune_expr storage -> Love_repr.is_const storage
            in
            match code, storage with
            | Some (LiveContract { version; root_struct }),
              Some (Value storage) ->
               let ctxt = { ctxt with
                            contracts = ContractMap.add addr
                                { version; code = root_struct; storage }
                                ctxt.contracts } in
               return (ctxt, Some (root_struct, storage))
            | _ -> return (ctxt, None)
            end
      end

let resolve_path_with_kt1 ctxt path (* with kt1 *) =
  let split_kt1 path =
    let maybe_kt1, id_opt = Ident.split path in
    match Utils.address_of_string_opt maybe_kt1 with
    | Some kt1 -> Some (kt1, id_opt)
    | None -> None
  in
  match split_kt1 path with
  | None -> raise (InvariantBroken "Invalid path : should begin with KT1")
  | Some (kt1, id_opt) ->
    get_script ctxt kt1 >>|? fun (ctxt, script) ->
    match script, id_opt with
    | Some (code, _storage), None ->
        ctxt, Some (LiveStructure.VStructure code)
    | Some (code, _storage), Some id ->
        ctxt, LiveStructure.resolve_id_in_struct code id
    | None, _ -> raise (InvariantBroken "Contract not found")

let resolve_path_with_kt1_cached ctxt path (* with kt1 *) =
  let split_kt1 path =
    let maybe_kt1, id_opt = Ident.split path in
    match Utils.address_of_string_opt maybe_kt1 with
    | Some kt1 -> Some (kt1, id_opt)
    | None -> None
  in
  match split_kt1 path with
  | None -> raise (InvariantBroken "Invalid path : should begin with KT1")
  | Some (kt1, id_opt) ->
    match ContractMap.find_opt kt1 ctxt.contracts, id_opt with
    | Some { version = _; code; storage = _ }, None ->
        Some (LiveStructure.VStructure code)
    | Some { version = _; code; storage = _ }, Some id ->
        LiveStructure.resolve_id_in_struct code id
    | None, _ -> raise (InvariantBroken "Contract not found")

let get_value ctxt path =
  match resolve_path_with_kt1_cached ctxt path with
  | Some (VValue { vvalue_code = v; _ }) -> v
  | _ -> raise (InvariantBroken "Value not found in contract cache")

let get_struct ctxt path =
  match resolve_path_with_kt1_cached ctxt path with
  | Some (VStructure s) -> s
  | _ -> raise (InvariantBroken "Structure not found in contract cache")

let get_sig ctxt path =
  match resolve_path_with_kt1_cached ctxt path with
  | Some (VSignature s) -> s
  | _ -> raise (InvariantBroken "Signature not found in contract cache")
