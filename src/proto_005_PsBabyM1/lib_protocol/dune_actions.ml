(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module StringMap = Map.Make(String)

let protocol_actions = ref StringMap.empty

let perform_protocol_action ctxt action =
  match StringMap.find_opt action !protocol_actions with
  | None ->
      Dune_misc.failwith "Undefined protocol action %S" action
  | Some action -> action ctxt

let register_protocol_action name f =
  if StringMap.mem name !protocol_actions then
    Dune_debug.printf ~n:0 "Warning: protocol action %S registered twice" name;
  protocol_actions := StringMap.add name f !protocol_actions



let accounts_actions = ref StringMap.empty

let perform_accounts_action ctxt balance_updates action amount contracts =
  match StringMap.find_opt action !accounts_actions with
  | None ->
      Dune_misc.failwith "Undefined accounts action %S" action
  | Some action ->
      fold_left_s
        (fun (ctxt, balances) contract ->
           action ctxt balances amount contract)
        (ctxt, balance_updates) contracts

let register_accounts_action name f =
  if StringMap.mem name !accounts_actions then
    Dune_debug.printf ~n:0 "Warning: accounts action %S registered twice" name;
  accounts_actions := StringMap.add name f !accounts_actions

let register_simple_accounts_action name f =
  register_accounts_action name (fun ctxt balances _amount contract ->
      f ctxt contract >>=? fun ctxt -> return (ctxt, balances)
    )

(* Protocol actions *)

let () =
  register_protocol_action "freeze_undelegated_accounts"
    (fun ctxt ->
       Storage.Contract.fold ctxt ~init:(Ok ctxt)
         ~f:(fun contract ctxt ->
             Lwt.return ctxt >>=? fun ctxt ->
             match Contract_repr.is_implicit contract with
             | None -> return ctxt
             | Some _key_hash ->
                 Delegate_storage.get ctxt contract >>=? function
                 | Some _ -> return ctxt
                 | None ->
                     Delegate_storage.freeze_account ctxt contract
           )
    );
  register_protocol_action "burn_all_commitments"
    (fun ctxt ->
       Storage.Commitments.clear ctxt >>= return
    );
  register_protocol_action "activate_deflation"
    (fun ctxt ->
       let cycle = (Raw_context.current_level ctxt).cycle in
       Storage.Cycle.Next_deflate.init_set ctxt Cycle_repr.( add cycle 1 )
       >>= fun ctxt -> return ctxt
    );
  register_protocol_action "deactivate_deflation"
    (fun ctxt ->
       Storage.Cycle.Next_deflate.remove ctxt
       >>= fun ctxt -> return ctxt
    );

  ()

(* Accounts actions *)

let () =
  register_simple_accounts_action "unfreeze"
    (fun ctxt contract ->
       Delegate_storage.unfreeze_account ctxt contract
    );

  register_simple_accounts_action "undelegate"
    (fun ctxt contract ->
       begin
         match Contract_repr.is_implicit contract with
         | None ->
             Dune_misc.failwith "Clear delegation only available for bakers"
         | Some delegate ->
             return delegate
       end >>=? fun delegate ->
       Delegate_storage.delegated_contracts ctxt delegate >>= fun contracts ->
       let n = ref 0 in
       fold_left_s (fun ctxt contract ->
           incr n;
           Delegate_storage.set ctxt contract None
         ) ctxt contracts
    );

  register_simple_accounts_action "add_allowed_baker"
    (fun ctxt contract ->
       Contract_storage.change_allowed_baker ctxt contract true
       >>= fun ctxt -> return ctxt
    );

  register_simple_accounts_action "remove_allowed_baker"
    (fun ctxt contract ->
       Contract_storage.change_allowed_baker ctxt contract false
       >>= fun ctxt -> return ctxt
    );

  register_simple_accounts_action "add_superadmin"
    (fun ctxt contract ->
       Contract_storage.change_superadmin ctxt contract true
       >>= fun ctxt -> return ctxt
    );

  register_simple_accounts_action "remove_superadmin"
    (fun ctxt contract ->
       Contract_storage.change_superadmin ctxt contract false
       >>= fun ctxt -> return ctxt
    );

  ()
