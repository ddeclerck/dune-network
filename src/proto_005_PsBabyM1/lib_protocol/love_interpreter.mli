(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

val normalize_script :
  Alpha_context.t ->
  Love_value.LiveContract.t ->
  (Alpha_context.t * Love_value.LiveContract.t *
   Love_value.FeeCode.t)
    tzresult Lwt.t

val typecheck_code :
  Alpha_context.t ->
  Love_ast.top_contract ->
  Alpha_context.t tzresult Lwt.t

val typecheck_data :
  Alpha_context.t ->
  Love_value.ValueSet.elt ->
  Love_type.t ->
  Alpha_context.t tzresult Lwt.t

val typecheck :
  Alpha_context.t ->
  code:Love_ast.top_contract ->
  storage:Love_value.ValueSet.elt ->
  self:Contract_repr.t ->
  (Alpha_context.t * Love_value.LiveContract.t *
   Love_value.ValueSet.elt)
    tzresult Lwt.t

val storage_big_map_diff :
  Alpha_context.t ->
  Love_value.ValueSet.elt ->
  (Alpha_context.t * Love_value.ValueSet.elt *
   Love_value.Op.big_map_diff option)
    tzresult Lwt.t

val execute :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  code:Love_value.LiveContract.t ->
  storage:Love_value.ValueSet.elt ->
  entrypoint:string ->
  parameter:Love_value.ValueSet.elt ->
  amount:Tez_repr.tez ->
  (Alpha_context.t *
   (Love_value.ValueSet.elt *
    Love_value.Op.internal_operation list *
    Love_value.Op.big_map_diff option * Love_value.ValueSet.elt))
    tzresult Lwt.t

val execute_fee_script :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  fee_code:Love_value.FeeCode.t ->
  storage:Love_value.ValueSet.elt ->
  entrypoint:string ->
  parameter:Love_value.ValueSet.elt ->
  amount:Tez_repr.tez ->
  (Alpha_context.t * (Tez_repr.tez * Z.t)) tzresult Lwt.t
