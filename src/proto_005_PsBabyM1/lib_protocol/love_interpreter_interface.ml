(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module M = struct

  open Error_monad
  open Alpha_context
  open Love_script_repr
  open Love_repr

  module Repr = Love_script_repr

  type execution_result = {
    ctxt : context;
    storage : const;
    result : const;
    big_map_diff : Contract.big_map_diff option;
    operations : packed_internal_operation list;
  }

  type fee_execution_result = {
    ctxt : context;
    max_fee : Tez.t;
    max_storage : Z.t;
  }

  let translate_op Love_value.Op.{ source; operation; nonce } =
    let open Love_value in
    let open Script_all in
    match operation with
    | Op.Origination {
        delegate; script = storage, code; credit; preorigination } ->
      let script =
        let storage = lazy_expr (Dune_expr (Const (Value storage))) in
        let code = lazy_expr (Dune_code (Code (LiveContract code))) in
        Script.Script { code; storage }
      in
      let operation =
        Alpha_context.Origination { delegate; script; credit; preorigination }
      in
      Internal_operation { source; operation; nonce }
    | Transaction { amount; parameters; entrypoint; destination } ->
      let parameters = match parameters with
        | None -> None
        | Some p -> Some (lazy_expr (Dune_expr (Const (Value p))))
      in
      let operation = Transaction { amount; parameters; entrypoint; destination;
                                    collect_call = None } in
      Internal_operation { source; operation; nonce }
    | Delegation delegate ->
      let operation = Delegation delegate in
      Internal_operation { source; operation; nonce }
    | Dune_manage_account
        { target; maxrolls; admin; white_list; delegation } ->
      let open Dune_operation_repr in
      let options = { maxrolls; admin; white_list; delegation } in
      let options = Options.encode options in
      let target = match target with
        | Some target_pkh -> Some (target_pkh, None)
        | None -> None
      in
      let operation =
        Dune_manager_operation (Dune_manage_account { target; options })
      in
      Internal_operation { source; operation; nonce }

  let translate_big_map_diff (diff : Love_value.Op.big_map_diff option) =
    let open Love_value in
    let open Script_all in
    match diff with
    | None -> None
    | Some diff ->
      Some (List.map (function
          | Op.Update { big_map; diff_key; diff_key_hash; diff_value } ->
            let diff_key = Dune_expr (Const (Value diff_key)) in
            let diff_value = match diff_value with
              | None -> None
              | Some v -> Some (Dune_expr (Const (Value v)))
            in
            Contract.Update { big_map ; diff_key ; diff_key_hash ; diff_value }
          | Clear id -> Contract.Clear id
          | Copy (ids, idd) -> Contract.Copy (ids, idd)
          | Alloc { big_map; key_type; value_type; } ->
            let key_type = Dune_expr (Const (Type key_type)) in
            let value_type = Dune_expr (Const (Type value_type)) in
            Contract.Alloc { big_map; key_type; value_type; }
        ) diff)

  let normalize_script ctxt code storage =
    match code with
    | Contract _ -> failwith "Contract can't be normalized"
    | LiveContract c ->
      Love_interpreter.normalize_script ctxt c
      >>=? fun (ctxt, code, fee_code) ->
      return ((LiveContract code, storage, Some (FeeCode fee_code)), ctxt)
    | FeeCode _ -> failwith "FeeCode can't be normalized"

  let denormalize_script ctxt code storage _fee_code =
    (* let _code = match code, fee_code with
     *   | Contract _, _ -> failwith "Contract can't be denormalized"
     *   | LiveContract _, _ -> failwith "LiveContract can't be denormalized"
     *   | FeeCode _, _ -> failwith "FeeCode can' be denormalized"
     * in *)
    return ((code, storage), ctxt)

  let typecheck_code ctxt code =
    match code with
    | Contract code -> Love_interpreter.typecheck_code ctxt code
    | _ -> failwith "Wrong kind of code for typecheck code"

  let typecheck_data ctxt data typ =
    match data, typ with
    | Value v, Type t -> Love_interpreter.typecheck_data ctxt v t
    | _ -> failwith "Wrong kind of data for typecheck data"

  let typecheck ctxt ~code ~storage ~self ~internal =
    match code, storage with
    | Contract code, Value storage ->
        Love_interpreter.typecheck ctxt ~code ~storage ~self
        >>=? fun (ctxt, code, storage) ->
        Love_interpreter.storage_big_map_diff ctxt storage
        >>=? fun (ctxt, storage, big_map_diff) ->
        normalize_script ctxt (LiveContract code) (Value storage)
        >>=? fun ((code, storage, fee_code), ctxt) ->
        let big_map_diff = translate_big_map_diff big_map_diff in
        return ((code, storage, fee_code), big_map_diff, ctxt)
    | LiveContract code, Value storage ->
        if not internal then
          failwith "Wrong kind of code/storage for typecheck"
        else
          Love_interpreter.storage_big_map_diff ctxt storage
          >>=? fun (ctxt, storage, big_map_diff) ->
          normalize_script ctxt (LiveContract code) (Value storage)
          >>=? fun ((code, storage, fee_code), ctxt) ->
          let big_map_diff = translate_big_map_diff big_map_diff in
          return ((code, storage, fee_code), big_map_diff, ctxt)
    | _ -> failwith "Wrong kind of code/storage for typecheck"

  let execute ctxt _mode step_constants
      ~code ~storage ~entrypoint ~parameter ~apply:_ =
    let code, storage, parameter = match code, storage, parameter with
      | LiveContract c, Value s, Value p -> c, s, p
      | _ -> failwith "Wrong kind of code or storage for execute"
    in
    let Script_interpreter.{ source; payer; self; amount; _ } =
      step_constants in
    Love_interpreter.execute ctxt ~source ~payer ~self
      ~code ~storage ~entrypoint ~parameter ~amount
    >>=? fun (ctxt, (result, operations, big_map_diff, storage)) ->
    let operations = List.map translate_op operations in
    let big_map_diff = translate_big_map_diff big_map_diff in
    return { ctxt; storage = Value storage; result = Value result;
             big_map_diff; operations }

  let execute_fee_script ctxt step_constants
      ~fee_code ~storage ~entrypoint ~parameter =
    let fee_code, storage, parameter = match fee_code, storage, parameter with
      | FeeCode fc, Value s, Value p -> fc, s, p
      | _ -> failwith "Wrong kind of code or storage for execute_fee_script"
    in
    let Script_interpreter.{ source; payer; self; amount; _ } =
      step_constants in
    Love_interpreter.execute_fee_script ctxt ~source ~payer ~self
      ~fee_code ~storage ~entrypoint ~parameter ~amount
    >>=? fun (ctxt, (max_fee, max_storage)) ->
    return { ctxt; max_fee; max_storage }

end

include M

let () =
  if false then
    let module TEST = (M : Dune_script_interpreter_sig.S) in
    ()
