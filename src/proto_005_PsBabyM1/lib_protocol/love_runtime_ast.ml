(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Ident

type location = Love_ast.location

type var_ident = string Ident.t
type cstr_name = string
type field_name = string

type visibility = Love_ast.visibility

type type_kind = Love_ast.type_kind

type exn_name =
  | RFail of Love_type.t
  | RException of string Ident.t

type const =
  | RCUnit
  | RCBool of bool
  | RCString of string
  | RCBytes of MBytes.t
  | RCInt of Z.t
  | RCNat of Z.t
  | RCDun of Tez_repr.t
  | RCKey of Signature.Public_key.t
  | RCKeyHash of Signature.Public_key_hash.t
  | RCSignature of Signature.t
  | RCTimestamp of Script_timestamp_repr.t
  | RCAddress of Contract_repr.t
  | RCPrimitive of Love_primitive.t

type pattern =
  | RPAny
  | RPVar of string
  | RPAlias of pattern * string
  | RPConst of const
  | RPNone
  | RPSome of pattern
  | RPList of pattern list
  | RPTuple of pattern list
  | RPConstr of string * pattern list
  | RPContract of string * Love_type.structure_type

type exp =
  (* Base language *)
  | RConst of const
  | RVar of var_ident
  | RLet of { bnd_pattern: pattern; bnd_val: exp; body: exp }
  | RLetRec of { bnd_var: string; bnd_val: exp;
                 val_typ: Love_type.t; body: exp }
  | RLambda of lambda
  | RApply of { exp: exp; args: param list }

  (* Common extensions *)
  | RSeq of exp list
  | RIf of { cond: exp; ifthen: exp; ifelse: exp }
  | RMatch of { arg: exp; cases: (pattern * exp) list }

  (* Exceptions *)
  | RRaise of { exn: exn_name; args: exp list; loc: location option}
  | RTryWith of { arg: exp; cases: ((exn_name * pattern list) * exp) list }

  (* Options *)
  | RNone
  | RSome of exp

  (* Collections *)
  | RNil
  | RList of exp list * exp

  (* Tuples *)
  | RTuple of exp list
  | RProject of { tuple: exp; indexes: int list } (* non-empty, >= 0 *)
  | RUpdate of { tuple: exp; updates: (int * exp) list } (* non-empty, >= 0 *)

  (* Sums *)
  | RConstructor of { type_name: Love_type.type_name;
                      ctyps : Love_type.t list;
                      constr: cstr_name; args: exp list }

  (* Records *)
  | RRecord of { type_name: Love_type.type_name;
                 contents: (field_name * exp) list }
  | RGetField of { record: exp; field: field_name }
  | RSetFields of { record: exp; updates: (field_name * exp) list }

  (* First-class structures *)
  | RPackStruct of reference

and binding =
  | RPattern of pattern * Love_type.t
  | RTypeVar of Love_type.type_var

and param =
  | RExp of exp
  | RType of Love_type.t

and lambda = {
  args: binding list; (* non-empty *)
  body: exp; (* different from Lambda *)
}

and entry = {
  entry_code: exp;
  entry_fee_code: exp option; (* An expression of type (fun, nat) *)
  entry_typ: Love_type.t; (* Parameter type *)
}

and view = {
  view_code: exp;
  view_typ: Love_type.t * Love_type.t; (* Parameter type * Return type *)
}

and value = {
  value_code: exp;
  value_typ: Love_type.t;
  value_visibility: visibility;
}

and content =
  | RDefType of type_kind * Love_type.typedef
  | RDefException of Love_type.t list
  | REntry of entry
  | RView of view
  | RValue of value
  | RStructure of structure
  | RSignature of Love_type.structure_sig

and structure = {
  kind : Love_type.struct_kind;
  structure_content : (string * content) list;
} (* unique names at top level *)

and reference =
  | RAnonymous of structure
  | RNamed of string Ident.t

type top_contract = {
  version: (int * int); (* (major, minor) *)
  code: structure;
}

let pp_visibility = Love_ast.pp_visibility

let kind_to_string = Love_ast.kind_to_string

let pp_type_kind = Love_ast.pp_type_kind

let type_kind_to_sigtype = Love_ast.type_kind_to_sigtype

let exn_id = function
  | RFail _ -> "Failure"
  | RException e -> Ident.get_final e

let print_exn_name fmt =
  function
  | RFail t -> Format.fprintf fmt "Failure [<%a>]" Love_type.pretty t
  | RException e -> print_strident fmt e

let exn_equal e1 e2 =
  match e1, e2 with
  | RFail t1, RFail t2 -> Love_type.bool_equal t1 t2
  | RException e1, RException e2 -> Ident.equal String.equal e1 e2
  | _, _ -> false

let is_module c =
  match c.kind with
  | Module -> true
  | Contract _ -> false
