(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module M = struct

  open Alpha_context
  module Repr = Lang1_script_repr

  open Repr

  type execution_result = {
    ctxt : context ;
    storage : const ;
    result : const ;
    big_map_diff : Contract.big_map_diff option ;
    operations : packed_internal_operation list ;
  }

  type fee_execution_result = {
    ctxt : context ;
    max_fee : Tez.t ;
    max_storage : Z.t ;
  }

  let interp ctxt _mode _step_constants state instr = match instr with
    | NOOP -> return (ctxt, state)

  let execute ctxt mode step_constants ~code ~storage ~entrypoint:_ ~parameter ~apply:_ =
    interp ctxt mode step_constants (parameter, storage) code
    >>=? fun (ctxt, (_result_state, _storage)) ->
    (*  Dune_storage.update_contract_storage ctxt self
        (Lang1_repr.Const storage) >>=? fun ctxt -> *)
    return { ctxt; storage; result = VOID;
             big_map_diff = None; operations = [] }

  let normalize_script ctxt code storage =
    return ((code, storage, None), ctxt)

  let denormalize_script ctxt code storage _fee_code =
    return ((code, storage), ctxt)

  let typecheck_code ctxt _code =
    return (ctxt)

  let typecheck_data ctxt _data _type =
    return (ctxt)

  let typecheck ctxt ~code ~storage ~self:_ ~internal:_ =
    return ((code, storage, None), None, ctxt)

  let execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint:_ ~parameter =
    interp ctxt Script_ir_translator.Readable step_constants (parameter, storage) fee_code
    >>=? fun (ctxt, _) ->
    return { ctxt ; max_fee = Tez.zero ; max_storage = Z.zero }

  (* Disable after revision 1 *)

  let typecheck ctxt ~code ~storage =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      typecheck ctxt ~code ~storage

  let execute ctxt mode step_constants ~code ~storage ~entrypoint ~parameter ~apply =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      execute ctxt mode step_constants ~code ~storage ~entrypoint ~parameter ~apply

  let execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter =
    if Dune_storage.has_protocol_revision ctxt 1 then
      failwith "No Lang1"
    else
      execute_fee_script ctxt step_constants ~fee_code ~storage ~entrypoint ~parameter

end

include M

let () =
  if false then
    let module TEST = ( M : Dune_script_interpreter_sig.S ) in
    ()
