(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)


module Options = struct

  (** Activates debug messages *)
  let debug = ref false

  let max_stack = ref 1024 (* Upper limit around 86900 *)

  let version = (1, 0)

end


module Exceptions = struct
  exception StackOverflow
  exception InvariantBroken of string
  exception GenericError of string
  exception BadArgument of string
  exception Uncomparable
  exception BadParameter
  exception BadReturnType
  exception BadStorage
  exception BadEntryPoint
  exception BadView
  exception DeserializeError of int * string
  exception DecodingError of string
  exception ParseError of string
end


module Log = struct

  let dummy_formatter =
    Format.formatter_of_out_functions Format.{
        out_string = (fun _ _ _ -> ());
        out_flush = (fun _ -> ());
        out_newline = (fun _ -> ());
        out_spaces = (fun _ -> ());
        out_indent = (fun _ -> ());
      }

  let fmt = ref (fun _ -> dummy_formatter)

  let print (msg: ('a, Format.formatter, unit) format) : 'a =
    Format.fprintf (!fmt ()) msg

  let debug  (msg: ('a, Format.formatter, unit) format) : 'a =
    if !Options.debug
    then print msg
    else (Format.ifprintf (!fmt ())) msg

end


module Utils = struct

  open Exceptions

  type ('a, 'annot) annoted = {
    content: 'a;
    annot : 'annot
  }

  let is_empty = function
    | [] -> true
    | _ -> false

  let flags_to_bits n fl =
    let rec aux b n = function
      | [] -> b
      | f :: fl ->
          if Compare.Int.(n < 0) then
            raise (InvariantBroken "Not enough bits for flags_to_bits")
          else
            let b = (b lsl 1) lor (if f then 1 else 0) in
            aux b (n - 1) fl
    in
    aux 0 n fl

  let bits_to_flags n b =
    let rec aux fl n b =
      if Compare.Int.(n = 0) then
        if Compare.Int.(b <> 0) then
          raise (InvariantBroken "Not enough bits for bits_to_flags")
        else fl
      else
        let fl = Compare.Int.((b land 1) <> 0) :: fl in
        aux fl (n - 1) (b lsr 1)
    in
    aux [] n b

  let add_uniq s sl =
    if List.mem s sl then sl else s :: sl

  let add_uniq_lst sl1 sl2 =
    List.fold_left (fun sl s -> add_uniq s sl) sl2 sl1

  let print_list_s (sep : ('a, Format.formatter, unit) format) pfun =
    Format.pp_print_list
      ~pp_sep:(fun fmt _ -> Format.fprintf fmt sep)
      pfun

  let bindings_to_map add empty bindings =
    List.fold_left (fun m (k, v) -> add k v m) empty bindings

  let list_equal l1 l2 =
    List.for_all (fun e -> List.mem e l2) l1 &&
    List.for_all (fun e -> List.mem e l1) l2

  let rec list_strict_equal eq l1 l2 =
    match l1, l2 with
      [], [] -> true
    | hd1 :: tl1, hd2 :: tl2 -> eq hd1 hd2 && list_strict_equal eq tl1 tl2
    | _ -> false

  let print_str_list sep =
    Format.pp_print_list
      ~pp_sep:(fun fmt _ -> Format.fprintf fmt "%s" sep)
      (fun fmt s -> Format.fprintf fmt "%s" s)

  (* helper functions to be able to print "const(x)" or "x list" only in
     debug mode, and x othrwise *)

  let wrap_debug debug fmt id1 id2 f x =
    if debug then
      Format.fprintf fmt "%s(%a)%s" id1 f x id2
    else if String.equal id2 "" then
      Format.fprintf fmt "%a" f x
    else
      Format.fprintf fmt "%a %s" f x id2

  (* prints "id(x)" if debug else x *)
  let wrap_debug_left debug fmt id f x =
    wrap_debug debug fmt id "" f x

  (* prints "(x)id" if debug else x *)
  let wrap_debug_right debug fmt id f x =
    wrap_debug debug fmt "" id f x

  let address_of_string_opt s =
    match Contract_repr.of_b58check s with
    | Ok addr -> Some addr
    | _ -> None

  let rec compare_list cmp l1 l2 =
    match l1, l2 with
      [], [] -> 0
    | hd1 :: tl1, hd2 :: tl2 ->
      let hd_cmp = cmp hd1 hd2 in
      if Compare.Int.equal hd_cmp 0
      then compare_list cmp tl1 tl2
      else hd_cmp
    | [], _ -> -1
    | _, [] -> 1

  let compare_pair cmp1 cmp2 (d11, d12) (d21, d22) =
    let pc = cmp1 d11 d21 in
    if Compare.Int.equal pc 0 then cmp2 d12 d22 else pc

  let compare_option cmp l1 l2 =
  match l1, l2 with
    None, None -> 0
  | Some l1, Some l2 -> cmp l1 l2
  | Some _, None -> -1
  | None, Some _ -> 1
end

module Ident = struct

  type 'a id =
    | LDot of 'a * 'a id
    | LName of 'a

  let rec compare cmp i1 i2 =
    match i1, i2 with
    | LDot (name1, t1), LDot (name2, t2) ->
      let res = cmp name1 name2  in
      if Compare.Int.equal res 0
      then compare cmp t1 t2
      else res
    | LName name1, LName name2 ->
      cmp name1 name2
    | LName _, LDot _ -> -1
    | LDot _, LName _ -> 1

  let rec equal eq i1 i2 =
    match i1, i2 with
    | LDot (name1, t1), LDot (name2, t2) -> eq name1 name2 && equal eq t1 t2
    | LName name1, LName name2 -> eq name1 name2
    | LName _, LDot _
    | LDot _, LName _ -> false

  let create_id (name : 'a) : 'a id = LName name

  let put_in_namespace (name_space : 'a) (id : 'a id) : 'a id =
    LDot (name_space, id)

  let put_in_namespaces (name_spaces : 'a list) (id : 'a id) =
    List.fold_left
      (fun acc ns -> put_in_namespace ns acc)
      id
      name_spaces

  let create_namespace (name_space : 'a) (l : 'a id list) =
    List.map
      (put_in_namespace name_space)
      l

  let rec iter (f : 'a -> unit) : 'a id -> unit =
    function
    | LDot (name, t) -> let () = f name in iter f t
    | LName name -> f name

  let rec fold (f : 'a -> 'b -> 'b) (t : 'a id) (acc : 'b) : 'b =
    match t with
    | LDot (name, t) -> fold f t (f name acc)
    | LName name -> f name acc

  let rec map (f : 'a -> 'b) : 'a id -> 'b id =
    function
    | LDot (name, t) -> LDot (f name, map f t)
    | LName name -> LName (f name)

  let rec get_final =
    function
    | LDot (_, i) -> get_final i
    | LName i -> i

  let split = function
    | LDot (n, i) -> n, Some i
    | LName i -> i, None

  let get_list t =
    let rec __get acc =
      function
      | LName i -> List.rev (i :: acc)
      | LDot (i, t) -> __get (i :: acc) t
    in __get [] t

  let put_list l =
    let rec __put acc =
      function
      | [] -> acc
      | i :: l -> __put (LDot (i, acc)) l
    in
    match List.rev l with
    | [] -> raise (Exceptions.GenericError "Empty list")
    | i :: l -> __put (LName i) l

  let rec concat i1 i2 =
    match i1 with
      LName n -> LDot (n, i2)
    | LDot (n, i) -> LDot (n, concat i i2)

  let rec change_last i last =
    match i with
      LName _ -> last
    | LDot (n,i) -> LDot (n, change_last i last)

  let rec pretty printer fmt =
    function
    | LDot (name, i) ->
      Format.fprintf fmt "%a.%a" printer name (pretty printer) i
    | LName i -> Format.fprintf fmt "%a" printer i

  module Comp (C : Compare.COMPARABLE) :
    Compare.COMPARABLE with type t = C.t id
  =
  struct
    type t =
      C.t id
    let compare = compare C.compare
  end

  module IdSet (C : Compare.COMPARABLE) = Set.Make (Comp (C))
  module IdMap (C : Compare.COMPARABLE) = Map.Make (Comp (C))

  type 'a t = 'a id

  let print_strident = pretty (fun fmt i -> Format.fprintf fmt "%s" i)

  let rec alter_ident alter d id =
    match id with
      LName n -> let n, d = alter d n in LName n, d
    | LDot (n, id) ->
      let n, d = alter d n in
      let rest, d = alter_ident alter d id in
      LDot (n,rest), d
end


module Path = struct

  type path_item =
    | Prev
    | Next of string

  type t = path_item list

  let pp_path_item fmt = function
      Prev -> Format.fprintf fmt ".."
    | Next s -> Format.fprintf fmt "%s" s

  let pp_path fmt l =
    Format.pp_print_list
      ~pp_sep:(fun fmt _ -> Format.fprintf fmt "/")
      pp_path_item fmt l

  let simplify_path p =
    let rec loop res p =
      match p with
        [] -> List.rev res
      | Next _ :: Prev :: l -> loop res l
      | elt :: tl -> loop (elt :: res) tl
    in
    loop [] p

  let eq_path_item p1 p2 = match p1, p2 with
      Prev, Prev -> true
    | Next s1, Next s2 -> String.equal s1 s2
    | Prev, Next _ | Next _, Prev -> false

  let eq_path l1 l2 =
    try
      List.for_all2
        eq_path_item
        (simplify_path l1)
        (simplify_path l2)
    with
      Invalid_argument _ -> false

  let path_of_id i =
    let rec loop res = function
        Ident.LDot (n,i) -> loop (Next n :: res) i
      | LName n -> List.rev res, n
    in loop [] i
end


module SimpleColl = struct
  module IntSet = Set.Make (Compare.Int)
  module IntMap = Map.Make (Compare.Int)

  module StringSet = Set.Make (String)
  module StringMap = struct
    include Map.Make (String)
    let add_list bindings m =
      List.fold_left (fun m (v, c) -> add v c m) m (List.rev bindings)
  end

  module ZSet = Set.Make (struct type t = Z.t let compare = Z.compare end)

end


module Collections = struct

  include SimpleColl

  module StringIdentSet = Ident.IdSet (String)
  module StringIdentMap = struct
    include Ident.IdMap (String)
    let add_list bindings m =
      List.fold_left (fun m (v, c) -> add v c m) m (List.rev bindings)
  end

  module Array = Dune_debug.Array
end



module Constants = struct

  let current = "__CurrentContract"
  let anonymous = "__Anonymous_subcontract"
  let current_id = Ident.create_id "__CurrentContract"
  let anonymous_id = Ident.create_id "__Anonymous_subcontract"

end
