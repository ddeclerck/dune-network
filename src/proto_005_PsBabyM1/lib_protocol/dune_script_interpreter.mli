(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context

val execute :
  Alpha_context.t ->
  Script_ir_translator.unparsing_mode ->
  Script_interpreter.step_constants ->
  code:Script.expr ->
  storage:Script.expr ->
  entrypoint:string ->
  parameter:Script.expr ->
  apply_manager_operation_content:
    (Alpha_context.t -> Script_ir_translator.unparsing_mode ->
     payer:Contract.t -> source:Contract.t ->
     chain_id:Chain_id.t ->
     internal:bool ->
     'kind manager_operation ->
     (context *
      'kind Apply_results.successful_manager_operation_result *
      packed_internal_operation list) tzresult Lwt.t) ->
  Script_interpreter.execution_result tzresult Lwt.t

val execute_fee_script :
  Alpha_context.t ->
  Script_interpreter.step_constants ->
  fee_code:Script.expr ->
  storage:Script.expr ->
  entrypoint:string ->
  parameter:Dune_lang_repr.expr ->
  Script_interpreter.fee_execution_result tzresult Lwt.t

val typecheck :
  ?type_only:bool ->
  context ->
  Script.t ->
  Contract.t ->
  internal:bool ->
  ( ( Script.canonical * Contract.big_map_diff option ) * context) tzresult Lwt.t

val readable_storage :
  context -> code:Script.lazy_expr -> storage:Script.lazy_expr -> Script.expr tzresult Lwt.t

val readable_script :
  context -> Script.canonical -> Script.t tzresult Lwt.t

val normalize_script :
  context -> Script.t ->
  (Script.canonical * context) tzresult Lwt.t

val denormalize_script :
  context ->
  Script.canonical ->
  (Script.t * context) tzresult Lwt.t

val typecheck_code :
  Alpha_context.t ->
  Dune_lang_repr.expr ->
  (Script_tc_errors.type_map * Alpha_context.t) tzresult Lwt.t

val typecheck_data :
  Alpha_context.t ->
  Dune_lang_repr.expr * Dune_lang_repr.expr ->
  Alpha_context.t tzresult Lwt.t

val trace:
  Alpha_context.t ->
  Script_ir_translator.unparsing_mode ->
  Script_interpreter.step_constants ->
  code: Script.expr ->
  storage: Script.expr ->
  entrypoint:string ->
  parameter: Script.expr ->
  (Script_interpreter.execution_result *
   Script_interpreter.execution_trace) tzresult Lwt.t
