(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type dune_manager_operation =
  | Dune_activate_protocol of
      { level : int32 ;
        protocol : Protocol_hash.t option ;
        protocol_parameters : Dune_parameters_repr.parameters option }
  | Dune_manage_accounts of MBytes.t
  | Dune_manage_account of {
      (* If we want to manage another account, we must sign the `options`
         with the secrete key of that account, i.e. we need to know both
         the key of the admin and the key of the administered account. *)
      target : ( Signature.Public_key_hash.t * Signature.t option ) option ;
      options : MBytes.t ;
    }
  | Dune_clear_delegations

val encoding : dune_manager_operation Data_encoding.t

type options = {
  maxrolls : int option option ;
  admin : Contract_repr.t option option ;
  white_list : Contract_repr.t list option ;
  delegation : bool option ;
}

module Options : Dune_misc.BINARY_JSON with type t := options
