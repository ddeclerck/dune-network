(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module S = struct

  type expr =
    | Michelson_expr of Script_repr.expr
    | Dune_expr of Dune_script_repr.const
    | Dune_code of Dune_script_repr.code

  type lazy_expr = expr Data_encoding.lazy_t

  type canonical = {
    code : lazy_expr ;
    storage : lazy_expr ;
    fee_code : lazy_expr option ;
  }

  type t = {
    code : lazy_expr ;
    storage : lazy_expr ;
  }

  type script_or_hash =
    | Script of t
    | Script_hash of {
        code_hash : Script_expr_hash.t ;
        storage : lazy_expr ;
      }
    | Script_code_hash of {
        code : lazy_expr ;
        code_hash : Script_expr_hash.t ;
        storage : lazy_expr ;
      }

  open Data_encoding

  let expr_encoding =
    union [
      case (Tag 0)
        ~title:"Michelson_expr"
        Script_repr.expr_encoding
        (function
          | Michelson_expr s -> Some s
          | _ -> None)
        (fun e -> Michelson_expr e)
      ;
      case (Tag 1)
        ~title:"Dune_expr"
        (obj1 (req "dune_expr" Dune_script_repr.const_encoding))
        (function
          | Dune_expr s -> Some s
          | _ -> None)
        (fun e -> Dune_expr e)
      ;
      case (Tag 2)
        ~title:"Dune_code"
        (obj1 (req "dune_code" Dune_script_repr.code_encoding))
        (function
          | Dune_code s -> Some s
          | _ -> None)
        (fun e -> Dune_code e)
      ;
    ]

  let lazy_expr_encoding = Data_encoding.lazy_encoding expr_encoding

  let lazy_expr expr =
    Data_encoding.make_lazy expr_encoding expr

  let encoding =
    let open Data_encoding in
    def "scripted.contracts" @@
    conv
      (fun { code ; storage } -> (code, storage))
      (fun (code, storage) -> { code ; storage })
      (obj2
         (req "code" lazy_expr_encoding)
         (req "storage" lazy_expr_encoding))

  let script_or_hash_encoding =
    let open Data_encoding in
    def "scripted.opt_or_hash" @@
    union ~tag_size:`Uint8 [
      (* formerly Noscript. Can we remove it ? *)
      case ~title:"No_script" ~description:"No script"
        (Tag 0) (* = None *)
        null
        (function _ -> None)
        (fun () -> assert false) ;
      (* Script *)
      case ~title:"Full_script" ~description:"Full script"
        (Tag 0xff) (* = Some _ *)
        encoding
        (function Script s -> Some s | _ -> None)
        (fun s -> Script s) ;
      (* Script_hash *)
      case ~title:"Script_hash" ~description:"A hash of a script's code"
        (Tag 1)
        (conv
           (function
             | Script_hash { code_hash ; storage } -> (code_hash, storage)
             | _ -> assert false)
           (fun (code_hash, storage) -> Script_hash { code_hash ; storage })
           (obj2
              (req "code_hash" Script_expr_hash.encoding)
              (req "storage" lazy_expr_encoding)))
        (function Script_hash _ as s -> Some s | _ -> None)
        (fun s -> s) ;
      (* Script_code_hash *)
      case ~title:"Script_code_hash"
        ~description:"A script's with the hash of its code"
        (Tag 2)
        (conv
           (function
             | Script_code_hash { code ; code_hash ; storage } ->
                 (code, code_hash, storage)
             | _ -> assert false)
           (fun (code, code_hash, storage) ->
              Script_code_hash { code ; code_hash ; storage })
           (obj3
              (req "code" lazy_expr_encoding)
              (req "code_hash" Script_expr_hash.encoding)
              (req "storage" lazy_expr_encoding)))
        (function Script_code_hash _ as s -> Some s | _ -> None)
        (fun s -> s) ;
    ]

  let deserialized_cost = function
    | Michelson_expr v -> Script_repr.deserialized_cost v
    | Dune_expr v -> Dune_script_repr.deserialized_const_cost v
    | Dune_code v -> Dune_script_repr.deserialized_code_cost v

  let force_decode lexpr =
    let account_deserialization_cost =
      Data_encoding.apply_lazy
        ~fun_value:(fun _ -> false)
        ~fun_bytes:(fun _ -> true)
        ~fun_combine:(fun _ _ -> false)
        lexpr in
    match Data_encoding.force_decode lexpr with
    | Some v ->
        if account_deserialization_cost then
          ok (v, deserialized_cost v)
        else
          ok (v, Gas_limit_repr.free)
    | None -> error Script_repr.Lazy_script_decode

  let minimal_deserialize_cost = Script_repr.minimal_deserialize_cost

  let traversal_cost = function
    | Michelson_expr v -> Script_repr.traversal_cost (Micheline.root v)
    | Dune_expr v -> Dune_script_repr.traversal_const_cost v
    | Dune_code v -> Dune_script_repr.traversal_code_cost v

  let force_bytes expr =
    let open Gas_limit_repr in
    let account_serialization_cost =
      Data_encoding.apply_lazy
        ~fun_value:(fun v -> Some v)
        ~fun_bytes:(fun _ -> None)
        ~fun_combine:(fun _ _ -> None)
        expr in
    match Data_encoding.force_bytes expr with
    | bytes ->
        begin match account_serialization_cost with
          | Some v -> ok (bytes, traversal_cost v +@ Script_repr.serialized_cost bytes)
          | None -> ok (bytes, Gas_limit_repr.free)
        end
    | exception _ -> error Script_repr.Lazy_script_decode

  let is_unit = function
    | Michelson_expr v ->
        begin
          match Micheline.root v with
          | Prim (_, Michelson_v1_primitives.D_Unit, [], _) -> true
          | _ -> false
        end
    | Dune_expr v -> Dune_script_repr.is_unit v
    | Dune_code _ -> false

  let get_unit = function
    | Michelson_expr _ ->
        Michelson_expr (
          Micheline.strip_locations (Prim (0,
                                           Michelson_v1_primitives.D_Unit,
                                           [], []))
        )
    | Dune_expr _ -> assert false
    | Dune_code v -> Dune_expr (Dune_script_repr.get_unit v)

  let strip_annotations = function
    | Michelson_expr v ->
        Michelson_expr (
          Micheline.strip_locations (
            Script_repr.strip_annotations (Micheline.root v)))
    | (Dune_expr _ | Dune_code _) as e -> e


  let canon e = lazy_expr (Michelson_expr e)
  let mic e = canon (Micheline.strip_locations e)

  let get_michelson e =
    match e with
    | Michelson_expr e -> e
    | _ -> assert false (* TODO add an explicit error *)


end

include S

(* Open this one to have Script_repr = Dune_lang_repr *)
module For_all = struct
  module Script_michelson = Script_repr
  module Script_repr = S

  module Micheline = struct
    include Micheline
    let strip_locations e = Michelson_expr (strip_locations e)
  end
end
