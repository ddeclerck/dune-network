(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Alpha_context

let custom_root =
  (RPC_path.(open_root / "context" / "contracts") : RPC_context.t RPC_path.context)

let big_map_root =
  (RPC_path.(open_root / "context" / "big_maps") : RPC_context.t RPC_path.context)

type info = {
  balance: Tez.t ;
  delegate: public_key_hash option ;
  counter: counter option ;
  script: Script.t option ;
}

let info_encoding =
  let open Data_encoding in
  conv
    (fun {balance ; delegate ; script ; counter } ->
      (balance, delegate, script, counter))
    (fun (balance, delegate, script, counter) ->
      {balance ; delegate ; script ; counter}) @@
  obj4
    (req "balance" Tez.encoding)
    (opt "delegate" Signature.Public_key_hash.encoding)
    (opt "script" Script.encoding)
    (opt "counter" n)

module S = struct

  open Data_encoding

  let balance =
    RPC_service.get_service
      ~description: "Access the balance of a contract."
      ~query: RPC_query.empty
      ~output: Tez.encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "balance")

  let manager_key =
    RPC_service.get_service
      ~description: "Access the manager of a contract."
      ~query: RPC_query.empty
      ~output: (option Signature.Public_key.encoding)
      RPC_path.(custom_root /: Contract.rpc_arg / "manager_key")

  let delegate =
    RPC_service.get_service
      ~description: "Access the delegate of a contract, if any."
      ~query: RPC_query.empty
      ~output: Signature.Public_key_hash.encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "delegate")

  let counter =
    RPC_service.get_service
      ~description: "Access the counter of a contract, if any."
      ~query: RPC_query.empty
      ~output: z
      RPC_path.(custom_root /: Contract.rpc_arg / "counter")

  let script =
    RPC_service.get_service
      ~description: "Access the code and data of the contract."
      ~query: RPC_query.empty
      ~output: (merge_objs
                  Script.encoding
                  (obj1 (req "code_hash" Script_expr_hash.encoding)))
      RPC_path.(custom_root /: Contract.rpc_arg / "script")

  let storage =
    RPC_service.get_service
      ~description: "Access the data of the contract."
      ~query: RPC_query.empty
      ~output: Script.expr_encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "storage")

  let entrypoint_type =
    RPC_service.get_service
      ~description: "Return the type of the given entrypoint of the contract"
      ~query: RPC_query.empty
      ~output: Script.expr_encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "entrypoints" /: RPC_arg.string)


  let list_entrypoints =
    RPC_service.get_service
      ~description: "Return the list of entrypoints of the contract"
      ~query: RPC_query.empty
      ~output: (obj2
                  (dft "unreachable"
                     (Data_encoding.list
                        (obj1 (req "path" (Data_encoding.list Michelson_v1_primitives.prim_encoding))))
                     [])
                  (req "entrypoints"
                     (assoc Script.expr_encoding)))
      RPC_path.(custom_root /: Contract.rpc_arg / "entrypoints")

  let contract_big_map_get_opt =
     RPC_service.post_service
       ~description: "Access the value associated with a key in a big map of the contract (deprecated)."
       ~query: RPC_query.empty
       ~input: (obj2
                  (req "key" Script.expr_encoding)
                  (req "type" Script.expr_encoding))
       ~output: (option Script.expr_encoding)
       RPC_path.(custom_root /: Contract.rpc_arg / "big_map_get")

  let big_map_get =
    RPC_service.get_service
      ~description: "Access the value associated with a key in a big map."
      ~query: RPC_query.empty
      ~output: Script.expr_encoding
      RPC_path.(big_map_root /: Big_map.rpc_arg /: Script_expr_hash.rpc_arg)

  let info =
    RPC_service.get_service
      ~description: "Access the complete status of a contract."
      ~query: RPC_query.empty
      ~output: info_encoding
      RPC_path.(custom_root /: Contract.rpc_arg)

  let list =
    RPC_service.get_service
      ~description:
        "All existing contracts (including non-empty default contracts)."
      ~query: RPC_query.empty
      ~output: (list Contract.encoding)
      custom_root

  let parameters =
    RPC_service.get_service
      ~description: "Access the parameters of a contract, if any."
      ~query: RPC_query.empty
      ~output: Dune_operation.Options.encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "parameters")

end

let register () =
  let open Services_registration in
  register0 S.list begin fun ctxt () () ->
    Contract.list ctxt >>= return
  end ;
  let register_field s f =
    register1 s (fun ctxt contract () () ->
        Contract.exists ctxt contract >>=? function
        | true -> f ctxt contract
        | false -> raise Not_found) in
  let register_opt_field s f =
    register_field s
      (fun ctxt a1 ->
         f ctxt a1 >>=? function
         | None -> raise Not_found
         | Some v -> return v) in
  let do_big_map_get ctxt id key =
    let open Script_ir_translator in
    let ctxt = Gas.set_unlimited ctxt in
    Big_map.exists ctxt id >>=? fun (ctxt, types) ->
    match types with
    | None -> raise Not_found
    | Some (_, Script.Michelson_expr value_type) ->
        begin

          Lwt.return (parse_ty ctxt
                        ~legacy:true ~allow_big_map:false ~allow_operation:false ~allow_contract:true
                        (Micheline.root value_type))
          >>=? fun (Ex_ty value_type, ctxt) ->
          Big_map.get_opt ctxt id key >>=? fun (_ctxt, value) ->
          match value with
          | Some (Michelson_expr value) ->
              parse_data ctxt ~legacy:true value_type (Micheline.root value) >>=? fun (value, ctxt) ->
              unparse_data ctxt Readable value_type value >>=? fun (value, _ctxt) ->
              return (Dune_lang_repr.Michelson_expr
                        (Micheline.strip_locations value))
          | None | Some _ -> raise Not_found
        end
    | _ -> assert false (* TODO Love *)
  in

  register1 S.parameters
    (fun ctxt contract () () ->
       begin
         match Contract.is_implicit contract with
         | Some pkh ->
             Roll.Delegate.get_maxrolls ctxt pkh
         | None -> return None
       end >>=? fun maxrolls ->
       Contract.get_admin ctxt contract >>=? fun admin ->
       Contract.get_whitelist ctxt contract >>= fun white_list ->
       Contract.get_delegation ctxt contract >>=? fun delegation ->
       return Dune_operation.{
           maxrolls = Some maxrolls ;
           admin = Some admin ;
           white_list = Some white_list ;
           delegation = Some delegation ;
         }
    ) ;
  register_field S.balance Contract.get_balance ;
  register1 S.manager_key
    (fun ctxt contract () () ->
       match Contract.is_implicit contract with
       | None -> raise Not_found
       | Some mgr ->
           Contract.is_manager_key_revealed ctxt mgr >>=? function
           | false -> return_none
           | true -> Contract.get_manager_key ctxt mgr >>=? return_some) ;
  register_opt_field S.delegate Delegate.get ;
  register1 S.counter
    (fun ctxt contract () () ->
       match Contract.is_implicit contract with
       | None -> raise Not_found
       | Some mgr -> Contract.get_counter ctxt mgr) ;
  register_opt_field S.script
    (fun c v ->
       Contract.get_script c v >>=? fun (c, script) ->
       match script with
       | None -> return_none
       | Some script ->
           Dune_script_interpreter.denormalize_script c script >>=? fun (script, _c) ->
           Contract.hash_script_code script >>=? fun hash ->
           return_some (script, hash)) ;
  register_opt_field S.storage (fun ctxt contract ->
      Contract.get_script_code ctxt contract >>=? fun (ctxt, code) ->
      Contract.get_storage ctxt contract >>=? fun (ctxt, storage) ->
      match code, storage  with
      | None, Some _ | Some _, None -> failwith "missing code or storage"
      | None, None -> return_none
      | Some code, Some storage ->
          let ctxt = Gas.set_unlimited ctxt in
          Dune_script_interpreter.readable_storage ctxt ~code ~storage
          >>=? fun storage ->
          return_some storage) ;
  register2 S.entrypoint_type
    (fun ctxt v entrypoint () () ->
       Contract.get_script_code ctxt v >>=? fun (_, expr) ->
       match expr with
       | None -> raise Not_found
       | Some expr ->
           let ctxt = Gas.set_unlimited ctxt in
           let legacy = true in
           let open Script_ir_translator in
           Script.force_decode ctxt expr >>=? fun (expr, _) ->
           match expr with
           | Michelson_expr expr ->
               begin
                 Lwt.return
                   begin
                     parse_toplevel ~legacy expr >>? fun (arg_type, _, _, root_name, _fee_code) ->
                     parse_ty ctxt ~legacy
                       ~allow_big_map:true ~allow_operation:false
                       ~allow_contract:true arg_type >>? fun (Ex_ty arg_type, _) ->
                     Script_ir_translator.find_entrypoint ~root_name arg_type
                       entrypoint
                   end >>= function
                   Ok (_f , Ex_ty ty)->
                     unparse_ty ctxt ty >>=? fun (ty_node, _) ->
                     return (Dune_lang_repr.Michelson_expr
                               (Micheline.strip_locations ty_node))
                 | Error _ ->
                     raise Not_found
               end
           | _ ->
               assert false (* TODO Love *)) ;
  register1 S.list_entrypoints
    (fun ctxt v () () ->
       Contract.get_script_code ctxt v >>=? fun (_, expr) ->
       match expr with
       | None -> raise Not_found
       | Some expr ->
           let ctxt = Gas.set_unlimited ctxt in
           let legacy = true in
           let open Script_ir_translator in
           Script.force_decode ctxt expr >>=? fun (expr, _) ->
           match expr with
           | Michelson_expr expr ->
               Lwt.return
                 begin
                   parse_toplevel ~legacy expr >>? fun (arg_type, _, _, root_name, _fee_code) ->
                   parse_ty ctxt ~legacy
                     ~allow_big_map:true ~allow_operation:false
                     ~allow_contract:true arg_type >>? fun (Ex_ty arg_type, _) ->
                   Script_ir_translator.list_entrypoints ~root_name arg_type ctxt
                 end >>=? fun (unreachable_entrypoint,map) ->
               return
                 (unreachable_entrypoint,
                  Entrypoints_map.fold
                    begin fun entry (_,ty) acc ->
                      (entry , Dune_lang_repr.Michelson_expr (Micheline.strip_locations ty)) ::acc end
                    map [])
           | _ -> assert false (* TODO Love *)
    ) ;
  register1 S.contract_big_map_get_opt (fun ctxt contract () (key, key_type) ->
      match key, key_type with
      | Michelson_expr key, Michelson_expr key_type ->
          begin
            Contract.get_script ctxt contract >>=? fun (ctxt, script) ->
            Lwt.return (Script_ir_translator.parse_packable_ty ctxt ~legacy:true (Micheline.root key_type)) >>=? fun (Ex_ty key_type, ctxt) ->
            Script_ir_translator.parse_data ctxt ~legacy:true key_type (Micheline.root key) >>=? fun (key, ctxt) ->
            Script_ir_translator.hash_data ctxt key_type key >>=? fun (key, ctxt) ->
            match script with
            | None -> raise Not_found
            | Some { code ; storage } ->
                let open Script_ir_translator in
                begin
                  Script.force_decode ctxt code >>=? fun (code, _ctxt) ->
                  Script.force_decode ctxt storage >>=? fun (storage, _ctxt) ->
                  match code, storage with
                  | Michelson_expr code, Michelson_expr storage ->
                      return { Script_michelson.code ; storage }
                  | _ -> assert false (* TODO Love *)
                end >>=? fun script ->
                let ctxt = Gas.set_unlimited ctxt in
                parse_script ctxt ~legacy:true script >>=? fun (Ex_full_script script, ctxt) ->
                Script_ir_translator.collect_big_maps ctxt script.storage_type script.storage >>=? fun (ids, _ctxt) ->
                let ids = Script_ir_translator.list_of_big_map_ids ids in
                let rec find = function
                  | [] -> return_none
                  | (id : Z.t) :: ids -> try do_big_map_get ctxt id key >>=? return_some with Not_found -> find ids in
                find ids
          end
      | _ -> assert false (* TODO Love *)
    ) ;
  register2 S.big_map_get (fun ctxt id key () () ->
      do_big_map_get ctxt id key) ;
  register_field S.info (fun ctxt contract ->
      Contract.get_balance ctxt contract >>=? fun balance ->
      Delegate.get ctxt contract >>=? fun delegate ->
      begin match Contract.is_implicit contract with
        | Some manager ->
            Contract.get_counter ctxt manager >>=? fun counter ->
            return_some counter
        | None -> return None
      end >>=? fun counter ->
      Contract.get_script ctxt contract >>=? fun (ctxt, script) ->
      begin match script with
        | None -> return_none
        | Some script ->
            let ctxt = Gas.set_unlimited ctxt in
            Dune_script_interpreter.readable_script ctxt script >>=?
            fun script -> return_some script
      end >>=? fun script ->
      return { balance ; delegate ; script ; counter })

let parameters ctxt block contract =
  RPC_context.make_call1 S.parameters ctxt block contract () ()

let list ctxt block =
  RPC_context.make_call0 S.list ctxt block () ()

let info ctxt block contract =
  RPC_context.make_call1 S.info ctxt block contract () ()

let balance ctxt block contract =
  RPC_context.make_call1 S.balance ctxt block contract () ()

let manager_key ctxt block mgr =
  RPC_context.make_call1 S.manager_key ctxt block (Contract.implicit_contract mgr) () ()

let delegate ctxt block contract =
  RPC_context.make_call1 S.delegate ctxt block contract () ()

let delegate_opt ctxt block contract =
  RPC_context.make_opt_call1 S.delegate ctxt block contract () ()

let counter ctxt block mgr =
  RPC_context.make_call1 S.counter ctxt block (Contract.implicit_contract mgr) () ()

let script ctxt block contract =
  RPC_context.make_call1 S.script ctxt block contract () ()

let script_opt ctxt block contract =
  RPC_context.make_opt_call1 S.script ctxt block contract () ()

let storage ctxt block contract =
  RPC_context.make_call1 S.storage ctxt block contract () ()

let entrypoint_type ctxt block contract entrypoint =
  RPC_context.make_call2 S.entrypoint_type ctxt block contract entrypoint () ()

let list_entrypoints ctxt block contract =
  RPC_context.make_call1 S.list_entrypoints ctxt block contract () ()

let storage_opt ctxt block contract =
  RPC_context.make_opt_call1 S.storage ctxt block contract () ()

let big_map_get ctxt block id key =
  RPC_context.make_call2 S.big_map_get ctxt block id key () ()

let contract_big_map_get_opt ctxt block contract key =
  RPC_context.make_call1 S.contract_big_map_get_opt ctxt block contract () key

module Account_info = struct

  type t = {
    balance: Tez.t ;
    delegate: public_key_hash option ;
    counter: counter option ;
    script: Script.t option ;
    nrolls: int ;
    maxrolls : int option ;
    delegation : bool ;
    admin : Contract.t option ;
    white_list : Contract.t list ;
    is_inactive : bool ;
    desactivation_cycle : Cycle.t option ;
  }

  let encoding =
    let open Data_encoding in
    conv
      (fun {balance ; delegate ; script ; counter ;
            nrolls ; maxrolls ; delegation ; admin ; white_list ;
            is_inactive ; desactivation_cycle } ->
        (balance, delegate, script, counter,
         nrolls, maxrolls, delegation, admin, white_list,
         is_inactive, desactivation_cycle ))
      (fun (balance, delegate, script, counter,
            nrolls, maxrolls, delegation, admin, white_list,
           is_inactive, desactivation_cycle) ->
         {balance ; delegate ; script ; counter ;
          nrolls ; maxrolls ; delegation ; admin ; white_list ;
          is_inactive ; desactivation_cycle }) @@
    obj11
      (req "balance" Tez.encoding)
      (opt "delegate" Signature.Public_key_hash.encoding)
      (opt "script" Script.encoding)
      (opt "counter" n)
      (dft "nrolls" int31 0)
      (opt "maxrolls" int31)
      (dft "delegation" bool true)
      (opt "admin" Contract.encoding)
      (dft "white_list" (list Contract.encoding) [])
      (dft "is_inactive" bool false)
      (opt "desactivation_cycle" Cycle.encoding)

  let pp ppf info =
    let json = Data_encoding.Json.construct encoding info in
    Data_encoding.Json.pp ppf json

end

module DUNE = struct

  open Account_info

  let account_info =
    RPC_service.get_service
      ~description: "Access the complete status of a Dune contract."
      ~query: RPC_query.empty
      ~output: Account_info.encoding
      RPC_path.(custom_root /: Contract.rpc_arg / "info")

  let codes_root =
    (RPC_path.(open_root / "context" / "codes") : RPC_context.t RPC_path.context)

  let known_code =
    RPC_service.post_service
      ~description: "Returns the hash of a code if it is already known."
      ~query: RPC_query.empty
      ~input: Script.expr_encoding
      ~output: Script_expr_hash.encoding
      RPC_path.(codes_root / "known")

  let get_hash_code =
    RPC_service.get_service
      ~description: "Returns the code, given a hash, if it is already known."
      ~query: RPC_query.empty
      ~output: Script.expr_encoding
      RPC_path.(codes_root / "code" /: Script_expr_hash.rpc_arg)

  let register_field s f =
    Services_registration.register1 s (fun ctxt contract () () ->
        Contract.exists ctxt contract >>=? function
        | true -> f ctxt contract
        | false -> raise Not_found)

  let register () =
    let open Services_registration in

    register0 known_code
      (fun ctxt () code ->
         Contract.hash_code (Script.lazy_expr code) >>=? fun hash ->
         Contract.known_hash_code ctxt hash >>= function
         | false -> raise Not_found
         | true -> return hash) ;
    register1 get_hash_code
      (fun ctxt hash () () ->
         Contract.get_hash_code ctxt hash >>=? fun code ->
         Script.force_decode ctxt code >>|? fun (code, _ctxt) -> code);
    register_field account_info (fun ctxt contract ->
        Contract.get_balance ctxt contract >>=? fun balance ->
        Delegate.get ctxt contract >>=? fun delegate ->
        begin match Contract.is_implicit contract with
          | Some manager ->
              Contract.get_counter ctxt manager >>=? fun counter ->
              Roll.Delegate.is_inactive ctxt manager >>=? fun is_inactive ->
              Roll.Delegate.desactivation_cycle ctxt manager >>=? fun
                desactivation_cycle ->
              Roll.Delegate.get_maxrolls ctxt manager >>=? fun maxrolls ->
              Roll.Delegate.get_nrolls ctxt manager >>=? fun nrolls ->
              return (Some counter, is_inactive,
                      desactivation_cycle,
                      maxrolls, nrolls)
          | None -> return (None, false, None, None, 0)
        end >>=? fun (counter, is_inactive, desactivation_cycle,
                     maxrolls, nrolls) ->
        Contract.get_script ctxt contract >>=? fun (ctxt, script) ->
        begin match script with
          | None -> return_none
          | Some script ->
              let ctxt = Gas.set_unlimited ctxt in
              Dune_script_interpreter.readable_script ctxt script >>=?
              fun script -> return_some script
        end >>=? fun script ->
        Contract.get_delegation ctxt contract >>=? fun delegation ->
        Contract.get_admin ctxt contract >>=? fun admin ->
        Contract.get_whitelist ctxt contract >>= fun white_list ->
        return { balance ; delegate ; script ; counter ;
                 maxrolls ; nrolls ; delegation ;
                 admin ; white_list ; is_inactive ; desactivation_cycle }) ;
    ()

end

let register () =
  register ();
  DUNE.register ()

let known_code ctxt block code =
  RPC_context.make_call0 DUNE.known_code ctxt block () code

let known_code_opt ctxt block code =
  RPC_context.make_opt_call0 DUNE.known_code ctxt block () code

let get_hash_code ctxt block hash =
  RPC_context.make_call1 DUNE.get_hash_code ctxt block hash () ()

let account_info ctxt block contract =
  RPC_context.make_call1 DUNE.account_info ctxt block contract () ()
