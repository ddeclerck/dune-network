(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Love_pervasives
open Exceptions
(* open Love_context *)
open Love_primitive
open Love_value
open Value
(* open Collections *)
open Love_type

let register = Love_prim_interp.register_primitive

let gas_1 = Gas.step_cost 1
let bad_arguments p args =
  raise (InvariantBroken (
      Format.asprintf
        "Love_primitive %S called with the wrong number or kind of arguments %d"
        p.prim_name (List.length args)))

let init () =

  register
    {
      prim_name = "Int.of_nat" ;
      prim_id = 128 ;
      prim_kind = PrimFunction ;
      prim_type = TNat @=> TInt ;
      prim_arity = 1 ;
    }
    gas_1
    (fun _apply_value ctxt _env p args ->
       match args with
       | [VNat i] -> (ctxt, VInt i)
       | _ -> bad_arguments p args
    ) ;

  register
    {
      prim_name = "Nat.of_int" ;
      prim_id = 129 ;
      prim_kind = PrimFunction ;
      prim_type = TInt @=> TOption TNat ;
      prim_arity = 1 ;
    }
    gas_1
    (fun _apply_value ctxt _env p args ->
       match args with
         [ VInt i ] ->
           if Compare.Int.(Z.compare i Z.zero < 0) then (ctxt, VNone)
           else (ctxt, VSome (VNat i))
       | _ -> bad_arguments p args
    ) ;

  ()
