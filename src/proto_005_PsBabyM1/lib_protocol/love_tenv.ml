(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Ident
open Log
open Collections
open Love_ast
open Love_type

exception EnvironmentError of string

(** If set to true, the environment will make internal and abstract type definitions visible *)
let internal_mode = ref false

let set_environment_mode b = internal_mode := b

module SMap =
struct
  module M = StringMap

  (** Every element is registered with a unique ID stating the order in which they have
      been registered. *)
  let counter = ref 0

  type flag = Deleted | Registered
  type 'a elts = {
    res : 'a;
    timestamp : int;
    reg_flag : flag
  }
  type 'a t = 'a elts M.t
  let add key res map =
    let timestamp = !counter in
    counter := !counter + 1;
    M.add key {res; timestamp; reg_flag = Registered} map

  let iter f (m : 'a t) =
    M.iter (fun k r ->
       match r.reg_flag with
         Deleted when not (!internal_mode) -> ()
       | _ -> f k (r.res, r.timestamp)
    )
    m

  let filter f (m : 'a t) =
    M.filter (fun k r -> f k (r.res, r.timestamp))
    m

  let fold f (m : 'a t) acc =
    M.fold (fun k r acc ->
       match r.reg_flag with
         Deleted when not (!internal_mode) -> acc
       | _ -> f k (r.res, r.timestamp) acc
    )
    m
    acc
  let empty = M.empty
  let find_opt (key : string) (map : 'a t) =
    match M.find_opt key map with
       None -> None
     | Some {reg_flag = Deleted; _} when not !internal_mode -> None
     | Some r -> Some (r.res, r.timestamp)

  let remove e (m : 'a t) =
    match find_opt e m with
        None -> m
      | Some (res, timestamp) -> M.add e {res; timestamp; reg_flag = Deleted} m

  let add_unique key elt map =
    if M.mem key map
    then raise (EnvironmentError ("Binding " ^ key ^ " already exists"))
    else add key elt map
end

module TVSet = Love_type.TypeVarSet
module TVMap = Love_type.TypeVarMap

type typ_constructor = {
    cparent : Love_type.t;
    cname : string;
    cargs : Love_type.t list;
    crec : Love_type.recursive
  }

type typ_field = {
  fparent : Love_type.t;
  fname : string;
  ftyp : Love_type.t;
  frec : Love_type.recursive
}

type alias = {
  aparams : Love_type.type_var list;
  atype : Love_type.t;
  absolute_path : Love_type.multiple_path
}

(** When registering values in the environment,
    they can either be entry points, or simple values (like variables or functions).
    Internal is reserved for expressions inside the definition of an entry point
    or a value and are discarded after its typing.

    For example, in "let x () = let y = 5 in y + 2", 'y' is Internal and 'x' is a value
*)
type val_kind =
  | Entry
  | View
  | Value of visibility
  | Internal

type 'a t = {
  sc_parent : 'a parent; (** The parent environment. *)
  sc_local : 'a env; (** The content of the current environment *)
}

and 'a parent =
    Root
  | SubStructure of string * 'a t

and 'a env =
  {
    is_module : bool;

    dependencies : (string * string) list;

    var_typ    : (val_kind * Love_type.t) SMap.t;
    (* Variable <-> (Entry type | Value type) *)

    sum_typ : typ_constructor list SMap.t;
    (* Typename <-> typ constructor infos *)

    constr_typ : typ_constructor SMap.t;
    (* Constuctor name <-> typ constructor info *)

    record_typ : typ_field list SMap.t;
    (* Typename <-> typ fields infos *)

    field_typ : typ_field SMap.t;
    (* Field <-> typ field info  *)

    signature_types : 'a t SMap.t;
    (* Signature <-> local and parent environment *)

    contract_types : 'a contract_type SMap.t;
    (* Contracts <-> local and parent environment *)

    exception_typ : Love_type.t list SMap.t;
    (* Exception constructor <-> Exception arguments  *)

    forall_typ : TVSet.t;
    (* Registers the set of universally quantified variables.
       If a typvar is registered twice, it raises an exception. *)

    aliases : alias SMap.t; (* error when using TNMap *)
    (* The set of aliases *)

    type_kind : (type_kind * type_var list * traits) SMap.t;
    (* Each type is mapped to its kind (internal, abstract, private or public) and
       its parameters *)

    all_exceptions : StringSet.t;
    (* Every exception defined in submodules *)

    data : 'a
    (* Additional data *)
  }

and 'a contract_type = 'a t

type path = Path.t

type ('res, 'env) request_result = {
  result : 'res; (* The request result *)
  path : path; (* The relative path of the environment in which the result has been found *)
  last_env : 'env t; (* The environment in which the result has been found *)
  index : int (* The eventual index of the result *)
}

type ('res, 'env) env_request = ('res, 'env) request_result option

let str_parent =
  function
    Root -> "main"
  | SubStructure (n,_) -> n

let index_of_env env =
  match env.sc_parent with
    Root -> max_int
  | SubStructure (n, env) -> (
      match SMap.find_opt n env.sc_local.contract_types with
        Some (_,i) -> i
      | None -> (
          match SMap.find_opt n env.sc_local.signature_types with
            Some (_, i) -> i
          | None -> max_int
        )
    )

let rec root_env e =
  match e.sc_parent with
    Root -> e
  | SubStructure (n, parent_e) ->
    let new_parent =
      let sc_local = parent_e.sc_local in
      let contract_types =
        let old_structures = parent_e.sc_local.contract_types in
        SMap.add_unique n e old_structures
      in
      let new_sc_local = {sc_local with contract_types = contract_types}
      in
      {parent_e with sc_local = new_sc_local}
    in
    root_env new_parent

let env_path_str : type a. a t -> string list =
  fun (env : a t) ->
  let rec loop =
    fun (res : string list) (env : a t) ->
      match env.sc_parent with
        Root -> res
      | SubStructure (n, env) -> loop (n :: res) env
  in
  loop [] env

let pp_sep = (fun fmt () -> Format.fprintf fmt ", ")

let pp_typ_constr fmt tc =
  Format.fprintf fmt
    "%s of (%a) : %a"
    tc.cname
    (Format.pp_print_list ~pp_sep Love_type.pretty) tc.cargs
    Love_type.pretty tc.cparent

let pp_typ_field fmt tf =
  Format.fprintf fmt
    "(Field %s : %a) : %a"
    tf.fname
    Love_type.pretty tf.ftyp
    Love_type.pretty tf.fparent

let pp_typ_alias fmt al =
  Format.fprintf fmt
    "Params = %a, type = %a, absolute_path = %a"
    (Format.pp_print_list Love_type.pp_typvar) al.aparams
    Love_type.pretty al.atype
    Love_type.pp_multiple_path al.absolute_path

let pp_simap indent printer =
  fun fmt map ->
  SMap.iter
    (fun key (bnd,_) ->
       Format.fprintf fmt
         "%s%s <-> %a\n"
         indent
         key
         printer bnd
    )
    map

let pp_tvset =
  fun fmt set ->
  TVSet.iter
    (fun key ->
       Format.fprintf fmt
         "%a,\n"
         pp_typvar key
    )
    set

let pp_valkind fmt = function
  | Entry -> Format.fprintf fmt "Entry "
  | View -> Format.fprintf fmt "View "
  | Value _ -> Format.fprintf fmt "Value "
  | Internal -> Format.fprintf fmt "Internal "

let rec pp_env ?(indent="") fmt env =
  let pp_cenv = pp_cenv ~indent:(indent ^ "  ") in
  let parent = env.sc_parent in
  let env = env.sc_local in

  Format.fprintf fmt
    "%s{Structure %s\n\
     %sIs module:%b@.\
     %sVariables:\n%a@.\
     \n\
     %sTypes:\n%a@.\
     \n\
     %sSum types:\n%a@.\
     %sConstructors:\n%a@.\
     \n\
     %sRecords:\n%a@.\
     %sFields:\n%a@.\
     \n\
     %sAliases:\n%a@.\
     \n\
     %sSubcontracts:\n%a@.\
     \n\
     %sSubsignatures:\n%a@.\
     \n\
     %sQuantified:\n%a@.\
     %s}"
    indent (str_parent parent)
    indent env.is_module
    indent (pp_simap indent (fun fmt (k, t) -> Format.fprintf fmt "%a %a" pp_valkind k Love_type.pretty t)) env.var_typ
    indent (pp_simap indent pp_typ_kind) env.type_kind
    indent (pp_simap indent (Format.pp_print_list ~pp_sep pp_typ_constr)) env.sum_typ
    indent (pp_simap indent pp_typ_constr) env.constr_typ
    indent (pp_simap indent (Format.pp_print_list ~pp_sep pp_typ_field)) env.record_typ
    indent (pp_simap indent pp_typ_field) env.field_typ
    indent (pp_simap indent pp_typ_alias) env.aliases
    indent (pp_simap indent pp_cenv) env.contract_types
    indent (pp_simap indent pp_env) env.signature_types
    indent pp_tvset env.forall_typ
    indent

and pp_typ_kind fmt (k,l, _traits) = Format.fprintf fmt
    "%a : %i parameters" Love_ast.pp_type_kind k (List.length l)

and pp_cenv ~indent fmt cenv = pp_env ~indent fmt cenv

let pp_env fmt env = pp_env fmt env

let empty_env data kind =
  let is_module, dependencies =
    match kind with
      Module -> true, []
    | Contract l -> false, l
  in
  {
    is_module;
    dependencies;
    var_typ = SMap.empty;
    sum_typ = SMap.empty;
    constr_typ = SMap.empty;
    record_typ = SMap.empty;
    field_typ = SMap.empty;
    forall_typ = TVSet.empty;
    signature_types = SMap.empty;
    contract_types = SMap.empty;
    exception_typ = SMap.empty;
    type_kind = SMap.empty;
    all_exceptions = StringSet.singleton "Failure";
    aliases = SMap.empty; (* TNMap causes error *)
    data
}

let add_var kind x t env =
  debug "[add_var] Adding %s -> %a to environment@."
    x
    Love_type.pretty t;
  match kind with
    Internal ->
    {env with var_typ = SMap.add x (kind, t) env.var_typ}
  | _ ->
    {env with var_typ = SMap.add_unique x (kind, t) env.var_typ}

let add_var ?(kind=Internal) x t env = {env with sc_local = add_var kind x t env.sc_local}

let add_sum params cparent crlist crec env =
  let params = TVSet.of_list params in
  let name =
    match cparent with
      TUser (name, _) -> Ident.get_final name
    | _ -> raise (EnvironmentError "Add sum : Trying to register a non user type") in
  let cstrs, env =
    List.fold_left
      (fun (acc_tcstr, env) (cname,cargs) ->
         let tcstr = {cparent; cname; cargs; crec}
         in
         let () =
           List.iter
             (fun carg ->
                if TVSet.subset (fvars carg) params
                then ()
                else
                  raise (EnvironmentError "Missing polymorphic arguments in params.")
             )
             cargs
         in
         tcstr :: acc_tcstr,
         {env with constr_typ = SMap.add_unique cname tcstr env.constr_typ}
      )
      ([], env)
      crlist in
  {env with sum_typ = SMap.add_unique name cstrs env.sum_typ}

let add_sum params cparent crlist crec env =
  {env with sc_local = add_sum params cparent crlist crec env.sc_local}

let add_record params fparent flist frec env =
  let params = TVSet.of_list params in
  let name =
    match fparent with
      TUser (name, _) -> Ident.get_final name
    | _ -> raise (EnvironmentError "Add record : Trying to register a non user type") in

  let fts, env =
    List.fold_left
      (fun (acc_tcstr, env) (fname,ftyp) ->
         let tcstr = {fparent; fname; ftyp; frec}
         in
         let fvars_args = fvars ftyp
         in
         let () =
           if (not (TVSet.subset fvars_args params))
           then (
             debug "[add_record] %a is not a subset of %a."
               (fun fmt s -> TVSet.iter (fun e -> Format.fprintf fmt "%a" Love_type.pp_typvar e) s) fvars_args
               (fun fmt s -> TVSet.iter (fun e -> Format.fprintf fmt "%a" Love_type.pp_typvar e) s)  params;
             raise (EnvironmentError "Type record using unknwown type variables"))
           else ()
         in
         tcstr :: acc_tcstr, {env with field_typ = SMap.add_unique fname tcstr env.field_typ}
      )
      ([], env)
      flist in
  {env with record_typ = SMap.add_unique name fts env.record_typ}

let add_record params fparent frlist frec env =
  {env with sc_local = add_record params fparent frlist frec env.sc_local}

let add_forall tv env =
  if TVSet.mem tv env.forall_typ
  then
    raise (
      EnvironmentError (
        Format.asprintf "Typ %a already registered"
          Love_type.pp_typvar tv
      )
    )
  else {env with forall_typ = TVSet.add tv env.forall_typ}

let add_forall tv env =
  {env with sc_local = add_forall tv env.sc_local}

let new_subcontract_env name is_module data env =
  let sc_local =
    match is_module with
      Module -> {(empty_env data is_module) with dependencies = env.sc_local.dependencies}
    | Contract l -> ( (* The dependencies of a sub contract must be included in the root
                         contract.  *)
        let () =
          List.iter
            (fun (key, kt1) ->
               match List.assoc_opt key env.sc_local.dependencies with
                 None ->
                 raise (
                   EnvironmentError (
                     Format.asprintf "Dependency %s must be declared previously" key)
                 )
               | Some kt1' ->
                 if String.equal kt1 kt1'
                 then ()
                 else
                   raise (
                     EnvironmentError (
                       Format.asprintf
                         "Dependency %s = %s is incorrect : expected %s = %s."
                         key kt1 key kt1')
                   )
            )
            l
        in
        let root = root_env env in
        let contract_types =
          List.fold_left
            (fun acc (key,_) ->
               match SMap.find_opt key root.sc_local.contract_types with
                 None ->
                 raise (EnvironmentError "Imported module not registered in root")
               | Some (s, _) ->
                 let s = {s with sc_parent = SubStructure (key, env)} in
                 SMap.add key s acc
            )
            SMap.empty
            l
        in
        {(empty_env data is_module) with dependencies = l; contract_types}
      )
  in
  {sc_local =
     {sc_local with
      forall_typ = env.sc_local.forall_typ;
      all_exceptions = sc_local.all_exceptions;
     }
  ; sc_parent = SubStructure (name,env)}

let add_exception exc_name tlist env =
  if StringSet.mem exc_name env.all_exceptions
  then raise (EnvironmentError ("Exception " ^ exc_name ^ " registered twtce"))
  else
  {env with exception_typ = SMap.add_unique exc_name tlist env.exception_typ}

let add_exception exc_name tlist t =
  try
    {t with sc_local = add_exception exc_name tlist t.sc_local}
  with EnvironmentError _ ->
    raise (
      EnvironmentError (
        Format.asprintf "Exception %s already registered in environment %s"
          (str_parent t.sc_parent)
          exc_name
      )
    )

let add_signature name s env =
  {env with signature_types = SMap.add_unique name s env.signature_types}

let add_signature name s env =
  {env with sc_local = add_signature name s env.sc_local}

let add_abstract_type name params env =
  let sc_local = env.sc_local in
  {env with
   sc_local = {
     sc_local with
     type_kind = SMap.add_unique name (TAbstract, params, default_trait) sc_local.type_kind
   }
  }

let fst_opt =
  function
    None -> None
  | Some e -> Some (fst e)

let print_int_as_before fmt i =
  if Compare.Int.(i = max_int)
  then Format.fprintf fmt "+inf"
  else Format.fprintf fmt "%i" i

let rec add_typedef (name : string) (k : Love_ast.type_kind) (td : Love_type.typedef) env =
  debug "[add_typedef] Adding type %a as a %a type.@."
    (Love_type.pp_typdef ~name ~privacy:(Love_ast.kind_to_string k)) td
    Love_ast.pp_type_kind k
  ;
  let sc_local = env.sc_local in
  match td with
  | Alias {aparams; atype} ->
    let absolute_path atype =
      match atype with
        TUser(nname, _) ->
        debug "[add_typedef] Alias %s = %a@." name Ident.print_strident nname;
        let name_env =
          match get_env_of_id nname env
          with
            None -> raise (EnvironmentError "Trying to register an alias with an undefined type")
          | Some (e,_) -> e
        in
        debug "[add_typedef] Env of %a is %s@."
          Ident.print_strident nname
          (str_parent name_env.sc_parent);
        let path = AbsolutePath (env_path_str name_env) in
        debug "[add_typedef] Type %a is defined in %a@."
          Ident.print_strident nname
          pp_multiple_path path;
        path
      | _ -> Local
    in
    let absolute_path =
      match atype with
        TTuple l -> Multiple (List.map absolute_path l)
      | TArrow (t1, t2)
      | TMap (t1, t2)
      | TBigMap (t1, t2) -> Multiple [absolute_path t1; absolute_path t2]
      | TEntryPoint t
      | TForall (_, t)
      | TSet t
      | TOption t -> absolute_path t
      | _ -> absolute_path atype
    in
    debug "[add_typedef] Adding alias %s with absolute path %a@."
      name
      pp_multiple_path absolute_path;
    let alias = {aparams; atype; absolute_path} in
    {env with
     sc_local = {
       sc_local with
       aliases = (*TNMap*)SMap.add_unique name alias sc_local.aliases;
       type_kind =
         SMap.add_unique
           name
           (k,
            aparams,
            {tcomparable = Love_type.isComparable (fun x -> isComp x env) atype})
           sc_local.type_kind
     }}
  | SumType {sparams; scons; srec} -> (
      let params = List.map (fun v -> TVar v) sparams in
      debug "[add_typedef] Adding Sum type %s@." name;
      let t = TUser (Ident.create_id name, params) in
      let isrec =
        match srec with
          Rec -> true
        | NonRec -> false
      in
      let env =
        if isrec
        then
          add_sum
            sparams
            t
            scons
            srec
            {env with sc_local = {
                 sc_local with
                 type_kind =
                   SMap.add_unique
                     name (k, sparams, {tcomparable = true}) sc_local.type_kind
               }
            }
        else env in
      let tcomparable =
        List.for_all
          (fun (_, tl) -> List.for_all (Love_type.isComparable (fun x -> isComp x env)) tl)
          scons in
      if isrec
      then
        let sc_local = env.sc_local in
        { env with sc_local = {
              sc_local with
              type_kind =
                SMap.add
                  name (k, sparams, {tcomparable}) sc_local.type_kind
            }
        }
      else
          add_sum
            sparams
            t
            scons
            srec
            {env with sc_local = {
                 sc_local with
                 type_kind =
                   SMap.add_unique
                     name (k, sparams, {tcomparable}) sc_local.type_kind
               }
            }
    )

  | RecordType {rparams; rfields; rrec} ->
    let params = List.map (fun v -> TVar v) rparams in
    let t = TUser (Ident.create_id name, params) in
    let isrec =
      match rrec with
        Rec -> true
      | NonRec -> false
    in
    let env =
      if isrec
      then
        add_record
          rparams
          t
          rfields
          rrec
          {env with sc_local = {
               sc_local with
               type_kind =
                 SMap.add_unique name (k, rparams, {tcomparable = true}) sc_local.type_kind
             }
          }
      else env in
    let tcomparable =
      List.for_all (
        fun (_, t) -> Love_type.isComparable (fun x -> isComp x env) t)
        rfields in
    if isrec
    then
      let sc_local = env.sc_local in
      { env with sc_local = {
            sc_local with
            type_kind =
              SMap.add
                name (k, rparams, {tcomparable}) sc_local.type_kind
          }
      }
    else
      add_record
        rparams
        t
        rfields
        rrec
        {env with sc_local = {
             sc_local with
             type_kind =
               SMap.add_unique name (k, rparams, {tcomparable}) sc_local.type_kind
           }
        }

and contract_sig_to_env
    sc_parent
    (data : 'a)
    (cs : Love_type.structure_sig)
    (parent_env : 'a t)
  : 'a t =

  let new_env = empty_env data cs.sig_kind in
  (* 1. Add dependencies *)
  let new_env =
    match cs.sig_kind with
      Module -> new_env
    | Contract deps -> (
        let current_deps = parent_env.sc_local.dependencies in
        let current_modules = parent_env.sc_local.contract_types in
        List.fold_left
          (fun new_env (mod_name, kt1) ->
             match List.assoc_opt mod_name current_deps with
               Some kt1' when String.equal kt1 kt1' -> (
                 match SMap.find_opt mod_name current_modules with
                 | Some (s,_) -> {
                     new_env with
                     contract_types = SMap.add_unique mod_name s new_env.contract_types
                   }
                 | None ->
                   raise (
                     EnvironmentError ("Dependency " ^ mod_name  ^ " is not registered" ))
               )
             | _ ->
               raise (EnvironmentError ("Dependency " ^ mod_name  ^ " is incorrect" ))
          )
          new_env
          deps
      )
  in

  (* 2. Add content *)
  let add_content s content env : 'a t =
    debug "[contract_sig_to_env] Adding %s to environment" s;
    match content with
    | SType ts -> (
        match ts with
          SPublic tdef -> add_typedef s TPublic tdef env
        | SPrivate tdef -> add_typedef s TPrivate tdef env
        | SAbstract tl ->
          let sc_local = env.sc_local in
          {env with
           sc_local =
             {sc_local with
              type_kind =
                SMap.add_unique s (TAbstract, tl, default_trait) sc_local.type_kind}
          }
      )
    | SException l ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           exception_typ =
             SMap.add_unique s l sc_local.exception_typ}
      }
    | SEntry t ->
      let sc_local = env.sc_local in
      if Love_type.is_module cs
      then raise (EnvironmentError "Module registered with an entry point.")
      else {
        env with
        sc_local =
          {sc_local with
           var_typ = SMap.add_unique s (Entry, TEntryPoint t) sc_local.var_typ
          }
      }
    | SView  (t1, t2) ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           var_typ = SMap.add_unique s (View, TView (t1, t2)) sc_local.var_typ
          }
      }
    | SValue t ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           var_typ = SMap.add_unique s (Value Public, t) sc_local.var_typ
          }
      }
    | SStructure str ->
      debug "[contract_sig_to_env] Structure in signature@.";
      let sc_local = env.sc_local in
      let ctype =
        match str with
          Named n -> (
            match find_signature n env with
              None -> raise (EnvironmentError "Unregistered signature")
            | Some {result; _} -> result
          )
        | Anonymous a -> contract_sig_to_env (SubStructure (s, env)) data a env
      in {
        env with
        sc_local =
          {sc_local with
           contract_types =
             SMap.add_unique
               s
               ctype
               sc_local.contract_types
          }
      }
    | SSignature s_sig  ->
      debug "[contract_sig_to_env] Signature definition in signature@.";
      let sc_local = env.sc_local in
      let ctype = contract_sig_to_env (SubStructure (s, env)) data s_sig env
      in {
        env with
        sc_local =
          {sc_local with
           signature_types =
             SMap.add_unique
               s
               ctype
               sc_local.signature_types
          }
      }
  in
  let empty_env = {
    sc_parent;
    sc_local = new_env
  }
  in
  List.fold_left
    (fun env (s,c) -> add_content s c env)
    empty_env
    cs.sig_content

and get_env_of_id ?before id env =
  match find_env ?before id env (fun _ _ -> Some ((),max_int))
  with
    None -> None
  | Some {last_env; index; _} -> Some (last_env, index)

and find_signature x (env : 'env t) : ('env t, 'env) env_request =
  debug "[find_contract] Searching signature %a@." print_strident x;
  match Ident.split x with
    "UnitContract", None ->
    Some {
      result = {sc_local = empty_env env.sc_local.data (Contract []); sc_parent = Root};
      path = [];
      last_env = env;
      index = 0;
    }
  | _ ->
    let search =
      (fun name env -> SMap.find_opt name env.sc_local.signature_types) in
    find_env x env search

and find_env :
  type b.
  ?before : int ->
  string Ident.t -> 'a t ->
  (string -> 'a t -> (b * int) option)
  -> (b, 'a) env_request = fun
  ?before
  (id : string Ident.t)
  env
  search ->
  let test_before =
    match before with
      None -> fun _ -> true
    | Some i ->
      fun i' ->
        let res = Compare.Int.(i > i')
        in
        if res then debug "[find_env] Test before succeeded@."
        else debug "[find_env] Test before failed@."; res
  in
  let print_before fmt = function
      None -> ()
    | Some i -> print_int_as_before fmt i in
  let rec __find_env id_acc id env =
    debug "[find_env] Searching for %a in environment %s %a@."
      Ident.print_strident id
      (str_parent env.sc_parent)
      print_before before
    ;
    match Ident.split id with
    | name, None ->
      debug "[find_env] Last id@."; Some (name, env, List.rev id_acc)
    | name, Some i -> (
        match SMap.find_opt name env.sc_local.var_typ with
          Some ((_, TContractInstance ct), _) -> (
            let next_env =
              match ct with
                Anonymous s -> contract_sig_to_env Root env.sc_local.data s env
              | Named n -> (
                  match find_signature n env with
                    None -> raise (EnvironmentError "Unknown signature")
                  | Some {result; _} -> result
                )
            in
            __find_env (Path.Next name :: id_acc) i next_env
        )
        | Some _ -> raise (EnvironmentError "Variable is not a first class module")
        | None -> (
            match SMap.find_opt name env.sc_local.contract_types with
            | Some (e, index) ->
              if test_before index
              then  (
                debug "[find_env] Searching in sub structure %s = %s (index = %i)@."
                  name (str_parent e.sc_parent)
                  index;
                __find_env (Path.Next name :: id_acc) i e
              )
              else None

            | None ->
              debug "[find_env] Not found.@."; (*
          if search_sig then
            match SMap.find_opt name env.sc_local.signature_types with
              None -> None
            | Some (e,index) ->
              if test_before index then __find_env (Next name :: id_acc) i e
              else None
          else *) None
          )
      )
  in
  let search_in_parent () =
    if env.sc_local.is_module
    then (
      match env.sc_parent with
        Root ->
        debug "[find_env] Current environment has no parent@."; None
      | SubStructure (_, p) ->
        debug "[find_env] Current environment has a parent@.";
        match find_env id p search with
          None -> None
        | Some r -> Some {r with path = Path.Prev :: r.path}
    )
    else (
      debug "[find_env] Current environment is a contract : must be self contained@.";
      None
    )
  in
  match __find_env [] id env with
    None -> (
      debug "[find_env] %a cannot be resolved environment, searching in parent@."
        Ident.print_strident id;
      search_in_parent ()
    )
  | Some (name, env, path) -> (
      match search name env with
        Some (result, index) -> Some {result; path; last_env = env; index}
      | None ->
        debug
          "[find_env] %s is not in the environment %a, searching in parent@."
          name
          pp_env env
        ;
        search_in_parent ()
    )

and find_type_kind x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.type_kind in
  find_env x env search

and isComp x env =
  debug "[Love_tenv.isComp] Checking comparability of %a" Ident.print_strident x;
  match find_type_kind x env with
    None ->
    raise (
      EnvironmentError (
        Format.asprintf "Unknown type %a for checking comparability"
          Ident.print_strident x))
  | Some {result = (_,_,traits); _} -> traits.tcomparable

let contract_sig_to_env (name : string option) (data : 'a) (cs : Love_type.structure_sig) env : 'a t =
  debug "[contract_sig_to_env] Contract signature %a@."
    Love_type.pp_contract_sig cs
  ;
  let parent = match name with None -> Root | Some n -> SubStructure (n, env) in
  let res = contract_sig_to_env parent data cs env in
  debug "[contract_sig_to_env] Done@.";
  res

let find_contract x env =
  debug "[find_contract] Searching contract %a@." print_strident x;
  let search = fun id env -> SMap.find_opt id env.sc_local.contract_types in
  find_env x env search

let find_contract x env =
  if String.equal (Ident.get_final x) Constants.current
  then (
    debug "[find_contract] Current contract@.";
    Some {result = env; path = []; last_env = env; index = max_int}
  )
  else find_contract x env

let constr_with_targs tconstr args env =
  let poly_args =
    match tconstr.cparent with
      TUser (_,l) -> l
    | _ -> assert false
  in
  let () =
    if ((Compare.Int.(<>)) (List.length args) (List.length poly_args))
    then raise (EnvironmentError "Bad arguments for constructor") in
  let map =
    List.fold_left2
      (fun acc ty -> function
           TVar tv -> TVMap.add tv ty acc
         | _ -> acc
      )
      TVMap.empty
      args
      poly_args
  in
  {tconstr with cargs = List.map (Love_type.replace_map (fun x -> isComp x env) map) tconstr.cargs}

let field_with_targs tf args env =
  let poly_args =
    match tf.fparent with
      TUser (_,l) -> l
    | _ -> assert false
  in
  let () =
    if ((Compare.Int.(<>)) (List.length args) (List.length poly_args))
    then raise (EnvironmentError "Bad arguments for field") in
  let map =
    List.fold_left2
      (fun acc ty -> function
           TVar tv -> TVMap.add tv ty acc
         | _ -> acc
      )
      TVMap.empty
      args
      poly_args
  in
  {tf with ftyp = Love_type.replace_map (fun x -> isComp x env) map tf.ftyp}

let find_def_path x env =
  match find_type_kind x env with
    None -> raise (EnvironmentError "Type should be defined")
  | Some {last_env; _} -> env_path_str last_env

let relative_path env1 path =
  let rec aux from_env to_env  =
    match from_env, to_env with
      [], _ -> to_env
    | hd1 :: tl1, hd2 :: tl2 -> (
        if String.equal hd1 hd2 then aux tl1 tl2
        else to_env
      )
    | _, [] -> []
  in
  aux (env_path_str env1) path

let rec put_type_in_correct_namespace env relative_path t : Love_type.t =
  match t with
    TUser (name, p) -> (
      debug "[put_type_in_correct_namespace] Searching for type %a (relative path = %a)@."
        print_strident name
        Path.pp_path relative_path;
      match find_type_kind name env with
        Some {path; _} ->
        let path = relative_path @ path in
        debug "[put_type_in_correct_namespace] Type found, path is [%a]@."
          Path.pp_path path;
        let to_str_path p =
          List.fold_left
            (fun acc p ->
               match p with
                 Path.Prev -> acc
               | Next s -> s :: acc
            )
            []
            (Path.simplify_path p)
        in
        TUser (
          put_in_namespaces (to_str_path path) (Ident.(create_id @@ get_final name)), p)
      | None -> (
          debug "[put_type_in_correct_namespace] Type not found";
          raise (EnvironmentError "Type not found while searching its position")
        )
    )
  | TUnit
  | TBool
  | TString
  | TBytes
  | TInt
  | TNat
  | TDun
  | TKey
  | TKeyHash
  | TSignature
  | TTimestamp
  | TAddress
  | TOperation
  | TPackedStructure _
  | TContractInstance _
  | TVar _ -> t

  | TTuple l -> TTuple ((List.map (put_type_in_correct_namespace env relative_path)) l)
  | TOption t -> TOption (put_type_in_correct_namespace env relative_path t)
  | TList t  -> TList (put_type_in_correct_namespace env relative_path t)

  | TSet t -> TSet (put_type_in_correct_namespace env relative_path t)
  | TMap (t1,t2) ->
    TMap (put_type_in_correct_namespace env relative_path t1,
          put_type_in_correct_namespace env relative_path t2)
  | TBigMap (t1, t2) ->
    TBigMap (put_type_in_correct_namespace env relative_path t1,
             put_type_in_correct_namespace env relative_path t2)
  | TEntryPoint t ->
    TEntryPoint (put_type_in_correct_namespace env relative_path t)
  | TView (t1, t2) ->
    TView (put_type_in_correct_namespace env relative_path t1,
           put_type_in_correct_namespace env relative_path t2)
  | TArrow (t1, t2) ->
    TArrow (put_type_in_correct_namespace env relative_path t1,
            put_type_in_correct_namespace env relative_path t2)
  | TForall (v, t) -> TForall (v, put_type_in_correct_namespace env relative_path t)

let correct_typ_constr tcstr path last_env =
  {tcstr with cargs = List.map (put_type_in_correct_namespace last_env path) tcstr.cargs}

let correct_typ_field tfld path last_env =
  {tfld with ftyp = put_type_in_correct_namespace last_env path tfld.ftyp}

let find_sum x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.sum_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {answer with
          result = List.map (fun tc -> correct_typ_constr tc path last_env) result}

let find_record x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.record_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {answer with result = List.map (fun tf -> correct_typ_field tf path last_env) result}

let find_constr x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.constr_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some { answer with result = correct_typ_constr result path last_env }

let find_field x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.field_typ in
  match find_env (Ident.create_id x) env search with
  None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {
      answer with
      result = correct_typ_field result path last_env
    }

let find_field_in path x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.field_typ in
  match find_env (Ident.concat path (Ident.create_id x)) env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {
      answer with
      result = {result with ftyp = put_type_in_correct_namespace last_env path result.ftyp}}

let find_exception x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.exception_typ in
  find_env x env search

let is_free x env = TVSet.mem x env.sc_local.forall_typ

let get_free env = env.sc_local.forall_typ

let add_signed_subcontract str sig_name env =
  match find_signature sig_name env with
    None ->
    raise (
      EnvironmentError
        "Trying to register a signed subcontract with a non existing signature name."
    )
  | Some {result; _} ->
    let sc_local = env.sc_local in
    {env with sc_local = {
         sc_local with
         contract_types = SMap.add_unique str ({result with sc_parent = SubStructure (str, env)}) sc_local.contract_types
       }
    }


let add_typedef_in_subcontract data id k (td : typedef) env =
  let rec add_submodules id env : 'a t =
    match Ident.split id with
      mname, Some path ->
      let sub_env =
        match SMap.find_opt mname env.sc_local.contract_types with
          None -> new_subcontract_env mname Module data env
        | Some (e, _) -> e
      in
      let res_env = add_submodules path sub_env in
      let sc_local = env.sc_local in
      let new_sc_local =
        {sc_local with
         contract_types = SMap.add_unique mname res_env env.sc_local.contract_types}
      in {env with sc_local = new_sc_local}
    | tname, None -> add_typedef tname k td env
  in
  add_submodules id env

let get_sumtypedef clist =
  let scons,sparams =
    List.fold_left
      (fun (acc_cons, acc_vars) tc ->
         let vars =
           List.fold_left
             (fun acc t ->
                Love_type.fvars t |> TVSet.union acc)
             acc_vars
             tc.cargs
         in
         (tc.cname, tc.cargs) :: acc_cons, vars
      )
      ([], TVSet.empty)
      clist
  in
  let srec =
    match clist with
      {crec; _}  :: _ -> crec
    | _ -> assert false
  in
  SumType
    {sparams = TVSet.elements sparams;
     scons;
     srec}

let get_rectypedef flist =
  let rfields,rparams =
    List.fold_left
      (fun (acc_cons, acc_vars) tf ->
         let vars = Love_type.fvars tf.ftyp |> TVSet.union acc_vars
         in
         (tf.fname, tf.ftyp) :: acc_cons, vars
      )
      ([], TVSet.empty)
      flist
  in
  let rrec =
    match flist with
      {frec; _}  :: _ -> frec
    | _ -> assert false
  in
  RecordType
    {rparams = TVSet.elements rparams;
     rfields;
     rrec}

let rec env_to_contract_sig (env : 'a t) =
  let open Collections in
  let map_content =
    SMap.fold
      (fun name ((k, t), id) (acc : (string * sig_content) IntMap.t) ->
         match k, t with
             Entry, TEntryPoint t -> IntMap.add id (name,(SEntry t)) acc
           | View, TView (t1, t2) -> IntMap.add id (name,(SView (t1, t2))) acc
           | Value Public, t ->      IntMap.add id (name,(SValue t)) acc
           | (Value _ | Internal), _ -> acc
           | (Entry | View), _ ->
             raise (EnvironmentError "Entry/View registered with invalid type")
      )
      env.sc_local.var_typ
      IntMap.empty
  in (*
  let add_opt k o map =
    match o with
      None -> map
    | Some e -> SMap.add_unique k e map in *)
  let map_content =
    SMap.fold
      (fun name (elt, id) acc ->
         IntMap.add id (name, (SStructure (Anonymous (env_to_contract_sig elt)))) acc
         )
      env.sc_local.contract_types
      map_content in
  let map_content =
    SMap.fold
      (fun name (e,id) acc ->
         IntMap.add id (name, SSignature (env_to_contract_sig e)) acc)
      env.sc_local.signature_types
      map_content
  in
  let map_content =
    SMap.fold
      (fun name (t, id) acc ->
         match SMap.find_opt name env.sc_local.type_kind with
           None -> raise (EnvironmentError (
             "SumType " ^ name ^ " has no registered kind."))
         | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
         | Some ((TPublic, _,_), _) ->
           let td = get_sumtypedef t in IntMap.add id (name, SType (SPublic td)) acc
         | Some ((TPrivate, _,_), _) ->
           let td = get_sumtypedef t in IntMap.add id (name, SType (SPrivate td)) acc
         | Some ((TInternal, _,_), _) -> acc
      )
      env.sc_local.sum_typ
      map_content
  in
  let map_content =
    SMap.fold
      (fun name (t, id) acc ->
         match SMap.find_opt name env.sc_local.type_kind with
           None -> raise (EnvironmentError (
             "RecType " ^ name ^ " has no registered kind."))
         | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
         | Some ((TPublic, _,_), _) ->
           let td = get_rectypedef t in IntMap.add id (name, SType (SPublic td)) acc
         | Some ((TPrivate, _,_), _) ->
           let td = get_rectypedef t in IntMap.add id (name, SType (SPrivate td)) acc
         | Some ((TInternal, _,_), _) -> acc
      )
      env.sc_local.record_typ
      map_content
  in
  let map_content =
    SMap.fold
      (fun name ({aparams; atype; _}, id) acc ->
         match SMap.find_opt name env.sc_local.type_kind with
           None -> raise (EnvironmentError (
             "Alias " ^ name ^ " has no registered kind."))
         | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
         | Some ((TPublic, _,_), _) ->
           let td = Alias {aparams; atype} in IntMap.add id (name, SType (SPublic td)) acc
         | Some ((TPrivate, _,_), _) ->
           let td = Alias {aparams; atype} in IntMap.add id (name, SType (SPrivate td)) acc
         | Some ((TInternal, _,_), _) -> acc
      )
      env.sc_local.aliases
      map_content in
  let map_content =
    SMap.fold
      (fun name (l, id) acc -> IntMap.add id (name, SException l) acc)
      env.sc_local.exception_typ
      map_content
  in
  let sig_content =
    List.map
      (fun (_,e) -> e)
      (IntMap.bindings map_content)
  in
  {
    sig_content;
    sig_kind =
      if env.sc_local.is_module then Module else Contract (env.sc_local.dependencies);
  }

let get_data e = e.sc_local.data

let set_data d e = {e with data = d}
let set_data d e = {e with sc_local = set_data d e.sc_local}

let _alias env name args =
  match SMap.find_opt name env.sc_local.aliases with
    None -> None

  | Some ({aparams; atype; absolute_path},index) ->
    let tvmap =
      try
        List.fold_left2
          (fun acc poly arg -> TVMap.add poly arg acc)
          TVMap.empty
          aparams
          args
      with Invalid_argument _ -> raise (EnvironmentError "Incorrect number of args")
    in
    let res_type = replace_map (fun x -> isComp x env) tvmap atype in
    debug "[alias] Alias type = %a@."
      Love_type.pretty res_type;
    Some ((res_type, absolute_path), index)


let alias ?before name args env =
  debug "[alias] Searching for %a@." Ident.print_strident name;
  find_env ?before name env (fun s env -> _alias env s args)

type alias_or_exist =
    EAlias of Love_type.t * multiple_path
  | ExistButNotAlias

let alias_or_exist ?before name args env =
  debug "[alias] Searching for %a@." Ident.print_strident name;
  find_env ?before name env
    (fun s env ->
       match _alias env s args with
         Some ((a, b),i) -> Some (EAlias (a, b), i)
       | None ->
         (
           match SMap.find_opt s env.sc_local.type_kind with
             None -> None
           | Some (_, i) -> Some (ExistButNotAlias, i)
         )
    )

let is_abstract name env =
  match find_type_kind name env with
    Some {result = (TAbstract, _, _); _} -> true
  | _ -> false

let _find_tdef name env : (Love_type.typedef * int) option =
  debug "[find_tdef] Searching definition of %s@." name;
  match SMap.find_opt name env.sc_local.record_typ with
    Some (r,i) -> Some (get_rectypedef r, i)
  | None ->
    debug "[find_tdef] Not a record@.";
    match SMap.find_opt name env.sc_local.sum_typ with
      Some (s,i) -> Some (get_sumtypedef s, i)
    | None ->
    debug "[find_tdef] Not a sum@.";
    match SMap.find_opt name env.sc_local.aliases with
      Some ({aparams; atype; _},i) ->
      debug "[find_tdef] Alias of %a@." Love_type.pretty atype;
      Some (Alias {aparams; atype}, i)
    | None -> debug "[find_tdef] Not an alias either@."; None

let find_tdef typename env =
  find_env typename env _find_tdef

let get_storage_type env =
  match find_tdef (Ident.create_id "storage") env
  with
    None -> None
  | Some {result; _} -> Some (Love_type.type_of_typedef "storage" result)

(** Given a list of field * type and a user type, returns the user type on which the correct
    type arguments are given
    Ex : [a, int; b, float] {a : 'a; b : 'b} = {a : int, b : float}
    Fails if the list of fields is different than the one registered in the environment
 *)
let record_type path (l : (field_name * Love_type.t) list) (env : 'a t) : Love_type.t =
  let find_tdef e tn =
    match find_tdef tn e with
      None -> raise (EnvironmentError "Unknown typedef")
    | Some {result; _} -> result in
  let (field,typ), tl = match l with hd :: tl -> hd, tl | [] -> assert false in
  let env =
    match path with
      None -> env
    | Some i -> (
        match
          get_env_of_id
            Ident.(concat i (create_id "__tmp"))
            env
        with
          None -> raise (EnvironmentError "No environment registered for record path")
        | Some (e,_ )-> e
      )
  in
  let typename,field_typ, parent =
    match find_field field env with
      None -> raise (
        EnvironmentError (
          Format.asprintf "Unknown field %s" field))
    | Some {
        result = {fparent = TUser (name, _) as parent; ftyp; _};
        _ } ->
      name, ftyp, parent
    | Some _ -> raise (
        EnvironmentError (
          Format.asprintf "Field %s associated to a non User type" field))
  in
  let hd_aliases =
    search_aliases
      (fun x -> isComp x env)
      (find_tdef env)
      field_typ
      typ in
  let all_aliases =
    List.fold_left
      (fun acc (field,typ) ->
         let fty =
           match find_field field env with
             None -> raise (
               EnvironmentError (
                 Format.asprintf "Unknown field %s" field))
           | Some {result = {fparent = TUser (n,_); ftyp; _}; _}
             when Love_type.equal_tname n typename -> ftyp
           | Some _ ->
             raise (EnvironmentError (
                 Format.asprintf "Field %s associated to a non User type" field))
         in
         let current_aliases =
           search_aliases (fun x -> isComp x env) (find_tdef env) fty typ in
         TypeVarMap.merge
           (fun _tv b1 b2 ->
              match b1, b2 with
                None, None -> None
              | None, Some b | Some b, None -> Some b
              | Some b1, Some b2 ->
                if Love_type.bool_equal b1 b2 then Some b1
                else raise (EnvironmentError "Types are incompatible")
           )
           current_aliases
           acc
      )
      hd_aliases
      tl
  in
  let t = Love_type.replace_map (fun x -> isComp x env) all_aliases parent in
  match path, t with
    None,_ -> t
  | Some p, TUser (name, par) -> TUser (Ident.concat p name, par)
  | _,_ -> assert false

let constr_type (name, args) env : Love_type.t =
  let find_tdef e tn =
    match find_tdef tn e with
      None -> raise (EnvironmentError "Unknown typedef")
    | Some {result; _} -> result in
  let poly_args, parent =
    match find_constr name env with
      None -> raise (EnvironmentError (Format.asprintf "Constructor %a is unknown" Ident.print_strident name))
    | Some {result = t; _} -> t.cargs, t.cparent
  in
  let args, poly_args =
    let asize, psize = List.length args, List.length poly_args in
    match asize, psize with
      1, 1 -> args, poly_args
    | _, 1 -> [TTuple args], poly_args
    | a, b when Compare.Int.(=) a b -> args, poly_args
    | _,_ ->
      raise (EnvironmentError "Constructor has an incorrect number of args")
  in
  let aliases =
    List.fold_left2
      (fun acc poly arg ->
         let current_aliases =
           search_aliases (fun x -> isComp x env) (find_tdef env) poly arg in
         TypeVarMap.merge
           (fun _tv b1 b2 ->
              match b1, b2 with
                None, None -> None
              | None, Some b | Some b, None -> Some b
              | Some b1, Some b2 ->
                if Love_type.bool_equal b1 b2 then Some b1
                else raise (EnvironmentError "Types are incompatible")
           )
           current_aliases
           acc
      )
      TypeVarMap.empty
      poly_args
      args
  in
  let t = replace_map (fun x -> isComp x env) aliases parent in
  match t with
    TUser (tname, p) -> TUser (Ident.change_last name tname, p) | _ -> assert false

let find_var : Love_ast.var_ident -> 'a t -> (val_kind * Love_type.t, 'a) env_request =
  fun (x : string Ident.t) (env : 'a t) ->
  let search =
    fun name env -> SMap.find_opt name env.sc_local.var_typ
  in
  match find_env x env search with
    None -> None
  | Some {result = (c, t); path; last_env; index} ->
    debug "[find_var] Found variable %a : %a@."
      Ident.print_strident x
      Love_type.pretty t;
    let new_type = put_type_in_correct_namespace last_env path t in
    debug "[find_var] Type of variable %a : %a@."
      Ident.print_strident x
      Love_type.pretty new_type;
    Some {result = (c, new_type); path; last_env; index}

let rec is_internal t env =
  let test t = is_internal t env in
  match t with
  | TVar _
  | TUnit
  | TBool
  | TString
  | TBytes
  | TInt
  | TNat
  | TDun
  | TKey
  | TKeyHash
  | TSignature
  | TTimestamp
  | TAddress
  | TOperation
  | TContractInstance _
  | TPackedStructure _ -> false

  | TEntryPoint t -> test t

  | TMap (t,t')
  | TBigMap (t,t')
  | TArrow (t,t')
  | TView  (t,t') -> test t || test t'

  | TTuple l ->
    List.exists test l

  | TUser (name,_) -> (
      match find_type_kind name env with
        None -> raise (EnvironmentError "Type undefined")
      | Some {result = TInternal, _, _; _} -> true
      | _ -> false
    )

  | TOption t | TList t | TSet t -> test t
  | TForall (_, t) -> test t

let add_subcontract_env sc (new_env : 'a t) (env : 'a t) =
  debug "[add_subcontract_env] Adding %s to environment@." sc;
  let sc_local = env.sc_local in
  debug "[add_subcontract_env] Removing abstract and internal typedef definitions@.";
  let new_sc_local = new_env.sc_local in
  let new_sc_local =
    let remove_type tn sc_loc =
      match SMap.find_opt tn sc_loc.record_typ with
        Some (l,_) ->
        List.fold_left
          (fun acc {fname;_} ->
             debug "[add_subcontract_env] \
                    Removing field %s@." fname;
             {acc with field_typ = SMap.remove fname acc.field_typ})
          {sc_loc with record_typ = SMap.remove tn sc_loc.record_typ}
          l
      | None -> (
        match SMap.find_opt tn sc_loc.sum_typ with
            Some (l,_) ->
            List.fold_left
              (fun acc {cname;_} ->
             debug "[add_subcontract_env] \
                    Removing constructor %s@." cname;
                 {acc with constr_typ = SMap.remove cname acc.constr_typ})
              {sc_loc with sum_typ = SMap.remove tn sc_loc.sum_typ}
              l
          | None -> (
             debug "[add_subcontract_env] \
                    Removing alias %s@." tn;
              {sc_loc with aliases = SMap.remove tn sc_loc.aliases}
            )
      )
    in
    SMap.fold
      (fun typename ((tkind, _, _),_) sc_local ->
         match tkind with
         | TPublic | TPrivate ->
           debug "[add_subcontract_env] %s can remain@." typename; sc_local
         | TAbstract -> debug "[add_subcontract_env] %s is abstract@." typename;
           remove_type typename sc_local
         | TInternal -> debug "[add_subcontract_env] %s is internal@." typename;
           let sc_local = remove_type typename sc_local in
           {sc_local with type_kind = SMap.remove typename sc_local.type_kind}
      )
      new_sc_local.type_kind
      new_sc_local in
  (* Now removing values with internal type *)
  let new_sc_local =
    let new_values =
      SMap.filter
        (fun _ ((vkind,_),_) ->
           match vkind with
             Internal | Value Private -> false
           | Value _ | View | Entry  -> true
        ) new_sc_local.var_typ
    in {new_sc_local with var_typ = new_values}
  in
  {env with
   sc_local =
     {sc_local
      with
       contract_types =
         SMap.add
           sc
           {sc_parent = SubStructure (sc, env); sc_local = new_sc_local}
           sc_local.contract_types;
       all_exceptions = new_sc_local.all_exceptions
     }
  }

let empty is_module data () =
  let sc_local = empty_env data is_module in {
    sc_local;
    sc_parent = Root
  }

let add_var_path id t env =
  let idl = Ident.get_list id in
  let rec aux env = function
    | [] -> env
    | [id] -> add_var id t env
    | id :: idl ->
       let sub_env = match SMap.find_opt id env.sc_local.contract_types with
         | Some (e, _) -> e
         | None -> new_subcontract_env "__unknown_structure" Module () env
       in
       let sub_env = aux sub_env idl in
       add_subcontract_env id sub_env env
  in
  aux env idl

let normalize_type : relative:bool -> Love_type.t -> 'a t -> Love_type.t =
  fun ~relative typ env ->

  (* From a type name and the environment containing it, returns the
     correct identifyer depending on the relative flag *)
  let normalize_name (type_name : string Ident.t) (absolute_path : string list)
    : string Ident.t =
    let type_name_path =
    if relative
    then (
      debug "[normalize_name] Relative path from %s to path %a@."
        (str_parent env.sc_parent)
        (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path
      ;
      let res = relative_path env absolute_path in
      debug "[normalize_name] Relative path from %s to path %a = %a@."
        (str_parent env.sc_parent)
        (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path
        (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  res;
      res
    ) else (
      debug "[normalize_name] Absolute path %a@."
        (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path ;
      absolute_path
    )
    in
    Ident.put_in_namespaces (List.rev type_name_path) type_name
  in

  let rec normalize_by_name before name normalized_params sub_env =
    debug "[normalize] User type %a (before %a)@."
      Ident.print_strident name
      print_int_as_before before;
    let rev_fake_params, tvar_map =
      List.fold_left (
        fun (acc_fake, acc_tvars) t ->
          let tvar = Love_type.fresh_typevar () in
          (TVar tvar :: acc_fake, TVMap.add tvar t acc_tvars)
      )
        ([], TVMap.empty)
        normalized_params
    in
    let fake_params = List.rev rev_fake_params in
    match alias_or_exist ~before name fake_params sub_env with
      None ->
      raise (EnvironmentError "Cannot find environment in which type is defined")
    | Some {result = ExistButNotAlias; last_env; _} ->
      debug "[normalize] %a is not an alias, but exists@." Ident.print_strident name;
        (* Searching where it is defined *)
        let path_to_definition_env = env_path_str last_env in
        debug "[normalize] Path to definition = %a@." (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s")) path_to_definition_env;
        let name =
          normalize_name (Ident.(create_id (get_final name))) path_to_definition_env
        in
        debug "[normalize] Final type name = %a@." Ident.print_strident name;
        TUser (name, normalized_params)
      | Some {result = EAlias (TUser (name, params), _); last_env; index; _} ->
        (* We must normalize name and TUser separately. *)
        let new_params = List.map (fun t -> normalize before t sub_env) params in
        let t = normalize_by_name index name new_params last_env in
        Love_type.replace_map (fun x -> isComp x env) tvar_map t
      | Some {result = EAlias (t, path); last_env; index; _} ->
        debug "[normalize] Path to environment = %a@."
          pp_multiple_path path;
        let t = normalize index t last_env in
        Love_type.replace_map (fun x -> isComp x env) tvar_map t

  and normalize : int -> Love_type.t -> 'a t -> Love_type.t =
    fun before typ sub_env ->
      debug "[normalize] Type %a (before %a)@."
        Love_type.pretty typ
        print_int_as_before before;
      match typ with
        TUser (name, params) ->
        let params = List.map (fun t -> normalize before t sub_env) params in
        normalize_by_name before name params sub_env
      | TVar _
      | TUnit
      | TBool
      | TString
      | TBytes
      | TInt
      | TNat
      | TDun
      | TKey
      | TKeyHash
      | TSignature
      | TTimestamp
      | TAddress
      | TOperation -> typ

      | TEntryPoint pt ->
        TEntryPoint (normalize before pt sub_env)
      | TView (pt, rt) ->
        TView (normalize before pt sub_env, normalize before rt sub_env)
      | TMap (pt,rt) ->
        TMap (normalize before pt sub_env, normalize before rt sub_env)
      | TBigMap (pt,rt) ->
        TBigMap (normalize before pt sub_env, normalize before rt sub_env)
      | TArrow (pt,rt) ->
        TArrow (normalize before pt sub_env, normalize before rt sub_env)

      | TTuple l ->
        TTuple (List.map (fun typ -> normalize before typ sub_env) l)

      | TOption t -> TOption (normalize before t sub_env)

      | TList t -> TList (normalize before t sub_env)
      | TSet t  -> TSet (normalize before t sub_env)
      | TForall (v, t) -> TForall (v, normalize before t sub_env)
      | TContractInstance _
      | TPackedStructure _ -> typ (* Warning : no normalization on contracts ?*)

  in
  debug "[normalize] Normalizing type %a@." Love_type.pretty typ;
  let res = normalize max_int typ env in
  debug "[normalize] Normalized Type %a = %a@."
    Love_type.pretty typ
    Love_type.pretty res;
  res

(* Allows to stop the next function when the first loop function returns *)
exception Stop of string Ident.t
let correct_absolute_name_with_kt1 (name : string Ident.t) (env : 'a t) : string Ident.t =
  let rec loop name =
    match Ident.split name with
      n, None -> (
        match List.assoc_opt n env.sc_local.dependencies with
          None -> name
        | Some kt1 -> Ident.create_id kt1
      )
    | n, Some i -> (
        match List.assoc_opt n env.sc_local.dependencies with
          None -> name
        | Some kt1 ->
          let id = Ident.put_in_namespace kt1 (loop i)
          in raise (Stop id)
      )
  in
  try loop name with Stop i -> i

let rec correct_absolute_type_with_kt1 (t : Love_type.t) (env : 'a t) : Love_type.t =
  let f t = correct_absolute_type_with_kt1 t env in
  match t with
  | TUnit
  | TBool
  | TString
  | TBytes
  | TInt
  | TNat
  | TDun
  | TKey
  | TKeyHash
  | TSignature
  | TTimestamp
  | TAddress
  | TContractInstance (Named _)
  | TPackedStructure (Named _) -> t
  | TContractInstance (Anonymous n) -> correct_absolute_type_with_kt1_struct n env
  | TPackedStructure (Anonymous n) -> correct_absolute_type_with_kt1_struct n env
  | TEntryPoint t -> TEntryPoint (f t)
  | TView (t1, t2) -> TView (f t1, f t2)
  | TSet e -> TSet (f e)
  | TMap (t1, t2) -> TMap (t1, t2)

  | TBigMap (t1, t2) -> TBigMap (f t1, f t2)
  | TOperation -> t

  | TTuple l -> TTuple (List.map f l)
  | TUser (n, l) -> TUser (correct_absolute_name_with_kt1 n env, List.map f l)
  | TOption t -> TOption (f t)
  | TList t  -> TList (f t)

  | TArrow (t1, t2) -> TArrow (f t1, f t2)
  | TVar _ -> t
  | TForall (v, t) ->  TForall (v, f t)

and correct_absolute_type_with_kt1_struct _ = failwith "TODO"

let get_subenv_struct str env =
  match SMap.find_opt str env.sc_local.contract_types with
    None -> None
  | Some (e, _) -> Some e

let get_subenv_sig str env =
  match SMap.find_opt str env.sc_local.signature_types with
    None -> None
  | Some (e, _) -> Some e
