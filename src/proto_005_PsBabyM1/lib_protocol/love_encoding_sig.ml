(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module type S = sig

  module Type : sig
    val encoding : Love_type.t Data_encoding.t
  end

  module Ast : sig
    val const_encoding :
      (Love_ast.raw_const, Love_ast.location option)
        Love_pervasives.Utils.annoted Data_encoding.t
    val exp_encoding :
      (Love_ast.raw_exp, Love_ast.location option)
        Love_pervasives.Utils.annoted Data_encoding.t
    val structure_encoding : Love_ast.structure Data_encoding.t
    val top_contract_encoding : Love_ast.top_contract Data_encoding.t
  end

  module RuntimeAst : sig
    val const_encoding : Love_runtime_ast.const Data_encoding.t
    val exp_encoding : Love_runtime_ast.exp Data_encoding.t
    val structure_encoding : Love_runtime_ast.structure Data_encoding.t
    val top_contract_encoding : Love_runtime_ast.top_contract Data_encoding.t
  end

  module Value : sig
    val encoding : Love_value.Value.t Data_encoding.t
    val live_structure_encoding : Love_value.LiveStructure.t Data_encoding.t
    val live_contract_encoding : Love_value.LiveContract.t Data_encoding.t
    val fee_code_encoding : Love_value.FeeCode.t Data_encoding.t

  end
end
