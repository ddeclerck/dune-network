(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(*
   The new operation that we want to add is along
   Activate_protocol_parameters { level ; protocol_parameters }
   We could also change the protocol using `Updater.activate`.

   Also, the following operations do not work on Dune:
   * Proposals
   * Ballot
   They are disabled in apply.ml
*)


(* If you add a new operation here, you will need:
   * to define a new encoding case in this file for the new operation
   * to define a result for the execution of the new operation, in the file
      `lib_protocol/dune_apply_result.ml`, together with its encoding.
      Also, the encodings for operations are re-implemented there.
   * to modify `lib_protocol/dune_apply.ml` to execute the new operation,
     and return its apply result
   * to modify `lib_client/dune_operation_result.ml` to print the
     new operation result.
*)

type dune_manager_operation =
  | Dune_activate_protocol of
      { level : int32 ;
        protocol : Protocol_hash.t option ;
        protocol_parameters : Dune_parameters_repr.parameters option }

  | Dune_manage_accounts of MBytes.t
  (* Binary JSON encoding of Dune_parameters.Accounts.t *)

  | Dune_manage_account of {
      (* If we want to manage another account, we must sign the `options`
         with the secrete key of that account, i.e. we need to know both
         the key of the admin and the key of the administered account. *)
      target : ( Signature.Public_key_hash.t * Signature.t option ) option ;
      options : MBytes.t ;
    }

  | Dune_clear_delegations

(* These operations are included in:
   | Manager_operation : {
      source: Contract_repr.contract ;
      fee: Tez_repr.tez ;
      counter: counter ;
      operation: 'kind manager_operation ;
      gas_limit: Z.t;
      storage_limit: Z.t;
    }
*)

open Data_encoding

let case tag name args proj inj =
  let open Data_encoding in
  case tag
    ~title:(String.capitalize_ascii name)
    (merge_objs
       (obj1 (req "dune_kind" (constant name)))
       args)
    (fun x -> match proj x with None -> None | Some x -> Some ((), x))
    (fun ((), x) -> inj x)

let activate_protocol_case =
  case
    (Tag 0)
    "dune_activate_protocol"
    (obj3
       (req "level" int32)
       (opt "protocol" Protocol_hash.encoding)
       (opt "protocol_parameters" Dune_parameters_repr.parameters_encoding))
    (function
      | Dune_activate_protocol { level ; protocol ; protocol_parameters } ->
          Some ( level, protocol, protocol_parameters )
      | _ -> None
    )
    (function (level , protocol , protocol_parameters ) ->
       Dune_activate_protocol { level ; protocol ; protocol_parameters })

let manage_accounts_case =
  case
    (Tag 1)
    "dune_manage_accounts"
    (obj1 (req "bytes" (dynamic_size Variable.bytes)))
    (function
      | Dune_manage_accounts bytes -> Some bytes
      | _ -> None
    )
    (function bytes -> Dune_manage_accounts bytes)

let manage_account_case =
  case
    (Tag 2)
    "dune_manage_account"
    (obj2
       (opt "target"
          (obj2
             (req "target" Signature.Public_key_hash.encoding)
             (opt "signature" Signature.encoding)))
       (req "options" (dynamic_size Variable.bytes)))
    (function
      | Dune_manage_account { target ; options } -> Some (target, options)
      | _ -> None
    )
    (function (target, options) -> Dune_manage_account { target ; options })

let clear_delegations_case =
  case
    (Tag 3)
    "dune_clear_delegations"
    Data_encoding.empty
    (function
      | Dune_clear_delegations-> Some ()
      | _ -> None
    )
    (function () -> Dune_clear_delegations)

let encoding =
  union
    ~tag_size:`Uint8 [

    activate_protocol_case ;
    manage_accounts_case ;
    manage_account_case ;
    clear_delegations_case ;
  ]
  (* Dune: all Dune operations are preceeded by their size *)
  |> dynamic_size

type options = {
  maxrolls : int option option ;
  admin : Contract_repr.t option option ;
  white_list : Contract_repr.t list option ;
  delegation : bool option ;
}

let options_encoding =
  conv
    (fun
      { maxrolls ; admin ; white_list ; delegation } ->
      ( maxrolls , admin , white_list , delegation ) )
    (fun
      ( maxrolls , admin , white_list , delegation ) ->
      { maxrolls ; admin ; white_list ; delegation } )
    (obj4
       (opt "maxrolls" (option int31))
       (opt "admin" (option Contract_repr.encoding))
       (opt "white_list" (list Contract_repr.encoding))
       (opt "delegation" bool)
       )


module Options = Dune_misc.MakeBinaryJson(struct
    type t = options
    let encoding = options_encoding
    let name = "Dune_manage_account options"
  end)
