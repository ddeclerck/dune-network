(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections
open Utils
open Data_encoding

let strbnd_to_map b =
  Utils.bindings_to_map StringMap.add StringMap.empty b

let encode_string s =
  `String s

let decode_string = function
  | `String s -> s
  | _ -> raise (DecodingError "Bad string")

let encode_ident id =
  `A (List.map encode_string (Ident.get_list id))

let decode_ident =
  let open Ident in
  function
  | `A idl ->
     let idl = List.map decode_string idl in
     begin match List.rev idl with
     | id :: l -> put_in_namespaces l (create_id id)
     | _ -> raise (DecodingError "Bad ident (empty list)")
     end
  | _ -> raise (DecodingError "Bad ident")

let encode_trait =
  let open Love_type in
  function
  | { tcomparable } -> `Bool tcomparable

let decode_trait =
  let open Love_type in
  function
  | `Bool tcomparable -> { tcomparable }
  | _ -> raise (DecodingError "Bad trait")

let encode_visibility =
  function
  | Love_ast.Private -> `String "private"
  | Public -> `String "public"

let decode_visibility =
  function
  | `String "private" -> Love_ast.Private
  | `String "public" -> Public
  | _ -> raise (DecodingError "Bad visibility")

let encode_pair1 encode (d1, d2) =
  `A [ encode d1; encode d2 ]

let decode_pair1 decode = function
  | `A [ d1; d2 ] -> (decode d1, decode d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_pair2 encode1 encode2 (d1, d2) =
  `A [ encode1 d1; encode2 d2 ]

let decode_pair2 decode1 decode2 = function
  | `A [ d1; d2 ] -> (decode1 d1, decode2 d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_tuple3 encode1 encode2 encode3 (d1, d2, d3) =
  `A [encode1 d1; encode2 d2; encode3 d3]

let decode_tuple3 decode1 decode2 decode3 = function
    `A [d1; d2; d3] -> (decode1 d1, decode2 d2, decode3 d3)
  | _ -> raise (DecodingError "Bad tuple3")

let encode_option encode = function
  | Some e -> `O [ "some", encode e ]
  | None -> `String "none"

let decode_option decode = function
  | `O [ "some", e ] -> Some (decode e)
  | `String "none" -> None
  | _ -> raise (DecodingError "Bad option")

let encode_list encode = function
  | [] -> `Null
  | l -> `A (List.map encode l)

let decode_list decode = function
  | `Null -> []
  | `A l -> (List.map decode l)
  | _ -> raise (DecodingError "Bad list")

let encode_bindings_list encode l =
  `O (List.map (fun (s, d) -> s, encode d) l)

let decode_bindings_list decode = function
  | `O l -> List.map (fun (s, d) -> s, decode d) l
  | _ -> raise (DecodingError "Bad bindings list")

module TypeEncoder = struct

  open Love_type

  let arrow_to_list t =
    let rec aux l = function
    | TArrow (t1, t2) -> aux (t1 :: l) t2
    | t -> t :: l
    in
    List.rev (aux [] t)

  let encode_rec = function
    | Rec -> `String "rec"
    | NonRec -> `String "nonrec"

  let rec encode_type : Love_type.t -> Data_encoding.json = function
    | TUnit -> `String "unit"
    | TBool -> `String "bool"
    | TString -> `String "string"
    | TBytes -> `String "bytes"
    | TInt -> `String "int"
    | TNat -> `String "nat"
    | TOption t -> `O [ "option", encode_type t ]
    | TTuple tl -> `O [ "tuple", encode_list encode_type tl ]
    | TUser (tn, tl) ->
       `O [ "user", `A [ encode_tname tn; encode_list encode_type tl ] ]
    | TList t -> `O [ "list", encode_type t ]
    | TSet t -> `O [ "set", encode_type t ]
    | TMap (t1, t2) -> `O [ "map", encode_pair1 encode_type (t1, t2) ]
    | TBigMap (t1, t2) -> `O [ "bigmap", encode_pair1 encode_type (t1, t2) ]
    | TDun -> `String "dun"
    | TKey -> `String "key"
    | TKeyHash -> `String "keyhash"
    | TSignature -> `String "signature"
    | TTimestamp -> `String "timestamp"
    | TAddress -> `String "address"
    | TOperation -> `String "operation"
    | TContractInstance ct -> `O [ "instance", encode_structure_type ct ]
    | TPackedStructure st -> `O [ "structure", encode_structure_type st ]
    | TEntryPoint t -> `O [ "entry", encode_type t ]
    | TView (t1, t2) -> `O [ "view", encode_pair1 encode_type (t1, t2) ]
    | TArrow _ as t -> `O [ "arrow", encode_list encode_type (arrow_to_list t) ]
    | TVar tv -> `O [ "var", encode_tvar tv ]
    | TForall (tv, t) -> `O [ "forall", `A [ encode_tvar tv; encode_type t ] ]

  and encode_structure_type = function
    | Named ctn -> encode_ident ctn (* Array *)
    | Anonymous cs -> encode_signature cs (* Object *)

  and encode_contract_type = function
    | StructType st -> `O [ "struct_type", encode_structure_type st ]
    | ContractInstance cn -> `O [ "contract_instance", encode_ident cn ]

  and encode_typedef = function
    | Alias { aparams; atype } ->
       `O [ "targs", encode_list encode_tvar aparams;
            "type", encode_type atype ]
    | SumType { sparams; scons; srec } ->
       `O [ "targs", encode_list encode_tvar sparams;
            "constr", encode_bindings_list (encode_list encode_type) scons;
            "recursive", encode_rec srec
          ]
    | RecordType { rparams; rfields; rrec } ->
       `O [ "targs", encode_list encode_tvar rparams;
            "fields", encode_bindings_list encode_type rfields;
            "recursive", encode_rec rrec ]

  and encode_sig_type = function
    | SPublic td -> `O ["public", encode_typedef td]
    | SPrivate td -> `O ["private", encode_typedef td]
    | SAbstract tl -> `O ["abstract", encode_list encode_tvar tl]

  and encode_sig_content = function
    | SType s -> `O ["type", encode_sig_type s]
    | SException tl -> `O ["exception", encode_list encode_type tl]
    | SEntry t ->
       `O ["entry_param", encode_type t]
    | SView (t1, t2) ->
       `O ["view_param", encode_type t1; "view_result", encode_type t2]
    | SValue t -> `O ["value", encode_type t]
    | SStructure st -> `O ["structure", encode_structure_type st]
    | SSignature s -> `O ["signature", encode_signature s]

  and encode_kind : Love_type.struct_kind -> Data_encoding.json = function
    | Module -> `String "module"
    | Contract l ->
      `O [ "contract", encode_list (encode_pair1 encode_string) l]

  and encode_signature { sig_kind; sig_content; } : Data_encoding.json =
    `O [ "kind", encode_kind sig_kind;
         "content", encode_list
           (encode_pair2 encode_string encode_sig_content) sig_content ]

  and encode_tvar { tv_name; tv_traits } =
    `O [ tv_name, encode_trait tv_traits ]

  and encode_tname = encode_ident


  and encode_exception (exn, tl) =
    `O [ "exn", `String exn; "args", encode_list encode_type tl ]

end

module TypeDecoder = struct

  open Love_type

  let list_to_arrow l =
    let rec aux t2 = function
      | t1 :: l -> aux (TArrow (t1, t2)) l
      | [] -> t2
    in
    match List.rev l with
    | t :: l ->  aux t l
    | [] -> raise (DecodingError "Empty type list")

  let decode_rec = function
      `String "rec" -> Rec
    | `String "nonrec" -> NonRec
    | _ -> raise (DecodingError "Unknown recurisve flag")

  let rec decode_type : Data_encoding.json -> Love_type.t = function
    | `String "unit" -> TUnit
    | `String "bool" -> TBool
    | `String "string" -> TString
    | `String "bytes" -> TBytes
    | `String "int" -> TInt
    | `String "nat" -> TNat
    | `O [ "option", t ] -> TOption (decode_type t)
    | `O [ "tuple", tl ] -> TTuple (decode_list decode_type tl)
    | `O [ "user", `A [ tn; tl ] ] ->
       TUser (decode_tname tn, decode_list decode_type tl)
    | `O [ "list", t ] -> TList (decode_type t)
    | `O [ "set", t ] -> TSet (decode_type t)
    | `O [ "map", `A [ t1; t2 ] ] -> TMap (decode_type t1, decode_type t2)
    | `O [ "bigmap", `A [ t1; t2 ] ] -> TBigMap (decode_type t1, decode_type t2)
    | `String "dun" -> TDun
    | `String "key" -> TKey
    | `String "keyhash" -> TKeyHash
    | `String "signature" -> TSignature
    | `String "timestamp" -> TTimestamp
    | `String "address" -> TAddress
    | `String "operation" -> TOperation
    | `O [ "instance", ct ] -> TContractInstance (decode_structure_type ct)
    | `O [ "structure", st ] -> TPackedStructure (decode_structure_type st)
    | `O [ "entry", t ] -> TEntryPoint (decode_type t)
    | `O [ "view", `A [ t1; t2 ] ] -> TView (decode_type t1, decode_type t2)
    | `O [ "arrow", tl ] -> list_to_arrow (decode_list decode_type tl)
    | `O [ "var", tv ] -> TVar (decode_tvar tv)
    | `O [ "forall", `A [ tv; t ] ] -> TForall (decode_tvar tv, decode_type t)
    | _ -> raise (DecodingError "Bad type")

  and decode_structure_type = function
    | (`A _) as sn -> Named (decode_ident sn) (* Array *)
    | (`O _) as s -> Anonymous (decode_signature s) (* Object *)
    | _ -> raise (DecodingError "Bad structure type")

  and decode_contract_type = function
    | `O [ "struct_type", st ] -> StructType (decode_structure_type st)
    | `O [ "contract_instance", cn ] -> ContractInstance (decode_ident cn)
    | _ -> raise (DecodingError "Bad contract type")

  and decode_sig_content = function
    | `O ["type", s] -> SType (decode_sigtype s)
    | `O ["exception", tl] -> SException (decode_list decode_type tl)
    | `O ["entry_param", t] -> SEntry (decode_type t)
    | `O ["view_param", t1; "view_result", t2] ->
       SView (decode_type t1, decode_type t2)
    | `O ["value", t] -> SValue (decode_type t)
    | `O ["structure", st] -> SStructure (decode_structure_type st)
    | `O ["signature", s] -> SSignature (decode_signature s)
    | _ -> raise (DecodingError "Bad signature content")

  and decode_kind = function
    | `String "module" -> Module
    | `O [ "contract", l ] -> Contract (decode_list (decode_pair1 decode_string) l)
    | _ -> raise (DecodingError "Bad structure kind")

  and decode_signature = function
    | `O [
           "kind", kind;
           "content", content;
         ] -> {
        sig_kind = decode_kind kind;
        sig_content =
          decode_list (decode_pair2 decode_string decode_sig_content) content }
    | _ -> raise (DecodingError "Bad signature")

  and decode_tvar = function
    | `O [ tv_name, tv_traits ] ->
       { tv_name = tv_name; tv_traits = decode_trait tv_traits }
    | _ -> raise (DecodingError "Bad type variable")

  and decode_tname = decode_ident

  and decode_sigtype = function
    | `O ["public", td] -> SPublic (decode_typedef td)
    | `O ["private", td] -> SPrivate (decode_typedef td)
    | `O ["abtract", tl] -> SAbstract (decode_list decode_tvar tl)
    | _ -> raise (DecodingError "Bad sigtype definition")

  and decode_typedef = function
    | `O [ "targs", aparams; "type", atype ] ->
       Alias { aparams = decode_list decode_tvar aparams;
               atype = decode_type atype }
    | `O [ "targs", sparams; "constr", scons; "recursive", srec ] ->
       SumType { sparams = decode_list decode_tvar sparams;
                 scons = decode_bindings_list (decode_list decode_type) scons;
                 srec = decode_rec srec
               }
    | `O [ "targs", rparams; "fields", rfields; "recursive", rrec ] ->
       RecordType { rparams = decode_list decode_tvar rparams;
                    rfields = decode_bindings_list decode_type rfields;
                    rrec = decode_rec rrec }
    | _ -> raise (DecodingError "Bad type definition")

  and decode_exception = function
    | `O [ "exn", `String exn; "args", tl ] ->
      exn, decode_list decode_type tl
    | _ -> raise (DecodingError "Bad exception definition")

end

module Type = struct
  let encoding =
    conv TypeEncoder.encode_type TypeDecoder.decode_type json
end


module PrimEncoder = struct

  open Love_primitive

  let encode_prim : Love_primitive.t -> Data_encoding.json = function
  | PCompare -> `String "comp"
  | PEq -> `String "eq" | PNe -> `String "ne"
  | PLt -> `String "lt" | PLe -> `String "le"
  | PGt -> `String "gt" | PGe -> `String "ge"
  | PIAdd -> `String "iadd" | PISub -> `String "isub"
  | PIMul -> `String "imul" | PIDiv -> `String "idiv"
  | PINeg -> `String "ineg" | PIAbs -> `String "iabs"
  | PNAdd -> `String "nadd" | PNSub -> `String "nsub"
  | PNMul -> `String "nmul" | PNDiv -> `String "ndiv"
  | PNNeg -> `String "nneg" | PNAbs -> `String "nabs"
  | PNIAdd -> `String "niadd" | PINAdd -> `String "inadd"
  | PNISub -> `String "nisub" | PINSub -> `String "insub"
  | PNIMul -> `String "nimul" | PINMul -> `String "inmul"
  | PNIDiv -> `String "nidiv" | PINDiv -> `String "indiv"
  | PDAdd  -> `String "dadd"  | PDSub  -> `String "dsub"
  | PDIMul -> `String "dimul" | PIDMul -> `String "idmul"
  | PDNMul -> `String "dnmul" | PNDMul -> `String "ndmul"
  | PDIDiv -> `String "didiv" | PDNDiv -> `String "dndiv"
  | PDDiv  -> `String "ddiv"
  | PTIAdd -> `String "tiadd" | PITAdd -> `String "itadd"
  | PTNAdd -> `String "tnadd" | PNTAdd -> `String "ntadd"
  | PTISub -> `String "tisub" | PTNSub -> `String "tnsub"
  | PTSub  -> `String "tsub"
  | PBAnd  -> `String "and"   | PBOr   -> `String "or"
  | PBXor  -> `String "xor"   | PBNot  -> `String "not"
  | PIAnd  -> `String "land"  | PIOr   -> `String "lor"
  | PIXor  -> `String "lxor"  | PINot  -> `String "lnot"
  | PILsl  -> `String "lsl"   | PILsr  -> `String "lsr"
  | PNAnd  -> `String "nland" | PNOr   -> `String "nlor"
  | PNXor  -> `String "nlxor"
  | PNLsl  -> `String "nlsl"  | PNLsr   -> `String "nlsr"
  | PLLength->`String "llen"  | PLConcat  -> `String "lcat"
  | PLCons -> `String "lcons" | PLRev     -> `String "lrev"
  | PLIter -> `String "liter" | PLMap     -> `String "lmap"
  | PLFold -> `String "lfold" | PLMapFold -> `String "lmfld"
  | PSCard -> `String "scard" | PSEmpty   -> `String "sempt"
  | PSAdd  -> `String "sadd"  | PSRemove  -> `String "srem"
  | PSIter -> `String "siter" | PSMap     -> `String "smap"
  | PSFold -> `String "sfold" | PSMapFold -> `String "smfld"
  | PSMem  -> `String "smem"
  | PMCard -> `String "mcard" | PMEmpty   -> `String "mempt"
  | PMAdd  -> `String "madd"  | PMRemove  -> `String "mrem"
  | PMMem  -> `String "mmem"  | PMFind    -> `String "mfind"
  | PMIter -> `String "miter" | PMMap     -> `String "mmap"
  | PMFold -> `String "mfold" | PMMapFold -> `String "mmfld"
  | PBMEmpty (tk, tv) -> `O [ "bmempt", `A [
      TypeEncoder.encode_type tk; TypeEncoder.encode_type tv ] ]
  | PBMAdd  -> `String "bmadd"  | PBMRemove  -> `String "bmrem"
  | PBMMem  -> `String "bmmem"  | PBMFind    -> `String "bmfind"
  | PLLoop  -> `String "lloop"
  | PSLength -> `String "slen"  | PSConcat   -> `String "scat"
  | PSSlice  -> `String "sslce"
  | PBLength -> `String "blen"  | PBConcat   -> `String "bcat"
  | PBSlice  -> `String "bslce" | PBPack     -> `String "bpack"
  | PBUnpack t-> `O [ "bupck", TypeEncoder.encode_type t ]
  | PBHash    -> `String "hash"
  | PCBlake2b -> `String "blk2b" | PCSha256  -> `String "sh256"
  | PCSha512  -> `String "sh512"
  | PCHashKey -> `String "hashk" | PCCheck   -> `String "check"
  | PCSetDeleg-> `String "deleg" | PCAddress -> `String "addr"
  | PCSelf ct -> `O [ "self", TypeEncoder.encode_contract_type ct ]
  | PCCall    -> `String "call"  | PCView    -> `String "cview"
  | PCCreate  -> `String "corig"
  | PCAt ct   -> `O [ "at", TypeEncoder.encode_contract_type ct ]
  | PADefault -> `String "deflt" | PATransfer -> `String "trnsf"
  | PABalanceOf->`String "abalof"
  | PAGetInfo  -> `String "ainfo"| PAManage   -> `String "amang"
  | PCBalance -> `String "bal"   | PCTime     -> `String "now"
  | PCAmount  -> `String "amnt"  | PCGas     -> `String "gas"
  | PCrSelf   -> `String "cself" | PCSource   -> `String "srce"
  | PCSender  -> `String "sndr"
  | PCLevel   -> `String "level" | PCCycle   -> `String "cycle"
  | PAddressOfKeyHash -> `String "aofkh"
  | PKeyHashOfAddress -> `String "khofa"
  | PGenericPrimitive _ as p -> `String ( Love_primitive.name p )
end

module PrimDecoder = struct

  open Love_primitive

  let decode_prim : Data_encoding.json -> Love_primitive.t = function
  | `String "comp" -> PCompare
  | `String "eq" -> PEq | `String "ne" -> PNe
  | `String "lt" -> PLt | `String "le" -> PLe
  | `String "gt" -> PGt | `String "ge" -> PGe
  | `String "iadd" -> PIAdd | `String "isub" -> PISub
  | `String "imul" -> PIMul | `String "idiv" -> PIDiv
  | `String "ineg" -> PINeg | `String "iabs" -> PIAbs
  | `String "nadd" -> PNAdd | `String "nsub" -> PNSub
  | `String "nmul" -> PNMul | `String "ndiv" -> PNDiv
  | `String "nneg" -> PNNeg | `String "nabs" -> PNAbs
  | `String "niadd" -> PNIAdd | `String "inadd" -> PINAdd
  | `String "nisub" -> PNISub | `String "insub" -> PINSub
  | `String "nimul" -> PNIMul | `String "inmul" -> PINMul
  | `String "nidiv" -> PNIDiv | `String "indiv" -> PINDiv
  | `String "dadd"  -> PDAdd  | `String "dsub"  -> PDSub
  | `String "dimul" -> PDIMul | `String "idmul" -> PIDMul
  | `String "dnmul" -> PDNMul | `String "ndmul" -> PNDMul
  | `String "didiv" -> PDIDiv | `String "dndiv" -> PDNDiv
  | `String "ddiv"  -> PDDiv
  | `String "tiadd" -> PTIAdd | `String "itadd" -> PITAdd
  | `String "tnadd" -> PTNAdd | `String "ntadd" -> PNTAdd
  | `String "tisub" -> PTISub | `String "tnsub" -> PTNSub
  | `String "tsub"  -> PTSub
  | `String "and"   -> PBAnd  | `String "or"    -> PBOr
  | `String "xor"   -> PBXor  | `String "not"   -> PBNot
  | `String "land"  -> PIAnd  | `String "lor"   -> PIOr
  | `String "lxor"  -> PIXor  | `String "lnot"  -> PINot
  | `String "lsl"   -> PILsl  | `String "lsr"   -> PILsr
  | `String "nland" -> PNAnd  | `String "nlor"  -> PNOr
  | `String "nlxor" -> PNXor
  | `String "nlsl"  -> PNLsl  | `String "nlsr"  -> PNLsr
  | `String "llen"  ->PLLength| `String "lcat"  -> PLConcat
  | `String "lcons" -> PLCons | `String "lrev"  -> PLRev
  | `String "liter" -> PLIter | `String "lmap"  -> PLMap
  | `String "lfold" -> PLFold | `String "lmfld" -> PLMapFold
  | `String "scard" -> PSCard | `String "sempt" -> PSEmpty
  | `String "sadd"  -> PSAdd  | `String "srem"  -> PSRemove
  | `String "siter" -> PSIter | `String "smap"  -> PSMap
  | `String "sfold" -> PSFold | `String "smfld" -> PSMapFold
  | `String "smem"  -> PSMem
  | `String "mcard" -> PMCard | `String "mempt" -> PMEmpty
  | `String "madd"  -> PMAdd  | `String "mrem"  -> PMRemove
  | `String "mmem"  -> PMMem  | `String "mfind" -> PMFind
  | `String "miter" -> PMIter | `String "mmap"  -> PMMap
  | `String "mfold" -> PMFold | `String "mmfld" -> PMMapFold
  | `O [ "bmempt", `A [ tk; tv ] ] ->
     PBMEmpty (TypeDecoder.decode_type tk, TypeDecoder.decode_type tv)
  | `String "bmadd"  -> PBMAdd  | `String "bmrem"  -> PBMRemove
  | `String "bmmem"  -> PBMMem  | `String "bmfind" -> PBMFind
  | `String "lloop" -> PLLoop
  | `String "slen"  -> PSLength | `String "scat"   -> PSConcat
  | `String "sslce" -> PSSlice
  | `String "blen"  -> PBLength | `String "bcat"   -> PBConcat
  | `String "bslce" -> PBSlice  | `String "bpack"  -> PBPack
  | `O [ "bupck", t ] -> PBUnpack (TypeDecoder.decode_type t)
  | `String "hash" ->  PBHash
  | `String "blk2b" -> PCBlake2b  | `String "sh256" -> PCSha256
  | `String "sh512" -> PCSha512
  | `String "hashk" -> PCHashKey  | `String "check" -> PCCheck
  | `String "deleg" -> PCSetDeleg | `String "addr"  -> PCAddress
  | `O [ "self", ct  ] -> PCSelf (TypeDecoder.decode_contract_type ct)
  | `String "call"  -> PCCall     | `String "cview" -> PCView
  | `String "corig" -> PCCreate
  | `O [ "at", ct ] -> PCAt (TypeDecoder.decode_contract_type ct)
  | `String "deflt" -> PADefault  | `String "trnsf" -> PATransfer
  | `String "abalof"-> PABalanceOf
  | `String "ainfo" -> PAGetInfo  | `String "amang" -> PAManage
  | `String "bal"   -> PCBalance  | `String "now"   -> PCTime
  | `String "amnt"  -> PCAmount   | `String "gas"   -> PCGas
  | `String "cself" -> PCrSelf    | `String "srce"  -> PCSource
  | `String "sndr"  -> PCSender
  | `String "level" -> PCLevel    | `String "cycle" -> PCCycle
  | `String "aofkh" -> PAddressOfKeyHash
  | `String "khofa" -> PKeyHashOfAddress
  | `String s ->
      begin
      match Love_primitive.from_string s with
      | None ->
          raise (DecodingError "Unknown primitive")
      | Some p ->
          PGenericPrimitive p
    end
  | _ ->
      raise (DecodingError "Unknown primitive")

end

let encode_annoted encode_content encode_annot a =
  `O [ "content", encode_content a.content;
       "annot", encode_annot a.annot ]

let decode_annoted decode_content decode_annot a =
  match a with
  | `O [ "content", content; "annot", annot ] ->
    { content = decode_content content;
      annot = decode_annot annot }
  | _ -> raise (DecodingError "Badly formed annotation")

let encode_type_kind = function
  | Love_ast.TPublic -> `String "public"
  | TPrivate -> `String "private"
  | TInternal -> `String "internal"
  | TAbstract -> `String "abstract"

let decode_type_kind = function
  | `String "public" -> Love_ast.TPublic
  | `String "private" -> TPrivate
  | `String "internal" -> TInternal
  | `String "abstract" -> TAbstract
  | _ -> raise (DecodingError "Badly formed type kind")

let location_encoding =
  conv
    (fun Love_ast.{pos_lnum; pos_bol; pos_cnum} ->
       (pos_lnum, pos_bol, pos_cnum))
    (fun (pos_lnum, pos_bol, pos_cnum) ->
       {pos_lnum; pos_bol; pos_cnum})
    (obj3
       (req "lnum" int31)
       (req "bol" int31)
       (req "cnum" int31))

let encode_loc = Json.construct location_encoding
let decode_loc = Json.destruct location_encoding

module Ast = struct

  open Love_ast

  let encode_annot = Json.construct (option location_encoding)
  let decode_annot = Json.destruct (option location_encoding)

  let encode_annoted enc a =
    `O [ "content", enc a.content; "annot", encode_annot a.annot]

  let decode_annoted dec =
    function
    | `O [ "content", c; "annot", a] ->
      {content = dec c; annot = decode_annot a}
    | _ -> raise (DecodingError "Badly formed annotation")

  let encode_exn_name = function
    | Love_ast.Fail t -> `O [ "fail", TypeEncoder.encode_type t ]
    | Exception e -> `O [ "exception", encode_ident e ]

  let decode_exn_name = function
    | `O [ "fail", t ] -> Love_ast.Fail (TypeDecoder.decode_type t)
    | `O [ "exception", e ] -> Exception (decode_ident e)
    | _ -> raise (DecodingError "Bad exception name")

  let rec encode_raw_const : raw_const -> Data_encoding.json =
    function
    | CUnit -> `Null
    | CBool b -> `Bool b
    | CString s -> `String s
    | CBytes b -> `O [ "bytes", Json.construct bytes b ]
    | CInt i -> `O [ "int", Json.construct z i ]
    | CNat i -> `O [ "nat", Json.construct z i ]
    | CDun d -> `O [ "dun", Json.construct Tez_repr.encoding d ]
    | CKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | CKeyHash kh ->
       `O [ "keyhash", Json.construct Signature.Public_key_hash.encoding kh ]
    | CSignature s -> `O [ "signature", Json.construct Signature.encoding s ]
    | CTimestamp t ->
       `O [ "timestamp", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | CAddress s -> `O [ "address", Json.construct Contract_repr.encoding s ]
    | CPrimitive p -> `O [ "primitive", PrimEncoder.encode_prim p ]

  and encode_const c = encode_annoted encode_raw_const c

  and encode_raw_pattern = function
    | PAny -> `Null
    | PVar v -> `String v
    | PAlias (p, v) -> `O [ "alias", `String v; "of", encode_pattern p ]
    | PConst c -> `O [ "const", encode_const c ]
    | PNone -> `O [ "none", `Null ]
    | PSome p -> `O [ "some", encode_pattern p ]
    | PList pl -> `O [ "list", encode_list encode_pattern pl ]
    | PTuple pl -> `O [ "tuple", encode_list encode_pattern pl ]
    | PConstr (cstr, pl) ->
       `O [ "constr", `A [ encode_string cstr; encode_list encode_pattern pl ] ]
    | PContract (n, st) ->
        `O [ "contract", encode_pair2 encode_string
               TypeEncoder.encode_structure_type (n, st) ]

  and encode_pattern p = encode_annoted encode_raw_pattern p

  and encode_lambda { arg_pattern; body; arg_typ } =
    `O [ "lambda", `A [ encode_pattern arg_pattern;
                        TypeEncoder.encode_type arg_typ ];
         "body", encode_exp body ]

  and encode_struct_ref = function
    | Named sn -> encode_ident sn (* Array *)
    | Anonymous s -> encode_structure s (* Object *)

  and encode_raw_exp (e : raw_exp) : Data_encoding.json =
    match e with
    | Const c -> `O [ "const", encode_const c ]
    | Var v -> `O [ "var", encode_ident v ]
    | Let { bnd_pattern; bnd_val; body } ->
       `O [ "let", encode_pattern bnd_pattern;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | LetRec { bnd_var; bnd_val; body; fun_typ } ->
       `O [ "letrec", `String bnd_var;
            "of_type", TypeEncoder.encode_type fun_typ;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | Lambda l ->
       encode_lambda l (* Object *)
    | Apply { fct; args } ->
       `O [ "apply", `A [ encode_exp fct; encode_list encode_exp args ] ]
    | TLambda { targ; exp } ->
       `O [ "tlambda", `A [ TypeEncoder.encode_tvar targ; encode_exp exp ] ]
    | TApply { exp; typ } ->
       `O [ "tapply", `A [ encode_exp exp; TypeEncoder.encode_type typ ] ]
    | Seq el ->
       `O [ "seq", encode_list encode_exp el ]
    | If { cond; ifthen; ifelse } ->
       `O [ "if", encode_exp cond;
            "then", encode_exp ifthen;
            "else", encode_exp ifelse ]
    | Match { arg; cases } ->
       `O [ "match", encode_exp arg;
            "with", encode_list (encode_pair2 encode_pattern encode_exp) cases ]
    | Constructor { constr; ctyps; args } ->
      `O [ "construct", `A [ encode_ident constr;
                             encode_list TypeEncoder.encode_type ctyps;
                             encode_list encode_exp args ] ]
    | ENone ->
       `String "none"
    | ESome e ->
       `O [ "some", encode_exp e ]
    | Nil ->
      `String "nil"
    | List (el, e) ->
       `O [ "cons", `A [ encode_list encode_exp el; encode_exp e ] ]
    | Tuple el ->
       `O [ "tuple", encode_list encode_exp el ]
    | Project { tuple; indexes } ->
       `O [ "project", encode_exp tuple;
            "on", encode_list (Json.construct int31) indexes ]
    | Update { tuple; updates } ->
       `O [ "update", encode_exp tuple;
            "with", encode_list (encode_pair2 (Json.construct int31)
                                   encode_exp) updates ]
    | Record { path; contents = fel } ->
       `O [ "record", encode_list (encode_pair2 encode_string encode_exp) fel;
            "path", encode_option encode_ident path ]
    | GetField { record; field } ->
       `O [ "get", encode_string field;
            "of", encode_exp record ]
    | SetFields { record; updates } ->
       `O [ "set", encode_list (encode_pair2 encode_string encode_exp) updates;
            "of", encode_exp record ]
    | PackStruct sr ->
       `O [ "packstruct", encode_struct_ref sr ]
    | Raise { exn; args; loc } ->
      `O [ "raise",
           `A [ encode_exn_name exn;
                encode_list encode_exp args;
                encode_option encode_loc loc ] ]
    | TryWith { arg; cases } ->
       `O [ "try", encode_exp arg;
            "with", encode_list (fun ((exn, pl), e) ->
                        `A [ encode_exn_name exn;
                             encode_list encode_pattern pl;
                             encode_exp e ]
              ) cases ]

  and encode_exp e = encode_annoted encode_raw_exp e

  and encode_content n = function
    | DefType (k,td) ->
      `O [ "typekind", encode_type_kind k;
           "typedef", `String n;
           "type", TypeEncoder.encode_typedef td ]
    | DefException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | Entry { entry_code; entry_fee_code; entry_typ } ->
       `O [ "entry", `String n;
            "code", encode_exp entry_code;
            "fee_code", encode_option encode_exp entry_fee_code;
            "type", TypeEncoder.encode_type entry_typ ]
    | View { view_code; view_typ } ->
       `O [ "view", `String n;
            "code", encode_exp view_code;
            "type", encode_pair1 TypeEncoder.encode_type view_typ ]
    | Value { value_code; value_typ; value_visibility } ->
       `O [ "value", `String n;
            "code", encode_exp value_code;
            "type", TypeEncoder.encode_type value_typ;
            "visibility", encode_visibility value_visibility ]
    | Structure s ->
       `O [ "structure", `String n;
            "contents", encode_structure s ]
    | Signature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_structure { kind; structure_content } =
  `O [ "kind", TypeEncoder.encode_kind kind;
       "contents", encode_list (
         fun (n, c) ->
           encode_content n c
       ) structure_content ]

  and encode_top_structure { version; code } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "code", encode_structure code ]

  let rec decode_raw_const : Data_encoding.json -> raw_const =
    function
    | `Null -> CUnit
    | `Bool b -> CBool b
    | `String s -> CString s
    | `O [ "bytes", b ] -> CBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> CInt (Json.destruct z i)
    | `O [ "nat", i ] -> CNat (Json.destruct z i)
    | `O [ "dun", d ] -> CDun (Json.destruct Tez_repr.encoding d)
    | `O [ "key", k ] -> CKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ] ->
       CKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ] -> CSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ] ->
       CTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ] -> CAddress (Json.destruct Contract_repr.encoding s)
    | `O [ "primitive", p ] -> CPrimitive (PrimDecoder.decode_prim p)
    | _ -> raise (DecodingError "Bad constant")

  and decode_const j = decode_annoted decode_raw_const j

  and decode_raw_pattern = function
    | `Null -> PAny
    | `String v -> PVar (v)
    | `O [ "alias", `String v; "of", p ] -> PAlias (decode_pattern p, v)
    | `O [ "const", c ] -> PConst (decode_const c)
    | `O [ "none", `Null ] -> PNone
    | `O [ "some", p ] -> PSome (decode_pattern p)
    | `O [ "list", pl ] -> PList (decode_list decode_pattern pl)
    | `O [ "tuple", pl ] -> PTuple (decode_list decode_pattern pl)
    | `O [ "constr", `A [ cstr; pl ] ] ->
       PConstr (decode_string cstr, decode_list decode_pattern pl)
    | `O [ "contract", nst ] ->
        let n, st = decode_pair2 decode_string
            TypeDecoder.decode_structure_type nst in
        PContract (n, st)
    | _ -> raise (DecodingError "Bad pattern")

  and decode_pattern j = decode_annoted decode_raw_pattern j

  and decode_lambda = function
    | `O [ "lambda", `A [ arg_pattern; arg_typ ]; "body", body ] ->
       { arg_pattern = decode_pattern arg_pattern;
         body = decode_exp body;
         arg_typ = TypeDecoder.decode_type arg_typ }
    | _ -> raise (DecodingError "Bad lambda")

  and decode_struct_ref = function
    | (`A _) as sn -> Named (decode_ident sn) (* Array *)
    | (`O _) as s -> Anonymous (decode_structure s) (* Object *)
    | _ -> raise (DecodingError "Bad structure reference")

  and decode_raw_exp : Data_encoding.json -> raw_exp =
    function
    | `O [ "const", c ] -> Const (decode_const c)
    | `O [ "var", v ] -> Var (decode_ident v)
    | `O [ "let", bnd_pattern; "equals", bnd_val; "in", body ] ->
        Let { bnd_pattern = decode_pattern bnd_pattern;
              bnd_val = decode_exp bnd_val; body = decode_exp body }
    | `O [ "letrec", `String bnd_var; "of_type", fun_typ;
           "equals", bnd_val; "in", body ] ->
      LetRec { bnd_var = bnd_var;
               bnd_val = decode_exp bnd_val; body = decode_exp body;
                fun_typ = TypeDecoder.decode_type fun_typ }
    | `O (("lambda", _) :: _) as l -> Lambda (decode_lambda l)
    | `O [ "apply", `A [ fct; args ] ] ->
       Apply { fct = decode_exp fct; args = decode_list decode_exp args }
    | `O [ "tlambda", `A [ tvar; exp ] ] ->
       TLambda { targ = TypeDecoder.decode_tvar tvar; exp = decode_exp exp }
    | `O [ "tapply", `A [ exp; typ ] ] ->
       TApply { exp = decode_exp exp; typ = TypeDecoder.decode_type typ }
    | `O [ "seq", el ] -> Seq (decode_list decode_exp el)
    | `O [ "if", cond; "then", ifthen; "else", ifelse ] ->
       If { cond = decode_exp cond;
            ifthen = decode_exp ifthen; ifelse = decode_exp ifelse }
    | `O [ "match", arg; "with", cases ] ->
       Match { arg = decode_exp arg;
               cases = decode_list
                         (decode_pair2 decode_pattern decode_exp) cases }
    | `O [ "construct", `A [ constr; ctyps; args ] ] ->
      Constructor { constr = decode_ident constr;
                    ctyps = decode_list TypeDecoder.decode_type ctyps;
                    args = decode_list decode_exp args }
    | `String "none" -> ENone
    | `O [ "some", e ] -> ESome (decode_exp e)
    | `String "nil" -> Nil
    | `O [ "cons", `A [ el; e ] ] ->
       List (decode_list decode_exp el, decode_exp e)
    | `O [ "tuple", el ] -> Tuple (decode_list decode_exp el)
    | `O [ "project", tuple; "on", indexes ] ->
       Project { tuple = decode_exp tuple;
                 indexes = decode_list (Json.destruct int31) indexes }
    | `O [ "update", tuple; "with", updates ] ->
       Update { tuple = decode_exp tuple;
                updates = decode_list (decode_pair2 (Json.destruct int31)
                                         decode_exp) updates }
    | `O [ "record", fel; "path", path ] ->
       Record { path = decode_option decode_ident path;
                contents = decode_list (decode_pair2
                                          decode_string decode_exp) fel }
    | `O [ "get", field; "of", record ] ->
       GetField { record = decode_exp record; field = decode_string field }
    | `O [ "set", updates; "of", record ] ->
       SetFields {
           record = decode_exp record;
           updates = decode_list (decode_pair2 decode_string
                                    decode_exp) updates }
    | `O [ "packstruct", sr ] -> PackStruct (decode_struct_ref sr)
    | `O [ "raise", `A [ exn; args; loc ] ] ->
      Raise { exn = decode_exn_name exn;
              args = decode_list decode_exp args;
              loc = decode_option decode_loc loc }
    | `O [ "try", arg; "with", cases ] ->
       TryWith { arg = decode_exp arg;
                 cases = decode_list (function
                             | `A [ exn; pl; e ] ->
                                (decode_exn_name exn,
                                 decode_list decode_pattern pl), decode_exp e
                             | _ -> raise (DecodingError "Bad trywith")) cases }
    | _ -> raise (DecodingError "Bad expression")

  and decode_exp j = decode_annoted decode_raw_exp j

  and decode_content = function
    | `O [ "typekind", k; "typedef", `String n; "type", td ] ->
        n, DefType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, DefException (decode_list TypeDecoder.decode_type tl)
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, Entry { entry_code = decode_exp code;
                   entry_fee_code = decode_option decode_exp fee_code;
                   entry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ ] ->
        n, View { view_code = decode_exp code;
                  view_typ = decode_pair1 TypeDecoder.decode_type typ }
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility ] ->
        n, Value { value_code = decode_exp code;
                   value_typ = TypeDecoder.decode_type typ;
                   value_visibility = decode_visibility visibility }
    | `O [ "structure", `String n; "contents", s ] ->
        n, Structure (decode_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, Signature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        kind = TypeDecoder.decode_kind kind;
        structure_content = decode_list decode_content contents;
      }
    | _ -> raise (DecodingError "Bad structure")

  and decode_top_structure = function
    | `O [ "version", version; "code", code ] ->
      { version = decode_pair1 (Json.destruct int31) version;
        code = decode_structure code }
    | _ -> raise (DecodingError "Bad top structure")

  let const_encoding =
    conv encode_const decode_const json
  let structure_encoding =
    conv encode_structure decode_structure json
  let top_contract_encoding =
    conv encode_top_structure decode_top_structure json
  let exp_encoding =
    conv encode_exp decode_exp json
end




module RuntimeAst = struct

  open Love_runtime_ast

  let encode_exn_name = function
    | RFail t -> `O [ "fail", TypeEncoder.encode_type t ]
    | RException e -> `O [ "exception", encode_ident e ]

  let decode_exn_name = function
    | `O [ "fail", t ] -> RFail (TypeDecoder.decode_type t)
    | `O [ "exception", e ] -> RException (decode_ident e)
    | _ -> raise (DecodingError "Bad exception name")

  let rec encode_const : const -> Data_encoding.json = function
    | RCUnit -> `Null
    | RCBool b -> `Bool b
    | RCString s -> `String s
    | RCBytes b -> `O [ "bytes", Json.construct bytes b ]
    | RCInt i -> `O [ "int", Json.construct z i ]
    | RCNat i -> `O [ "nat", Json.construct z i ]
    | RCDun d -> `O [ "dun", Json.construct Tez_repr.encoding d ]
    | RCKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | RCKeyHash kh ->
       `O [ "keyhash", Json.construct Signature.Public_key_hash.encoding kh ]
    | RCSignature s -> `O [ "signature", Json.construct Signature.encoding s ]
    | RCTimestamp t ->
       `O [ "timestamp", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | RCAddress s -> `O [ "address", Json.construct Contract_repr.encoding s ]
    | RCPrimitive p -> `O [ "primitive", PrimEncoder.encode_prim p ]

  and encode_pattern = function
    | RPAny -> `Null
    | RPVar v -> `String v
    | RPAlias (p, v) -> `O [ "alias", `String v; "of", encode_pattern p ]
    | RPConst c -> `O [ "const", encode_const c ]
    | RPNone -> `O [ "none", `Null ]
    | RPSome p -> `O [ "some", encode_pattern p ]
    | RPList pl -> `O [ "list", encode_list encode_pattern pl ]
    | RPTuple pl -> `O [ "tuple", encode_list encode_pattern pl ]
    | RPConstr (cstr, pl) ->
       `O [ "constr", `A [ encode_string cstr; encode_list encode_pattern pl ] ]
    | RPContract (n, st) ->
        `O [ "contract", encode_pair2 encode_string
               TypeEncoder.encode_structure_type (n, st) ]

  and encode_binding = function
    | RPattern (p, t) ->
       `O [ "pattern",
            encode_pair2 encode_pattern TypeEncoder.encode_type (p, t) ]
    | RTypeVar tv ->
       `O [ "typevar", TypeEncoder.encode_tvar tv ]

  and encode_param = function
    | RExp e ->`O [ "exp", encode_exp e ]
    | RType t -> `O [ "type", TypeEncoder.encode_type t ]

  and encode_struct_ref = function
    | RNamed sn -> encode_ident sn (* Array *)
    | RAnonymous s -> encode_structure s (* Object *)

  and encode_exp : exp -> Data_encoding.json = function
    | RConst c -> `O [ "const", encode_const c ]
    | RVar v -> `O [ "var", encode_ident v ]
    | RLet { bnd_pattern; bnd_val; body } ->
       `O [ "let", encode_pattern bnd_pattern;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | RLetRec { bnd_var; bnd_val; val_typ; body } ->
       `O [ "letrec", `String bnd_var;
            "of_type", TypeEncoder.encode_type val_typ;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | RLambda { args; body } ->
       `O [ "lambda", encode_list encode_binding args;
            "body", encode_exp body ]
    | RApply { exp; args } ->
       `O [ "apply", `A [ encode_exp exp; encode_list encode_param args ] ]
    | RSeq el ->
       `O [ "seq", encode_list encode_exp el ]
    | RIf { cond; ifthen; ifelse } ->
       `O [ "if", encode_exp cond;
            "then", encode_exp ifthen;
            "else", encode_exp ifelse ]
    | RMatch { arg; cases } ->
       `O [ "match", encode_exp arg;
            "with", encode_list (encode_pair2 encode_pattern encode_exp) cases ]
    | RConstructor { type_name; constr; ctyps; args } ->
      `O [ "construct", `O [ constr, encode_list encode_exp args ] ;
           "type_appl",  encode_list TypeEncoder.encode_type ctyps;
            "type_name", encode_ident type_name ]
    | RNone ->
       `String "none"
    | RSome e ->
       `O [ "some", encode_exp e ]
    | RNil ->
      `String "nil"
    | RList (el, e) ->
       `O [ "cons", `A [ encode_list encode_exp el; encode_exp e ] ]
    | RTuple el ->
       `O [ "tuple", encode_list encode_exp el ]
    | RProject { tuple; indexes } ->
       `O [ "project", encode_exp tuple;
            "on", encode_list (Json.construct int31) indexes ]
    | RUpdate { tuple; updates } ->
       `O [ "update", encode_exp tuple;
            "with", encode_list (encode_pair2 (Json.construct int31)
                                   encode_exp) updates ]
    | RRecord { type_name; contents = fel } ->
       `O [ "record", encode_list (encode_pair2 encode_string encode_exp) fel ;
            "type_name", encode_ident type_name ]
    | RGetField { record; field } ->
       `O [ "get", encode_string field;
            "of", encode_exp record ]
    | RSetFields { record; updates } ->
       `O [ "set", encode_list (encode_pair2 encode_string encode_exp) updates;
            "of", encode_exp record ]
    | RPackStruct sr ->
       `O [ "packstruct", encode_struct_ref sr ]
    | RRaise { exn; args; loc } ->
      `O [ "raise",
           `A [ encode_exn_name exn;
                encode_list encode_exp args;
                encode_option encode_loc loc ] ]
    | RTryWith { arg; cases } ->
       `O [ "try", encode_exp arg;
            "with", encode_list (fun ((exn, pl), e) ->
                        `A [ encode_exn_name exn;
                             encode_list encode_pattern pl;
                             encode_exp e ]
              ) cases ]

  and encode_content n = function
    | RDefType (k,td) ->
      `O [ "typekind", encode_type_kind k;
           "typedef", `String n;
           "type", TypeEncoder.encode_typedef td ]
    | RDefException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | REntry { entry_code; entry_fee_code; entry_typ } ->
       `O [ "entry", `String n;
            "code", encode_exp entry_code;
            "fee_code", encode_option encode_exp entry_fee_code;
            "type", TypeEncoder.encode_type entry_typ ]
    | RView { view_code; view_typ } ->
       `O [ "view", `String n;
            "code", encode_exp view_code;
            "type", encode_pair1 TypeEncoder.encode_type view_typ ]
    | RValue { value_code; value_typ; value_visibility } ->
       `O [ "value", `String n;
            "code", encode_exp value_code;
            "type", TypeEncoder.encode_type value_typ;
            "visibility", encode_visibility value_visibility ]
    | RStructure s ->
       `O [ "structure", `String n;
            "contents", encode_structure s ]
    | RSignature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_structure { structure_content; kind } =
    `O [ "kind", TypeEncoder.encode_kind kind;
         "contents", encode_list (fun (n, c) ->
             encode_content n c) structure_content; ]

  and encode_top_structure { version; code } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "code", encode_structure code ]

  let rec decode_const : Data_encoding.json -> const = function
    | `Null -> RCUnit
    | `Bool b -> RCBool b
    | `String s -> RCString s
    | `O [ "bytes", b ] -> RCBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> RCInt (Json.destruct z i)
    | `O [ "nat", i ] -> RCNat (Json.destruct z i)
    | `O [ "dun", d ] -> RCDun (Json.destruct Tez_repr.encoding d)
    | `O [ "key", k ] -> RCKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ] ->
       RCKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ] -> RCSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ] ->
       RCTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ] -> RCAddress (Json.destruct Contract_repr.encoding s)
    | `O [ "primitive", p ] -> RCPrimitive (PrimDecoder.decode_prim p)
    | _ -> raise (DecodingError "Bad constant")

  and decode_pattern = function
    | `Null -> RPAny
    | `String v -> RPVar (v)
    | `O [ "alias", `String v; "of", p ] -> RPAlias (decode_pattern p, v)
    | `O [ "const", c ] -> RPConst (decode_const c)
    | `O [ "none", `Null ] -> RPNone
    | `O [ "some", p ] -> RPSome (decode_pattern p)
    | `O [ "list", pl ] -> RPList (decode_list decode_pattern pl)
    | `O [ "tuple", pl ] -> RPTuple (decode_list decode_pattern pl)
    | `O [ "constr", `A [ cstr; pl ] ] ->
       RPConstr (decode_string cstr, decode_list decode_pattern pl)
    | `O [ "contract", nst ] ->
        let n, st = decode_pair2 decode_string
            TypeDecoder.decode_structure_type nst in
        RPContract (n, st)
    | _ -> raise (DecodingError "Bad pattern")

  and decode_binding = function
    | `O [ "pattern", pt ] ->
       let p, t = decode_pair2 decode_pattern TypeDecoder.decode_type pt in
       RPattern (p, t)
    | `O [ "typevar", tv ] ->
       RTypeVar (TypeDecoder.decode_tvar tv)
    | _ -> raise (DecodingError "Bad binding")

  and decode_param = function
    | `O [ "exp", e ] -> RExp (decode_exp e)
    | `O [ "type", t ] -> RType (TypeDecoder.decode_type t)
    | _ -> raise (DecodingError "Bad param")

  and decode_lambda = function
    | `O [ "lambda", args; "body", body ] ->
       { args = decode_list decode_binding args; body = decode_exp body }
    | _ -> raise (DecodingError "Bad lambda")

  and decode_struct_ref = function
    | (`A _) as sn -> RNamed (decode_ident sn) (* Array *)
    | (`O _) as s -> RAnonymous (decode_structure s) (* Object *)
    | _ -> raise (DecodingError "Bad structure reference")

  and decode_exp : Data_encoding.json -> exp = function
    | `O [ "const", c ] -> RConst (decode_const c)
    | `O [ "var", v ] -> RVar (decode_ident v)
    | `O [ "let", bnd_pattern; "equals", bnd_val; "in", body ] ->
       RLet { bnd_pattern = decode_pattern bnd_pattern;
              bnd_val = decode_exp bnd_val; body = decode_exp body }
    | `O [ "letrec", `String bnd_var; "of_type", fun_typ;
           "equals", bnd_val; "in", body ] ->
       RLetRec { bnd_var = bnd_var; bnd_val = decode_exp bnd_val;
                 val_typ = TypeDecoder.decode_type fun_typ;
                 body = decode_exp body }
    | `O (("lambda", _) :: _) as l -> RLambda (decode_lambda l)
    | `O [ "apply", `A [ exp; args ] ] ->
       RApply { exp = decode_exp exp; args = decode_list decode_param args }
    | `O [ "seq", el ] -> RSeq (decode_list decode_exp el)
    | `O [ "if", cond; "then", ifthen; "else", ifelse ] ->
       RIf { cond = decode_exp cond;
             ifthen = decode_exp ifthen; ifelse = decode_exp ifelse }
    | `O [ "match", arg; "with", cases ] ->
       RMatch { arg = decode_exp arg;
                cases = decode_list
                          (decode_pair2 decode_pattern decode_exp) cases }
    | `O [ "construct", `O [ constr, args ];
           "type_appl", ctyps;
           "type_name", type_name ] ->
      RConstructor { type_name = decode_ident type_name; constr;
                     ctyps = decode_list TypeDecoder.decode_type ctyps;
                     args = decode_list decode_exp args }
    | `String "none" -> RNone
    | `O [ "some", e ] -> RSome (decode_exp e)
    | `String "nil" -> RNil
    | `O [ "cons", `A [ el; e ] ] ->
       RList (decode_list decode_exp el, decode_exp e)
    | `O [ "tuple", el ] -> RTuple (decode_list decode_exp el)
    | `O [ "project", tuple; "on", indexes ] ->
       RProject { tuple = decode_exp tuple;
                  indexes = decode_list (Json.destruct int31) indexes }
    | `O [ "update", tuple; "with", updates ] ->
       RUpdate { tuple = decode_exp tuple;
                 updates = decode_list (decode_pair2 (Json.destruct int31)
                                          decode_exp) updates }
    | `O [ "record", fel; "type_name", type_name ] ->
       RRecord { type_name = decode_ident type_name;
                 contents =
                   decode_list (decode_pair2 decode_string decode_exp) fel }
    | `O [ "get", field; "of", record ] ->
       RGetField { record = decode_exp record; field = decode_string field }
    | `O [ "set", updates; "of", record ] ->
       RSetFields {
           record = decode_exp record;
           updates = decode_list (decode_pair2 decode_string
                                    decode_exp) updates }
    | `O [ "packstruct", sr ] -> RPackStruct (decode_struct_ref sr)
    | `O [ "raise", `A [ exn; args; loc ] ] ->
       RRaise { exn = decode_exn_name exn;
                args = decode_list decode_exp args;
                loc = decode_option decode_loc loc }
    | `O [ "try", arg; "with", cases ] ->
       RTryWith { arg = decode_exp arg;
                  cases = decode_list (function
                             | `A [ exn; pl; e ] ->
                                (decode_exn_name exn,
                                 decode_list decode_pattern pl), decode_exp e
                             | _ -> raise (DecodingError "Bad trywith")) cases }
    | _ -> raise (DecodingError "Bad expression")

  and decode_content = function
    | `O [ "typekind", k; "typedef", `String n; "type", td ] ->
        n, RDefType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, RDefException (decode_list TypeDecoder.decode_type tl)
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, REntry { entry_code = decode_exp code;
                   entry_fee_code = decode_option decode_exp fee_code;
                   entry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ ] ->
        n, RView { view_code = decode_exp code;
                  view_typ = decode_pair1 TypeDecoder.decode_type typ }
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility ] ->
        n, RValue { value_code = decode_exp code;
                   value_typ = TypeDecoder.decode_type typ;
                   value_visibility = decode_visibility visibility }
    | `O [ "structure", `String n; "contents", s ] ->
        n, RStructure (decode_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, RSignature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        structure_content = decode_list decode_content contents;
        kind = TypeDecoder.decode_kind kind
      }
    | _ -> raise (DecodingError "Bad structure")

  and decode_top_structure = function
    | `O [ "version", version; "code", code ] ->
      { version = decode_pair1 (Json.destruct int31) version;
        code = decode_structure code }
    | _ -> raise (DecodingError "Bad top structure")

  let const_encoding =
    conv encode_const decode_const json
  let structure_encoding =
    conv encode_structure decode_structure json
  let top_contract_encoding =
    conv encode_top_structure decode_top_structure json
  let exp_encoding =
    conv encode_exp decode_exp json
end




module ValueEncoder = struct

  open Love_value
  open Value
  open Op

  let rec encode_value (v : Love_value.Value.t) : Data_encoding.json =
    match Value.unrec_closure v with
    | VUnit -> `Null
    | VBool b -> `Bool b
    | VString s -> `String s
    | VBytes b -> `O [ "bytes", Json.construct bytes b ]
    | VInt i -> `O [ "int", Json.construct z i ]
    | VNat i -> `O [ "nat", Json.construct z i ]
    | VNone -> `O [ "none", `Null ]
    | VSome v -> `O [ "some", encode_value v ]
    | VTuple vl -> `O [ "tuple", encode_list encode_value vl ]
    | VConstr (cn, vl) ->
       `O [ "constr", encode_pair2 (Json.construct string)
                        (encode_list encode_value) (cn, vl) ]
    | VRecord fcl ->
       `O [ "record", encode_list
                        (encode_pair2 encode_string encode_value) fcl ]
    | VList vl -> `O [ "list", encode_list encode_value vl ]
    | VSet vs -> `O [ "set", encode_list encode_value (ValueSet.elements vs) ]
    | VMap vm ->
       `O [ "map", encode_list (encode_pair1 encode_value)
                     (ValueMap.bindings vm) ]
    | VBigMap { id; diff; key_type; value_type } ->
       `O [ "bigmap", encode_option (Json.construct z) id;
            "key_type", TypeEncoder.encode_type key_type;
            "val_type", TypeEncoder.encode_type value_type;
            "diff", encode_list (encode_pair2 encode_value
                                   (encode_option encode_value))
              (ValueMap.bindings diff) ]
    | VDun d -> `O [ "dun", Json.construct Tez_repr.encoding d ]
    | VKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | VKeyHash kh ->
       `O [ "keyhash", Json.construct Signature.Public_key_hash.encoding kh ]
    | VSignature s -> `O [ "signature", Json.construct Signature.encoding s ]
    | VTimestamp t ->
       `O [ "timestamp", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | VAddress s -> `O [ "address", Json.construct Contract_repr.encoding s ]
    | VOperation op -> `O [ "operation", encode_op op ]
    | VPackedStructure (p, c) ->
       `O [ "packedstruct", encode_pair2
              encode_ident encode_live_contract (p, c) ]
    | VContractInstance (s, a) ->
       `O [ "instance", Json.construct Contract_repr.encoding a;
            "signature", TypeEncoder.encode_signature s ]
    | VEntryPoint (a, e) ->
       `O [ "entry", `String e;
            "addr", Json.construct Contract_repr.encoding a ]
    | VView (a, e) ->
       `O [ "view", `String e;
            "addr", Json.construct Contract_repr.encoding a ]
    | VClosure { call_env; lambda = { args; body } } ->
       let values = encode_list
           (encode_pair2 encode_ident
              (encode_local_or_global encode_value
                 (encode_pointer_or_inlined encode_value)))
           call_env.values in
       let structs = encode_list
           (encode_pair2 encode_ident
              (encode_pointer_or_inlined encode_live_structure))
           call_env.structs in
       let sigs = encode_list
           (encode_pair2 encode_ident
              (encode_pointer_or_inlined TypeEncoder.encode_signature))
           call_env.sigs in
       let exns = encode_list
           (encode_pair1 encode_ident) call_env.exns in
       let types = encode_list
           (encode_pair1 encode_ident) call_env.types in
       let tvars = encode_list
           (encode_pair2 encode_string
              TypeEncoder.encode_type) call_env.tvars in
       let args = encode_list RuntimeAst.encode_binding args in
       let body = RuntimeAst.encode_exp body in
       `O [ "closure", values; "structs", structs; "sigs", sigs; "exns", exns;
            "types", types; "tvars", tvars; "args", args; "body", body ]
    | VPrimitive (p, vl) ->
      `O [ "primitive", `A [ PrimEncoder.encode_prim p;
                             encode_list encode_value vl ] ]

  and encode_local_or_global :
    type t1 t2. (t1 -> json) -> (t2 -> json) ->
      (t1, t2) Value.local_or_global -> json =
    fun encode_local encode_global -> function
    | Local l -> `O [ "local", encode_local l ]
    | Global g -> `O [ "global", encode_global g ]

  and encode_pointer_or_inlined :
    type t. (t -> json) -> t Value.ptr_or_inlined -> json =
    fun encode_content -> function
    | Inlined (p, c) ->
        `O [ "inlined", encode_pair2 encode_ident encode_content (p, c) ]
    | Pointer p -> `O [ "pointer", encode_ident p ]

  and encode_manager_operation = function
    | Origination { delegate; script; credit; preorigination } ->
       `O [ "origination",
            `O [ "delegate", encode_option
                   (Json.construct Signature.Public_key_hash.encoding) delegate;
                 "script", encode_pair2 encode_value
                   encode_live_contract script;
                 "credit", Json.construct Tez_repr.encoding credit;
                 "preorigination", encode_option
                   (Json.construct Contract_repr.encoding) preorigination ] ]
    | Transaction { amount; parameters; entrypoint; destination } ->
       `O [ "transaction",
            `O [ "amount", Json.construct Tez_repr.encoding amount;
                 "parameters", encode_option encode_value parameters;
                 "entrypoint", encode_string entrypoint;
                 "destination", Json.construct
                                  Contract_repr.encoding destination ] ]
    | Delegation d ->
       `O [ "delegation", encode_option
                        (Json.construct Signature.Public_key_hash.encoding) d ]
    | Dune_manage_account
        { target; maxrolls; admin; white_list; delegation } ->
       `O [ "dune_manage_account",
            `O [ "target", encode_option ((Json.construct
                              Signature.Public_key_hash.encoding)) target;
                 "maxrolls",
                     encode_option (encode_option (
                     Json.construct int31)) maxrolls;
                 "admin",
                     encode_option (encode_option (
                     Json.construct Contract_repr.encoding)) admin;
                 "whitelist",
                     encode_option (encode_list (
                     Json.construct Contract_repr.encoding)) white_list;
                 "delegation",
                 encode_option (Json.construct bool) delegation ] ]

  and encode_internal_op { source; operation; nonce } =
    `O [ "source", Json.construct Contract_repr.encoding source;
         "operation", encode_manager_operation operation;
         "nonce", Json.construct int31 nonce ]

  and encode_big_map_diff_item = function
    | Update { big_map; diff_key; diff_key_hash; diff_value } ->
        `O [ "update", Json.construct z big_map;
             "diff_key", encode_value diff_key;
             "diff_key_hash", Json.construct
               Script_expr_hash.encoding diff_key_hash;
             "diff_value", encode_option encode_value diff_value ]
    | Clear id ->
        `O [ "clear", Json.construct z id ]
    | Copy (ids, idd) ->
        `O [ "copy", `A [ Json.construct z ids; Json.construct z idd ] ]
    | Alloc { big_map; key_type; value_type }  ->
        `O [ "alloc", Json.construct z big_map;
             "key_type", TypeEncoder.encode_type key_type;
             "value_type", TypeEncoder.encode_type value_type ]

  and encode_op (operation, big_map_diff_opt) =
    `A [ encode_internal_op operation;
         encode_option (encode_list encode_big_map_diff_item) big_map_diff_opt ]

  and encode_content n =
    let open LiveStructure in
    function
    | VType (k, td) ->
      `O [ "typedef", `String n;
           "kind", encode_type_kind k;
           "type", TypeEncoder.encode_typedef td ]
    | VException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | VEntry { ventry_code; ventry_fee_code; ventry_typ } ->
       `O [ "entry", `String n;
            "code", encode_value ventry_code;
            "fee_code", encode_option encode_value ventry_fee_code;
            "type", TypeEncoder.encode_type ventry_typ ]
    | VView { vview_code; vview_typ } ->
       `O [ "view", `String n;
            "code", encode_value vview_code;
            "type", encode_pair1 TypeEncoder.encode_type vview_typ ]
    | VValue { vvalue_code; vvalue_typ; vvalue_visibility } ->
       `O [ "value", `String n;
            "code", encode_value vvalue_code;
            "type", TypeEncoder.encode_type vvalue_typ;
            "visibility", encode_visibility vvalue_visibility ]
    | VStructure s ->
       `O [ "structure", `String n;
            "contents", encode_live_structure s ]
    | VSignature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_live_structure { kind; content } =
    `O [
      "kind", TypeEncoder.encode_kind kind;
      "contents",
         encode_list (fun (n, c) -> encode_content n c) content; ]

  and encode_live_contract LiveContract.{ version; root_struct } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "root_struct", encode_live_structure root_struct ]

  and encode_fee_code FeeCode.{ version; root_struct; fee_codes } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "top_struct", encode_live_structure root_struct;
         "fee_codes", encode_list
           (encode_pair2 (Json.construct string)
              (encode_pair2 encode_value TypeEncoder.encode_type)) fee_codes ]

end

module ValueDecoder = struct

  open Love_value
  open Value
  open Op

  let rec decode_value (j : Data_encoding.json) : Love_value.Value.t =
    Value.rec_closure @@
    match j with
    | `Null -> VUnit
    | `Bool b -> VBool b
    | `String s -> VString s
    | `O [ "bytes", b ] -> VBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> VInt (Json.destruct z i)
    | `O [ "nat", i ] -> VNat (Json.destruct z i)
    | `O [ "none", `Null ] -> VNone
    | `O [ "some", v ] -> VSome (decode_value v)
    | `O [ "tuple", vl ] -> VTuple (decode_list decode_value vl)
    | `O [ "constr", `A [ cn; vl ] ] ->
       VConstr (Json.destruct string cn, decode_list decode_value vl)
    | `O [ "record", fcl ] ->
       VRecord (decode_list (decode_pair2 decode_string decode_value) fcl)
    | `O [ "list", vl ] -> VList (decode_list decode_value vl)
    | `O [ "set", vl ] -> VSet (ValueSet.of_list (decode_list decode_value vl))
    | `O [ "map", bl ] ->
       VMap (Utils.bindings_to_map ValueMap.add ValueMap.empty
               (decode_list (decode_pair1 decode_value) bl))
    | `O [ "bigmap", id; "key_type", key_type;
           "val_type", value_type; "diff", diff ] ->
       VBigMap { id = decode_option (Json.destruct z) id;
                 diff = Utils.bindings_to_map ValueMap.add ValueMap.empty
                     (decode_list (decode_pair2 decode_value
                                     (decode_option decode_value)) diff);
                 key_type = TypeDecoder.decode_type key_type;
                 value_type = TypeDecoder.decode_type value_type }
    | `O [ "dun", d ] -> VDun (Json.destruct Tez_repr.encoding d)
    | `O [ "key", k ] -> VKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ] ->
       VKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ] -> VSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ] ->
       VTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ] -> VAddress (Json.destruct Contract_repr.encoding s)
    | `O [ "operation", op ] -> VOperation (decode_op op)
    | `O [ "packedstruct", pc ] ->
       let p, c = decode_pair2 decode_ident decode_live_contract pc in
       VPackedStructure (p, c)
    | `O [ "instance", a; "signature", s ] ->
       VContractInstance (TypeDecoder.decode_signature s,
                          Json.destruct Contract_repr.encoding a)
    | `O [ "entry", `String e; "addr", a ] ->
       VEntryPoint (Json.destruct Contract_repr.encoding a, e)
    | `O [ "view", `String e; "addr", a ] ->
       VView (Json.destruct Contract_repr.encoding a, e)
    | `O [ "closure", values; "structs", structs; "sigs", sigs; "exns", exns;
           "types", types; "tvars", tvars; "args", args; "body", body ] ->
       let values = decode_list
           (decode_pair2 decode_ident
              (decode_local_or_global decode_value
                 (decode_pointer_or_inlined decode_value))) values in
       let structs = decode_list
           (decode_pair2 decode_ident
              (decode_pointer_or_inlined decode_live_structure)) structs in
       let sigs = decode_list
           (decode_pair2 decode_ident
              (decode_pointer_or_inlined TypeDecoder.decode_signature)) sigs in
       let exns = decode_list
           (decode_pair1 decode_ident) exns in
       let types = decode_list
           (decode_pair1 decode_ident) types in
       let tvars = decode_list
           (decode_pair2 decode_string
              TypeDecoder.decode_type) tvars in
       let args = decode_list RuntimeAst.decode_binding args in
       let body = RuntimeAst.decode_exp body in
       VClosure { call_env = { values; structs; sigs; exns; types; tvars };
                  lambda = { args; body }}
    | `O [ "primitive", `A [ p; vl ] ] ->
      VPrimitive (PrimDecoder.decode_prim p, decode_list decode_value vl)
    | _ -> raise (DecodingError "Bad value")

  and decode_local_or_global :
    type t1 t2. (json -> t1) -> (json -> t2) -> json ->
      (t1, t2) Value.local_or_global =
    fun decode_local decode_global -> function
    | `O [ "local", l ] -> Local (decode_local l)
    | `O [ "global", g ] -> Global (decode_global g)
    | _ -> raise (DecodingError "Bad local or global")

  and decode_pointer_or_inlined :
    type t. (json -> t) -> json -> t Value.ptr_or_inlined =
    fun decode_content -> function
    | `O [ "inlined", pc ] ->
        let p, c = decode_pair2 decode_ident decode_content pc in
        Inlined (p, c)
    | `O [ "pointer", p ] -> Pointer (decode_ident p)
    | _ -> raise (DecodingError "Bad pointer or inlined")

  and decode_manager_operation = function
    | `O [ "origination",
           `O [ "delegate", delegate; "script", script;
                "credit", credit; "preorigination", preorigination ] ] ->
       Origination { delegate =
                       decode_option (Json.destruct
                         Signature.Public_key_hash.encoding) delegate;
                     script = decode_pair2 decode_value
                                decode_live_contract script;
                     credit = Json.destruct Tez_repr.encoding credit;
                     preorigination = decode_option
                                        (Json.destruct Contract_repr.encoding)
                                        preorigination }
    | `O [ "transaction",
           `O [ "amount", amount; "parameters", parameters;
                "entrypoint", entrypoint; "destination", destination ] ] ->
       Transaction { amount = Json.destruct Tez_repr.encoding amount;
                     parameters = decode_option decode_value parameters;
                     entrypoint = decode_string entrypoint;
                     destination = Json.destruct
                                     Contract_repr.encoding destination }
    | `O [ "delegation", d ] ->
       Delegation (decode_option
                     (Json.destruct Signature.Public_key_hash.encoding) d)

    | `O [ "dune_manage_account",
           `O [ "target", target; "maxrolls", maxrolls; "admin", admin;
                 "whitelist", white_list; "delegation", delegation ] ] ->
      Dune_manage_account {
        target = decode_option (Json.destruct
                                  Signature.Public_key_hash.encoding) target;
        maxrolls = decode_option (decode_option (
                     Json.destruct int31)) maxrolls;
        admin = decode_option (decode_option (
                     Json.destruct Contract_repr.encoding)) admin;
        white_list = decode_option (decode_list (
                     Json.destruct Contract_repr.encoding)) white_list;
        delegation = decode_option (Json.destruct bool) delegation }
    | _ -> raise (DecodingError "Bad manager operation")

  and decode_internal_op = function
    | `O [ "source", source ; "operation", operation ; "nonce", nonce ] ->
       { source = Json.destruct Contract_repr.encoding source;
         operation = decode_manager_operation operation;
         nonce = Json.destruct int31 nonce }
    | _ -> raise (DecodingError "Bad internal operation")

  and decode_big_map_diff_item = function
    | `O [ "update", big_map; "diff_key", diff_key;
           "diff_key_hash", diff_key_hash; "diff_value", diff_value ] ->
        Update { big_map = Json.destruct z big_map;
                 diff_key = decode_value diff_key;
                 diff_key_hash = Json.destruct
                     Script_expr_hash.encoding diff_key_hash;
                 diff_value = decode_option decode_value diff_value }
    | `O [ "clear", id ] ->
        Clear (Json.destruct z id)
    | `O [ "copy", `A [ ids; idd ] ] ->
        Copy (Json.destruct z ids, Json.destruct z idd)
    | `O [ "alloc", big_map; "key_type", key_type;
           "value_type", value_type ] ->
        Alloc { big_map = Json.destruct z big_map;
                key_type = TypeDecoder.decode_type key_type;
                value_type = TypeDecoder.decode_type value_type }
    | _ -> raise (DecodingError "Bad big map diff item")

  and decode_op = function
    | `A [ operation; big_map_diff_opt ] ->
      (decode_internal_op operation,
       decode_option (decode_list decode_big_map_diff_item) big_map_diff_opt)
    | _ -> raise (DecodingError "Bad operation")

  and decode_content =
    let open LiveStructure in
    function
    | `O [ "typedef", `String n; "kind", k; "type", td ] ->
        n, VType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, VException (decode_list TypeDecoder.decode_type tl)
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, VEntry { ventry_code = decode_value code;
                   ventry_fee_code = decode_option decode_value fee_code;
                   ventry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ ] ->
        n, VView { vview_code = decode_value code;
                  vview_typ = decode_pair1 TypeDecoder.decode_type typ }
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility ] ->
        n, VValue { vvalue_code = decode_value code;
                   vvalue_typ = TypeDecoder.decode_type typ;
                   vvalue_visibility = decode_visibility visibility }
    | `O [ "structure", `String n; "contents", s ] ->
        n, VStructure (decode_live_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, VSignature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_live_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        kind = TypeDecoder.decode_kind kind;
        content = decode_list decode_content contents; }
    | _ -> raise (DecodingError "Bad live structure")

  and decode_live_contract = function
    | `O [ "version", version; "root_struct", root_struct ] ->
      LiveContract.{ version = decode_pair1 (Json.destruct int31) version;
                     root_struct = decode_live_structure root_struct }
    | _ -> raise (DecodingError "Bad live contract")

  and decode_fee_code = function
    | `O [ "version", version; "root_struct", root_struct;
           "fee_codes", fee_codes ] ->
      FeeCode.{ version = decode_pair1 (Json.destruct int31) version;
                root_struct = decode_live_structure root_struct;
                fee_codes = decode_list
                    (decode_pair2 (Json.destruct string)
                       (decode_pair2 decode_value TypeDecoder.decode_type)
                    ) fee_codes }
    | _ -> raise (DecodingError "Bad fee code")

end

module Value = struct
  let encoding =
    conv ValueEncoder.encode_value
      ValueDecoder.decode_value json
  let live_structure_encoding =
    conv ValueEncoder.encode_live_structure
      ValueDecoder.decode_live_structure json
  let live_contract_encoding =
    conv ValueEncoder.encode_live_contract
      ValueDecoder.decode_live_contract json
  let fee_code_encoding =
    conv ValueEncoder.encode_fee_code
      ValueDecoder.decode_fee_code json
end
