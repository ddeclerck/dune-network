(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)


type const = Dune_script_registration.const
type code = Dune_script_registration.code
type lang = Dune_script_sig.lang

module type S = sig
  module Internal : Dune_script_sig.S
  type Dune_script_registration.const += Const of Internal.const
  type Dune_script_registration.code += Code of Internal.code
  include Dune_script_sig.S
    with type const := Internal.const
     and type code := Internal.code
  val tag : int
  val const : Internal.const -> const
  val code : Internal.code -> code
  val is_const : const -> Internal.const option
  val is_code : code -> Internal.code option
  val lang_str : string
  val lang_encoding : lang Data_encoding.t

  (* to call from Dune_script_repr *)
  val init : unit -> unit
end

type lazy_const = const Data_encoding.lazy_t

type lazy_code = code Data_encoding.lazy_t

(*
type t = {
  lang : lang ;
  code : lazy_code ;
  storage : lazy_const ;
}

val encoding : t Data_encoding.t
*)
val lang_encoding : lang Data_encoding.t
val const_encoding : const Data_encoding.t
val code_encoding : code Data_encoding.t

val unit : lang -> const
val is_unit : const -> bool
val get_unit : code -> const

val lazy_const : const -> lazy_const
val lazy_code : code -> lazy_code
val lazy_const_encoding : lazy_const Data_encoding.t
val lazy_code_encoding : lazy_code Data_encoding.t
val force_decode_const : lazy_const -> (const * Gas_limit_repr.cost) tzresult
val force_decode_code : lazy_code -> (code * Gas_limit_repr.cost) tzresult
val force_bytes_const : lazy_const -> (MBytes.t * Gas_limit_repr.cost) tzresult
val force_bytes_code : lazy_code -> (MBytes.t * Gas_limit_repr.cost) tzresult
val deserialized_const_cost : const -> Gas_limit_repr.cost
val deserialized_code_cost : code -> Gas_limit_repr.cost

val serialized_cost : MBytes.t -> Gas_limit_repr.cost
val minimal_deserialize_cost : 'a Data_encoding.lazy_t -> Gas_limit_repr.cost

val pp_const : Format.formatter -> const -> unit

val traversal_const_cost : const -> Gas_limit_repr.cost
val traversal_code_cost : code -> Gas_limit_repr.cost

val module_of_header : string -> (module S)
val module_of_code : code -> (module S)
val module_of_const : const -> (module S)
