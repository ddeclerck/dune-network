(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type const = ..
type code = ..
type lang = Dune_script_sig.lang

module type S = sig
  module Internal : Dune_script_sig.S
  type const += Const of Internal.const
  type code += Code of Internal.code
  include Dune_script_sig.S
    with type const := Internal.const
     and type code := Internal.code
  val tag : int
  val const : Internal.const -> const
  val code : Internal.code -> code
  val is_const : const -> Internal.const option
  val is_code : code -> Internal.code option
  val lang_str : string
  val lang_encoding : lang Data_encoding.t

  (* to call from Dune_script_repr *)
  val init : unit -> unit
end

val close_registration : unit -> unit

module Make (D : Dune_script_sig.S) :  S with module Internal := D

val lang_of_const : const -> (module S)
val lang_of_code : code -> (module S)

val languages :  (module S) Dune_script_sig.MapLang.t  ref
