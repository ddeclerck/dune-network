(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_ast
open Love_ast_utils
open Love_type
open Log
open Ident
open Utils
open Collections (* for IntMap and StringMap *)

open Love_printer
open Error_monad

type typ = Love_type.t

exception TypingError of string * Love_ast.location option * unit Love_tenv.t
exception UnknownType of type_name
exception UnknownConstr of cstr_name
exception UnknownField of field_name
exception TypingErrorLwt of (Love_context.t * string *  unit Love_tenv.t)

type where =
    TopLevel
  | Storage
  | Parameter
  | ViewReturn
  | MapKey
  | BigMapKey
  | BigMapValue

module TNSet = Collections.StringIdentSet
module TVSet = Love_type.TypeVarSet
module TVMap = Love_type.TypeVarMap




let spf = Format.asprintf

(** Tests is the type in argument is well formed, i.e. composed of
    well defined types. It also checks that its kind is compabible
    with the type of its children.
    Example : type t = t1 * t2 is safe if both t1 and t2 are safe and
    their kind are compatible with t (here, t1 and t2 are not internal).
*)
let is_safe_res
    (env : unit Love_tenv.t)
    (k : Love_ast.type_kind)
    (t : typ) : (unit, string) result =
  debug "[is_safe] Checking type %a in environment %a@."
    (Love_type.pretty) t
    Love_tenv.pp_env env
  ;
  let rec test ~comparable env t =
    let privacy_and_comparable_test =
      match t with
        TUser (n, p) -> (
          let params_and_privacy =
            try
              let tkind = Love_tenv.find_type_kind n env in
              let sub_k,params, traits =
                match tkind with
                  Some {result = (sub_k,p, traits); _} ->
                  debug "[is_safe] Type %a is registered has %i parameters and traits %a@."
                    Ident.print_strident n (List.length p)
                    Love_type.pp_trait traits;
                  sub_k, p, traits
                | None ->
                  debug "[is_safe] Type %a is not registered@." Ident.print_strident n;
                  raise (UnknownType n)
              in
              if comparable && not (traits.tcomparable)
              then Error "Type is not comparable"
              else (
                let rec aux tvars params : ('a, 'b) result =
                  match tvars,params with
                    [], [] -> Ok ()
                  | tv :: tltv, hdp :: tlp -> (
                      match
                        test ~comparable:(tv.tv_traits.tcomparable||comparable) env hdp
                      with
                        Ok () -> aux tltv tlp
                      | r -> r
                    )
                  | _,_ -> Error "Bad number of polymorphic parameters" in
                match aux params p with
                  Error _ as e -> e
                | Ok () -> (
                    debug "[is_safe] Correct number of poymorphic arguments@.";
                    match sub_k, k with
                      TPublic, _
                    | TPrivate, TPrivate
                    | TPrivate, TAbstract
                    | TPrivate, TInternal

                    | TAbstract, TAbstract
                    | TAbstract, TInternal

                    | TInternal, TInternal
                    | TInternal, TAbstract
                    | TAbstract, TPublic
                    | TAbstract, TPrivate -> (Ok () : (unit, _) result)

                    | TPrivate, TPublic
                    | TInternal, TPublic
                    | TInternal, TPrivate ->
                      Error (
                        Format.asprintf
                          "Type %a is %a while its parent is %a"
                          Ident.print_strident n
                          Love_ast.pp_type_kind sub_k
                          Love_ast.pp_type_kind k
                      )
                  )
              )
            with
              UnknownType i ->
              Error (
                Format.asprintf
                  "User type %a is unknown"
                  print_strident i
              )
          in params_and_privacy
        )

      | TUnit
      | TBool
      | TString
      | TBytes
      | TInt
      | TNat
      | TDun
      | TKey
      | TKeyHash
      | TSignature
      | TTimestamp
      | TAddress
      | TOperation -> Ok ()
      | TPackedStructure _
      | TContractInstance _ ->
        if comparable then Error "Strucrures & instances are not comparable"
        else Ok ()
      | TBigMap (t1, t2) -> (
          match test ~comparable env t1 with
            Ok () -> test ~comparable env t2
          | r -> r
        )
      | TMap (t1, t2) -> (
          match test ~comparable:true env t1 with
            Ok () -> test ~comparable env t2
          | r -> r
        )
      | TEntryPoint t ->
        if comparable
        then Error "Entry points are not comparable else "
        else test ~comparable env t
      | TView (t1, t2) | TArrow (t1, t2) -> (
          if comparable
          then Error "Views & Lambdas are not comparable"
          else
            match test ~comparable env  t1 with
              Ok () -> test ~comparable env t2
            | r -> r
        )
      | TTuple l ->
        List.fold_left
          (function (Ok () : ('a,'b) result) -> test ~comparable env | r -> fun _ -> r)
          (Ok ())
          l
      | TOption t
      | TList t -> test ~comparable env t
      | TSet t -> test ~comparable:true env t
      | TVar _ -> Ok ()
      (* In type definitions, comparability is not settable.
         In foralls, the test is done previously.
         Hence, we don't need to check TVars. *)
      | TForall (tv, t) ->
        if comparable && not tv.tv_traits.tcomparable
        then Error "Comparable forall while quantifying on a non comparable type"
        else test ~comparable (Love_tenv.add_forall tv env) t
    in
    match privacy_and_comparable_test with
      Ok _ ->
      debug "[is_safe] Parameter's list is OK @.";
      let fv = Love_type.fvars t in
      debug "[is_safe] Type vars =";
      TypeVarSet.iter (fun v -> debug "%a@." Love_type.pp_typvar v) fv;
      let free = Love_tenv.get_free env in
      debug "@.[is_safe] Free vars =";
      TypeVarSet.iter (fun v -> debug "%a@." Love_type.pp_typvar v) free;
      debug "@.";
      if TVSet.subset fv free
      then (Ok () : (unit, _) result)
      else
        begin
          match TVSet.find_first_opt (fun e -> not @@ TVSet.mem e free) fv with
          | None -> assert false
          | Some tv ->
            Error (Format.asprintf
                     "Polymorphic variable %a not found" Love_type.pp_typvar tv)
        end
    | r -> r
  in test ~comparable:false env t

let is_safe_res_list env k l : (unit,string) result =
  let rec aux = function
      [] -> (Ok () : _ result)
    | hd :: tl -> (
        match is_safe_res env k hd with
          Ok () -> aux tl
        | e -> e
      )
  in aux l

let is_safe e (k : Love_ast.type_kind) t : bool =
  match is_safe_res e k t with
    Ok _ -> true
  | Error s ->
    debug "[is_safe] Error: %s" s;
    false

(* Todo : better error message with is_safe_res *)
let is_safe_tdef k name tdef env : bool =
  let name_is_forbidden =
    match name with
    | "unit"
    | "bool"
    | "string"
    | "bytes"
    | "int"
    | "nat"
    | "dun"
    | "key"
    | "keyhash"
    | "signature"
    | "timestamp"
    | "address"
    | "operation"
    | "set"
    | "map"
    | "bigmap"
    | "option"
    | "list"
    | "instance" -> true
    | _ -> false
  in
  if name_is_forbidden then false
  else
    match tdef with
    | SumType {sparams; scons; srec} ->
      let env_for_safe_test =
        let env =
          match srec with
            NonRec -> env
          | Rec -> Love_tenv.add_typedef name TPublic tdef env in
        List.fold_left
          (fun env tv -> Love_tenv.add_forall tv env) env sparams in
      List.for_all (fun (_,l) -> List.for_all (is_safe env_for_safe_test k) l) scons
    | RecordType {rparams; rfields; rrec} ->
      let env_for_safe_test =
        let env =
          match rrec with
            NonRec -> env
          | Rec -> Love_tenv.add_typedef name TPublic tdef env in
        List.fold_left
          (fun env tv -> Love_tenv.add_forall tv env) env rparams in
      List.for_all (fun (_,t) -> is_safe env_for_safe_test k t) rfields
    | Alias {aparams; atype} ->
      (* Private aliases are forbidden *)
      match k with
        TPrivate ->
        Log.debug "Error : Aliases with private types are forbidden";
        false
      | _ ->
        let env_for_safe_test =
          List.fold_left
            (fun env tv -> Love_tenv.add_forall tv env) env aparams in
        is_safe env_for_safe_test k atype

let check_private loc name env =
  let check_path p =
    List.for_all
      (function Path.Prev -> true | Next _ -> false)
      p in
  match Love_tenv.find_type_kind name env with
    None -> assert false
  | Some {result = (TPublic,_, _); _} (* valid : this type is public *) -> ()
  | Some {result = ((TPrivate | TAbstract | TInternal), _, _); path; _}
    when check_path path
    (* valid : these types are used locally. *)
    -> ()

  | _ ->
    raise (
      TypingError (
        Format.asprintf "Sum type %a cannot be build" Ident.print_strident name,
        loc, env
      )
    )

let rec check_forbidden_type rec_type ~where env = function
  | TOperation
  | TPackedStructure _ -> false
  | TEntryPoint t -> (
      match where with
        TopLevel -> check_forbidden_type rec_type ~where:Parameter env t
      | Storage
      | Parameter
      | ViewReturn
      | MapKey
      | BigMapKey
      | BigMapValue -> false
    )
  | TView (t1, t2) -> (
    match where with
        TopLevel ->
        check_forbidden_type rec_type ~where:Parameter env t1 &&
        check_forbidden_type rec_type ~where:ViewReturn env t2
      | Storage
      | Parameter
      | ViewReturn
      | MapKey
      | BigMapKey
      | BigMapValue -> false
  )
  | TForall (tv, t) -> (
      match where with
        TopLevel
      | ViewReturn ->
        check_forbidden_type
          rec_type
          ~where
          (Love_tenv.add_forall tv env) t
      | Storage
      | Parameter
      | MapKey
      | BigMapKey
      | BigMapValue -> false
    )

  | TContractInstance _ -> (
      match where with
        TopLevel
      | Parameter -> true
      | Storage (* temporary restriction *)
      | ViewReturn
      | MapKey
      | BigMapKey
      | BigMapValue -> false
    )
  | TUnit
  | TBool
  | TString
  | TBytes
  | TInt
  | TNat
  | TDun
  | TKey
  | TKeyHash
  | TSignature
  | TTimestamp
  | TAddress -> true
  | TVar tv -> Love_tenv.is_free tv env
  | TArrow _ -> (
      match where with
       | TopLevel | Storage | ViewReturn -> true
       | Parameter -> false (* temporary restriction *)
       | MapKey | BigMapKey | BigMapValue -> false
    )

  | TTuple l -> List.for_all (check_forbidden_type ~where rec_type env) l

  | TOption t | TList t | TSet t -> (check_forbidden_type ~where rec_type env) t
  | TMap (t,t') ->
    (check_forbidden_type ~where rec_type env t) &&
    (check_forbidden_type ~where rec_type env t')
  | TBigMap (t, t') -> (
      match where with
       | Storage ->
          (check_forbidden_type ~where:BigMapKey rec_type env) t &&
          (check_forbidden_type ~where:BigMapValue rec_type env) t'
       | Parameter -> false (* temporary restriction *)
       | TopLevel | MapKey | BigMapKey | BigMapValue | ViewReturn -> false
    )

  | TUser (n,l) as t -> (
      debug "[check_forbidden_type] Type %a@." Ident.print_strident n;
      TNSet.mem n rec_type || (
      List.for_all (check_forbidden_type ~where rec_type env) l &&
      match Love_tenv.find_sum n env with
        Some {result; _} ->
        List.for_all (
          fun Love_tenv.{cargs; crec; cparent; _} ->
            let rec_type = match crec with
                Rec -> TNSet.add n rec_type
              | NonRec -> rec_type in
            let env =
              let abs_params =
                match cparent with
                  TUser (_, l) -> l
                | _ ->  raise (
                    Exceptions.InvariantBroken
                      "Registered sum types must TUsers"
                  ) in
              List.fold_left (
                fun env -> function
                    TVar tv -> Love_tenv.add_forall tv env
                  | _ -> raise (
                      Exceptions.InvariantBroken
                        "Registered sum types must have only tvars as type arguments"
                    )
              )
                env
                abs_params
            in
            List.for_all (
              fun t ->
                debug "[check_forbidden_type] Checking type %a@."
                  (Love_type.pretty) t;
                check_forbidden_type ~where rec_type env t
            ) cargs
        ) result
      | None -> (
        match Love_tenv.find_record n env with
            Some {result; _} ->
            List.for_all
              (fun Love_tenv.{ftyp; frec; fparent; _} ->
                 let rec_type = match frec with Rec -> TNSet.add n rec_type | NonRec -> rec_type in
                 let env =
                   let abs_params =
                     match fparent with
                       TUser (_, l) -> l
                     | _ ->  raise (
                         Exceptions.InvariantBroken
                           "Registered record types must TUsers"
                       ) in
                   List.fold_left (
                     fun env -> function
                         TVar tv -> Love_tenv.add_forall tv env
                       | _ -> raise (
                           Exceptions.InvariantBroken
                             "Registered record types must have only tvars as type arguments"
                         )
                   )
                     env
                     abs_params
                 in
                 check_forbidden_type ~where rec_type env ftyp) result
          | None -> (
            let t' = Love_tenv.normalize_type ~relative:true t env
            in
            if (Compare.Int.equal (Love_type.compare t t') 0)
            then true
            else check_forbidden_type ~where rec_type env t'
        )
      )
    )
    )

let check_forbidden_type = check_forbidden_type TNSet.empty

let type_error loc env =
  let pre = fun m -> raise (TypingError (m, loc, env)) in
  Format.kasprintf pre

let sig_of_contract loc env n =
  match Love_tenv.find_contract n env with
  | Some { result; _ } ->
      Love_tenv.env_to_contract_sig result
  | _ -> type_error loc env "Structure %a not found" Ident.print_strident n

let print_error t1 t2 st1 st2 =
  debug
    "[check] Error : Type %a is not equal to %a because %a and %a are incompatible@."
    Love_type.pretty t1
    Love_type.pretty t2
    Love_type.pretty st1
    Love_type.pretty st2;
  match t1, t2 with
    TArrow _, TArrow _ -> ()
  | TArrow _, _ -> debug "[check] Did you forget arguments ?@."
  | _, TArrow _ -> debug "[check] Did you provide too many arguments ?@."
  | TForall _, TForall _ -> ()
  | _t, TForall _
  | TForall _, _t -> debug "[check] Check the polymorphic type applications ([<type>])@."
  | _,_ -> ()

let subtyp loc env t1 t2 =
  let signatures =
    fun n ->
      match Love_tenv.find_signature n env with
        None ->
        type_error loc env "Signature not found"
      | Some {result; _} -> Love_tenv.env_to_contract_sig result
  in
  Love_type.subtyp
    signatures
    (Love_tenv.normalize_type ~relative:false t1 env)
    (Love_tenv.normalize_type ~relative:false t2 env)

let check ?(error="") ?loc env (t1 : typ) (t2 : typ) : unit =
  debug
    "[check] Checking %a < %a@."
    Love_type.pretty t1
    Love_type.pretty t2;
  match subtyp loc env t1 t2 with
    Ok -> ()
  | res ->
    debug "[check] Error : %s" error;
    type_error loc env "%s" (Love_type.str_subtyp_result res)

(* TODO : if unused, remove *)
(*let rec typecheck_const_list loc env
    (cs : const list) : typ list =
  let rec simplify_args acc args =
    match args with
      arg :: arg_tl -> (
        let t = typecheck_const loc env arg in
        simplify_args (t :: acc) arg_tl
      )
    | [] -> acc
  in
  List.rev @@ simplify_args [] cs

  and *)

let typecheck_const loc env (c : const) : Love_type.t =
  debug
    "[typecheck_const] Typing constant@.";
  match c.content with
    CUnit ->
    debug "[typecheck_const] Unit@.";
    TUnit
  | CBool _b ->
    debug "[typecheck_const] Bool@.";
    TBool
  | CString _s ->
    debug "[typecheck_const] String@.";
    TString
  | CBytes _b ->
    debug "[typecheck_const] Bytes@.";
    TBytes
  | CInt _i ->
    debug "[typecheck_const] Int@.";
    TInt
  | CNat _i ->
    debug "[typecheck_const] Nat@.";
    TNat
  | CDun _d ->
    debug "[typecheck_const] Dun@.";
    TDun
  | CKey _k ->
    debug "[typecheck_const] Key@.";
    TKey
  | CKeyHash _kh ->
    debug "[typecheck_const] KeyHash@.";
    TKeyHash
  | CSignature _s ->
    debug "[typecheck_const] Signature@.";
    TSignature
  | CAddress _a ->
    debug "[typecheck_const] Address@.";
    TAddress
  | CTimestamp _t ->
    debug "[typecheck_const] Timestamp@.";
    TTimestamp
  | CPrimitive p ->
    debug "[typecheck_const] Love_primitive %s@." (Love_primitive.name p);
    Love_primitive.type_of ~sig_of_contract:(sig_of_contract loc env) p

let tpc_pany _ _ env : 'a Love_tenv.t option = Some env

let tpc_pconst tpc_cst loc t c env : 'a Love_tenv.t option =
  let typ = tpc_cst loc env c in
  check ~error:"Constant patterns with different types" ?loc env t typ;
  Some env

let tpc_pvar _ t v e : 'a Love_tenv.t option = Some (Love_tenv.add_var v t e)

let tpc_palias tpc_pat loc t v pat env : 'a Love_tenv.t option =
  let env' = tpc_pat loc env t pat in
  Some (Love_tenv.add_var v t env')

let tpc_pnone _ t env =
  match t with
    TOption _ -> Some env
  | _ -> None

let tpc_psome tpc_pat loc t pat env : 'a Love_tenv.t option =
  match t with
    TOption t -> Some (tpc_pat loc env t pat)
  | _ -> None

let tpc_pnil _ t env =
  match t with
    TList _ -> Some env
  | _ -> None

let tpc_plist tpc_pat loc t pl env : 'a Love_tenv.t option =
  let rev_pl = List.rev pl in
  let last, rest =
    match rev_pl with
      [] -> assert false
    | hd :: tl -> hd, tl in
  let env_with_last = tpc_pat loc env t last in
    match t with
      TList t ->
      Some
        (List.fold_left
           (fun env p -> tpc_pat loc env t p)
           env_with_last
           rest
        )
    | _ -> None

let tpc_ptuple tpc_pat loc t pl env : 'a Love_tenv.t option =
  match t with
    TTuple tl -> (
      try
        Some
          (List.fold_left2
             (fun env p etype -> tpc_pat loc env etype p
             )
             env
             pl
             tl
          )
      with Invalid_argument _ ->
        type_error loc env
          "Pattern tuple with bad arity :\
           %i elements in tuple, expected %i by type %a"
          (List.length pl)
          (List.length tl)
          (Love_type.pretty) t
    )
  | _ -> None

let tpc_pconstr tpc_pat loc t c plist env : 'a Love_tenv.t option =
  match t with
    TUser (name, l) -> (
      let tlist =
        let c, parameters = Ident.change_last name @@ Ident.create_id c, l in
        let tlist, tparent =
          match Love_tenv.find_constr c env, plist with
            None,_ -> raise (UnknownConstr c)
          | Some {result = {cargs = [TTuple _] as cargs;cparent; _}; _},[_] ->
            cargs,cparent
          | Some {result = {cargs = [TTuple l];cparent; _}; _},_ -> l,cparent
          | Some {result = {cargs; cparent; _}; _}, _ -> cargs, cparent in
        let poly_parameters =
          match tparent with
            TUser (_, l) -> l
          | _ -> assert false (* A registered sum parent must always be a user type *)
        in
        let update_map =
          List.fold_left2
            (fun acc poly_par par ->
               match poly_par with
                 TVar v -> TVMap.add v par acc
               | _ -> assert false
            )
            TVMap.empty
            poly_parameters
            parameters
        in
        List.map (Love_type.replace_map (fun x -> Love_tenv.isComp x env) update_map) tlist
      in
      let env =
        try
          List.fold_left2
            (fun env p etype -> tpc_pat loc env etype p)
            env
            plist
            tlist
        with Invalid_argument _ ->
          type_error loc env
            "Constructor with bad number of arguments :\
             %i elements expected, %i provided"
            (List.length tlist)
            (List.length plist)
      in Some env
    )
  | _ -> None

let tpc_pcontract _ t name st env : 'a Love_tenv.t option =
  let norm_t = Love_tenv.normalize_type ~relative:true t env in
  let norm_st = Love_tenv.normalize_type ~relative:true (TContractInstance st) env in
  check env norm_st norm_t;
  match norm_t with
    TContractInstance (Anonymous a) ->
    let new_env = Love_tenv.contract_sig_to_env (Some name) () a env in
    Some (Love_tenv.add_subcontract_env name new_env env)
  | TContractInstance (Named n) ->
    Some (Love_tenv.add_signed_subcontract name n env)
  | _ -> None

let pt_error loc env p t =
  type_error loc env "Pattern %a is not of type %a"
    Love_printer.Ast.print_pattern p
    (Love_type.pretty) t

let rec typecheck_pattern (loc : Love_ast.location option) env t pattern : unit Love_tenv.t =
  debug "[typecheck_pattern] Typechecking pattern %a with type %a@."
    Ast.print_pattern pattern (Love_type.pretty) t;
  let t = Love_tenv.normalize_type ~relative:true t env in
  let opt_res =
    match pattern.content with
      PAny                 -> tpc_pany loc t env
    | PConst c             -> tpc_pconst typecheck_const loc t c env
    | PVar v               -> tpc_pvar loc t v env
    | PAlias (pat, v)      -> tpc_palias typecheck_pattern loc t v pat env
    | PNone                -> tpc_pnone loc t env
    | PSome pat            -> tpc_psome typecheck_pattern loc t pat env
    | PList []             -> tpc_pnil loc t env
    | PList pl             -> tpc_plist typecheck_pattern loc t pl env
    | PTuple pl            -> tpc_ptuple typecheck_pattern loc t pl env
    | PConstr (c, plist)   -> tpc_pconstr typecheck_pattern loc t c plist env
    | PContract (name, st) -> tpc_pcontract loc t name st env
  in
  match opt_res with
    Some res -> res
  | None -> pt_error loc env pattern t

let tcp_evar loc v env =
  match Love_tenv.find_var v env with
    None -> type_error loc env "Unknown variable %a" print_strident v
  | Some {result = _,t; _} -> t

let rec typecheck_exp (env : unit Love_tenv.t) (exp : exp) : exp * typ =
  debug "[typecheck_exp loc] Typechecking exp %a@." Ast.print_exp exp;
  let loc = exp.annot in
  let (e : exp), t =
    match exp.content with
    | Const c ->
      debug "[typecheck_exp] Constant@.";
      let t = typecheck_const loc env c
      in
      exp, t
    | Var v -> (
        debug "[typecheck_exp] Variable %a@." print_strident v;
        let new_typ = tcp_evar loc v env
        in
        debug "[typecheck_exp] %a:%a@." print_strident v Love_type.pretty new_typ;
        exp, new_typ
      )

    | Let { bnd_pattern; bnd_val; body} ->
      debug "[typecheck_exp] Let@.";
      let bnd_val, typ = typecheck_exp env bnd_val in
      let env' = typecheck_pattern loc env typ bnd_pattern in
      let body, typ = typecheck_exp env' body
      in mk_let bnd_pattern bnd_val body, typ

    | LetRec { bnd_var; bnd_val; body; fun_typ} -> (
        debug "[typecheck_exp] LetRec of type %a@." Love_type.pretty fun_typ;
        match is_safe_res env TInternal fun_typ with
          Ok () -> (
          let env' = Love_tenv.add_var bnd_var fun_typ env in
          let bnd_val, t' = typecheck_exp env' bnd_val in
          check ?loc env fun_typ t';
          let body, typ = typecheck_exp env' body in
          mk_let_rec bnd_var bnd_val body fun_typ, typ
        )
        | Error s ->
          type_error loc env
            "Type %a is unsafe : %s"
            (Love_type.pretty) fun_typ s
      )
    | Lambda {arg_pattern; body; arg_typ} -> (
        match is_safe_res env TInternal arg_typ with
          Ok () -> (
            debug "[typecheck_exp] Lambda : (%a : %a) -> ...@."
              Ast.print_pattern arg_pattern Love_type.pretty arg_typ;
            let env' = typecheck_pattern loc env arg_typ arg_pattern in
            let body, typ = typecheck_exp env' body in
            mk_lambda arg_pattern body arg_typ,
            TArrow (arg_typ, typ)
          )
        | Error s -> (
            type_error loc env
              "Type %a of argument %a is unsafe : %s"
              (Love_type.pretty) arg_typ
              Love_printer.Ast.print_pattern arg_pattern
              s
          )
      )
    | Apply { fct; args } ->
      debug "[typecheck_exp] Application of function %a@."
        Ast.print_exp fct;
      let fct', typ = typecheck_exp env fct in
      debug "[typecheck_exp] Unaliased type : %a@." Love_type.pretty typ;
      let rec check_arrow acc ftype args =
        match args with
          [] ->
          debug "[typecheck_exp] No more argument, type is %a"
            Love_type.pretty ftype;
          mk_apply fct' (List.rev acc), ftype
        | hd :: tl -> (
            match ftype with
            | TArrow (t1, t2) ->
              debug "[typecheck_exp] fun %a -> %a@."
                Love_type.pretty t1
                Love_type.pretty t2;
              let e, t = typecheck_exp env hd in
              debug "[typecheck_exp] Argument %a has type %a.\
                     Checking against %a, the type of %a@."
                Ast.print_exp hd
                Love_type.pretty t
                Love_type.pretty t1
                Ast.print_exp fct;
              check ?loc env t1 t;
              debug "[typecheck_exp] Argument has the correct type, continuing with %a.@."
              Love_type.pretty t2;
              check_arrow (e :: acc) t2 tl
            | _ -> (
              debug "[typecheck_exp] %a is not callable.@."
                Love_type.pretty ftype;
              let is_forall = match ftype with TForall _ -> true | _ -> false in
              let msg : _ format4 =
                if is_forall
                then
                  "Call error : %a is applied to %a of type %a, which is not a function. \
                   Did you forget the type application ?"
                else
                  "Call error : %a is applied to %a of type %a, which is not a function."
              in
              type_error loc env msg
                Love_printer.Ast.print_exp hd
                Love_printer.Ast.print_exp fct
                Love_type.pretty ftype
            )
          )
      in
      begin match typ with
      | TEntryPoint pt ->
         (* Allow calling of entry points as function applications *)
         let open Love_ast_utils in
         let loc = exp.annot in
         let e = mk_const ?loc (mk_cprimitive PCCall) in
         let e = mk_tapply ?loc e pt in
         let e = mk_apply ?loc e (fct :: args) in
         typecheck_exp env e
      | TView (pt, rt) ->
         (* Allow calling of views as function applications *)
         let open Love_ast_utils in
         let loc = exp.annot in
         let e = mk_const ?loc (mk_cprimitive PCView) in
         let e = mk_tapply ?loc e pt in
         let e = mk_tapply ?loc e rt in
         let e = mk_apply ?loc e (fct :: args) in
         typecheck_exp env e
      | _ -> check_arrow [] typ args
      end
    | Seq el ->
      let rec check_seq acc =
        function
          [] -> type_error loc env "Empty sequences are forbidden"
        | [e] -> let e, t = typecheck_exp env e in
          Love_ast_utils.mk_seq (List.rev (e :: acc)), t
        | hd :: tl ->
          let e, shouldbeunit = typecheck_exp env hd in
          check ?loc env shouldbeunit TUnit;
          check_seq (e :: acc) tl
      in check_seq [] el

    | If { cond; ifthen; ifelse } -> (
        debug "[typecheck_exp] If ";
        let cond, shouldbebool = typecheck_exp env cond in
        check ~error:"Condition is not boolean" ?loc env shouldbebool TBool;
        let ifthen, thentype = typecheck_exp env ifthen in
        let ifelse, elsetype = typecheck_exp env ifelse in
        try
          check ?loc env thentype elsetype;
          mk_if cond ifthen ifelse, thentype
        with TypingError _ ->
          check ?loc env elsetype thentype; mk_if cond ifthen ifelse, elsetype
      )

    | Match { arg; cases } ->
      let arg', typ = typecheck_exp env arg in
      debug "[typecheck_exp] Match on %a : %a@."
        Ast.print_exp arg (Love_type.pretty) typ;
      let treat_case env (pat,exp) =
        let env' = typecheck_pattern loc env typ pat in
        let exp, exptyp = typecheck_exp env' exp in
        (pat, exp), exptyp
      in
      let hd_case, other_cases =
        match cases with
          [] -> type_error loc env "Empty pattern matching are forbidden"
        | hd :: tl -> hd, tl in
      let hd_exp, hd_typ = treat_case env hd_case in
      let tref = ref hd_typ in
      (* We will update this ref in the next loop. We want to prove that
         every branch have the same type, so we will perform a subtype check
         on the first type against the second, the second against the thierd, etc. and
         the lasr againse the first. *)
      let tl_cases =
        List.map
          (fun case ->
             let case, typ = treat_case env case in
             check ?loc env !tref typ;
             tref := typ;
             case
          )
          other_cases in
      check ?loc env hd_typ !tref;
      (mk_match arg' (hd_exp :: tl_cases)), hd_typ

    | Constructor { constr; ctyps; args } -> (
        let args, arg_typs =
          List.map
            (fun exp ->
               typecheck_exp env exp
            )
            args
          |> List.split
        in
        (* AST transformation : if the constructor expects 1 argument
           and args has more or equal  than 2 arguments, then args is
           converted to a tuple *)
        debug "[typecheck_exp] Searching type of constructor %a@."
          Ident.print_strident constr;
        let ctyp = Love_tenv.find_constr constr env in
        match ctyp with
        | None ->
          type_error loc env "Constructor %a is unknown" Ident.print_strident constr
        | Some {result = ct; _} ->
          let targs =
            let c_init_params =
              match ct.cparent with
                TUser (n, l) ->
                debug "[typecheck_exp] Constructor %a belongs to type %a@."
                  Ident.print_strident constr Ident.print_strident n;
                l
              | _ -> raise (Exceptions.InvariantBroken "Registered parent must be a TUser")
            in
            let replace_map =
              try
                List.fold_left2
                  (fun acc old new_t ->
                     match old with
                       TVar tv ->
                       debug "[typecheck_exp] Replacing %a by %a@."
                         Love_type.pp_typvar tv
                         (Love_type.pretty) new_t
                       ;
                       TypeVarMap.add tv new_t acc
                     | _ -> raise (
                         Exceptions.InvariantBroken
                           "Registered parent must be a TUser with only tvars as parameters")
                  )
                  TypeVarMap.empty
                  c_init_params
                  ctyps
              with
                Invalid_argument _ ->
                type_error loc env
                  "Constructor %a expects %i type arguments, but %i are provided."
                  Ident.print_strident constr
                  (List.length c_init_params)
                  (List.length ctyps)
            in
            List.map
              (Love_type.replace_map (fun x -> Love_tenv.isComp x env) replace_map)
              ct.cargs in
          let targs =
            match targs, arg_typs with
            | [TTuple _], [TTuple _] -> targs
            | [TTuple l], _ -> l
            | t,_ -> t in
          let () =
            try List.iter2 (check env) targs arg_typs
            with Invalid_argument _ ->
              type_error loc env
                "Constructor %a expects %i arguments, but only %i are provided."
                Ident.print_strident constr
                (List.length targs)
                (List.length args)
          in
          debug "[typecheck_exp] Check OK.@.";
          let args =
            if Compare.Int.(List.length ct.Love_tenv.cargs = 1 &&
                            List.length ctyps >= 2)
            then (
              debug "[typecheck_exp] Expecting a tuple.";
              [ mk_tuple args ]
            )
            else args in

          let constr_typ =
            match ct.cparent with
              TUser (name, _) ->
              (* Additional check : is this constructor public, private, abstract, internal ?*)
              let name = Ident.change_last constr name in
              check_private loc name env;
              TUser (name, ctyps)
            (* Check done *)
            | _ -> assert false
          in
          mk_constr constr ctyps args, constr_typ
      )

    | ESome e -> let e, t = typecheck_exp env e in mk_some e, TOption t

    | ENone -> exp, Love_type.type_cnone
    | List (el, e) -> (
        match el with
          [] -> (* This should never happen, (:: l) has no sense*)
          type_error loc env "List expressions must be prefixed by at least 1 argument"
        | _ ->
          let e, etyp =  typecheck_exp env e in
          let eltlist_type =
            match etyp with
              TList t -> t
            | TForall _ ->
              type_error loc env
                "List expression %a has type %a, which is polymorphic. \
                 Did you forget the type application?"
                Love_printer.Ast.print_exp e
                (Love_type.pretty) etyp
            | _ ->
              type_error loc env
                "List expression %a has type %a, which is not a list"
                Love_printer.Ast.print_exp e
                (Love_type.pretty) etyp
          in
          let el =
            List.map
              (fun elt ->
                 let elt', elt_typ = typecheck_exp env elt in
                 if Love_type.bool_equal elt_typ eltlist_type
                 then elt'
                 else (
                   type_error loc env
                     "List element %a has type %a, \
                      while it was expected to have the type %a"
                     Ast.print_exp elt
                     Love_type.pretty elt_typ
                     Love_type.pretty eltlist_type;
                 )
              )
              el in
          {content = List (el, e); annot = None}, etyp
      )
    | Nil -> mk_enil (), Love_type.type_empty_list
    | Tuple []  ->
      type_error loc env "Empty tuples are not allowed"
    | Tuple [_] ->
      type_error loc env
        "Tuple %a has only 1 element: must at least have 2 elements"
        Love_printer.Ast.print_exp exp
    | Tuple el  ->
      let el, ety =
        List.map (fun e ->
            let e', t = typecheck_exp env e in
            debug "[typecheck_exp] Tuple element %a : %a@."
              Ast.print_exp e Love_type.pretty t;
            e',t
          )
          el |> List.split
      in
      mk_tuple el, TTuple ety

    | Project { tuple; indexes } -> (
        let tuple, typ = typecheck_exp env tuple in
        match typ with
        | TTuple l -> (
            let tys = List.map (fun i ->
               match List.nth_opt l i with
               | Some t -> t
               | None ->
                 type_error loc env
                   "Projection out of bounds: \
                    %ith element (starting at 0) of %a is undefined"
                   i
                   Love_printer.Ast.print_exp tuple
              ) indexes
            in
            match tys with
            | [t] -> mk_projection tuple indexes, t
            | tl -> mk_projection  tuple indexes, TTuple tl
          )
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot project on a non-tuple expression"
            Love_printer.Ast.print_exp tuple
            (Love_type.pretty) typ
      )

    | Update { tuple; updates } ->
      (* Invariants : Updates not empty, never update twice the same field
         and max lower than tuple size  *)
      let tuple, tup_type = typecheck_exp env tuple in
      let tup_types, tup_length =
        match tup_type with
          TTuple l -> l, List.length l
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot update a non-tuple expression"
            Love_printer.Ast.print_exp tuple
            (Love_type.pretty) tup_type
      in
      let rec check_equal acc l =
        match l with
          [] -> acc
        | (i, e) :: tl ->
          if Compare.Int.(i < tup_length)
          then check_equal (IntMap.add i (typecheck_exp env e) acc) tl
          else
            type_error loc env
              "Tuple %a has size %i: update on index %i is impossible"
              Love_printer.Ast.print_exp tuple
              tup_length
              i
      in
      let update_types = check_equal IntMap.empty updates in
      let () =
        List.iteri
          (fun i tup_type ->
             match IntMap.find_opt i update_types with
               None -> ()
             | Some (_, t) ->
               check ~error:"Type of update is inconsistent with tuple type."
                 ?loc env t tup_type
          )
          tup_types
      in
      let updates =
        List.map
          (fun (i, _) ->
             match IntMap.find_opt i update_types with
               None -> assert false
             | Some (e, _) -> (i, e)
          )
          updates in
      mk_update tuple updates, tup_type

    | Record { contents = []; _} -> type_error loc env "Empty record are forbidden"
    | Record { path; contents = l }  ->
      let l, ftyp =
        List.split @@
        List.rev @@
        List.fold_left
          (fun acc (name, exp) ->
             let e, t = typecheck_exp env exp in
             ((name, e), (name, t)) :: acc
          )
          []
          l
      in
      let typ = Love_tenv.record_type path ftyp env in
      mk_record path l, typ

    | GetField { record; field } -> (
        let record, typ = typecheck_exp env record in
        match typ with
          TUser (name, args) ->
          let rec_def =
            match Love_tenv.find_record name env with
            | None ->
              type_error loc env
                "Record type %a is unknown"
                print_strident name
            | Some t -> t.result in
          let ftyp_opt = List.find_opt (fun tf ->
              String.equal tf.Love_tenv.fname field
            ) rec_def in
          let ftyp = match ftyp_opt with
            | None -> raise (UnknownField field)
            | Some t -> t in
          let ftyp_args = Love_tenv.field_with_targs ftyp args env in
          mk_get_field record field, ftyp_args.ftyp
        | _ ->
          type_error loc env
            "Expression %a has type %a: expected a record type."
            Love_printer.Ast.print_exp record
            (Love_type.pretty) typ
      )
    | SetFields { updates = [];_ } ->
      type_error loc env "SetFields expression should have at least one update"

    | SetFields {record; updates} ->
      debug "[typecheck_exp] Update of record %a@."
        Ast.print_exp record;
      let record, typ_rec = typecheck_exp env record in
      debug "[typecheck_exp] Record type %a@."
        (Love_type.pretty) typ_rec;
      let rec_def, targs = match typ_rec with
        | TUser (name, args) ->
          begin match Love_tenv.find_record name env with
            | None ->
              type_error loc env
                "Record type %a is unknown"
                print_strident name
            | Some t -> t.result, args
          end
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot set fields on a non-record type."
            Love_printer.Ast.print_exp record
            (Love_type.pretty) typ_rec
      in
      let poly_args_to_arg_map =
        match (List.hd rec_def).Love_tenv.fparent with
          TUser (_, a) ->
          List.fold_left2
            (fun acc parg arg ->
               match parg with
                 TVar tv -> TypeVarMap.add tv arg acc
               | _ ->  raise (Exceptions.InvariantBroken "Record type def should be polymorphic")
            )
            TypeVarMap.empty
            a
            targs
        | _ -> raise (Exceptions.InvariantBroken "Record type should be a User Type")
        | exception Failure _ -> raise (
            Exceptions.InvariantBroken "Record type should define at least 1 field")
      in
      let replace = Love_type.replace_map (fun x -> Love_tenv.isComp x env) poly_args_to_arg_map in
      let treat_field (field, updt) =
        debug "[typecheck_exp] Field %s takes %a@." field
          Ast.print_exp updt;
        let ftyp_opt =
          List.find_opt
            (fun tf ->
               String.equal tf.Love_tenv.fname field
            ) rec_def in
        let ftyp =
          match ftyp_opt with
            None ->
            type_error loc env "Field %s not registered in environment" field
          | Some t -> t
        in
        check ~error:(
          spf "Field %s not belonging to the correct record" field)
          ?loc
          env
          (replace ftyp.fparent)
          typ_rec;
        let updt, t_updt = typecheck_exp env updt in
        check ~error:(spf "Badly typed field %s" field)
          ?loc
          env
          t_updt
          (replace ftyp.ftyp);
        (field, updt)
      in
      let updates = List.map treat_field updates in
      mk_set_field record updates, typ_rec

    | TLambda {targ; exp} ->
      debug "[typecheck_exp] TLambda (%a,%a)@."
        Love_type.pp_typvar targ Ast.print_exp exp;
      let env = Love_tenv.add_forall targ env in
      let exp', t = typecheck_exp env exp in
      debug "[typecheck_exp] TLambda (%a,%a) : %a@."
        Love_type.pp_typvar targ Ast.print_exp exp (Love_type.pretty) t;
      mk_tlambda targ exp', TForall (targ, t)

    | TApply {exp = {content = TLambda {targ; exp = exp'}; _}; typ} -> (
        debug "[typecheck_exp] TLambda in TApply : changing %a into %a@."
          Love_type.pp_typvar targ (Love_type.pretty) typ
        ;
        match is_safe_res env TInternal typ with
        Ok () ->
          let env = Love_tenv.add_forall targ env in
          let exp', t = typecheck_exp env exp' in
          debug "[typecheck_exp] TApply : Before replacing %a by %a, type is %a@."
            Love_type.pp_typvar targ (Love_type.pretty) typ
            (Love_type.pretty) t;
          let t = Love_type.replace (fun x -> Love_tenv.isComp x env) targ typ t in
          debug "[typecheck_exp] TApply : Type is %a@." (Love_type.pretty) t;
          mk_tapply (mk_tlambda targ exp') typ, t
        | Error s ->
          type_error loc env
            "Unsafe type application of %a: %s"
            (Love_type.pretty) typ
            s
      )
    | TApply {exp; typ} -> (
        debug "[typecheck_exp] Polymorphic expression %a applied to type %a@."
          Ast.print_exp exp
          Love_type.pretty typ;
        match is_safe_res env TInternal typ with
        Ok () -> (
        let exp', t' = typecheck_exp env exp in
        debug "[typecheck_exp] Polymorphic expression %a has type %a@."
          Ast.print_exp exp
          Love_type.pretty t';
        match t' with
          TForall (tvar, t) ->
          debug "[typecheck_exp] Replacing %a by %a@."
            Love_type.pp_typvar tvar Love_type.pretty typ;
          let new_type = Love_type.replace (fun x -> Love_tenv.isComp x env) tvar typ t in
          debug "[typecheck_exp] Updated type : %a@."
            Love_type.pretty new_type;
          mk_tapply exp' typ, new_type
        (*| TUser (n,l) -> (
          debug "[typecheck_exp] Polymorphic TUser typ@.";
          let rec replace_first_arg tl =
            match tl with
              [] ->
              debug "[typecheck_exp] Cannot apply [%a] on TUser %a@."
                (Love_type.pretty) typ (Love_type.pretty) t';
              type_error loc env
                "Expression %a has type %a, which is not quantified: cannot apply type %a"
                Love_printer.Ast.print_exp exp
                (Love_type.pretty) t'
                (Love_type.pretty) typ
              raise (TypingError ("TApply on a non quantified TUser type.", loc, env))
            | ((TVar tv) as t) :: tl ->
              if Love_tenv.is_free tv env
              then (
                debug "[typecheck_exp] Type variable %s already used@."
                tv.tv_name;
                t :: replace_first_arg tl ) else typ :: tl
            | t :: tl -> t :: replace_first_arg tl
          in
          let t = TUser (n, replace_first_arg l)
          in
          debug "[typecheck_exp] Polymorphic TUser typ = %a@." (Love_type.pretty) t;
          mk_tapply exp' typ, t
          ) *)
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot apply %a on non-polymorphic expressions."
            Love_printer.Ast.print_exp exp
            (Love_type.pretty) t'
            (Love_type.pretty) typ
      )
        | Error s ->
          type_error loc env "Unsafe application of type %a: %s"
            (Love_type.pretty) typ
            s
      )
    | PackStruct sr -> (
        match sr with
          Named n -> (
            match Love_tenv.find_contract n env with
              Some {result; _} ->
              mk_packstruct (Named n),
              TPackedStructure (Anonymous (Love_tenv.env_to_contract_sig result))
            | None ->
              type_error loc env
                "Contract %a not found in environment" print_strident n
          )
        | Anonymous s ->
          let emptyenv =
            Love_tenv.new_subcontract_env
              Constants.anonymous
              s.kind () env
          in
          let s, newenv = typecheck_struct loc emptyenv s in
          let ctrtyp = Love_tenv.env_to_contract_sig newenv in
          mk_packstruct (Anonymous s), TPackedStructure (Anonymous ctrtyp)
      )
    | Raise { exn; args; loc } ->
      debug "[typecheck_exp] Raise@.";
      let args_typ =
        match exn with
          Fail t -> [t]
        | Exception exn ->
          match Love_tenv.find_exception exn env with
            None ->
            type_error loc env "Undefined exception %a" print_strident exn
          | Some {result; _} -> result
      in
      let args, args_typ =
        match args, args_typ with
          [], [TUnit] ->
          [mk_const (mk_cunit ())], [TUnit]
        | [{content = Tuple l;_}], [TTuple t] -> l, t
        | [{content = Tuple l;_}], t -> l, t
        | l, [TTuple t] -> l, t
        | _,_ -> args, args_typ in
      let alen = List.length args in let tlen = List.length args_typ in
      let () =
        if ((Compare.Int.(<>)) alen tlen)
        then
          type_error loc env
            "Exception %a expects %i arguments, but it is applied to %i arguments."
            print_exn_name exn
            tlen alen
      in
      let rec check_args acc args args_typ =
        match args, args_typ with
          [], [] -> List.rev acc
        | arg :: tlarg, targ :: tlargt ->
          let arg, t = typecheck_exp env arg in
          check ?loc env t targ;
          check_args (arg :: acc) tlarg tlargt
        | _,_ -> assert false
      in
      let args = check_args [] args args_typ in
      let tvar = Love_type.fresh_typevar () in
      mk_raise ?loc exn args, TForall (tvar, TVar (tvar))

    | TryWith { arg; cases } ->
      debug "[typecheck_exp] TryWith@.";
      let arg, typ = typecheck_exp env arg in
      let typecheck_case ((ename, plist), exp) =
        let test_exp env pats =
          let exp, etyp = typecheck_exp env exp in
          if Love_type.bool_equal typ etyp
          then (ename, pats), exp
          else
            type_error loc env
              "Exception cases must have the same type: %a and %a are incompatible"
              (Love_type.pretty) typ
              (Love_type.pretty) etyp
        in
        let rec check_pat acc_pats acc_env pats args_typ =
          match pats, args_typ with
            [], [] -> test_exp acc_env (List.rev acc_pats)
          | pat :: tlarg, targ :: tlargt ->
            let env = typecheck_pattern loc acc_env targ pat in
            check_pat (pat :: acc_pats) env tlarg tlargt
          | _,_ ->
            type_error loc env "Failure duing pattern typechecking in exception."
        in
        match ename with
          Love_ast.Fail t -> (
            match is_safe_res env TInternal t with
              Ok () -> (
                debug "[typecheck_exp] Fail (%a)" (Love_type.pretty) t;
                let t = Love_tenv.normalize_type ~relative:true t env in
                match plist, t with
                  [], TUnit -> test_exp env []
                | [],_ ->
                  type_error loc env
                    "Failure with no argument must have type unit, but it has type %a"
                    (Love_type.pretty) t
                | [{content = PTuple plist; _}], TTuple l
                | plist, TTuple l -> check_pat [] env plist l
                | [p], t -> check_pat [] env [p] [t]
                | _,t ->
                  type_error loc env
                    "Failure badly typed by %a" (Love_type.pretty) t
              )
            | Error s ->
              type_error loc env
                "Failure applied the unsafe type %a: %s"
                (Love_type.pretty) t
                s
          )
        | Exception ename ->
          debug "[typecheck_exp] Fail (%a)" Ident.print_strident ename;
          let args_typ =
            match Love_tenv.find_exception ename env with
              None ->
              type_error loc env
                "Undefined exception %a" print_strident ename
            | Some {result; _} -> result
          in
          let plist, args_typ =
            match plist, args_typ with
              [], [TUnit] -> [mk_pany ()], [TUnit]
            | [{content = PTuple l; _}], [TTuple t] -> l, t
            | [{content = PTuple l; _}], t -> l, t
            | l, [TTuple t] -> l, t
            | _,_ -> plist, args_typ in
          let () =
            let plen = List.length plist in let tlen = List.length args_typ in
            if ((Compare.Int.(<>)) plen tlen)
            then
              type_error loc env
                "Exception in try with %a expects %i arguments,\
                 but it is applied to %i arguments.@."
                print_strident ename
                tlen plen
          in
          check_pat [] env plist args_typ
      in
      let l = List.map typecheck_case cases in
      mk_try_with arg l, typ
  in
  debug "[typecheck_exp] Expression %a : %a@."
    Ast.print_exp exp Love_type.pretty t;
  let t = Love_tenv.normalize_type ~relative:true t env in
  debug "[typecheck_exp] Expression %a with normalized type : %a@."
    Ast.print_exp exp Love_type.pretty t;
  e, t

and typecheck_signature
    loc
    (parent_env : unit Love_tenv.t)
    (name : String.t)
    (sign : Love_type.structure_sig) =
  debug "[typecheck_signature] Adding signature %s = %a to environment@." name
    Love_type.pp_contract_sig sign
  ;
  let sig_env = Love_tenv.new_subcontract_env name sign.sig_kind () parent_env
  in
  let typecheck_sig_content sig_env (name, content) =
    (* to check : non polymorphic storage type,
       safe type definitions and type uses
       no entry points on modules *)
    match content with
    | SType ts -> (
        let storage_test tdef =
          if String.equal name "storage" && not (Love_type.is_module sign)
          then match Love_type.typedef_parameters tdef with [] -> true | _ -> false
          else true
        in
        match ts with
          SPublic tdef -> (
            if is_safe_tdef TPublic name tdef sig_env && storage_test tdef
            then Love_tenv.add_typedef name TPublic tdef sig_env
            else type_error loc sig_env "Unsafe public type %s in signature" name
          )

        | SPrivate tdef -> (
            if is_safe_tdef TPrivate name tdef sig_env && storage_test tdef
            then Love_tenv.add_typedef name TPrivate tdef sig_env
            else type_error loc sig_env "Unsafe private type %s in signature" name
          )

        | SAbstract tl -> (
            let ok () = Love_tenv.add_abstract_type name tl sig_env in
            if String.equal name "storage" && not (Love_type.is_module sign)
            then match tl with
                [] -> ok ()
              | _ -> type_error loc sig_env "Unsafe abstract type %s in signature" name
            else ok ()
          )
      )
    | SException l -> (
        match is_safe_res_list sig_env TAbstract l with
          Ok () -> Love_tenv.add_exception name l sig_env
        | Error s -> type_error loc sig_env "Unsafe exception %s in signature: %s" name s
      )

    | SEntry t -> (
        if Love_type.is_module sign
        then
          type_error loc sig_env
            "Signature of module cannot contain entry points (%s)"
            name
        else (
          match is_safe_res sig_env TAbstract t with
            Ok () -> Love_tenv.add_var ~kind:Entry name (TEntryPoint t) sig_env
          | Error s ->
            type_error loc sig_env "Type %a for entry point %s is unsafe: %s"
              (Love_type.pretty) t
              name
              s
        )
      )
    | SView (t1, t2) -> (
        if Love_type.is_module sign
        then
          type_error loc sig_env "Signature of module cannot contain views (%s)" name
        else (
          match is_safe_res sig_env TAbstract t1, is_safe_res sig_env TAbstract t2 with
            Ok (), Ok () -> Love_tenv.add_var ~kind:View name (TView (t1, t2)) sig_env
          | Error s, _ ->
            type_error loc sig_env
              "Unsafe parameter type %a for view %s: %s"
              (Love_type.pretty) t1
              name
              s
          | _, Error s ->
            type_error loc sig_env
              "Unsafe return type %a for view %s: %s"
              (Love_type.pretty) t2
              name
              s
        )
      )

    | SValue t -> (
        match is_safe_res sig_env TAbstract t with
          Ok () -> Love_tenv.add_var ~kind:(Value Public) name t sig_env
        | Error s ->
          type_error loc sig_env
            "Unsafe type %a for value %s: %s"
            (Love_type.pretty) t
            name
            s
      )

    | SStructure str -> (
        match str with
          Named n -> (
            match Love_tenv.find_signature n sig_env with
              None ->
              type_error loc sig_env
                "Signature %a for structure %s is undefined" Ident.print_strident n name
            | Some _ -> Love_tenv.add_signed_subcontract name n sig_env
          )
        | Anonymous s ->
          let str_env = typecheck_signature loc sig_env name s in
          Love_tenv.add_subcontract_env name str_env sig_env
      )
    | SSignature s_sig  ->
      let sub_sig_env =
        typecheck_signature
          loc
          sig_env
          name
          s_sig
      in Love_tenv.add_signature name sub_sig_env sig_env

  in
  let env =
    List.fold_left
      typecheck_sig_content
      sig_env
      sign.sig_content
  in
  debug "[typecheck_signature] New environment: %a@." Love_tenv.pp_env env;
  env

and typecheck_struct
    loc
    (env : unit Love_tenv.t)
    (ctr : structure) : structure * unit Love_tenv.t =
  debug "[typecheck_struct] Typechecking a %s@."
    (if Love_ast.is_module ctr then "module" else "contract");
  let treat_content name_acc env (name, content) :
    Collections.StringSet.t * content * unit Love_tenv.t =
    let () =
      if Collections.StringSet.mem name name_acc
      then
        type_error loc env "Top level %s is defined twice" name
    in
    let name_acc = Collections.StringSet.add name name_acc in
    match content with
    | DefType (k,t) -> (
        if is_safe_tdef k name t env
        then (
          let tdef, new_env = DefType (k,t), Love_tenv.add_typedef name k t env in
          if String.equal name "storage" && not (Love_ast.is_module ctr)
          then
            match k, Love_type.typedef_parameters t with
            | TInternal, _ ->
              type_error loc env "Storage type definition cannot be internal"
            | _, _ :: _ ->
              type_error loc env "Storage definition cannot be polymorphic"
            | _, [] -> name_acc, tdef, new_env
          else name_acc, tdef, new_env
        )
        else
          type_error loc env
            "Definition of type %s = %a is unsafe"
            name (Love_type.pp_typdef ~privacy:(Love_ast.kind_to_string k) ~name) t
      )

    | DefException tl -> (
        match is_safe_res_list env TAbstract tl with
          Ok () ->
          name_acc, DefException tl, Love_tenv.add_exception name tl env
        | Error s ->
          type_error loc env "Definition of exception %s is unsafe: %s"
            name s
      )

    | Entry ({ entry_code; entry_fee_code; entry_typ } as e) -> (
        debug "[typecheck_struct] Entry = %a : %a@."
          Ast.print_entry e
          (Love_type.pretty) (entry_typ)
        ;
        let () =
          if check_forbidden_type ~where:TopLevel env (TEntryPoint entry_typ)
          then ()
          else
            type_error loc env
              "Error on %s: elements of type %a are forbidden at top-level"
              name
              (Love_type.pretty) (TEntryPoint entry_typ)
        in
        match is_safe_res env TPublic entry_typ with
          Ok () -> (
            let () =
              if Love_ast.is_module ctr ||
                 not (check_forbidden_type env ~where:Parameter entry_typ)
              then
                type_error loc env "Forbidden entry point in module (%s)" name
            in
            debug "[typecheck_struct] Entry : starting the typechecking@.";
            let env =
              Love_tenv.add_var
                ~kind:Love_tenv.(Entry)
                name
                (TEntryPoint (entry_typ))
                env in
            let env, typ_as_lambda =
              let env, type_storage =
                match Love_tenv.get_storage_type env with
                  None ->
                  Love_tenv.add_typedef
                    "storage" TPublic (Alias {aparams = []; atype = TUnit}) env,
                  TUnit
                | Some t -> env, t in
              let lambda =
                Love_type.entryPointType
                  ~type_storage
                  entry_typ in
              env, Love_tenv.normalize_type ~relative:true lambda env
            in
            let entry_code, t = typecheck_exp env entry_code in
            debug "[typecheck_struct] Entry (code) : Checking compatibility between %a and %a@."
              Love_type.pretty typ_as_lambda Love_type.pretty t;
            check ?loc env typ_as_lambda t;

            let entry_fee_code = match entry_fee_code with
              | None -> None
              | Some fee_code ->
                match Love_ast_utils.Raw.lambdas_to_params entry_code with
                (* As code has been typechecked, it should be a lambda with 3 arguments *)
                | (p1, t1) :: (p2, t2) :: (p3, t3) :: _ ->
                  let env = typecheck_pattern loc env t1 p1 in
                  let env = typecheck_pattern loc env t2 p2 in
                  let env = typecheck_pattern loc env t3 p3 in
                  let fee_code, t = typecheck_exp env fee_code in
                  let expected_ty = TTuple [TDun; TNat] in
                  debug "[typecheck_struct] Entry (fee_code) :\
                         Checking compatibility between %a and %a@."
                    Love_type.pretty expected_ty
                    Love_type.pretty t;
                  check ?loc env expected_ty t;
                  Some fee_code
                | _ -> assert false
            in

            name_acc, Entry ({ entry_code; entry_fee_code; entry_typ }), env
          )
        | Error s ->
          type_error loc env "Type %a of entry point %s is unsafe: %s"
            (Love_type.pretty) entry_typ name s
      )

    | View ({ view_code; view_typ = (t1, t2) } as v) -> (
        let () =
          if check_forbidden_type ~where:TopLevel env (TView (t1, t2))
          then ()
          else
            type_error loc env
              "Error on %s: elements of type %a are forbidden at top-level"
              name
              (Love_type.pretty) (TView (t1, t2))
        in
        debug "[typecheck_struct] View = %a : %a -> %a@."
          Ast.print_view v
          (Love_type.pretty) t1
          (Love_type.pretty) t2
        ;
        match is_safe_res env TAbstract t1, is_safe_res env TAbstract t2 with
          Ok (), Ok () -> (
            let () =
              if Love_ast.is_module ctr
              then
                type_error loc env "Forbidden view in module (%s)" name
            in
            debug "[typecheck_struct] View : starting the typechecking@.";
            let env =
              Love_tenv.add_var
                ~kind:Love_tenv.(View)
                name
                (TView (t1, t2))
                env in
            let env, typ_as_lambda =
              let env, type_storage =
                match Love_tenv.get_storage_type env with
                  None ->
                  Love_tenv.add_typedef
                    "storage" TPublic (Alias {aparams = []; atype = TUnit}) env,
                  TUnit
                | Some t -> env, t in
              let lambda =
                Love_type.viewType ~type_storage t1 t2 in
              env, Love_tenv.normalize_type ~relative:true lambda env
            in
            let view_code, t = typecheck_exp env view_code in
            debug "[typecheck_struct] View : Checking compatibility between %a and %a@."
              Love_type.pretty typ_as_lambda Love_type.pretty t;
            check ?loc env typ_as_lambda t;
            name_acc, View ({ view_code; view_typ = t1, t2 }), env
          )
        | Error s, _ ->
          type_error loc env
            "Unsafe parameter type %a for view %s: %s"
            (Love_type.pretty) t1
            name
            s
        | _, Error s ->
          type_error loc env
            "Unsafe return type %a for view %s: %s"
            (Love_type.pretty) t2
            name
            s
      )

    | Value ({ value_code; value_typ; value_visibility } as v) -> (
        debug "[typecheck_struct] Value = %a : %a@."
          Ast.print_value v
          (Love_type.pretty) value_typ
        ;
        let () = (* Checking if the type is internal and the visibility public *)
          if check_forbidden_type ~where:TopLevel env value_typ
          then (
            match value_visibility with
              Public ->
              if Love_tenv.is_internal value_typ env
              then
                type_error loc env "Value %s is public, but its type is internal" name
              else ()
            | Private -> ()
          )
          else
            type_error loc env
              "Value %s has type %a, which is forbidden at top level" name
              (Love_type.pretty) value_typ
        in
        match is_safe_res env TInternal value_typ with
          Ok () -> (
            let value_code, t = typecheck_exp env value_code in
            debug "[typecheck_struct] Value : Checking compatibility between %a and %a@."
              Love_type.pretty value_typ Love_type.pretty t;
            check ?loc env value_typ t;
            name_acc,
            Value { value_code; value_typ; value_visibility },
            Love_tenv.add_var ~kind:(Value (value_visibility)) name value_typ env
          )
        | Error s ->
          type_error loc env "Type %a of value %s is unsafe: %s"
            (Love_type.pretty) value_typ
            name
            s
      )

    | Structure c ->
      debug "[typecheck_struct] Struct = %a@." Ast.print_structure c;
      let sub_env = Love_tenv.new_subcontract_env name c.kind () env in
      let c, sub_env = typecheck_struct loc sub_env c in
      name_acc, Structure c, Love_tenv.add_subcontract_env name sub_env env

    | Signature s ->
      name_acc,
      Signature s,
      Love_tenv.add_signature name (typecheck_signature loc env name s) env
  in
  let _, rev_content, new_env =
    List.fold_left
      (fun (string_set, acc_content, acc_env) ((name, _) as c) ->
         debug "[typecheck_struct] Typechecking %s@." name;
         try let sset, c, e = treat_content string_set acc_env c in
           (sset, (name, c) :: acc_content, e)
         with TypingError (s, loc, env) ->
           debug
             "[typecheck_struct] Typing error, failing with environment %a"
             Love_tenv.pp_env env;
           raise (TypingError (s, loc, env))
            | e ->
              debug
                "[typecheck_struct] Generic error, failing with environment %a"
                Love_tenv.pp_env acc_env;
              raise e
      )
      (Collections.StringSet.empty, [], env)
      ctr.structure_content in
  let () =
    if Love_ast.is_module ctr
    then ()
    else
      match Love_tenv.get_storage_type new_env with
        None -> Log.print "Warning : Contract defined with no storage.@."
      | Some t -> (
          match is_safe_res new_env TAbstract t with
            Ok () -> ()
          | Error s -> type_error loc env "Unsafe storage type: %s" s
        )
  in {
    structure_content = List.rev rev_content;
    kind = ctr.kind
  }, new_env

let typecheck_top_contract
    loc
    (env : unit Love_tenv.t)
    ({ version; code } : top_contract) : top_contract * unit Love_tenv.t =
  let code, env = typecheck_struct loc env code in
  { version; code }, env

let extract_deps
    (loc : location option)
    (ctxt : Love_context.t)
    (env : unit Love_tenv.t)
    (deps : (string * string) list)
  : (Love_context.t * unit Love_tenv.t) Error_monad.tzresult Lwt.t
  =
  List.fold_left
    (fun env (mname, kt1) ->
       env >>=?
       (fun (ctxt, env) ->
          debug "[extract_deps] Dep %s@." mname;
          let b58 = Contract_repr.of_b58check kt1 in
          match b58 with
          | Error _ ->
            type_error loc env "Invalid contract hash %s" kt1
          | Ok kt1' ->
            debug "[extract_deps] Contract found@.";
            (Love_context.get_script ctxt kt1') >>=?
            (function
                _, None ->
                type_error loc env "Contract %s not found" kt1
              | ctxt, Some (ls, _stor) ->
                debug "[extract_deps] Found@.";
                let ls_sig =
                  Love_value.sig_of_structure
                    ~only_typedefs:false
                    ls
                in
                debug "[extract_deps] Signature generated@.";
                let ls_env = Love_tenv.contract_sig_to_env (Some mname) () ls_sig env in
                return (ctxt, Love_tenv.add_subcontract_env mname ls_env env)
            )
       )
    )
    (return (ctxt, env))
    deps

let typecheck_top_contract_deps
    loc
    ctxt
    (env : unit Love_tenv.t)
    (({code; _} as c) : top_contract)
  : (Love_context.t * (top_contract * unit Love_tenv.t)) Error_monad.tzresult Lwt.t =
  let open Error_monad in
  debug "[typecheck_top_contract_deps] Starting to add dependencies@.";
  let deps_name =
    match code.kind with
      Module -> []
    | Contract l -> l
  in
  debug "[typecheck_top_contract_deps] Dependencies\n%a@."
    (Format.pp_print_list (fun fmt (n,k) -> Format.fprintf fmt "%s = %s" n k)) deps_name
  ;
  let env : (Love_context.t * unit Love_tenv.t) Error_monad.tzresult Lwt.t =
    extract_deps loc ctxt env deps_name in
  env >>=?
  (fun (ctxt,e) -> return @@ (ctxt, typecheck_top_contract loc e c))

(* Typechecking of the Runtime AST *)
(* All this duplication is necessary just because the contract parameter,
   which may contain a lambda, must be typechecked against the expected
   contract parameter type *)

let typecheck_rconst _ env (c : Love_runtime_ast.const) : Love_type.t =
  match c with
  | RCUnit        -> TUnit
  | RCBool _      -> TBool
  | RCString _    -> TString
  | RCBytes _     -> TBytes
  | RCInt _       -> TInt
  | RCNat _       -> TNat
  | RCDun _       -> TDun
  | RCKey _       -> TKey
  | RCKeyHash _   -> TKeyHash
  | RCSignature _ -> TSignature
  | RCAddress _   -> TAddress
  | RCTimestamp _ -> TTimestamp
  | RCPrimitive p ->
    Love_primitive.type_of ~sig_of_contract:(sig_of_contract None env) p

let pt_error loc env p t =
  type_error loc env "Runtime Pattern %a is not of type %a"
    Love_printer.Runtime.print_rpattern p
    (Love_type.pretty) t

let rec typecheck_rpattern loc env t pattern : unit Love_tenv.t =
  let opt_res =
    match pattern with
    | Love_runtime_ast.RPAny -> tpc_pany loc t env
    | RPConst c -> tpc_pconst typecheck_rconst loc t c env
    | RPVar v -> tpc_pvar loc t v env
    | RPAlias (pat, v) -> tpc_palias typecheck_rpattern loc t v pat env
    | RPNone -> tpc_pnone loc t env
    | RPSome pat -> tpc_psome typecheck_rpattern loc t pat env
    | RPList [] -> tpc_pnil None t env
    | RPList pl -> tpc_plist typecheck_rpattern loc t pl env
    | RPTuple pl -> tpc_ptuple typecheck_rpattern loc t pl env
    | RPConstr (c, plist) -> tpc_pconstr typecheck_rpattern loc t c plist env
    | RPContract (name, st) -> tpc_pcontract loc t name st env in
  match opt_res with
    Some res -> res
  | None -> pt_error loc env pattern t

let typecheck_rpattern = typecheck_rpattern None

let rec typecheck_rexp (env : unit Love_tenv.t) (exp : Love_runtime_ast.exp) : typ =
  let loc = None in
  let t =
    match exp with
    | RConst c -> typecheck_rconst loc env c

    | RVar v -> tcp_evar loc v env

    | RLet { bnd_pattern; bnd_val; body} ->
      let t = typecheck_rexp env bnd_val in
      let env' = typecheck_rpattern env t bnd_pattern in
      typecheck_rexp env' body

    | RLetRec { bnd_var; bnd_val; body; val_typ } ->
      if is_safe env TInternal val_typ
      then begin
        let env' = Love_tenv.add_var bnd_var val_typ env in
        let t' = typecheck_rexp env' bnd_val in
        check env val_typ t';
        typecheck_rexp env' body
      end
      else raise (TypingError (
          Format.asprintf
            "Type %a is unsafe : it contains unknown variable types"
            (Love_type.pretty) val_typ, None, env))

    | RLambda { args = []; body } -> typecheck_rexp env body

    | RLambda { args = (RPattern (arg_pattern, arg_typ)) :: args; body } ->
      if is_safe env TInternal arg_typ
      then
        let env' = typecheck_rpattern env arg_typ arg_pattern in
        TArrow (arg_typ, typecheck_rexp env' (RLambda { args; body }))
      else raise (TypingError (
          Format.asprintf "Type of argument %a is not safe"
            (Love_type.pretty) arg_typ, None, env))

    | RLambda { args = (RTypeVar tv) :: args; body } ->
      let env' = Love_tenv.add_forall tv env in
      TForall (tv, typecheck_rexp env' (RLambda { args; body }))

    | RApply { exp; args } ->
      let typ = typecheck_rexp env exp in
      let rec check_arrow ftype = function
        | [] -> ftype
        | (Love_runtime_ast.RExp e) :: al ->
          begin match ftype with
            | TArrow (t1, t2) ->
              check env t1 (typecheck_rexp env e);
              check_arrow t2 al
            | _ -> raise (TypingError (
                "Arguments applied to a non-function value", None, env))
          end
        | (RType typ) :: al ->
          if is_safe env TInternal typ then
            match ftype with
            | TForall (tvar, t) ->
              check_arrow
                (Love_type.replace (fun x -> Love_tenv.isComp x env) tvar typ t) al
            | TUser (n, tl) ->
              let rec replace_first_arg = function
                | [] -> raise (TypingError (
                    "TApply on a non quantified TUser type.", None, env))
                | TVar _tv :: tl -> typ :: tl
                | t :: tl -> t :: replace_first_arg tl
              in
              check_arrow (TUser (n, replace_first_arg tl)) al
            | _ -> raise (TypingError (
                "TApply on a non quantified type.", None, env))
          else raise (TypingError ("TApply on an unsafe type", None, env))
      in
      check_arrow typ args

    | RSeq el ->
      let rec check_seq = function
        | [] -> raise (TypingError (
            "Empty sequences are forbidden", None, env))
        | [e] -> typecheck_rexp env e
        | e :: el ->
          check env (typecheck_rexp env e) TUnit;
          check_seq el
      in
      check_seq el

    | RIf { cond; ifthen; ifelse } ->
      let ct = typecheck_rexp env cond in
      check ~error:"Condition is not boolean" env ct TBool;
      let tt = typecheck_rexp env ifthen in
      let et = typecheck_rexp env ifelse in
      (try check env tt et; tt
       with TypingError _ -> check env et tt; et)

    | RMatch { arg; cases } ->
      let t = typecheck_rexp env arg in
      let treat_case env (pat, exp) =
        let env' = typecheck_rpattern env t pat in
        typecheck_rexp env' exp
      in
      let hd_case, other_cases =
        match cases with
        | [] -> raise (TypingError ("Empty pattern matching", None, env))
        | hd :: tl -> hd, tl in
      let hd_typ = treat_case env hd_case in
      let tref = ref hd_typ in
      List.iter (fun case ->
          let t = treat_case env case in
          check env !tref t;
          tref := t
        ) other_cases;
      check env hd_typ !tref;
      hd_typ

    | RConstructor { type_name; constr; ctyps; args } -> (
        let constr = Ident.change_last type_name (Ident.create_id constr) in
        let arg_typs =
          List.map
            (fun exp ->
               typecheck_rexp env exp
            )
            args
        in
        (* AST transformation : if the constructor expects 1 argument
           and args has more or equal  than 2 arguments, then args is
           converted to a tuple *)
        debug "[typecheck_exp] Searching type of constructor %a@."
          Ident.print_strident constr;
        let ctyp = Love_tenv.find_constr constr env in
        match ctyp with
        | None ->
          type_error loc env "Constructor %a is unknown" Ident.print_strident constr
        | Some {result = ct; _} ->
          let targs =
            let c_init_params =
              match ct.cparent with
                TUser (_, l) -> l
              | _ -> raise (Exceptions.InvariantBroken "Registered parent must be a TUser")
            in
            let replace_map =
              try
                List.fold_left2
                  (fun acc old new_t ->
                     match old with
                       TVar tv ->
                       debug "[typecheck_exp] Replacing %a by %a@."
                         Love_type.pp_typvar tv
                         (Love_type.pretty) new_t
                       ;
                       TypeVarMap.add tv new_t acc
                     | _ -> raise (
                         Exceptions.InvariantBroken
                           "Registered parent must be a TUser with only tvars as parameters")
                  )
                  TypeVarMap.empty
                  c_init_params
                  ctyps
              with
                Invalid_argument _ ->
                type_error loc env
                  "Constructor %a expects %i type arguments, but %i are provided."
                  Ident.print_strident constr
                  (List.length c_init_params)
                  (List.length ctyps)
            in
            List.map
              (Love_type.replace_map (fun x -> Love_tenv.isComp x env) replace_map)
              ct.cargs in
          let targs =
            match targs, arg_typs with
            | [TTuple _], [TTuple _] -> targs
            | [TTuple l], _ -> l
            | t,_ -> t in
          let () =
            try List.iter2 (check env) targs arg_typs
            with Invalid_argument _ ->
              type_error loc env
                "Constructor %a expects %i arguments, but only %i are provided."
                Ident.print_strident constr
                (List.length targs)
                (List.length args)
          in
          let args_typ =
            if Compare.Int.(List.length ct.Love_tenv.cargs = 1 &&
                            List.length ctyps >= 2)
            then (
              [ TTuple ctyps ]
            )
            else ctyps in

          let constr_typ = match ct.cparent with
              TUser (name, _) ->
              (* Additional check : is this constructor public, private, abstract, internal ?*)
              check_private loc name env;
              TUser (name, args_typ)
            (* Check done *)
            | _ -> assert false
          in
          constr_typ
      )

    | RNone -> Love_type.type_cnone

    | RSome e -> TOption (typecheck_rexp env e)

    | RNil -> Love_type.type_empty_list

    | RList (el, e) ->
      begin match el with
        | [] -> raise (TypingError ("Badly formed list", None, env))
        | _ ->
            let lt = typecheck_rexp env e in
            let et = match Love_type.isListOf lt with
              | None -> raise (TypingError (
                  "Right side of list is not a list.", None, env))
              | Some t -> t
            in
            List.iter (fun elt ->
                let t = typecheck_rexp env elt in
                if Love_type.bool_equal t et then ()
                else raise (TypingError (
                    "Elements of list must all have the same type",
                    None, env))
              ) el;
            lt
      end

  | RTuple ([] | [_]) -> raise (TypingError ("Ill-formed tuple", None, env))

  | RTuple el  -> TTuple (List.map (typecheck_rexp env) el)

  | RProject { tuple; indexes } ->
      begin match typecheck_rexp env tuple with
        | TTuple l ->
            let tl = List.map (fun i ->
              match List.nth_opt l i with
              | Some t -> t
              | _ -> raise (TypingError (
                  "Projection out of bounds", None, env))
            ) indexes
            in
            begin match tl with
              | [t] -> t
              | tl -> TTuple tl
            end
        | _ -> raise (TypingError (
            "Projection on a non tuple element", None, env))
      end

  | RUpdate { tuple; updates } ->
      let tup_type = typecheck_rexp env tuple in
      let tup_types, tup_length =
        match tup_type with
        | TTuple l -> l, List.length l
        | _ -> raise (TypingError ("Update on a non tuple element", None, env))
      in
      let rec check_equal acc l =
        match l with
        | [] -> acc
        | (i, e) :: tl ->
          if Compare.Int.(i < tup_length)
          then check_equal (IntMap.add i (typecheck_rexp env e) acc) tl
          else raise (TypingError ("No update on update field", None, env))
      in
      let update_types = check_equal IntMap.empty updates in
      List.iteri (fun i tup_type ->
          match IntMap.find_opt i update_types with
          | None -> ()
          | Some t ->
              check ~error:"Type of update is inconsistent with tuple type."
                env t tup_type
        ) tup_types;
      tup_type

  | RRecord { contents = []; _ } ->
      raise (TypingError ("Empty record are forbidden", None, env))

  | RRecord { contents = l; _ }  ->
      let ftyp = List.map (fun (name, exp) ->
        (name, typecheck_rexp env exp)) l in
      Love_tenv.record_type None ftyp env

  | RGetField { record; field } ->
      begin match typecheck_rexp env record with
        | TUser (name, args) ->
          let rec_def =
            match Love_tenv.find_record name env with
            | Some t -> t.result
            | None -> raise (TypingError (
                "Record type not found in env", None, env)) in
          let ftyp_opt = List.find_opt (fun tf ->
              String.equal tf.Love_tenv.fname field) rec_def in
          let ftyp = match ftyp_opt with
            | Some t -> t
            | None -> raise (UnknownField field) in
          let ftyp_args = Love_tenv.field_with_targs ftyp args env in
          ftyp_args.ftyp
        | _ -> raise (TypingError (
            "Record has not the type record", None, env))
      end

  | RSetFields { updates = [];_ } -> (* Bad case *)
      raise (Exceptions.InvariantBroken "Expression SetFields has no updates")

  | RSetFields { record; updates } ->
      let typ_rec = typecheck_rexp env record in
      let rec_def = match typ_rec with
        | TUser (name, _args) ->
            begin match Love_tenv.find_record name env with
              | Some t -> t.result
              | None -> raise (TypingError (
                  "Record type not found in env", None, env))
            end
        | _ -> raise (TypingError (
            "Record has not the type record", None, env))
      in
      let treat_field (field, updt) =
        let ftyp_opt = List.find_opt (fun tf ->
            String.equal tf.Love_tenv.fname field) rec_def in
        let ftyp =
          match ftyp_opt with
          | Some t -> t
          | None -> raise (TypingError (
              spf "Field %s not registered in environment" field, None, env))
        in
        check ~error:(
          spf "Field %s not belonging to the correct record" field)
          env ftyp.fparent typ_rec;
        let t_updt = typecheck_rexp env updt in
        check ~error:(spf "Badly typed field %s" field) env t_updt ftyp.ftyp
      in
      List.iter treat_field updates;
      typ_rec

  | RPackStruct sr ->
      begin match sr with
        | RNamed n ->
          begin match Love_tenv.find_contract n env with
            | Some { result = local_env; _ } ->
                TPackedStructure (
                  Anonymous (Love_tenv.env_to_contract_sig local_env))
            | None ->
                raise (TypingError (
                  Format.asprintf "Contract %a not found in environment"
                    print_strident n, None, env))
          end
        | RAnonymous s ->
            let emptyenv = Love_tenv.new_subcontract_env
                Constants.anonymous s.kind () env in
            let newenv = typecheck_rstruct emptyenv s in
            TPackedStructure (Anonymous (
                Love_tenv.env_to_contract_sig newenv))
      end

  | RRaise { exn; args; loc } ->
      let args_typ = match exn with
        | RFail t -> [t]
        | RException exn ->
            match Love_tenv.find_exception exn env with
              | None ->
                  raise (TypingError (
                    Format.asprintf "Undefined exception %a"
                      print_strident exn, None, env))
              | Some { result; _ } -> result
      in
      let args, args_typ = match args, args_typ with
        | [], [TUnit] -> [Love_runtime_ast.RConst RCUnit], [TUnit]
        | [RTuple l], [TTuple t] -> l, t
        | [RTuple l], t -> l, t
        | l, [TTuple t] -> l, t
        | _, _ -> args, args_typ in
      let alen = List.length args in let tlen = List.length args_typ in
      let () =
        if Compare.Int.(alen <> tlen)
        then raise (TypingError (
            Format.asprintf "Bad number of arguments for exception %a"
              (Love_runtime_ast.print_exn_name ) exn, loc, env))
      in
      let rec check_args args args_typ =
        match args, args_typ with
        | [], [] -> ()
        | arg :: tlarg, targ :: tlargt ->
            check env (typecheck_rexp env arg) targ;
            check_args tlarg tlargt
        | _,_ -> assert false
      in
      check_args args args_typ;
      let tvar = Love_type.fresh_typevar () in
      TForall (tvar, TVar (tvar))

  | RTryWith { arg; cases } ->
      let typ = typecheck_rexp env arg in
      let typecheck_case ((ename, plist), exp) : unit =
        let test_exp env =
          if Love_type.bool_equal typ (typecheck_rexp env exp) then ()
          else raise (TypingError (
              "Exception cases must have the same type", None, env))
        in
        let rec check_pat acc_env pats args_typ =
          match pats, args_typ with
          | [], [] -> test_exp acc_env
          | pat :: tlarg, targ :: tlargt ->
              check_pat (typecheck_rpattern acc_env targ pat) tlarg tlargt
          | _, _ -> raise (TypingError (
              "Failure during pattern typechecking in exception.", None, env))
        in
        match ename with
        | Love_runtime_ast.RFail t ->
            begin match plist, t with
              | [], TUnit -> test_exp env
              | [], _ -> raise (TypingError (
                  "Failure with no argument must have type Unit", None, env))
              | [Love_runtime_ast.RPTuple plist], TTuple l
              | plist, TTuple l -> check_pat env plist l
              | [p], t -> check_pat env [p] [t]
              | _, _ -> raise (TypingError (
                  "Exception pattern badly typed", None, env))
            end
        | RException ename ->
            let args_typ = match Love_tenv.find_exception ename env with
              | None -> raise (TypingError (
                  Format.asprintf "Undefined exception %a"
                    print_strident ename, None, env))
              | Some { result; _ } -> result
            in
            let plist, args_typ = match plist, args_typ with
              | [], [TUnit] -> [Love_runtime_ast.RPAny], [TUnit]
              | [RPTuple l], [TTuple t] -> l, t
              | [RPTuple l], t -> l, t
              | l, [TTuple t] -> l, t
              | _,_ -> plist, args_typ in
            let () =
              let plen = List.length plist in
              let tlen = List.length args_typ in
              if Compare.Int.(plen <> tlen)
              then raise (TypingError (
                  Format.asprintf
                    "Bad number of arguments for exception %a in try with"
                    print_strident ename, None, env))
            in
            check_pat env plist args_typ
      in
      List.iter typecheck_case cases;
      typ
  in Love_tenv.normalize_type ~relative:true t env

and typecheck_rstruct (env : unit Love_tenv.t)
    (ctr : Love_runtime_ast.structure) : unit Love_tenv.t =
  let treat_content name_acc env (name, content) :
    Collections.StringSet.t * unit Love_tenv.t =
    let () =
      if Collections.StringSet.mem name name_acc
      then raise (TypingError (name ^ " is defined twice", None, env)) in
    let name_acc = Collections.StringSet.add name name_acc in
    match content with
    | Love_runtime_ast.RDefType (k, t) ->
        if is_safe_tdef k name t env
        then
          let new_env = Love_tenv.add_typedef name k t env in
          if String.equal name "storage" && not (Love_runtime_ast.is_module ctr)
          then match k, Love_type.typedef_parameters t with
            | TInternal, _ -> raise (TypingError (
                "Storage type definition cannot be internal", None, env))
            | _, _ :: _ -> raise (TypingError (
                "Storage definition cannot be polymorphic", None, env) )
            | _, [] -> name_acc, new_env
          else name_acc, new_env
        else raise (TypingError (
            Format.asprintf "Definition of %a is unsafe"
              (Love_type.pp_typdef ~privacy:(Love_ast.kind_to_string k) ~name)
              t, None, env))

    | RDefException tl ->
        if List.for_all (fun t -> is_safe env TInternal t) tl
        then name_acc, Love_tenv.add_exception name tl env
        else raise (TypingError (
            "Definition of exception is unsafe", None, env))

    | REntry { entry_code; entry_fee_code; entry_typ } ->
        if is_safe env TPublic (entry_typ)
        then begin
          let () =
            if (Love_runtime_ast.is_module ctr) ||
               not (check_forbidden_type ~where:Parameter env entry_typ)
            then raise (TypingError (
                  spf "Forbidden entry point in module %s" name, None, env)) in
          let env = Love_tenv.add_var ~kind:Love_tenv.(Entry) name
              (TEntryPoint (entry_typ)) env in
          let env, typ_as_lambda =
            let env, type_storage =
              match Love_tenv.get_storage_type env with
              | None ->
                  Love_tenv.add_typedef
                   "storage" TPublic (Alias {aparams = []; atype = TUnit}) env,
                  TUnit
              | Some t -> env, t in
            let lambda = Love_type.entryPointType ~type_storage entry_typ in
            env, Love_tenv.normalize_type ~relative:true lambda env
          in
          let t = typecheck_rexp env entry_code in
          check env typ_as_lambda t;
          let () = match entry_fee_code with
            | None -> ()
            | Some fee_code ->
              match Love_ast_utils.lambdas_to_params entry_code with
              | RPattern (p1, t1) :: RPattern (p2, t2) ::
                RPattern (p3, t3) :: _ ->
                let env = typecheck_rpattern env t1 p1 in
                let env = typecheck_rpattern env t2 p2 in
                let env = typecheck_rpattern env t3 p3 in
                let t = typecheck_rexp env fee_code in
                let expected_ty = TTuple [TDun; TNat] in
                check env expected_ty t
              | _ -> assert false
          in
          name_acc, env
        end else raise (TypingError ("Entry point type is unsafe", None, env))

    | RView { view_code; view_typ } ->
        if is_safe env TAbstract (fst view_typ) &&
           is_safe env TAbstract (snd view_typ)
        then begin
          let () =
            if Love_runtime_ast.is_module ctr
            then raise (TypingError (
                  spf "Forbidden view in module %s" name, None, env)) in
          let env = Love_tenv.add_var ~kind:Love_tenv.(View) name
              (TView (fst view_typ, snd view_typ)) env in
          let env, typ_as_lambda =
            let env, type_storage =
              match Love_tenv.get_storage_type env with
              | None ->
                  Love_tenv.add_typedef
                    "storage" TPublic (Alias {aparams = []; atype = TUnit}) env,
                  TUnit
              | Some t -> env, t in
            let lambda =
              Love_type.viewType ~type_storage (fst view_typ) (snd view_typ) in
            env, Love_tenv.normalize_type ~relative:true lambda env
          in
          let t = typecheck_rexp env view_code in
          check env typ_as_lambda t;
          name_acc, env
        end else raise (TypingError ("View type is unsafe", None, env))

    | RValue { value_code; value_typ; value_visibility } ->
        let () = match value_visibility with
          | Public ->
              if Love_tenv.is_internal value_typ env
              then raise (TypingError (
                  "Value " ^ name ^ " is public, but its type is internal",
                  None, env))
          | Private -> ()
        in
        if is_safe env TInternal value_typ then begin
          let t = typecheck_rexp env value_code in
          check env value_typ t;
          name_acc,
          Love_tenv.add_var ~kind:(Value (value_visibility)) name value_typ env
        end else raise (TypingError (
            Format.asprintf "Value type %a is unsafe"
              (Love_type.pretty) value_typ, None, env))

    | RStructure c ->
        let sub_env = Love_tenv.new_subcontract_env name c.kind () env in
        let sub_env = typecheck_rstruct sub_env c in
        name_acc, Love_tenv.add_subcontract_env name sub_env env

    | RSignature s ->
        name_acc,
        Love_tenv.add_signature name (typecheck_signature None env name s) env
  in
  let _, new_env =
    List.fold_left (fun (string_set, acc_env) c ->
        try treat_content string_set acc_env c
        with
        | TypingError (s, loc, env) -> raise (TypingError (s, loc, env))
        | e -> raise e
      ) (Collections.StringSet.empty, env) ctr.structure_content in
  let () =
    if Love_runtime_ast.is_module ctr then ()
    else match Love_tenv.get_storage_type new_env with
    | None -> ()
    | Some t ->
        if is_safe new_env TInternal t then ()
        else raise (TypingError ("Unsafe storage type", None, env))
  in
  new_env

let rec typecheck_value_list
    (env : unit Love_tenv.t)
    (ctxt : Love_context.t)
    (typl : typ list)
    (vs : Love_value.Value.t list) : Love_context.t Error_monad.tzresult Lwt.t =
  let rec simplify_args ctxt (tlist : typ list) args :
    Love_context.t Error_monad.tzresult Lwt.t =
    match args, tlist with
      arg :: arg_tl, typ :: typ_tl -> (
        typecheck_value env ctxt typ arg >>=?
        (fun ctx -> simplify_args ctx typ_tl arg_tl)
      )
    | [],[] -> return ctxt
    | _, [] ->
      raise (
        TypingErrorLwt (
          ctxt,
          "Typecheck value fail : More arguments than types",
          env))
    | [], _ ->
      raise (
        TypingErrorLwt (
          ctxt,
          "Typecheck value fail : More types than arguments",
          env))
  in
  simplify_args ctxt typl vs

and typecheck_value
    (env : unit Love_tenv.t)
    (ctxt : Love_context.t)
    (typ : Love_type.t)
    (v : Love_value.Value.t) : Love_context.t Error_monad.tzresult Lwt.t =
  let open Love_value in
  debug
    "[typecheck_value] Checking value %a with type %a@."
    (Love_printer.Value.print ) v
    (Love_type.pretty) typ;
  let check e ctxt t t' =
    check e t t';
    return ctxt in
  let typ = Love_tenv.normalize_type ~relative:true typ env in
  match v with
  | VUnit ->
    debug "[typecheck_value] Unit@.";
    check env ctxt TUnit typ
  | VBool _b ->
    debug "[typecheck_value] Bool@.";
    check env ctxt TBool typ
  | VString _s ->
    debug "[typecheck_value] String@.";
    check env ctxt TString typ
  | VBytes _b ->
    debug "[typecheck_value] Bytes@.";
    check env ctxt TBytes typ
  | VInt _i ->
    debug "[typecheck_value] Int@.";
    check env ctxt TInt typ
  | VNat _i ->
    debug "[typecheck_value] Nat@.";
    check env ctxt TNat typ
  | VNone -> (
      debug "[typecheck_value] None@.";
      match typ with
        TOption _ -> return ctxt
      | _ ->
        debug "[typecheck_value] Error : expected type : %a@."
          (Love_type.pretty) typ;
        raise (TypingErrorLwt (ctxt, "None is not typed as an Option", env))
    )
  | VSome v -> (
      debug "[typecheck_value] Some@.";
      match typ with
        TOption t -> typecheck_value env ctxt t v
      | _ -> raise (TypingErrorLwt (
          ctxt, "Some _ is not typed as an Option", env))
    )
  | VTuple vl -> (
      debug "[typecheck_value] Tuple@.";
      match typ with
        TTuple l -> typecheck_value_list env ctxt l vl
      | _ -> raise (TypingErrorLwt (
          ctxt, "Tuple value is not typed as a Tuple", env))
    )
  | VConstr (cname, args) -> (
      debug "[typecheck_value] Constructor : %s@." cname;
      match typ with
        | TUser (tname, _par) ->
            begin match Love_tenv.find_sum tname env with
              | None -> raise (UnknownType tname)
              | Some {result; _} ->
                  let tc_opt = List.find_opt (fun tc ->
                      String.equal cname tc.Love_tenv.cname
                    ) result
                  in
                  begin match tc_opt with
                    | None -> raise (UnknownConstr (Ident.create_id cname))
                    | Some tc -> typecheck_value_list env ctxt tc.cargs args
                  end
            end
        | _ ->
          raise
            (TypingErrorLwt
               (ctxt, "Constructor value is not typed as a Constructor", env))
    )

  | VRecord fields ->
      debug "[typecheck_value] Record@.";
      let rec check_fields ctxt fl rfl =
        match fl, rfl with
        | (fn, v) :: fl, Love_tenv.{ fname; ftyp; _ } :: rfl ->
          if not (String.equal fn fname) then
            raise
              (TypingErrorLwt
                 (ctxt, "Record value and Record type are incompatible", env))
          else
            typecheck_value env ctxt ftyp v >>=? fun ctxt ->
            check_fields ctxt fl rfl
        | [], [] ->
            return ctxt
        | _ ->
          raise
            (TypingErrorLwt
               (ctxt, "Record value and Record type are incompatible", env))
      in
      begin match typ with
        | TUser (tname, _par) ->
            begin match Love_tenv.find_record tname env with
              | None -> raise (UnknownType tname)
              | Some {result; _} ->
                  let fields =
                    List.fast_sort (fun (f1, _) (f2, _) ->
                        String.compare f1 f2) fields in
                  let rfields =
                    List.fast_sort
                      (fun Love_tenv.{ fname = f1; _ } Love_tenv.{ fname = f2; _ } ->
                         String.compare f1 f2) result in
                  check_fields ctxt fields rfields
            end
        | _ ->
          raise
            (TypingErrorLwt
               (ctxt, "Record value is not typed as a Record", env))
      end
  | VList vlist -> (
      debug "[typecheck_value] List@.";
      match vlist with
        [] -> (
          match typ with
            TList _ -> return ctxt
          | _ -> raise (TypingErrorLwt (
              ctxt, "Empty VList not typed as a list", env))
        )
      | l ->(
          match typ with
            TList t ->
            List.fold_left
              (fun lwt elt ->
                 lwt >>=? (fun ctxt -> typecheck_value env ctxt t elt))
              (return ctxt)
              l
          | _ -> raise (TypingErrorLwt (ctxt, "VList not typed as a list", env))
        )
    )
  | VSet vset -> (
      debug "[typecheck_value] Set@.";
      if ValueSet.is_empty vset
      then
        match typ with
        | TSet _ -> return ctxt
        | _ -> raise (TypingErrorLwt (
            ctxt, "Empty VSet not typed as a set", env))
      else
        match typ with
        | TSet t ->
          ValueSet.fold
            (fun elt lwt -> lwt >>=? (fun ctxt ->
                 typecheck_value env ctxt t elt))
            vset
            (return ctxt)
        | _ -> raise (TypingErrorLwt (ctxt, "VSet not typed as a set", env))
    )
  | VMap map -> (
      debug "[typecheck_value] Map@.";
      if ValueMap.is_empty map
      then
        match typ with
        | TMap _ -> return ctxt
        | _ -> raise (TypingErrorLwt (
            ctxt, "Empty VMap not typed as a map", env))
      else
        match typ with
        | TMap (tk, tb) ->
          ValueMap.fold
            (fun key bnd lwt ->
               lwt >>=? (
                 fun ctxt ->
                   typecheck_value env ctxt tk key >>=? (
                     fun ctxt -> typecheck_value env ctxt tb bnd
                   )
               )
            )
            map
            (return ctxt)
        | _ -> raise (TypingErrorLwt (ctxt, "VMap not typed as a map", env))
    )

  | VBigMap { diff; key_type; value_type; _ } ->  (
      debug "[typecheck_value] BigMap@.";
      check env ctxt (TBigMap (key_type, value_type)) typ >>=? fun ctxt ->
      ValueMap.fold
        (fun key bnd lwt ->
           lwt >>=? (
             fun ctxt ->
               typecheck_value env ctxt key_type key >>=? (
                 fun ctxt ->
                   match bnd with
                   | None -> return ctxt
                   | Some bnd -> typecheck_value env ctxt value_type bnd
               )
           )
        )
        diff
        (return ctxt)
    )

  | VDun _d ->
    debug "[typecheck_value] Dun@.";
    check env ctxt TDun typ
  | VKey _k ->
    debug "[typecheck_value] Key@.";
    check env ctxt TKey typ
  | VKeyHash _kh ->
    debug "[typecheck_value] KeyHash@.";
    check env ctxt TKeyHash typ
  | VSignature _s ->
    debug "[typecheck_value] Signature@.";
    check env ctxt TSignature typ

  | VContractInstance (c, a) ->
    debug "[typecheck_value] ContractInstance@.";
    check
      env
      ctxt
      (TContractInstance (Anonymous c))
      typ >>=?
    (fun ctxt ->
       Love_context.get_script ctxt a >>=?
       (fun (ctxt, opt) ->
          match opt with
          | None -> raise (TypingErrorLwt (ctxt, "Contract not found", env))
          | Some (str, _) ->
            check
              env
              ctxt
              (TContractInstance
                (Anonymous (Love_value.sig_of_structure
                              ~only_typedefs:false str)))
              typ
       )
    )

  | VPackedStructure (_p, c) ->
    debug "[typecheck_value] ContractStructure@.";
    check env ctxt (TPackedStructure (Anonymous (
        Love_value.sig_of_structure ~only_typedefs:false c.root_struct))) typ

  | VAddress _a ->
    debug "[typecheck_value] Address@.";
    check env ctxt TAddress typ
  | VTimestamp _t ->
    debug "[typecheck_value] Timestamp@.";
    check env ctxt TTimestamp typ
  | VOperation _o ->
    check env ctxt TOperation typ
  | VEntryPoint (_crepr, _name) -> (*forbidden in storage/param -> never typed*)
    raise (TypingErrorLwt (ctxt, "Entry point values cannot be typechecked", env))

  | VView (_crepr, _name) -> (* forbidden in storage/param -> never typed*)
    raise (TypingErrorLwt (ctxt, "View values cannot be typechecked", env))

  | VClosure { call_env = { values = []; structs = []; sigs = [];
                            exns = []; types = []; tvars = [] }; lambda } ->
      debug "[typecheck_value] Closure@.";
      begin
        match typ with
      | TArrow _ -> check env ctxt typ (typecheck_rexp env (RLambda lambda))
      | _ -> raise (TypingErrorLwt (
            ctxt, Format.asprintf "VClosure of type %a is forbidden"
              (Love_type.pretty) typ, env))
      end

  | VClosure { call_env = { values = _; structs = _; sigs = _;
                            exns = _; types = _; tvars = _ }; lambda = _ } ->
    raise (
      TypingErrorLwt (
        ctxt, "VClosures with non-empty closures cannot be typechecked", env))

  | VPrimitive (p, l) ->
    debug "[typecheck_value] Love_primitive %s@." (Love_primitive.name p);
    typecheck_primitive env ctxt p l >>=?
    (fun (ctxt, t) -> check env ctxt t typ)

(** Returns the new constant primitive, its type and the new arguments and their
    respective types *)
and typecheck_primitive env ctxt p (args : Love_value.Value.t list) =
  debug "[typecheck_primitive] Love_primitive %s applied to (%a)@."
    (Love_primitive.name p)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       Love_printer.Value.print) args;
  let primitive_type =
    Love_primitive.type_of ~sig_of_contract:(sig_of_contract None env) p in
  let advance_in_arrow arrow args ctxt =
    let rec __advance_in_arrow arrow args acc =
      match args with
        [] -> acc >>=? (fun ctxt -> return (ctxt, arrow))
      | arg :: tl -> (
          match arrow with
            TArrow (t1, t2) ->
            __advance_in_arrow
              t2
              tl
              (acc >>=? (fun ctxt -> typecheck_value env ctxt t1 arg))
          | _ -> raise (TypingErrorLwt (ctxt, "Love_primitive called with too many arguments", env))
        )
    in
    __advance_in_arrow arrow args (return ctxt) in
  advance_in_arrow primitive_type args ctxt

let typecheck_value
    (env : unit Love_tenv.t)
    (ctxt : Love_context.t)
    (typ_exp : Love_type.type_exp)
    (v : Love_value.Value.t) =
  debug "[typecheck_value] Starting with environment %a@."
    Love_tenv.pp_env env;
  let env =
    List.fold_left
      (fun acc (id, (tdef : Love_type.typedef)) ->
         debug "[typecheck_value] Adding %a = %a in environment@."
           Ident.print_strident id
           (Love_type.pp_typdef ~name:"" ~privacy:"") tdef;
         Love_tenv.add_typedef_in_subcontract () id TPublic tdef acc)
      env
      typ_exp.lettypes
  in
  debug "[typecheck_value] Typechecking value@.";
  try
    typecheck_value env ctxt typ_exp.body v
  with
    exc ->
    debug "[typecheck_value] Error : failing with environment %a@."
      Love_tenv.pp_env env;
    raise exc
