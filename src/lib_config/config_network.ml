(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Json_encoding
open Config_type

let () =
  Printexc.register_printer (function
      | Json_encoding.Cannot_destruct (path, exn) ->
          Some (Printf.sprintf "%s (%s)"
                  (String.concat "/"
                     (List.map (function
                            `Field f -> f
                          | `Index n -> string_of_int n
                          | `Next -> "."
                          | `Star -> "*"
                        )
                         path)) (Printexc.to_string exn))
      | _ -> None)

let default = match Set_config.config_option with
  | Some config -> config
  | None -> Set_config_private.config

let dft x v =
  match x with
  | None -> v
  | Some x -> x

let encoding =
  conv
    (fun x ->
       (
         Some x.network ,
         Some x.genesis_key ,
         Some x.p2p_version ,
         Some x.max_operation_data_length ,
         Some x.encoding_version ,
         Some x.prefix_dir ,
         Some x.p2p_port ,
         Some x.rpc_port ,
         Some x.bootstrap_peers ,
         Some x.genesis_time ) ,
       (
         ( Some x.genesis_block ,
           Some x.genesis_protocol ,
           Some x.forced_protocol_upgrades ,
           Some x.cursym ,
           Some x.discovery_port ,
           Some x.signer_tcp_port ,
           Some x.signer_http_port ,
           Some x.protocol_revision ,
           Some x.only_allowed_bakers ,
           Some x.expected_pow ),
         Some x.self_amending
       )
    )
    (fun
      (
        (
          network , genesis_key , p2p_version ,
          max_operation_data_length , encoding_version ,
          prefix_dir , p2p_port , rpc_port ,
          bootstrap_peers  , genesis_time ) ,
        (( genesis_block , genesis_protocol ,
           forced_protocol_upgrades , cursym ,
           discovery_port , signer_tcp_port , signer_http_port,
           protocol_revision , only_allowed_bakers ,
           expected_pow) , self_amending
        )
      )
      ->
        let forced_protocol_upgrades =
          dft forced_protocol_upgrades default.forced_protocol_upgrades in
        let genesis_key = dft genesis_key default.genesis_key in
        let p2p_version = dft p2p_version default.p2p_version in
        let max_operation_data_length =
          dft max_operation_data_length default.max_operation_data_length in
        let encoding_version = dft encoding_version default.encoding_version in
        let prefix_dir = dft prefix_dir default.prefix_dir in
        let p2p_port = dft p2p_port default.p2p_port in
        let rpc_port = dft rpc_port default.rpc_port in
        let bootstrap_peers = dft bootstrap_peers default.bootstrap_peers in
        let genesis_time = dft genesis_time default.genesis_time in
        let genesis_block = dft genesis_block default.genesis_block in
        let genesis_protocol = dft genesis_protocol default.genesis_protocol in
        let network = dft network default.network in
        let cursym = dft cursym default.cursym in
        let discovery_port = dft discovery_port default.discovery_port in
        let signer_tcp_port = dft signer_tcp_port default.signer_tcp_port in
        let signer_http_port = dft signer_http_port default.signer_http_port in
        let protocol_revision = dft protocol_revision default.protocol_revision in

        let only_allowed_bakers = dft only_allowed_bakers default.only_allowed_bakers in
        let expected_pow = dft expected_pow default.expected_pow in
        let self_amending = dft self_amending default.self_amending in
        {
          network ; genesis_key ; p2p_version ;
          max_operation_data_length ; encoding_version ;
          prefix_dir ; p2p_port ; rpc_port ;
          bootstrap_peers ;
          genesis_time ;  genesis_block ; genesis_protocol ;
          forced_protocol_upgrades ; cursym ; tzcompat=false ;
          discovery_port ; signer_tcp_port ; signer_http_port ;
          protocol_revision ; only_allowed_bakers ;
          expected_pow ; self_amending ;
        }
    )
    (merge_objs
       (obj10
          (opt "network" string)
          (opt "genesis_key" string)
          (opt "p2p_version" string)
          (opt "max_operation_data_length" int)
          (opt "encoding_version" int)
          (opt "prefix_dir" string)
          (opt "p2p_port" int)
          (opt "rpc_port" int)
          (opt "bootstrap_peers" (list string))
          (opt "genesis_time" string)
       )
       (merge_objs
          (obj10
             (opt "genesis_block" string)
             (opt "genesis_protocol" string)
             (opt "forced_protocol_upgrades" (list (tup2 int32 string)))
             (opt "cursym" string)
             (opt "discovery_port" int)
             (opt "signer_tcp_port" int)
             (opt "signer_http_port" int)
             (opt "protocol_revision" int)
             (opt "only_allowed_bakers" bool)
             (opt "expected_pow" float)
          )
          (obj1
             (opt "self_amending" bool)
          )
       )
    )



let variable = Tezos_stdlib.Environment_variable.make "DUNE_CONFIG"
    ~description:"Select predefined or custom configuration"
    ~allowed_values:[ "mainnet" ; "testnet" ; "private" ; "<filename.json>" ]

let read_file filename =
  let ic = open_in filename in
  let b = Buffer.create 100 in
  let len = 100 in
  let s = Bytes.create len in
  let rec iter () =
    let nread = input ic s 0 len in
    if nread > 0 then begin
      Buffer.add_subbytes b s 0 nread;
      iter ()
    end
  in
  iter ();
  close_in ic;
  Buffer.contents b


let get_config () =
  let config_filename = Tezos_stdlib.Environment_variable.get variable
  in
  Printf.eprintf "%s=%S\n%!" variable.name config_filename;
  match config_filename with
  | "mainnet" -> Set_config_mainnet.config
  | "testnet" -> Set_config_testnet.config
  | "private" -> Set_config_private.config
  | _ ->
      if not (Sys.file_exists config_filename) then begin
        Printf.eprintf "Error: %s targets inexistent file %S.\n%!"
          variable.name config_filename;
        exit 2
      end;
      let config =
        let s = read_file config_filename in
        try
          let json = Ezjsonm.from_string s in
          Json_encoding.destruct encoding json
        with exn ->
          Printf.eprintf "Error: %s in %S\n%!"
            (Printexc.to_string exn) config_filename;
          exit 2
      in
      config

type config = Config_type.config = {
  network : string ;
  forced_protocol_upgrades : (Int32.t * string) list;
  genesis_key : string ;
  genesis_time : string ;
  genesis_block : string ;
  genesis_protocol : string ;
  p2p_version : string ;
  max_operation_data_length : int ;
  encoding_version : int ;
  prefix_dir : string ; (* "dune" or "tezos" *)
  p2p_port : int ; (* 9733 *)
  rpc_port : int ; (* 8733 *)
  discovery_port: int ; (* 10733 *)
  signer_tcp_port: int ; (* 7733 *)
  signer_http_port: int ; (* 6733 *)
  bootstrap_peers  : string list ; (* [ "boot.tzbeta.net" ] *)
  cursym : string ;
  tzcompat : bool ;
  protocol_revision : int ;
  only_allowed_bakers : bool ;
  expected_pow : float ;
  self_amending : bool ;
}

let config = try Some (get_config ()) with Not_found -> None

let config = match config with
  | Some config -> config
  | None ->
      match Set_config.config_option with
      | Some config -> config
      | None ->
          Printf.eprintf
            "Error: the env variable %s must be defined.\n%!"
            variable.name;
          exit 2


let config = if Env_config.tezos_compatible_mode then
    { config with
      tzcompat = true ;
      cursym = "\xEA\x9C\xA9" ;
      self_amending = true }
  else config

let () =
  if not config.tzcompat then begin
    Printf.eprintf "Config: %s. Revision: %d\n%!"
      config.network Source_config.max_revision;
  end;
  ExternalCallback.register "get-config" (fun () -> config)

let config = ExternalCallback.callback "get-config" ()
