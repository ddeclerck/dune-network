(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)


module TYPES = struct

  type identity = {
    pseudo : string option ;
    identity : string ;
    public_key : string ;
    private_key : string ;
    account : string ;
  }

  type config = {
    mutable number_of_nodes : int ;
    mutable node_directory : string ;
    mutable src_directory : string ;
    mutable rpc_port : int ;
    mutable p2p_port : int ;
    mutable activate_proto : bool ;
    mutable pow_power : float ;
    mutable proto_name : string ;
    mutable proto_hash : string ;
    mutable identities : identity list ;
  }



end
open TYPES

type protocol = {
  protocol_name : string ;
  protocol_hash : string ;
}

let protocol_alpha = {
  protocol_name = "alpha";
  protocol_hash = "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK";
}

let protocol_mainnet = {
  protocol_name = "002-PsYLVpVv";
  protocol_hash = "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt";
}

let identities = [
  {
    pseudo = Some "bootstrap1" ;
    identity=
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
      else "dn1GLMm5dMXRxCwqmkV22keRCcoWwrrani9F";
    public_key="edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav";
    private_key="edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh";
    account = "4000000000000" ;
  };
  {
    pseudo = Some "bootstrap2" ;
    identity=
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN"
      else "dn1dEUBy5oQ4HjXzC8QPZ4zniPiMCxisbbu5";
    public_key="edpktzNbDAUjUk697W7gYg2CRuBQjyPxbEg8dLccYYwKSKvkPvjtV9";
    private_key="edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo";
    account = "4000000000000" ;
  };
  {
    pseudo = Some "bootstrap3" ;
    identity=
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
      else "dn1c5mt3XTbLo5mKBpaTqidP6bSzUVD9T5By";
    public_key="edpkuTXkJDGcFd5nh6VvMz8phXxU3Bi7h6hqgywNFi1vZTfQNnS1RV";
    private_key="edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ";
    account = "4000000000000" ;
  };
  {
    pseudo = Some "bootstrap4" ;
    identity=
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
      else "dn1XcnRktbepk2ZrasF3XSZq5L6EzbVdZWv3";
    public_key="edpkuFrRoDSEbJYgxRtLx2ps82UdaYc1WwfS9sE11yhauZt5DgCHbU";
    private_key="edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3";
    account = "4000000000000" ;
  };
  {
    pseudo = Some "bootstrap5" ;
    identity=
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv"
      else "dn1a8V6DRnGC5B2EnARBd3g2UXKcj6zrBYFK";
    public_key="edpkv8EUUH68jmo3f7Um5PezmfGrRF24gnfLpH3sVNwJnV5bVCxL2n";
    private_key="edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm";
    account = "4000000000000" ;
  };
  {
    pseudo = Some "activator" ;
    identity =
      if Tezos_config.Env_config.tezos_compatible_mode
      then "tz1id1nnbxUwK1dgJwUra5NEYixK37k6hRri"
      else "dn1Pmo3JSKF525a323xNHDDxkRkBAWSan2L6";
    public_key = "edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA";
    private_key = "edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm";
    account = "0";
  }
]

let c = {
  number_of_nodes = 2 ;
  node_directory = "nodes" ;
  src_directory = "." ;
  rpc_port = 8730 ;
  p2p_port = 9730 ;
  activate_proto = false ;
  pow_power = 24. ;
  proto_name = protocol_mainnet.protocol_name ;
  proto_hash = protocol_mainnet.protocol_hash ;
  identities = identities;
}

module FILE = struct

  type config = {
    number_of_nodes_opt : int option ;
    node_directory_opt : string option ;
    src_directory_opt : string option ;
    rpc_port_opt : int option ;
    p2p_port_opt : int option ;
    activate_proto_opt : bool option ;
    pow_power_opt : float option ;
    proto_name_opt : string option ;
    proto_hash_opt : string option ;
    identities_opt : identity list option ;
  }

  open Json_encoding

  let identity_encoding =
    conv
      (fun
        {
          pseudo ;
          identity ;
          public_key ;
          private_key ;
          account
        }
        ->
          (
            pseudo ,
            identity ,
            public_key ,
            private_key ,
            account
          )
      )
      (fun
        (
          pseudo ,
          identity ,
          public_key ,
          private_key ,
          account
        )
        ->
          {
            pseudo ;
            identity ;
            public_key ;
            private_key ;
            account
          }
      )
      (obj5
         (opt "pseudo" string)
         (req "id" string)
         (req "pubkey" string)
         (req "privkey" string)
         (dft "account" string "0")
      )

  let config_encoding =
    conv
      (fun
        {
          number_of_nodes_opt ;
          node_directory_opt ;
          src_directory_opt ;
          rpc_port_opt ;
          p2p_port_opt ;
          activate_proto_opt ;
          pow_power_opt ;
          proto_name_opt ;
          proto_hash_opt ;
          identities_opt
        }
        ->
          ( number_of_nodes_opt ,
            node_directory_opt ,
            src_directory_opt ,
            rpc_port_opt ,
            p2p_port_opt ,
            activate_proto_opt ,
            pow_power_opt ,
            proto_name_opt ,
            proto_hash_opt ,
            identities_opt
          )
      )
      (fun
        ( number_of_nodes_opt ,
          node_directory_opt ,
          src_directory_opt ,
          rpc_port_opt ,
          p2p_port_opt ,
          activate_proto_opt ,
          pow_power_opt ,
          proto_name_opt ,
          proto_hash_opt ,
          identities_opt
        )
        ->
          {
            number_of_nodes_opt ;
            node_directory_opt ;
            src_directory_opt ;
            rpc_port_opt ;
            p2p_port_opt ;
            activate_proto_opt ;
            pow_power_opt ;
            proto_name_opt ;
            proto_hash_opt ;
            identities_opt
          }
      )
      (obj10
         (opt "number_of_nodes" int)
         (opt "node_directory" string)
         (opt "src_directory" string)
         (opt "rpc_port" int)
         (opt "p2p_port" int)
         (opt "activate_proto" bool)
         (opt "pow_power" float)
         (opt "proto_name" string)
         (opt "proto_hash" string)
         (opt "identities" (list identity_encoding))
      )

  let load filename =
    let config_diff =
      let s = FileString.read_file filename in
      try
        let json = Ezjsonm.from_string s in
        Json_encoding.destruct config_encoding json
      with exn ->
        Printf.eprintf "Error: %s in %S\n%!"
          (Printexc.to_string exn) filename;
        exit 2
    in
    (match config_diff.number_of_nodes_opt with
     | None -> ()
     | Some n -> c.number_of_nodes <- n);
    (match config_diff.node_directory_opt with
     | None -> ()
     | Some n -> c.node_directory <- n);
    (match config_diff.src_directory_opt with
     | None -> ()
     | Some n -> c.src_directory <- n);
    (match config_diff.rpc_port_opt with
     | None -> ()
     | Some n -> c.rpc_port <- n);
    (match config_diff.p2p_port_opt with
     | None -> ()
     | Some n -> c.p2p_port <- n);
    (match config_diff.activate_proto_opt with
     | None -> ()
     | Some n -> c.activate_proto <- n);
    (match config_diff.pow_power_opt with
     | None -> ()
     | Some n -> c.pow_power <- n);
    (match config_diff.proto_name_opt with
     | None -> ()
     | Some n -> c.proto_name <- n);
    (match config_diff.proto_hash_opt with
     | None -> ()
     | Some n -> c.proto_hash <- n);
    (match config_diff.identities_opt with
     | None -> ()
     | Some n -> c.identities <- n);
    ()

  let construct ?(compact=false) encoding data =
    let ezjson =
      (module Json_repr.Ezjsonm : Json_repr.Repr with type value = Json_repr.ezjsonm ) in
    Json_repr.pp
      ~compact
      ezjson
      Format.str_formatter
      (Json_encoding.construct encoding data) ;
    Format.flush_str_formatter ()

  let save filename =
    let config_diff = {
      number_of_nodes_opt = Some c.number_of_nodes ;
      node_directory_opt = Some c.node_directory ;
      src_directory_opt = Some c.src_directory ;
      rpc_port_opt = Some c.rpc_port ;
      p2p_port_opt = Some c.p2p_port ;
      activate_proto_opt = Some c.activate_proto;
      pow_power_opt = Some c.pow_power;
      proto_name_opt = Some c.proto_name;
      proto_hash_opt = Some c.proto_hash;
      identities_opt = Some c.identities;
    } in
    let s = construct config_encoding config_diff in
    FileString.write_file filename s;
    Printf.eprintf "Config saved to file %s\n%!" filename

end

let load = FILE.load
let save = FILE.save
