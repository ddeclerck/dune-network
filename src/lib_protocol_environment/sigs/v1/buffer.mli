
type t
val create : int -> t
val contents : t -> string
val add_char : t -> char -> unit
