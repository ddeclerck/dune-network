(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

val debug_iron : bool

module Combine : functor (A: Irmin_sig.S)(B:Irmin_sig.S) -> Irmin_sig.S

type player =
  ?record_size:bool ->
  ?reads_also:bool ->
  ?max_tree_diff:int ->
  ?no_progress:bool ->
  ?max_steps:int ->
  ?switch:int * (unit -> unit) ->
  trace:string -> db:string -> unit -> unit Lwt.t

module Record(B : Irmin_sig.S) : sig

  include Irmin_sig.S

  val play : player
  val print : trace : string -> int

  val copy : ?reads_also:bool -> ?max_steps:int ->
    src:string -> dst:string -> unit

end
