(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

include Ocplib_ironmin.Ironmin.Key
module STRING : sig
  val get : string -> int -> string * int
end

val stats : unit -> int array
