(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

open Tezos_micheline (* For Micheline_parser *)

type original =
  | Michelson_parsed of Michelson_v1_parser.parsed
  | Dune_parsed

type parsed = {
  source : string ;
  expanded : Script.expr ;
  original : original ;
}

val parse_toplevel : ?check:bool -> string ->
  parsed Micheline_parser.parsing_result

val parse_expression : ?check:bool -> string ->
  parsed Micheline_parser.parsing_result
