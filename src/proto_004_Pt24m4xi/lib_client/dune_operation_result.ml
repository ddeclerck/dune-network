(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Dune_apply_results
open Dune_operation

let pp_manager_operation_content ppf ~source ~internal op pp_result result =
  match op with
  | Dune_activate_protocol { level ; protocol ; protocol_parameters } ->
      Format.fprintf ppf "@[<v 2>Dune %sactivation of protocol:@,\
                          From: %a@,\
                          At: %ld@,\
                         "
        (if internal then "internal " else "")
        Contract.pp source
        level;
      begin match protocol with
        | None -> ()
        | Some hash ->
            Format.fprintf ppf "Protocol: %a@," Protocol_hash.pp hash
      end;
      begin match protocol_parameters with
        | None -> ()
        | Some _protocol ->
            Format.fprintf ppf "Protocol parameters: yes@,"
      end;
      pp_result ppf result;
      Format.fprintf ppf "@]"
  | Dune_manage_accounts bytes ->
      Format.fprintf ppf "@[<v 2>Dune %smanage accounts:@,\
                          From: %a@,\
                          Length: %d@,\
                         "
        (if internal then "internal " else "")
        Contract.pp source
        (MBytes.length bytes);
      pp_result ppf result;
      Format.fprintf ppf "@]"

let pp_result _ppf op =
  match op with
  | Dune_activate_protocol_result -> ()
  | Dune_manage_accounts_result -> ()
