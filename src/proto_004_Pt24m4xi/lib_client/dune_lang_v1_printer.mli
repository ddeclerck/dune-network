(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

val print_expr :
  Format.formatter -> Script.expr -> unit

val print_expr_unwrapped :
  Format.formatter -> Script.expr -> unit

val print_execution_trace:
  Format.formatter ->
  (Script_michelson.location * Gas.t *
   (Script_michelson.expr * string option) list) list -> unit

(** Insert the type map returned by the typechecker as comments in a
    printable Micheline AST. *)
val print_with_injected_types :
  Script_tc_errors.type_map ->
  Format.formatter ->
  Dune_lang_v1_parser.parsed ->
  unit

(** Unexpand the macros and produce the result of parsing an
    intermediate pretty printed source. Useful when working with
    contracts extracted from the blockchain and not local files. *)
val unparse_toplevel : ?type_map: Script_tc_errors.type_map ->
  Script.expr -> Dune_lang_v1_parser.parsed
