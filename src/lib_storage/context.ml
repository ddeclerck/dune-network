(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Tezos - Versioned (key x value) store (over Irmin) *)

module UseIrmin = Dune_ironmin.UseIrmin
module UseIronmin = Dune_ironmin.UseIronmin
module UseIrminCompat = struct
  include UseIronmin
  let init () =
    Ocplib_ironmin.Ironmin.storage_config := IrminCompat;
    init ()
end
module UseBoth = Dune_ironmin.GitStores.Combine(UseIrmin)(UseIronmin)
module UseRecordIrmin = Dune_ironmin.GitStores.Record(UseIrmin)
module UseRecordIronmin = Dune_ironmin.GitStores.Record(UseIronmin)

let dune_context_storage = Environment_variable.make "DUNE_CONTEXT_STORAGE"
    ~description:"Select backend for storage of context"
    ~allowed_values:[
      "irmin" ;
      "ironmin" ;
      "irmin-compat" ;
      "record" ; "record-irmin" ;
      "record-ironmin" ;
      "both" ;
    ]

let usedStorage =
  try
    match Environment_variable.get dune_context_storage with
    | "irmin" -> (module UseIrmin : Dune_ironmin.Irmin_sig.S)
    | "ironmin" -> (module UseIronmin : Dune_ironmin.Irmin_sig.S)
    | "irmin-compat" -> (module UseIrminCompat : Dune_ironmin.Irmin_sig.S)
    | "record" | "record-irmin" ->
        (module UseRecordIrmin : Dune_ironmin.Irmin_sig.S)
    | "record-ironmin" -> (module UseRecordIronmin : Dune_ironmin.Irmin_sig.S)
    | "both" -> (module UseBoth : Dune_ironmin.Irmin_sig.S)
    | s ->
        Printf.eprintf "Unexpected value for DUNE_CONTEXT_STORAGE: %S\n%!"
          s;
        exit 2
  with Not_found ->
    (module UseIrmin : Dune_ironmin.Irmin_sig.S)

module UsedStorage = (val usedStorage : Dune_ironmin.Irmin_sig.S)

let () = UsedStorage.init ()

open UsedStorage

type index = {
  path: string ;
  repo: GitStore.Repo.t ;
  patch_context: context -> context Lwt.t ;
}

and context = {
  index: index ;
  parents: GitStore.Commit.t list ;
  tree: GitStore.tree ;
}
type t = context

(*-- Version Access and Update -----------------------------------------------*)

let current_protocol_key = ["protocol"]
let current_test_chain_key = ["test_chain"]
let current_data_key = ["data"]

let exists index key =
  GitStore.Commit.of_hash index.repo key >>= function
  | None -> Lwt.return_false
  | Some _ -> Lwt.return_true

let checkout index key =
  GitStore.Commit.of_hash index.repo key >>= function
  | None -> Lwt.return_none
  | Some commit ->
      GitStore.Commit.tree commit >>= fun tree ->
      let ctxt = { index ; tree ; parents = [commit] } in
      Lwt.return_some ctxt

let checkout_exn index key =
  checkout index key >>= function
  | None -> Lwt.fail Not_found
  | Some p -> Lwt.return p

let raw_commit ~time ?(message = "") context =
  let info =
    Irmin.Info.v ~date:(Time.Protocol.to_seconds time) ~author:"Tezos" message in
  GitStore.Commit.v
    context.index.repo ~info ~parents:context.parents context.tree

let commit ~time ?message context =
  raw_commit ~time ?message context >>= fun commit ->
  let h = GitStore.Commit.hash commit in
  Lwt.return h

let hash ~time ?(message = "") { parents ; tree } =
  hash ~date:(Time.Protocol.to_seconds time) ~message ~parents ~tree

(*-- Generic Store Primitives ------------------------------------------------*)

let data_key key = current_data_key @ key

type key = string list
type value = MBytes.t

let mem ctxt key =
  GitStore.Tree.mem ctxt.tree (data_key key) >>= fun v ->
  Lwt.return v

let dir_mem ctxt key =
  GitStore.Tree.mem_tree ctxt.tree (data_key key) >>= fun v ->
  Lwt.return v

let raw_get ctxt key =
  GitStore.Tree.find ctxt.tree key
let get t key = raw_get t (data_key key)

let raw_set ctxt key data =
  GitStore.Tree.add ctxt.tree key data >>= fun tree ->
  Lwt.return { ctxt with tree }
let set t key data = raw_set t (data_key key) data

let raw_del ctxt key =
  GitStore.Tree.remove ctxt.tree key >>= fun tree ->
  Lwt.return { ctxt with tree }
let del t key = raw_del t (data_key key)

let remove_rec ctxt key =
  GitStore.Tree.remove ctxt.tree (data_key key) >>= fun tree ->
  Lwt.return { ctxt with tree }

let copy ctxt ~from ~to_ =
  GitStore.Tree.find_tree ctxt.tree (data_key from) >>= function
  | None -> Lwt.return_none
  | Some sub_tree ->
      GitStore.Tree.add_tree ctxt.tree (data_key to_) sub_tree >>= fun tree ->
      Lwt.return_some { ctxt with tree }

let fold ctxt key ~init ~f =
  GitStore.Tree.list ctxt.tree (data_key key) >>= fun keys ->
  Lwt_list.fold_left_s
    begin fun acc (name, kind) ->
      let key =
        match kind with
        | `Contents -> `Key (key @ [name])
        | `Node -> `Dir (key @ [name]) in
      f key acc
    end
    init keys

(*-- Predefined Fields -------------------------------------------------------*)

let get_protocol v =
  raw_get v current_protocol_key >>= function
  | None -> assert false
  | Some data -> Lwt.return (Protocol_hash.of_bytes_exn data)
let set_protocol v key =
  raw_set v current_protocol_key (Protocol_hash.to_bytes key)

let get_test_chain v =
  raw_get v current_test_chain_key >>= function
  | None -> Lwt.fail (Failure "Unexpected error (Context.get_test_chain)")
  | Some data ->
      match Data_encoding.Binary.of_bytes Test_chain_status.encoding data with
      | None -> Lwt.fail (Failure "Unexpected error (Context.get_test_chain)")
      | Some r -> Lwt.return r

let set_test_chain v id =
  raw_set v current_test_chain_key
    (Data_encoding.Binary.to_bytes_exn Test_chain_status.encoding id)
let del_test_chain v  = raw_del v current_test_chain_key

let fork_test_chain v ~protocol ~expiration =
  set_test_chain v (Forking { protocol ; expiration })

(*-- Initialisation ----------------------------------------------------------*)

let init ?patch_context ?mapsize ?readonly root =
  GitStore.Repo.v
    (Irmin_lmdb.config ?mapsize ?readonly root) >>= fun repo ->
  Lwt.return {
    path = root ;
    repo ;
    patch_context =
      match patch_context with
      | None -> (fun ctxt -> Lwt.return ctxt)
      | Some patch_context -> patch_context
  }

let get_branch chain_id = Format.asprintf "%a" Chain_id.pp chain_id


let commit_genesis index ~chain_id ~time ~protocol =
  let tree = GitStore.empty_tree () in
  let ctxt = { index ; tree ; parents = [] } in
  index.patch_context ctxt >>= fun ctxt ->
  set_protocol ctxt protocol >>= fun ctxt ->
  set_test_chain ctxt Not_running >>= fun ctxt ->
  raw_commit ~time ~message:"Genesis" ctxt >>= fun commit ->
  GitStore.Branch.set index.repo (get_branch chain_id) commit >>= fun () ->
  Lwt.return (GitStore.Commit.hash commit)

let compute_testchain_chain_id genesis =
  let genesis_hash = Block_hash.hash_bytes [Block_hash.to_bytes genesis] in
  Chain_id.of_block_hash genesis_hash

let compute_testchain_genesis forked_block =
  let genesis = Block_hash.hash_bytes [Block_hash.to_bytes forked_block] in
  genesis

let commit_test_chain_genesis ctxt (forked_header : Block_header.t) =
  let message =
    Format.asprintf "Forking testchain at level %ld." forked_header.shell.level in
  raw_commit ~time:forked_header.shell.timestamp ~message ctxt >>= fun commit ->
  let faked_shell_header : Block_header.shell_header = {
    forked_header.shell with
    proto_level = succ forked_header.shell.proto_level ;
    predecessor = Block_hash.zero ;
    validation_passes = 0 ;
    operations_hash = Operation_list_list_hash.empty ;
    context = GitStore.Commit.hash commit ;
  } in
  let forked_block = Block_header.hash forked_header in
  let genesis_hash = compute_testchain_genesis forked_block in
  let chain_id = compute_testchain_chain_id genesis_hash in
  let genesis_header : Block_header.t =
    { shell = { faked_shell_header with predecessor = genesis_hash } ;
      protocol_data = MBytes.create 0 } in
  let branch = get_branch chain_id in
  GitStore.Branch.set ctxt.index.repo branch commit >>= fun () ->
  Lwt.return genesis_header


(* Fabrice: These functions are useless. Tezos never reads branches. *)

let clear_test_chain index chain_id =
  (* TODO remove commits... ??? *)
  let branch = get_branch chain_id in
  GitStore.Branch.remove index.repo branch

let set_head index chain_id commit =
  let branch = get_branch chain_id in
  GitStore.Commit.of_hash index.repo commit >>= function
  | None ->
      Format.eprintf "Error: the context/ database is broken. Maybe you removed it without removing also the store/ database ?@.";
      assert false
  | Some commit ->
      GitStore.Branch.set index.repo branch commit

let set_master index commit =
  GitStore.Commit.of_hash index.repo commit >>= function
  | None -> assert false
  | Some commit ->
      GitStore.Branch.set index.repo GitStore.Branch.master commit

(* Context dumping *)

module Pruned_block = struct

  type t = {
    block_header : Block_header.t ;
    operations : (int * Operation.t list ) list ;
    operation_hashes : (int * Operation_hash.t list) list ;
  }

  let encoding =
    let open Data_encoding in
    conv
      (fun { block_header ; operations ; operation_hashes} ->
         (operations, operation_hashes, block_header))
      (fun (operations, operation_hashes, block_header) ->
         { block_header ; operations ; operation_hashes})
      (obj3
         (req "operations" (list (tup2 int31 (list (dynamic_size Operation.encoding)))))
         (req "operation_hashes" (list (tup2 int31 (list (dynamic_size Operation_hash.encoding)))))
         (req "block_header" Block_header.encoding)
      )

  let encoding2 =
    let open Data_encoding in
    conv
      (fun { block_header ; operations ; operation_hashes} ->
         (operations, operation_hashes, block_header))
      (fun (operations, operation_hashes, block_header) ->
         { block_header ; operations ; operation_hashes})
      DUNE.(obj3
              (req "operations"
                 (list (tup2 int (list (dynamic_size Operation.encoding)))))
              (req "operation_hashes"
                 (list (tup2 int31
                          (list (dynamic_size Operation_hash.encoding)))))
              (req "block_header" Block_header.encoding)
           )

  let to_bytes pruned_block =
    Data_encoding.Binary.to_bytes_exn encoding pruned_block

  let of_bytes pruned_block =
    Data_encoding.Binary.of_bytes encoding pruned_block

  let header { block_header } = block_header

end

module Block_data = struct

  type t = {
    block_header : Block_header.t ;
    operations : Operation.t list list ;
  }

  let header { block_header } = block_header

  let encoding =
    let open Data_encoding in
    conv
      (fun { block_header  ;
             operations} ->
        (operations,
         block_header))
      (fun (operations,
            block_header) ->
        { block_header ;
          operations})
      (obj2
         (req "operations" (list (list (dynamic_size Operation.encoding))))
         (req "block_header" Block_header.encoding
         ))

  let to_bytes =
    Data_encoding.Binary.to_bytes_exn encoding

  let of_bytes =
    Data_encoding.Binary.of_bytes encoding

end

module Protocol_data = struct

  type info = {
    author : string ;
    message : string ;
    timestamp : Time.Protocol.t ;
  }

  let info_encoding =
    let open Data_encoding in
    conv
      (fun {author ; message ; timestamp} ->
         (author, message, timestamp))
      (fun (author, message, timestamp) ->
         {author ; message ; timestamp} )
      (obj3
         (req "author" string)
         (req "message" string)
         (req "timestamp" Time.Protocol.encoding))

  type data = {
    info : info ;
    protocol_hash : Protocol_hash.t ;
    test_chain_status : Test_chain_status.t ;
    data_key : Context_hash.t ;
    parents : Context_hash.t list ;
  }

  let data_encoding =
    let open Data_encoding in
    conv
      (fun { info ; protocol_hash ; test_chain_status ; data_key ; parents } ->
         (info, protocol_hash, test_chain_status, data_key, parents))
      (fun (info, protocol_hash, test_chain_status, data_key, parents) ->
         { info ; protocol_hash ; test_chain_status ; data_key ; parents })
      (obj5
         (req "info" info_encoding)
         (req "protocol_hash" Protocol_hash.encoding)
         (req "test_chain_status" Test_chain_status.encoding)
         (req "data_key" Context_hash.encoding)
         (req "parents" (list Context_hash.encoding)))

  type t = (Int32.t * data)

  let encoding =
    let open Data_encoding in
    tup2
      int32
      data_encoding

  let to_bytes =
    Data_encoding.Binary.to_bytes_exn encoding

  let of_bytes =
    Data_encoding.Binary.of_bytes encoding

end

module Dumpable_context = struct
  type nonrec index = index
  type nonrec context = context
  type tree = GitStore.tree
  type hash = Dune_ironmin.Irmin_sig.tree_hash
  type step = string
  type key = step list
  type commit_info = Irmin.Info.t

  let hash_export = function
    | `Contents ( h, () ) -> `Blob, Context_hash.to_bytes h
    | `Node h -> `Node, Context_hash.to_bytes h
  let hash_import ty mb =
    Context_hash.of_bytes mb >>? fun h ->
    match ty with
    | `Node -> ok @@ `Node h
    | `Blob -> ok @@ `Contents ( h, () )
  let hash_equal h1 h2 =
    match h1, h2 with
    | `Contents ( h1, () ), `Contents ( h2, () )
    | `Node h1, `Node h2 -> Context_hash.( h1 = h2 )
    | `Contents _, `Node _ | `Node _, `Contents _ -> false

  let commit_info_encoding =
    let open Data_encoding in
    conv
      (fun irmin_info ->
         let author = Irmin.Info.author irmin_info in
         let message = Irmin.Info.message irmin_info in
         let date = Irmin.Info.date irmin_info in
         (author, message, date))
      (fun (author, message, date) ->
         Irmin.Info.v ~author ~date message)
      (obj3
         (req "author" string)
         (req "message" string)
         (req "date" int64))

  let blob_encoding =
    let open Data_encoding in
    conv
      (fun (`Blob h) -> h)
      (fun h -> `Blob h)
      (obj1 (req "blob" bytes))

  let node_encoding =
    let open Data_encoding in
    conv
      (fun (`Node h) -> h)
      (fun h -> `Node h)
      (obj1 (req "node" bytes))

  let hash_encoding : hash Data_encoding.t =
    let open Data_encoding in
    let kind_encoding = string_enum [("node", `Node) ; ("blob", `Blob) ] in
    conv
      begin fun hash -> hash_export hash end
      begin function
        | (`Node, h) -> `Node (Context_hash.of_bytes_exn h)
        | (`Blob, h) -> `Contents (Context_hash.of_bytes_exn h, ())
      end
      (obj2 (req "kind" kind_encoding) (req "value" bytes))

  let context_parents ctxt =
    match ctxt with
    | { parents = [commit]; _ } ->
        context_parents ctxt.index.repo commit
    | _ -> assert false

  let context_info = function
    | { parents = [c]; _ } -> GitStore.Commit.info c
    | _ -> assert false
  let context_info_export i = Irmin.Info.( date i, author i, message i )
  let context_info_import ( date, author, message) = Irmin.Info.v ~date ~author message

  let get_context idx bh = checkout idx bh.Block_header.shell.context
  let set_context memcache ~info ~parents ctxt bh =
    let parents = List.sort Context_hash.compare parents in
    GitStore.Tree.hash ctxt.index.repo ctxt.tree >>= function
    | `Node node ->
        MEMCACHE.set_context memcache ctxt.index.repo ~info ~node ~parents
        >>= fun ctxt_h ->
        if Context_hash.equal bh.Block_header.shell.context ctxt_h
        then Lwt.return_some bh
        else Lwt.return_none
    | `Contents _ -> assert false

  let fold_context_path ?progress ctxt f =
    fold_tree_path ?progress ctxt.index.repo ctxt.tree f

  let make_context index = { index ; tree = GitStore.empty_tree() ; parents = [] ; }
  let update_context context tree = { context with tree ; }

  (* Fabrice: let's introduce a memcache, a in-memory data-structure
     that will store temporary associations between hashes and
     sub-trees.  Irmin has that for tree with LMDB, but not Ironmin.
  *)

  type memcache = UsedStorage.MEMCACHE.memcache
  let make_memcache = MEMCACHE.new_memcache
  let add_mbytes memcache index bytes =
    MEMCACHE.add_mbytes memcache index.repo bytes >>= fun _tree ->
    Lwt.return_unit
  let add_hash memcache index tree key hash =
    MEMCACHE.add_hash memcache index.repo tree key hash

  let add_dir memcache index l =
    let rec fold_list sub_tree = function
      | [] -> Lwt.return_some sub_tree
      | ( step, hash ) :: tl ->
          begin
            add_hash memcache index sub_tree [step]hash >>= function
            | None -> Lwt.return_none
            | Some sub_tree -> fold_list sub_tree tl
          end
    in
    fold_list (GitStore.empty_tree()) l >>= function
    | None -> Lwt.return_none
    | Some tree ->
        MEMCACHE.add_tree memcache index.repo tree >>= fun _ ->
        Lwt.return_some tree

  module Commit_hash = Context_hash
  module Block_header = Block_header
  module Block_data = Block_data
  module Pruned_block = Pruned_block
  module Protocol_data = Protocol_data
end

(* Protocol data *)

let data_node_hash index context =
  GitStore.Tree.find_tree context.tree current_data_key >>= function
  | None ->
      Printf.kprintf invalid_arg
        "Context.data_node_hash: %S not found"
        (String.concat "." current_data_key)
  | Some dt ->
      GitStore.Tree.hash index.repo dt >>= fun dt_hash ->
      match dt_hash with `Node x -> Lwt.return x | _ -> assert false

let get_transition_block_headers pruned_blocks =
  let rec aux hs x bs = match bs with
    | [] ->
        x :: hs
    | b :: bs ->
        let xl = x.Pruned_block.block_header.shell.proto_level in
        let bl = b.Pruned_block.block_header.shell.proto_level in
        if not (xl = bl) then
          aux (x :: hs) b bs
        else
          aux hs b bs
  in match pruned_blocks with
  | [] -> assert false
  | x :: xs -> aux [] x xs

let get_protocol_data_from_header index block_header =
  checkout_exn index block_header.Block_header.shell.context
  >>= fun context ->
  let level = block_header.shell.level in
  let irmin_info = Dumpable_context.context_info context in
  let date = Irmin.Info.date irmin_info in
  let author = Irmin.Info.author irmin_info in
  let message = Irmin.Info.message irmin_info in
  let info = {
    Protocol_data.timestamp = Time.Protocol.of_seconds date ;
    author ;
    message ;
  } in
  Dumpable_context.context_parents context >>= fun parents ->
  get_protocol context >>= fun protocol_hash ->
  get_test_chain context >>= fun test_chain_status ->
  data_node_hash index context >>= fun data_key ->
  Lwt.return (level , {
      Protocol_data.parents ;
      protocol_hash ;
      test_chain_status ;
      data_key ;
      info ;
    })

let validate_context_hash_consistency_and_commit
    ~data_hash
    ~expected_context_hash
    ~timestamp
    ~test_chain
    ~protocol_hash
    ~message
    ~author
    ~parents
    ~index
  =
  let protocol_value = Protocol_hash.to_bytes protocol_hash in
  let test_chain_value = Data_encoding.Binary.to_bytes_exn
      Test_chain_status.encoding test_chain in
  let tree = GitStore.empty_tree() in
  GitStore.Tree.add tree current_protocol_key protocol_value >>= fun tree ->
  GitStore.Tree.add tree current_test_chain_key test_chain_value >>= fun tree ->
  let info = Irmin.Info.v
      ~date:(Time.Protocol.to_seconds timestamp) ~author message in
  let ( computed_context_hash , mock_parents , data_tree ) =
    compute_context_hash index.repo tree ~info ~parents_hashes:parents data_hash
  in
  if Context_hash.equal expected_context_hash computed_context_hash then
    let ctxt = {index ; tree = GitStore.empty_tree() ;
                parents = mock_parents} in
    set_test_chain ctxt test_chain >>= fun ctxt ->
    set_protocol ctxt protocol_hash >>= fun ctxt ->
    GitStore.Tree.add_tree ctxt.tree current_data_key data_tree >>= fun new_tree ->
    GitStore.Commit.v ctxt.index.repo ~info ~parents:ctxt.parents new_tree
    >>= fun commit ->
    let ctxt_h = GitStore.Commit.hash commit in
    Lwt.return (Context_hash.equal ctxt_h expected_context_hash)
  else
    Lwt.return_false

(* Context dumper *)

module Context_dumper = Context_dump.Make(Dumpable_context)

include Context_dumper (* provides functions dump_contexts and restore_contexts *)

type error += Cannot_create_file of string
let () = register_error_kind `Permanent
    ~id:"context_dump.write.cannot_open"
    ~title:"Cannot open file for context dump"
    ~description:""
    ~pp:(fun ppf uerr ->
        Format.fprintf ppf
          "@[Error while opening file for context dumping: %s@]"
          uerr)
    Data_encoding.(obj1 (req "context_dump_cannot_open" string) )
    (function Cannot_create_file e -> Some e
            | _ -> None)
    (fun e -> Cannot_create_file e)

type error += Cannot_open_file of string
let () = register_error_kind `Permanent
    ~id:"context_dump.read.cannot_open"
    ~title:"Cannot open file for context restoring"
    ~description:""
    ~pp:(fun ppf uerr ->
        Format.fprintf ppf
          "@[Error while opening file for context restoring: %s@]"
          uerr)
    Data_encoding.(obj1 (req "context_restore_cannot_open" string) )
    (function Cannot_open_file e -> Some e
            | _ -> None)
    (fun e -> Cannot_open_file e)

type error += Suspicious_file of int
let () = register_error_kind `Permanent
    ~id:"context_dump.read.suspicious"
    ~title:"Suspicious file: data after end"
    ~description:""
    ~pp:(fun ppf uerr ->
        Format.fprintf ppf
          "@[Remaining bytes in file after context restoring: %d@]"
          uerr)
    Data_encoding.(obj1 (req "context_restore_suspicious" int31) )
    (function Suspicious_file e -> Some e
            | _ -> None)
    (fun e -> Suspicious_file e)

let dump_contexts idx datas ~filename =
  let file_init () =
    Lwt_unix.openfile filename Lwt_unix.[O_WRONLY; O_CREAT; O_TRUNC] 0o666
    >>= return
  in
  Lwt.catch file_init
    (function
      | Unix.Unix_error (e,_,_) -> fail @@ Cannot_create_file (Unix.error_message e)
      | exc ->
          let msg = Printf.sprintf "unknown error: %s" (Printexc.to_string exc) in
          fail (Cannot_create_file msg))
  >>=? fun fd ->
  dump_contexts_fd idx datas ~fd

let restore_contexts idx ~filename k_store_pruned_block pipeline_validation =
  let file_init () =
    Lwt_unix.openfile filename Lwt_unix.[O_RDONLY;] 0o600
    >>= return
  in
  Lwt.catch file_init
    (function
      | Unix.Unix_error (e,_,_) -> fail @@ Cannot_open_file (Unix.error_message e)
      | exc ->
          let msg = Printf.sprintf "unknown error: %s" (Printexc.to_string exc) in
          fail (Cannot_open_file msg))
  >>=? fun fd ->
  Lwt.finalize
    (fun () ->
       restore_contexts_fd idx ~fd k_store_pruned_block pipeline_validation
       >>=? fun result ->
       Lwt_unix.lseek fd 0 Lwt_unix.SEEK_CUR
       >>= fun current ->
       Lwt_unix.fstat fd
       >>= fun stats ->
       let total = stats.Lwt_unix.st_size in
       if current = total
       then return result
       else fail @@ Suspicious_file (total - current)
    )
    (fun () -> Lwt_unix.close fd)

let gc index ~genesis ~current = gc index.repo ~genesis ~current
let revert index = revert index.repo
