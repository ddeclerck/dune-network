#! /bin/sh

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

. "$script_dir"/version.sh

# getting version
dune exec "${script_dir#$PWD/}"/get_p2p_version/get_p2p_version.exe | diff - "$script_dir"/alphanet_version >/dev/null && exit 0
res=$?
>&2 echo Docker image will have wrong network version, please run "$script_dir"/update_docker_network_version.sh
exit $res
