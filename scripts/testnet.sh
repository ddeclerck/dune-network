#! /usr/bin/env bash

set -e

if ! which docker > /dev/null 2>&1 ; then
    echo "Docker does not seem to be installed."
    exit 1
fi

if ! which docker-compose > /dev/null 2>&1 ; then
    echo "Docker-compose does not seem to be installed."
    exit 1
fi

docker_version="$(docker version -f "{{ .Server.Version }}")"
docker_major="$(echo "$docker_version" | cut -d . -f 1)"
docker_minor="$(echo "$docker_version" | cut -d . -f 2)"

docker_repo="registry.gitlab.com/dune-network/dune-network"

if ([ "$docker_major" -gt 1 ] ||
    ( [ "$docker_major" -eq 1 ] && [ "$docker_minor" -ge 13 ] )) ; then
    docker_1_13=true
else
    docker_1_13=false
fi

current_dir="$(pwd -P)"
src_dir="$(cd "$(dirname "$0")" && echo "$current_dir/")"
cd "$src_dir"

update_compose_file() {

    update_active_protocol_version
    set_internal_rpc_port

    if [ "$#" -ge 2 ] && [ "$1" = "--rpc-port" ] ; then
        export_rpc="
      - \"$2:$internal_rpc_port\""
        shift 2
    fi

    cat > "$docker_compose_yml" <<EOF
version: "2"

volumes:
  node_data:
  client_data:

services:

  node:
    image: $docker_image
    hostname: node
    command: dune-node --net-addr :$port $@
    ports:
      - "$port:$port"$export_rpc
    expose:
      - "$internal_rpc_port"
    volumes:
      - node_data:/var/run/dune-network/node
      - client_data:/var/run/dune-network/client
    restart: on-failure

  upgrader:
    image: $docker_image
    hostname: node
    command: dune-upgrade-storage
    volumes:
      - node_data:/var/run/dune-network/node
      - client_data:/var/run/dune-network/client
    restart: on-failure

EOF

if [ -n "$local_snapshot_path" ]; then
    cat >> "$docker_compose_yml" <<EOF

  importer:
    image: $docker_image
    hostname: node
    command: dune-snapshot-import
    volumes:
      - node_data:/var/run/dune-network/node
      - client_data:/var/run/dune-network/client
      - $local_snapshot_path:/snapshot
    restart: on-failure

EOF

fi

for proto in $(cat "$active_protocol_versions") ; do

    cat >> "$docker_compose_yml" <<EOF
  baker-$proto:
    image: $docker_image
    hostname: baker-$proto
    environment:
      - PROTOCOL=$proto
    command: dune-baker --max-priority 128
    links:
      - node
    volumes:
      - node_data:/var/run/dune-network/node:ro
      - client_data:/var/run/dune-network/client
    restart: on-failure

  endorser-$proto:
    image: $docker_image
    hostname: endorser-$proto
    environment:
      - PROTOCOL=$proto
    command: dune-endorser
    links:
      - node
    volumes:
      - client_data:/var/run/dune-network/client
    restart: on-failure

  accuser-$proto:
    image: $docker_image
    hostname: accuser-$proto
    environment:
      - PROTOCOL=$proto
    command: dune-accuser
    links:
      - node
    volumes:
      - client_data:/var/run/dune-network/client
    restart: on-failure

EOF

done

}

call_docker_compose() {
    docker-compose -f "$docker_compose_yml" -p "$docker_compose_name" "$@"
}

exec_docker() {
    if [ -t 0 ] && [ -t 1 ] && [ -t 2 ] && [ -z "$TESTNET_EMACS" ]; then
        local interactive_flags="-i"
    else
        local interactive_flags=""
    fi
    local node_container="$(container_name "$docker_node_container")"
    declare -a container_args=();
    tmpdir="/tmp"
    for arg in "$@"; do
        if [[ "$arg" == 'container:'* ]]; then
            local_path="${arg#container:}"
            if [[ "$local_path" != '/'* ]]; then
                local_path="$current_dir/$local_path"
            fi
            file_name=$(basename "${local_path}")
            docker_path="$tmpdir/$file_name"
            docker cp "${local_path}" "$node_container:${docker_path}"
            docker exec $interactive_flags "$node_container" sudo chown tezos "${docker_path}"
            container_args+=("file:$docker_path");
        else
            container_args+=("${arg}");
        fi
    done
    docker exec $interactive_flags -e 'DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER' "$node_container" "${container_args[@]}"
}

## Container ###############################################################

update_active_protocol_version() {
    docker run --rm --entrypoint /bin/cat "$docker_image" \
           /usr/local/share/dune-network/active_protocol_versions > "$active_protocol_versions"
}

may_update_active_protocol_version() {
    if [ ! -f "$active_protocol_versions" ] ; then
        update_active_protocol_version
    fi
}

pull_image() {
    if [ "$DUNE_TESTNET_DO_NOT_PULL" = "yes" ] \
           || [ "$TESTNET_EMACS" ] \
           || [ "$docker_image" = "$(echo $docker_image | tr -d '/')" ] ; then
        return ;
    fi
    docker pull "$docker_image"
    update_active_protocol_version
    date "+%s" > "$docker_pull_timestamp"
}

may_pull_image() {
    if [ ! -f "$docker_pull_timestamp" ] \
         || [ 3600 -le $(($(date "+%s") - $(cat $docker_pull_timestamp))) ]; then
        pull_image
    fi
}

uptodate_container() {
    running_image=$(docker inspect \
                           --format="{{ .Image }}" \
                           --type=container "$(container_name "$1")")
    latest_image=$(docker inspect \
                          --format="{{ .Id }}" \
                          --type=image "$docker_image")
    [ "$latest_image" = "$running_image" ]
}

uptodate_containers() {
    container=$1
    if [ ! -z "$container" ]; then
        shift 1
        uptodate_container $container && uptodate_containers $@
    fi
}

assert_container() {
    call_docker_compose up --no-start
}

container_name() {
    local name="$(docker ps --filter "name=$1" --format "{{.Names}}")"
    if [ -n "$name" ]; then echo "$name"; else echo "$1"; fi
}

set_internal_rpc_port() {
    if ! $(docker run --rm -i -v="$docker_node_volume":/tmp/data busybox \
                  test -f /tmp/data/data/config.json) ; then
        internal_rpc_port=8733
        return
    fi

    if ! which jq > /dev/null 2>&1 ; then
        echo "jq does not seem to be installed."
        exit 1
    fi

    if ! which awk > /dev/null 2>&1 ; then
        echo "awk does not seem to be installed."
        exit 1
    fi

    local port=$(docker run --rm -i -v="$docker_node_volume":/tmp/data busybox \
                        cat /tmp/data/data/config.json \
                     | jq -r '.rpc."listen-addr"' \
                     | awk -F ":" '{print $NF}')
    if [ "$port" == "null" ] ; then
        internal_rpc_port=8733
    else
        internal_rpc_port="$port"
    fi
}

## Node ####################################################################

check_node_volume() {
    docker volume inspect "$docker_node_volume" > /dev/null 2>&1
}

clear_node_volume() {
    if check_node; then
        echo -e "\033[31mCannot clear data while the node is running.\033[0m"
        exit 1
    fi
    if check_node_volume ; then
        docker volume rm "$docker_node_volume" > /dev/null
        echo -e "\033[32mThe chain data has been removed from the disk.\033[0m"
    else
        echo -e "\033[32mNo remaining data to be removed from the disk.\033[0m"
    fi
}

migrate_volume() {
    local docker_volume="$1"
    local old_volume=${docker_volume#"dune"}
    if ! $(docker volume inspect "$old_volume" > /dev/null 2>&1) ; then
        echo -e "\033[31mNo volume $old_volume to migrate!\033[0m"
    elif [ "$old_volume" != "$docker_volume" ] ; then
        if ! $(docker volume inspect "$docker_volume" > /dev/null 2>&1) ; then
            echo "Creating destination volume $docker_volume..."
            docker volume create --name $docker_volume
        fi
        docker run --rm -i \
               -v="$old_volume":/"$old_volume" \
               -v="$docker_volume":/"$docker_volume" \
               busybox sh -c "cp -fav /$old_volume/. /$docker_volume/"
        if [ "$?" = "0" ] ; then
            echo -e "\033[32mThe $docker_volume has been migrated to new volume $docker_volume.\033[0m"
            echo "You can issue \"docker volume rm $old_volume\" when you have confirmed data has been imported."
            # docker volume rm "$old_volume" > /dev/null
            # echo -e "\033[32mOld volume $old_volume has been removed.\033[0m"
        fi
    fi
}

migrate_volumes() {
    if check_node; then
        echo -e "\033[31mCannot migate data while the node is running.\033[0m"
        exit 1
    fi
    migrate_volume "$docker_node_volume"
    migrate_volume "$docker_client_volume"
}

check_node() {
    res=$(docker inspect \
                 --format="{{ .State.Running }}" \
                 --type=container "$(container_name "$docker_node_container")" 2>/dev/null || echo false)
    [ "$res" = "true" ]
}

assert_node() {
    if ! check_node; then
        echo -e "\033[31mNode is not running!\033[0m"
        exit 0
    fi
}

warn_node_uptodate() {
    if ! uptodate_container "$docker_node_container"; then
        echo -e "\033[33mThe current node is not the latest available.\033[0m"
    fi
}

assert_node_uptodate() {
    may_pull_image
    assert_node
    if ! uptodate_container "$docker_node_container"; then
        echo -e "\033[33mThe current node is not the latest available.\033[0m"
        exit 1
    fi
}

status_node() {
    may_pull_image
    if check_node; then
        echo -e "\033[32mNode is running\033[0m"
        warn_node_uptodate
    else
        echo -e "\033[33mNode is not running\033[0m"
    fi
}

start_node() {
    pull_image
    if check_node; then
        echo -e "\033[31mNode is already running\033[0m"
        exit 1
    fi
    update_compose_file "$@"
    call_docker_compose up --no-start
    call_docker_compose start node
    echo -e "\033[32mThe node is now running.\033[0m"
}

log_node() {
    may_pull_image
    assert_node_uptodate
    call_docker_compose logs --tail=100 -f node
}

stop_node() {
    if ! check_node; then
        echo -e "\033[31mNo node to kill!\033[0m"
        exit 1
    fi
    echo -e "\033[32mStopping the node...\033[0m"
    call_docker_compose stop node
}


## Baker ###################################################################

check_baker() {
    update_active_protocol_version
    bakers="$(sed "s/^\(.*\)$/baker-\1/g" "$active_protocol_versions")"
    docker_baker_containers="$(sed "s/^\(.*\)$/${docker_compose_name}_baker-\1_1/g" "$active_protocol_versions")"
    for docker_baker_container in $docker_baker_containers; do
        res=$(docker inspect \
                     --format="{{ .State.Running }}" \
                     --type=container "$(container_name "$docker_baker_container")" 2>/dev/null || echo false)
        if ! [ "$res" = "true" ]; then return 1; fi
    done
}

assert_baker() {
    if ! check_baker; then
        echo -e "\033[31mBaker is not running!\033[0m"
        exit 0
    fi
}

assert_baker_uptodate() {
    assert_baker
    if ! uptodate_containers $docker_baker_containers; then
        echo -e "\033[33mThe current baker is not the latest available.\033[0m"
        exit 1
    fi
}

status_baker() {
    if check_baker; then
        echo -e "\033[32mBaker is running\033[0m"
        may_pull_image
        if ! uptodate_containers $docker_baker_containers; then
            echo -e "\033[33mThe current baker is not the latest available.\033[0m"
        fi
    else
        echo -e "\033[33mBaker is not running\033[0m"
    fi
}

start_baker() {
    if check_baker; then
        echo -e "\033[31mBaker is already running\033[0m"
        exit 1
    fi
    pull_image
    assert_node_uptodate
    call_docker_compose start $bakers
    echo -e "\033[32mThe baker is now running.\033[0m"
}

log_baker() {
    may_pull_image
    assert_baker_uptodate
    call_docker_compose logs -f $bakers
}

stop_baker() {
    if ! check_baker; then
        echo -e "\033[31mNo baker to kill!\033[0m"
        exit 1
    fi
    echo -e "\033[32mStopping the baker...\033[0m"
    call_docker_compose stop $bakers
}

## Endorser ###################################################################

check_endorser() {
    update_active_protocol_version
    endorsers="$(sed "s/^\(.*\)$/endorser-\1/g" "$active_protocol_versions")"
    docker_endorser_containers="$(sed "s/^\(.*\)$/${docker_compose_name}_endorser-\1_1/g" "$active_protocol_versions")"
    for docker_endorser_container in $docker_endorser_containers; do
        res=$(docker inspect \
                     --format="{{ .State.Running }}" \
                     --type=container "$(container_name "$docker_endorser_container")" 2>/dev/null || echo false)
        if ! [ "$res" = "true" ]; then return 1; fi
    done
}

assert_endorser() {
    if ! check_endorser; then
        echo -e "\033[31mEndorser is not running!\033[0m"
        exit 0
    fi
}

assert_endorser_uptodate() {
    assert_endorser
    if ! uptodate_containers $docker_endorser_containers; then
        echo -e "\033[33mThe current endorser is not the latest available.\033[0m"
        exit 1
    fi
}

status_endorser() {
    if check_endorser; then
        echo -e "\033[32mEndorser is running\033[0m"
        may_pull_image
        if ! uptodate_containers $docker_endorser_containers; then
            echo -e "\033[33mThe current endorser is not the latest available.\033[0m"
        fi
    else
        echo -e "\033[33mEndorser is not running\033[0m"
    fi
}

start_endorser() {
    if check_endorser; then
        echo -e "\033[31mEndorser is already running\033[0m"
        exit 1
    fi
    pull_image
    assert_node_uptodate
    call_docker_compose start $endorsers
    echo -e "\033[32mThe endorser is now running.\033[0m"
}

log_endorser() {
    may_pull_image
    assert_endorser_uptodate
    call_docker_compose logs -f $endorsers
}

stop_endorser() {
    if ! check_endorser; then
        echo -e "\033[31mNo endorser to kill!\033[0m"
        exit 1
    fi
    echo -e "\033[32mStopping the endorser...\033[0m"
    call_docker_compose stop $endorsers
}

## Accuser ###################################################################

check_accuser() {
    update_active_protocol_version
    accusers="$(sed "s/^\(.*\)$/accuser-\1/g" "$active_protocol_versions")"
    docker_accuser_containers="$(sed "s/^\(.*\)$/${docker_compose_name}_accuser-\1_1/g" "$active_protocol_versions")"
    for docker_accuser_container in $docker_accuser_containers; do
        res=$(docker inspect \
                     --format="{{ .State.Running }}" \
                     --type=container "$(container_name "$docker_accuser_container")" 2>/dev/null || echo false)
        if ! [ "$res" = "true" ]; then return 1; fi
    done
}

assert_accuser() {
    if ! check_accuser; then
        echo -e "\033[31mAccuser is not running!\033[0m"
        exit 0
    fi
}

assert_accuser_uptodate() {
    assert_accuser
    if ! uptodate_containers $docker_accuser_containers; then
        echo -e "\033[33mThe current accuser is not the latest available.\033[0m"
        exit 1
    fi
}

status_accuser() {
    if check_accuser; then
        echo -e "\033[32mAccuser is running\033[0m"
        may_pull_image
        if ! uptodate_containers $docker_accuser_containers; then
            echo -e "\033[33mThe current accuser is not the latest available.\033[0m"
        fi
    else
        echo -e "\033[33mAccuser is not running\033[0m"
    fi
}

start_accuser() {
    if check_accuser; then
        echo -e "\033[31mAccuser is already running\033[0m"
        exit 1
    fi
    pull_image
    assert_node_uptodate
    call_docker_compose start $accusers
    echo -e "\033[32mThe accuser is now running.\033[0m"
}

log_accuser() {
    may_pull_image
    assert_accuser_uptodate
    call_docker_compose logs -f $accusers
}

stop_accuser() {
    if ! check_accuser; then
        echo -e "\033[31mNo accuser to kill!\033[0m"
        exit 1
    fi
    echo -e "\033[32mStopping the accuser...\033[0m"
    call_docker_compose stop $accusers
}

## Misc ####################################################################

run_client() {
    assert_node_uptodate
    exec_docker "dune-client" "$@"
}

run_admin_client() {
    assert_node_uptodate
    exec_docker "dune-admin-client" "$@"
}

run_shell() {
    assert_node_uptodate
    if [ $# -eq 0 ]; then
        exec_docker /bin/sh
    else
        exec_docker /bin/sh -c "$@"
    fi
}

display_head_stat() {
    local head="$@"
    local timestamp_UTC="$(jq -r .timestamp <<< $head)"
    local timestamp_local="$(date --date=$timestamp_UTC)"
    local timestamp_s="$(date --date=$timestamp_UTC '+%s')"
    local now="$(date '+%s')"
    local delay=$(( $now - timestamp_s))
    local status="\033[41mSynchronizing ($((delay / 3600)) hours late)\033[0m"
    local levelcolor=31
    if [ $delay -lt 60 ] ; then
        status="\033[42mSynchronized\033[0m                           "
        levelcolor=32
    elif [ $delay -lt 120 ] ; then
        status="\033[43mSynchronizing\033[0m                          "
        levelcolor=33
    fi
    echo -e "Status:\t\t$status"
    echo -e "Level:\t\t\033[$levelcolor;1m$(jq .level <<< $head)\033[0m"
    echo -e "Hash:\t\t\033[35m$(jq -r .hash <<< $head)\033[0m"
    echo -e "Timestamp:\t\033[36m$(date --date=`jq -r .timestamp <<< $head`)\033[0m"
}

display_head() {
    assert_node_uptodate
    display_head_stat "$(exec_docker dune-client rpc get /chains/main/blocks/head/header)"
}

install_curl() {
    if ! exec_docker "which" "curl" > /dev/null ; then
        run_shell "sudo apk add curl"
    fi
}

monitor_head() {
    warn_node_uptodate
    install_curl
    set_internal_rpc_port
    echo; echo; echo; echo
    tput cuu 4
    tput sc
    trap "kill 0" EXIT
    run_shell "curl -N -s http://localhost:$internal_rpc_port/monitor/heads/main" | \
        while read -r head; do
            if [ ! -z "$PID" ]; then
                kill "$PID" &> /dev/null
            fi
            while true ; do
                tput rc
                tput sc
                display_head_stat $head
                sleep 5
            done &
            local PID=$!
        done
    wait
}

## Main ####################################################################

start() {
    pull_image
    update_compose_file "$@"
    call_docker_compose up -d --remove-orphans
    warn_script_uptodate
}

stop() {
    call_docker_compose down
}

kill_() {
    call_docker_compose kill
    stop
}

status() {
    status_node
    status_baker
    status_endorser
    warn_script_uptodate verbose
}

snapshot_import() {
    pull_image
    local_snapshot_path="$1"
    update_compose_file
    call_docker_compose up importer
    warn_script_uptodate
}

warn_script_uptodate() {
    if [[ $TESTNET_EMACS ]]; then
       return
    fi
    docker run --rm --entrypoint /bin/cat "$docker_image" \
       "/usr/local/share/dune-network/testnet.sh" > ".testnet.sh.new"
    if ! diff .testnet.sh.new  "$0" >/dev/null 2>&1 ; then
        echo -e "\033[33mWarning: the container contains a new version of 'testnet.sh'.\033[0m"
        echo -e "\033[33mYou might run '$0 update_script' to synchronize.\033[0m"
    elif [ "$1" = "verbose" ] ; then
        echo -e "\033[32mThe script is up to date.\033[0m"
    fi
    rm .testnet.sh.new
}

update_script() {
    docker run --rm --entrypoint /bin/cat "$docker_image" \
       "/usr/local/share/dune-network/testnet.sh" > ".testnet.sh.new"
    if ! diff .testnet.sh.new  "$0" >/dev/null 2>&1 ; then
        mv .testnet.sh.new "$0"
        echo -e "\033[32mThe script has been updated.\033[0m"
    else
        rm .testnet.sh.new
        echo -e "\033[32mThe script is up to date.\033[0m"
    fi
}

upgrade_node_storage() {
    pull_image
    local_snapshot_path="$1"
    update_compose_file
    call_docker_compose up upgrader
    warn_script_uptodate
}

usage() {
    echo -e "Usage: $0 [GLOBAL_OPTIONS] <command> [OPTIONS]"
    echo -e "  Main commands:"
    echo -e "    \e[1m$0 \e[92mstart\e[39m [--rpc-port <int>] [OPTIONS]\e[0m"
    echo -e "       Launch a full Dune testnet node in a docker container"
    echo -e "       automatically generating a new network identity."
    echo -e "       OPTIONS (others than --rpc-port) are directly passed to the"
    echo -e "       Dune node, see '$0 shell dune-node config --help'"
    echo -e "       for more details."
    echo -e "       By default, the RPC port is not exported outside the docker"
    echo -e "       container. WARNING: when exported some RPCs could be harmful"
    echo -e "       (e.g. 'inject_block', 'force_validation', ...), it is"
    echo -e "       advised not to export them publicly."
    echo -e "    \e[1m$0 <\e[31mstop\e[39m|\e[91mkill\e[39m>\e[0m"
    echo -e "       Friendly or brutally stop the node."
    echo -e "    \e[1m$0 \e[32mrestart\e[39m\e[0m"
    echo -e "       Friendly stop the node, fetch the latest docker image and "
    echo -e "       update this script, then start the node again."
    echo -e "       The blockchain data are preserved."
    echo -e "    \e[1m$0 \e[31mclear\e[39m\e[0m"
    echo -e "       Remove all the blockchain data from the disk (except"
    echo -e "       for secret keys and other configuration backup)."
    echo -e "    \e[1m$0 \e[32mmigrate-volumes\e[39m\e[0m"
    echo -e "       Move all the blockchain data to the new docker volume (use"
    echo -e "       this if you have updated the image but lost the data)."
    echo -e "    \e[1m$0 \e[33mstatus\e[39m\e[0m"
    echo -e "       Check that the running node is running and up to date."
    echo -e "       Upgrade is automatically done by the start command."
    echo -e "    \e[1m$0 \e[33mhead\e[39m\e[0m"
    echo -e "       Display info about the current head of the blockchain."
    echo -e "    \e[1m$0 \e[33mmonitor\e[39m\e[0m"
    echo -e "       Monitor the current head of the blockchain, indicating"
    echo -e "       synchronization status."
    echo -e "    \e[1m$0 \e[94mclient\e[39m <COMMAND>\e[0m"
    echo -e "       Pass a command to the dune client."
    echo -e "    \e[1m$0 update_script\e[0m"
    echo -e "       Replace '$0' with the one found in the docker image."
    echo -e "  Advanced commands:"
    echo -e "    $0 \e[94mnode\e[39m <\e[92mstart\e[39m|\e[31mstop\e[39m|\e[33mstatus\e[39m|\e[36mlog\e[39m>"
    echo -e "    $0 \e[94mupgrade\e[39m"
    echo -e "    $0 \e[94msnapshot import\e[39m <snapshot_file>"
    echo -e "    $0 \e[94mbaker\e[39m <\e[92mstart\e[39m|\e[31mstop\e[39m|\e[33mstatus\e[39m|\e[36mlog\e[39m>"
    echo -e "    $0 \e[94mendorser\e[39m <\e[92mstart\e[39m|\e[31mstop\e[39m|\e[33mstatus\e[39m|\e[36mlog\e[39m>"
    echo -e "    $0 \e[94mshell\e[39m"
    echo -e "Node configuration backup directory: $data_dir"
    echo -e "Global options are currently limited to:"
    echo -e "  --port <int>"
    echo -e "      change the public Dune node P2P port"
    echo -e "Container prefix:"
    echo -e "    container:<FILE>"
    echo -e "      can be used anywhere 'file:<FILE>' is permitted in client commands."
    echo -e "      It will cause the referenced file to be copied into the docker conainer."
    echo -e "      Files will be renamed, which may make errors difficult to read"
}

## Dispatch ################################################################

if [ "$#" -ge 2 ] && [ "$1" = "--port" ] ; then
    port="$2"
    shift 2
fi

command="$1"
if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi

case $(basename "$0") in
    mainnet.sh)
        docker_base_dir="$HOME/.dune-mainnet"
        docker_image="$docker_repo:mainnet"
        docker_compose_base_name="dunemainnet"
        default_port=9733
        ;;
    testnet.sh)
        docker_base_dir="$HOME/.dune-testnet"
        docker_image="$docker_repo:testnet"
        docker_compose_base_name="dunetestnet"
        default_port=9734
        ;;
    *)
        script_basename=$(basename $0)
        script_name=${scr%.*}
        docker_base_dir="$HOME/.dune-$script_name"
        docker_image="$docker_repo:next"
        docker_compose_base_name="dune$script_name"
        default_port=19733
        ;;
esac

if [ -n "$port" ] ; then
    mkdir -p "$docker_base_dir"
    echo "$port" > "$docker_base_dir/default_port"
elif [ -f "$docker_base_dir/default_port" ]; then
    port=$(cat "$docker_base_dir/default_port")
else
    port=$default_port
fi

docker_dir="$docker_base_dir"
docker_compose_yml="$docker_dir/docker-compose.yml"
docker_pull_timestamp="$docker_dir/docker_pull.timestamp"
active_protocol_versions="$docker_dir/active_protocol_versions"
docker_compose_name="$docker_compose_base_name"

docker_node_container=${docker_compose_name}_node_1

docker_node_volume=${docker_compose_name}_node_data
docker_client_volume=${docker_compose_name}_client_data

mkdir -p "$docker_dir"

case "$command" in

    ## Main

    start)
        start "$@"
        ;;
    restart)
        stop
        update_script
        export DUNE_TESTNET_DO_NOT_PULL=yes
        exec "$0" start "$@"
        ;;
    clear)
        clear_node_volume
        ;;
    migrate-volumes)
        migrate_volumes
        ;;
    status)
        status
        ;;
    stop)
        stop
        ;;
    kill)
        kill_
        ;;

    ## Node

    node)
        subcommand="$1"
        if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi
        case "$subcommand" in
            start)
                start_node "$@"
                ;;
            status)
                status_node
                ;;
            log)
                log_node
                ;;
	        upgrade)
		        upgrade_node_storage
		        ;;
            stop)
                stop_node
                ;;
            *)
                usage
                exit 1
        esac ;;

    ## Snapshot import

    snapshot)
        subcommand="$1"
        if [ "$#" -ne 2 ] ; then usage ; exit 1;  else shift ; fi
        snapshot_file="$1"
        case "$subcommand" in
            import)
                snapshot_import "$snapshot_file"
                ;;
            *)
                usage
                exit 1
        esac ;;

    ## Baker

    baker)
        subcommand="$1"
        if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi
        case "$subcommand" in
            status)
                status_baker
                ;;
            start)
                start_baker
                ;;
            log)
                log_baker
                ;;
            stop)
                stop_baker
                ;;
            *)
                usage
                exit 1
        esac ;;

    ## Endorser

    endorser)
        subcommand="$1"
        if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi
        case "$subcommand" in
            status)
                status_endorser
                ;;
            start)
                start_endorser
                ;;
            log)
                log_endorser
                ;;
            stop)
                stop_endorser
                ;;
            *)
                usage
                exit 1
        esac ;;

    ## Accuser

    accuser)
        subcommand="$1"
        if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi
        case "$subcommand" in
            status)
                status_accuser
                ;;
            start)
                start_accuser
                ;;
            log)
                log_accuser
                ;;
            stop)
                stop_accuser
                ;;
            *)
                usage
                exit 1
        esac ;;

    ## Misc.

    head)
        display_head
        ;;
    monitor)
        monitor_head
        ;;
    shell)
        run_shell "$@"
        ;;
    client)
        run_client "$@"
        ;;
    admin-client)
        run_admin_client "$@"
        ;;
    check_script)
        warn_script_uptodate verbose
        ;;
    update_script)
        update_script
        ;;
    *)
        usage
        exit 1
        ;;
esac
