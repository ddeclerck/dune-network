(**************************************************************************)
(*                                                                        *)
(*  Copyright Origin Labs 2019                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* should only be used for 8 bits values *)
module ANY = EndianString.BigEndian

(* 25% faster than the loop with shift *)
let len_uint64 value =
  if value < 0L then 9 else
  if value < 268435456L then
    if value < 16384L then
      if value < 128L then 1 else 2
    else
    if value < 2097152L then 3 else 4
  else
  if value < 4398046511104L then
    if value < 34359738368L then 5 else 6
  else
  if value < 72057594037927936L then
    if value < 562949953421312L then 7 else 8
  else 9

let len_uint value = len_uint64 (Int64.of_int value)

module BIGSTRING = struct
  module BANY = EndianBigstring.BigEndian

  let get_uint8 s pos = BANY.get_uint8 s pos, pos+1

  let get_uint64 s pos =
    let rec iter s pos offset n =
      (*      Printf.eprintf "pos=%d\n%!" pos; *)
      let byte,pos = get_uint8 s pos in
      if byte >= 128 then
        let byte = byte land 0x7f in
        let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
        let offset = offset + 7 in
        iter s pos offset n
      else
        let n = Int64.add n
            (Int64.shift_left (Int64.of_int byte) offset) in
        n, pos
    in
    iter s pos 0 0L

  let get_uint s pos =
    let (n, pos) = get_uint64 s pos in
    (Int64.to_int n, pos)
end

module STRING = struct
  module ANY = EndianString.BigEndian

  let get_uint8 s pos = ANY.get_uint8 s pos, pos+1

  let get_uint64 s pos =
    let rec iter s pos offset n =
      (*      Printf.eprintf "pos=%d\n%!" pos; *)
      let byte,pos = get_uint8 s pos in
      if byte >= 128 then
        let byte = byte land 0x7f in
        let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
        let offset = offset + 7 in
        iter s pos offset n
      else
        let n = Int64.add n
            (Int64.shift_left (Int64.of_int byte) offset) in
        n, pos
    in
    iter s pos 0 0L

  let get_uint s pos =
    let (n, pos) = get_uint64 s pos in
    (Int64.to_int n, pos)
end

module BYTES = struct
  module ANY = EndianBytes.BigEndian

  let get_uint8 s pos = ANY.get_uint8 s pos, pos+1
  let set_uint8 s pos v = ANY.set_int8 s pos v; pos+1

  let get_uint64 s pos =
    let rec iter s pos offset n =
      (*      Printf.eprintf "pos=%d\n%!" pos; *)
      let byte,pos = get_uint8 s pos in
      if byte >= 128 then
        let byte = byte land 0x7f in
        let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
        let offset = offset + 7 in
        iter s pos offset n
      else
        let n = Int64.add n
            (Int64.shift_left (Int64.of_int byte) offset) in
        n, pos
    in
    iter s pos 0 0L

  let get_uint s pos =
    let (n, pos) = get_uint64 s pos in
    (Int64.to_int n, pos)


  (* In this representation, the terminator is the strongest bit set to
     0, while intermediary bytes have the strongest bit set to 1 (128).
     256 = "\128\002"
  *)

  let set_uint64 s pos value =
    let rec iter s pos value =
      if value >= 128L then begin
        ANY.set_int8 s pos
          (0x80 lor ( Int64.to_int (Int64.logand value 0x7fL)) );
        iter s (pos+1)
          (Int64.shift_right_logical value 7)
      end else begin
        ANY.set_int8 s pos ( Int64.to_int value );
        pos+1
      end
    in
    iter s pos value

  let set_uint s pos value =
    set_uint64 s pos (Int64.of_int value)

end

module MAKE_CONSUMER_GET(S: sig
    type t
    val get_uint8 : t -> int
  end) = struct

  let get_uint64 s =
    let rec iter s offset n =
      let byte = S.get_uint8 s in
      if byte >= 128 then
        let byte = byte land 0x7f in
        let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
        let offset = offset + 7 in
        iter s offset n
      else
        let n = Int64.add n
            (Int64.shift_left (Int64.of_int byte) offset) in
        n
    in
    iter s 0 0L

  let get_uint s = get_uint64 s |> Int64.to_int
end

(* We implement this interface only because it is used in
   Binary_stream_reader. *)
module MAKE_STREAM_GET(S: sig
    type t
    type b
    type 'a s
    val get_uint8 : (b -> 'a s) -> t -> (int * t -> 'a s) -> 'a s
  end) = struct

  let get_uint64 r s k =
    let rec iter r s offset n k =
      S.get_uint8 r s (fun (byte,s) ->
          if byte >= 128 then
            let byte = byte land 0x7f in
            let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
            let offset = offset + 7 in
            iter r s offset n k
          else
            let n = Int64.add n
                (Int64.shift_left (Int64.of_int byte) offset) in
            k (n,s)
        )
    in
    iter r s 0 0L k

  let get_uint r s k = get_uint64 r s (fun (v,s) -> k (Int64.to_int v,s))
end

module MAKE_GET(S : sig
    type t
    val get_uint8 : t -> int -> int
  end) = struct

  let get_uint8 s pos = S.get_uint8 s pos, pos+1

  let get_uint64 s pos =
    let rec iter s pos offset n =
      let byte,pos = get_uint8 s pos in
      if byte >= 128 then
        let byte = byte land 0x7f in
        let n = Int64.add n (Int64.of_int ( byte lsl offset)) in
        let offset = offset + 7 in
        iter s pos offset n
      else
        Int64.add n
          (Int64.shift_left (Int64.of_int byte) offset)
    in
    iter s pos 0 0L

  let get_uint s pos =
    get_uint64 s pos |> Int64.to_int
end

module MAKE_SET(S: sig
    type t
    val set_uint8 : t -> int -> int -> unit
  end) = struct

  let set_uint64 s pos value =
    let rec iter s pos value =
      if value >= 128L then begin
        S.set_uint8 s pos
          (0x80 lor ( Int64.to_int (Int64.logand value 0x7fL)) );
        iter s (pos+1)
          (Int64.shift_right_logical value 7)
      end else begin
        S.set_uint8 s pos ( Int64.to_int value );
        pos+1
      end
    in
    iter s pos value

  let set_uint s pos value =
    set_uint64 s pos (Int64.of_int value)

end

(*
let () =
  Printf.eprintf "starting tests...\n%!";
  let b = Bytes.create 20 in
  for _i = 1 to 10_000_000 do
    let n = Random.int (1 lsl 29) in
    let n64 = Int64.of_int n in
    let pos1 = BYTES.set_uint64 b 0 n64 in
    (* let s = Bytes.to_string b in *)
    (* Printf.eprintf "%Ld %S\n%!" n64 s; *)
    let s = Bigstring.of_bytes b in
    let m64, pos2 = BIGSTRING.get_uint64 s 0 in
    assert (pos1 = pos2);
    if m64 <> n64 then begin
      Printf.eprintf "%Ld <> %Ld\n%!" m64 n64;
      exit 2
    end
  done;
  Printf.eprintf "done with tests\n%!";
   ()

let () =
  Printf.eprintf "starting tests...\n%!";
  let b = Bytes.create 20 in
  let f64 n64 len =
    let pos1 = BYTES.set_uint64 b 0 n64 in
    Printf.eprintf "n = %Ld len = %d pos1 = %d\n%!" n64 len pos1;
    assert (pos1 = len);
    assert (pos1 = len_uint64 n64);
    (* let s = Bytes.to_string b in *)
    (* Printf.eprintf "%Ld %S\n%!" n64 s; *)
    let s = Bigstring.of_bytes b in
    let m64, pos2 = BIGSTRING.get_uint64 s 0 in
    assert (pos1 = pos2);
    if m64 <> n64 then begin
      Printf.eprintf "%Ld <> %Ld\n%!" m64 n64;
      exit 2
    end
  in
  let f n len =
    f64 ( Int64.of_int n ) len
  in
  List.iter (fun (n, len) ->
      f (n-1) len;
      f n (len+1);
    ) [
    128, 1;
    16384, 2;
    2097152, 3 ;
    268435456, 4;
    34359738368, 5;
    4398046511104, 6;
    562949953421312, 7;
    72057594037927936, 8
  ];
  f64 Int64.max_int 9;
  Printf.eprintf
    "successfully done with tests. You can modify 'uintvar.ml'.\n%!";
  exit 3
*)
